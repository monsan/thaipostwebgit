<%@ page language="java" contentType="text/html;charset=TIS-620"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="javax.swing.text.Document"%>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Date"%>
<%@ page import="com.ss.tp.security.ProcessResult"%>
<%@ page import="com.ss.tp.security.UserInfo"%>
<%@ page import="com.ss.tp.dto.DefaultYearSectionVO" %>

<%
	Calendar now = Calendar.getInstance(Locale.US);
	UserInfo uf = (UserInfo) request.getSession().getAttribute(
			"UserLogin");
	DefaultYearSectionVO defaultYear = (DefaultYearSectionVO)session.getAttribute("DefaultYearAndSection");
	java.util.Date dd = new java.util.Date();
	java.text.SimpleDateFormat fmd = new java.text.SimpleDateFormat(
			"dd/MM/yyyy", new java.util.Locale("th", "TH"));
	String date = fmd.format(dd);
	String userId = uf.getUserId();
	String ouCode = uf.getOuCode();
	String hidPage = request.getParameter("hidPage") == null
			? ""
			: request.getParameter("hidPage");
	String pageEdit = request.getParameter("pageEdit") == null
			? "0"
			: request.getParameter("pageEdit");
	

	if (request.getSession().getAttribute("processResult") != null) {
		ProcessResult processResult = (ProcessResult) request
				.getSession().getAttribute("processResult");
	
	}
%>
<html>
<head>
<title>��§ҹ  CTPFRP019 PVF 209</title>
<!-- Include -->
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript" src="dwr/util.js"></script>
<script type="text/javascript" src="script/payroll_util.js"></script>
<!-- Javascript Script File -->
<SCRIPT type="text/javascript"
	src="dwr/interface/SuUserOrganizationService.js"></SCRIPT>
<SCRIPT type="text/javascript" src="dwr/interface/PnEmployeeService.js"></SCRIPT>
<SCRIPT type="text/javascript"
	src="dwr/interface/SrPvfEmpService.js"></SCRIPT>


<script type="text/javascript" src="script/gridScript.js"></script>
<script type="text/javascript" src="script/payrollComboBox.js"></script>
<script type="text/javascript" src="page/NavigatePage.jsp"></script>
<script type="text/javascript" src="script/dojo.js"></script>
<script type="text/javascript" src="script/dateCalendar.js"></script>
<script type="text/javascript" src="script/json.js"></script>

<script type="text/javascript">
	//Load Dojo's code relating to widget managing functions
	dojo.require("dojo.widget.*");
	dojo.require("dojo.widget.Menu2");
	dojo.require("dojo.widget.Button");
	dojo.require("dojo.widget.ComboBox");
	dojo.require("dojo.widget.DropDownButton");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.ContentPane");
	dojo.require("dojo.widget.LayoutContainer");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.Toolbar");
	dojo.require("dojo.widget.html.*");
	dojo.require("dojo.widget.Menu2");
	dojo.hostenv.writeIncludes();
	
	//Event
	dojo.require("dojo.event.*");
</script>

<script type="text/javascript">
     var statusConfirm = 'N';

	// =========================== Start LOV ===========================
   
    function onLoadDateFromCallback()
    {
    	try
    	{
	    	var cboSourceFrom = [];
	     	var cboFrom = dojo.widget.byId("dateCboFrom");
	     	
	     	//var workYear = DWRUtil.getValue("workYear");
			//var workMonth = DWRUtil.getValue("workMonth");
	     
	     
			     	
			     	<c:forEach items="${ResultInSecurity}" var="result" >		 
						cboSourceFrom.push(["<c:out value='${result}' />","<c:out value='${result}' />"]);
					</c:forEach>
	     	
	     	         cboFrom.dataProvider.setData(cboSourceFrom);
	     	     
	     
	   
     	}catch(e)
     	{
     		alert(e.message);
    	}
	}    
   function whenSelectDateFromOption()
	{
    	DWRUtil.useLoadingMessage("Loading ...");
		var cbo = dojo.widget.byId("dateCbo");
		whenFetchDateFrom(cbo.textInputNode.value);
	}
	
	function whenFetchDateFrom(dateF)
	{
     	DWRUtil.useLoadingMessage("Loading ...");
     	var cboFrom = dojo.widget.byId("dateCboFrom");
		     	
		    
		     	
		     	var cboSourceFrom = [];
		     	
		     	<c:forEach items="${ResultInSecurity}" var="result" >			     	
						cboSourceFrom.push(["<c:out value='${result}' />","<c:out value='${result}' />"]);
				</c:forEach>	
		     	
		     	cboFrom.dataProvider.setData(cboSourceFrom);
     	
    }
	
	

	function init()
	{		
	
		//var cboEmpFrom = dojo.widget.byId("empFromCbo");
		var cboDateFrom = dojo.widget.byId("dateCboFrom");
		//var cboVolumeTo = dojo.widget.byId("volumeCboTo");
	
		//dojo.event.connect(cboEmpFrom, "selectOption", "whenSelectEmpOption");
		dojo.event.connect(cboDateFrom, "selectOption", "whenSelectDateFromOption");
		//dojo.event.connect(cboVolumeTo, "selectOption", "whenSelectVolumeToOption");
		
	
		//whenCountDataAll();
		
	}
	
  	dojo.addOnLoad(init);

    dojo.addOnLoad(onLoadDateFromCallback);
    //dojo.addOnLoad(onLoadVolumeToCallback);
	//dojo.addOnLoad(onLoadEmployeeCallback);

	
	// =========================== End LOV ===========================
	
	
	function query(){
		whenShowReport();
	}
	
	
	function addCommas(nStr) {
		nStr += '';
		x = nStr.split('.');
		x1 = x[0];
		x2 = x.length > 1 ? '.' + x[1] : '';
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}
		return x1 + x2;
	}

	function formatCurrency(num) {
		num = isNaN(num) ||
num === '' || num === null ? 0.00 : num;
	    return addCommas(parseFloat(num).toFixed(2));
	}

	
	
</script>
<%
	String keySeq = request.getParameter("keySeq");
%>
</head>
<body>
	<table width="100%">
		<tr>
			<td class="font-head">[ CTSRRP019 ] PVF 209 ��§ҹ�Ӣ�����¹�ŧ��º��ŧ�ع 
			</td>
		</tr>
	</table>
	<form name="searchForm" action="" method="post">
		<!-- <input type="hidden" name="workMonth"  />--> 
		<inputtype="hidden" name="dataLength"> <br />
		<br />
		<br />
		<br />
		<table align="center" border="1" bordercolor="#6699CC">
			<tr>
				<td><BR />
					<table width="430" border="0" align="center" cellspacing="1">
						
						<tr>
							<td class="font-field" align="right">�ѹ���������ռ�&nbsp;</td>
							<td align="left"><SELECT dojoType="ComboBox"
								widgetId="dateCboFrom" style="width: 100"  forceValidOption="true"></SELECT></td>
							<input type="hidden" name="dateSetFrom">										
						</tr>
						
					</table>
					<center>
						<FONT color="#6699CC">__________________________________________________________________</FONT>
			
		<CENTER>
			<input type="button" class="button" style="width: 80px"
				value="�ʴ�����§ҹ" name="add" onclick="whenShowReport();" />
		</CENTER>
	</form>
</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
function whenShowReport(){
	//alert("test");
	var frm = document.forms[0];
	//var workYear = DWRUtil.getValue("workYear");
	//var workMonth = DWRUtil.getValue("workMonth");
	var comB1=dojo.widget.byId("dateCboFrom").textInputNode.value;
	//alert(comB1);
	
	if(comB1!=''){
		dojo.widget.byId("dateCboFrom").comboBoxSelectionValue.value = splitCombo(dojo.widget.byId("dateCboFrom").textInputNode.value);
		dateCboFrom = splitCombo(dojo.widget.byId("dateCboFrom").textInputNode.value);
		//alert(dateCboFrom);
	}

	forController(dateCboFrom);
	frm.action="srPvfReport019.htm?reqCode=doPrintReport";
	frm.target = "_blank";
	frm.submit();
	frm.target = "_self";
}

function forController(dateCboFrom){
	DWRUtil.setValue("dateSetFrom",dateCboFrom);
	

}
</SCRIPT>

