<%@ page language="java" contentType="text/html;charset=TIS-620"%>
<%@ page import="com.ss.tp.security.UserInfo" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>

<%
	UserInfo userInfo = (UserInfo)session.getAttribute("UserLogin");
	String userId = userInfo.getUserId();
	String ouCode = userInfo.getOuCode();
	Calendar now = Calendar.getInstance(Locale.US);	
	String year =String.valueOf(now.get(Calendar.YEAR)+543);
%>
<html>
<head>
<title>Report Time Attendance</title>
<!-- You have to include these two JavaScript files from DWR -->
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript" src="dwr/util.js"></script>
<script type="text/javascript" src="script/dojo.js"></script>
<script type="text/javascript" src="script/payroll_util.js"></script>
<!-- This JavaScript file is generated specifically for your application -->
<script type="text/javascript" src="dwr/interface/WeWgEmpTextReportService.js"></script>
<script type="text/javascript" src="dwr/interface/WeWgEmployeeTextService.js"></script>
</head>
<script type="text/javascript">
	dojo.require("dojo.widget.*");	
	dojo.require("dojo.widget.Menu2");
	dojo.require("dojo.widget.Button");
	dojo.require("dojo.widget.ComboBox");
	dojo.require("dojo.widget.DropDownButton");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.ContentPane");
	dojo.require("dojo.widget.LayoutContainer");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.Toolbar");
	dojo.require("dojo.widget.html.*");
	dojo.require("dojo.widget.Menu2");
	dojo.hostenv.writeIncludes();
//Event
	dojo.require("dojo.event.*");
	var empCodeDataFrom=[];
	var empCodeDataTo=[];
	var orgCodeShowDataF=[];
	var orgCodeShowDataT=[];
	
	
function init(){
	var cboOrgFrom = dojo.widget.byId("orgFromCbo");
	var cboEmpFrom = dojo.widget.byId("empCodeFrom");
	dojo.event.connect(cboOrgFrom, "selectOption", "whenSelectOrgOption");
	dojo.event.connect(cboEmpFrom, "selectOption", "whenSelectEmpOption");

	}
function onLoadOrganization(){
	var cboOrgFrom = dojo.widget.byId("orgFromCbo");
	var cboOrgTo = dojo.widget.byId("orgToCbo");
	var cboSource = [];
		<c:forEach items="${OrganizationInSecurity}" var="result" >		 
				cboSource.push(["<c:out value='${result.orgCode}' />"+" "+"<c:out value='${result.orgShowDesc}' />","<c:out value='${result.orgCode}' />"]);
				orgCodeShowDataF.push(["<c:out value='${result.orgCode}' />"]);
		</c:forEach>
		cboOrgFrom.dataProvider.setData(cboSource);
		cboOrgTo.dataProvider.setData(cboSource);
	}
function onLoadEmployee(){
	var comB1=dojo.widget.byId("empCodeFrom");
	var comB2=dojo.widget.byId("empCodeTo");
	var arrData=[]
		<c:forEach items="${WgEmployeeInSecurity}" var="result" >		 
			arrData.push(["<c:out value='${result}' />","<c:out value='${result}' />"]);
			empCodeDataFrom.push(["<c:out value='${result}' />"]);
		</c:forEach>
	comB1.dataProvider.setData(arrData);
	comB2.dataProvider.setData(arrData);
	}

 



dojo.addOnLoad(onLoadOrganization);
dojo.addOnLoad(onLoadEmployee);


 // Even ComboBox valueChange
function whenSelectOrgOption(){
		    	DWRUtil.useLoadingMessage("Loading ...");
				var cbo = dojo.widget.byId("orgFromCbo");
				whenFetchOrganizationTo(cbo.textInputNode.value);
	}
function whenFetchOrganizationTo(orgCode){
		     	DWRUtil.useLoadingMessage("Loading ...");
				var cboTo = dojo.widget.byId("orgToCbo");
				var cboSource = [];
				if(orgCode >cboTo.comboBoxSelectionValue.value){
					cboTo.textInputNode.value = '';
					cboTo.comboBoxSelectionValue.value='';
				}
			<c:forEach items="${OrganizationInSecurity}" var="result" >
				if("<c:out value='${result.orgCode}' />"+" "+"<c:out value='${result.orgDesc}' />" >= orgCode){		 
					cboSource.push(["<c:out value='${result.orgCode}' />"+" "+"<c:out value='${result.orgDesc}' />","<c:out value='${result.orgCode}' />"]);
					orgCodeShowDataT.push(["<c:out value='${result.orgCode}' />"]);
				}
			</c:forEach>
				cboTo.dataProvider.setData(cboSource);
		     	//SuUserOrganizationService.findOrganizationByUserIdAndOuCodeToOrgCode('<%=userId%>','<%=ouCode%>',orgCode , {callback:whenFetchOrganizationToCallback});
	}	
function whenFetchOrganizationToCallback(data){
		    	try{
			     	var cboSource = [];
			     	var cboTo = dojo.widget.byId("orgToCbo");
			     	for(var i=0; i<data.length; i++){
			     		var org = data[i];
			     		cboSource.push([org.orgCode, org.codeSeq]);
			     		orgCodeShowDataT.push([org.orgCode]);
						
			     	}
			     	cboTo.dataProvider.setData(cboSource);
		     	}catch(e){
		     		alert(e.message);
		     	}
}
function whenSelectEmpOption(){
	DWRUtil.useLoadingMessage("Loading ...");
	var cbo = dojo.widget.byId("empCodeFrom");
	whenFetchEmployeeTo(cbo.textInputNode.value);
	}
function whenFetchEmployeeTo(empCode){
	DWRUtil.useLoadingMessage("Loading ...");
	var cboTo = dojo.widget.byId("empCodeTo");
	var arrData=[];
	if( empCode > cboTo.comboBoxSelectionValue.value ){
		cboTo.textInputNode.value = '';
		cboTo.comboBoxSelectionValue.value = '';
	}
	<c:forEach items="${WgEmployeeInSecurity}" var="result" >
		if("<c:out value='${result}' />" >= empCode){		 
			arrData.push(["<c:out value='${result}' />","<c:out value='${result}' />"]);
			empCodeDataTo.push(["<c:out value='${result}' />"]);
		}
	</c:forEach>
	cboTo.dataProvider.setData(arrData);
	//PnEmployeeService.findToEmpBySecurity('<%=userId%>','<%=ouCode%>',empCode , {callback:whenFetchEmployeeToCallback});
}
function whenFetchEmployeeToCallback(data){
		    	try{
		    		var arrData=[];
			     	var cboTo = dojo.widget.byId("empCodeTo");
			     	for(i=0;i<data.length;i++){
						arrData.push([data[i].empCode,data[i].empCode]);
						empCodeDataTo.push([data[i].empCode]);
						}
			     	cboTo.dataProvider.setData(arrData);
		     	}catch(e){
		     		alert(e.message);
		     	}
}
</script>
<body>
<table width="100%" >
<tr>
<td class="font-head">[ CTRWPNTRP001 ]  ��§ҹ�����Ū��͹��ʡ�� (�����ѧ���) �ͧ��ѡ�ҹ/�١��ҧ��Ш�</td>
</tr>
<tr>  
<td>
<form action="weWgEmployeeTextReport.htm?reqCode=doPrintReport" method="post" name="mainform"  ><div style="height:400px;">
	<table  width="100%" align="center" border="0">
	<tr>
		<td aling ="center"> <input type="hidden" name="ouCode" value="<%=ouCode%>"><input type="hidden" name="userId" value="<%=userId%>">
 			<table width="800" align="center" border="0" cellpadding="2" cellspacing="2">
				
            		<td class="font-field"  >������ѧ�Ѵ��Ժѵԧҹ</td>
            		<td colspan="3" nowarp><select  dojotype="ComboBox"  widgetid="orgFromCbo" style="width:570;" ></select></td>
				</tr>
				<tr>
            		<td class="font-field" >�֧�ѧ�Ѵ��Ժѵԧҹ </td>
            		<td colspan="3" nowarp><select  dojotype="ComboBox" widgetid="orgToCbo" style="width:570;"></select></td>
					<input type="hidden" name="orgCodeFrom" ><input type="hidden" name="orgCodeTo" >
          		</tr>
				<tr >
					<td width="160" class="font-field">������Ţ��Шӵ��</td>
					<td width="150" align="left"><SELECT  widgetId="empCodeFrom" dojoType="ComboBox" style="width:120"></SELECT>	</td>
					<td width="100" class="font-field" >�֧�Ţ��Шӵ��</td>
					<td width="350" align="left"><SELECT  widgetId="empCodeTo" dojoType="ComboBox" style="width:120"></SELECT></td>
					<input type="hidden" name="empF" ><input type="hidden" name="empT" >
				</tr>				
				<tr>
                   <td colspan="4" align="center"><input type="button" class=" button "  value=" �ʴ���§ҹ " onclick="whenLoadTaReport();" /></td>				
				</tr>
 			</table>
		</td>
	</tr>

	<table>
	</table>
		
	</form>
</td>
</tr>
</table>
</body>
<script>
	
	function whenLoadTaReport(){
		var frm = document.forms[0];
	
		var comB1=dojo.widget.byId("empCodeFrom").textInputNode.value;
		var comB2=dojo.widget.byId("empCodeTo").textInputNode.value;
		var nameOrgFrom = dojo.widget.byId("orgFromCbo").textInputNode.value;
		var nameOrgTo = dojo.widget.byId("orgToCbo").textInputNode.value;
		
		var orgTempF="";
		var orgTempT="";
		var empCF="";
		var empCT="";
		
		var orgCodeFrom = "";
		var orgCodeTo = "";
	
		var codeFrom="";
		var codeTo="";
	
		if(nameOrgFrom!=''){
			dojo.widget.byId("orgFromCbo").comboBoxSelectionValue.value = splitCombo(dojo.widget.byId("orgFromCbo").textInputNode.value);
			orgCodeFrom = splitCombo(dojo.widget.byId("orgFromCbo").textInputNode.value);
			
		}
		if(nameOrgTo !=''){
			dojo.widget.byId("orgToCbo").comboBoxSelectionValue.value = splitCombo(dojo.widget.byId("orgToCbo").textInputNode.value);
			orgCodeTo =  splitCombo(dojo.widget.byId("orgToCbo").textInputNode.value);
			
		}
		
		if(dojo.widget.byId("orgToCbo").textInputNode.value!=""){
			if(dojo.widget.byId("orgFromCbo").textInputNode.value==""){
				alert("��سҡ�͡ �ѧ�Ѵ��Ժѵԧҹ������� ");
				return false;
			}
		}
		if(comB1!=''){
			dojo.widget.byId("empCodeFrom").comboBoxSelectionValue.value = splitCombo(dojo.widget.byId("empCodeFrom").textInputNode.value);
			codeFrom = splitCombo(dojo.widget.byId("empCodeFrom").textInputNode.value);
			
		}
		if(comB2 !=''){
			dojo.widget.byId("empCodeTo").comboBoxSelectionValue.value = splitCombo(dojo.widget.byId("empCodeTo").textInputNode.value);
			codeTo =  splitCombo(dojo.widget.byId("empCodeTo").textInputNode.value);
			
		}
		if(dojo.widget.byId("empCodeTo").textInputNode.value!=""){
			if(dojo.widget.byId("empCodeFrom").textInputNode.value==""){
				alert("��سҡ�͡ �Ţ��Шӵ��������� ");
				return false;
			}
		}


		
		
		
	
		forController(orgCodeFrom,orgCodeTo,codeFrom,codeTo);
		frm.target = "_blank";
		frm.submit();	
	}
	function forController(orgCodeFrom,orgCodeTo,codeFrom,codeTo){
		
		DWRUtil.setValue("orgCodeFrom",orgCodeFrom);
		DWRUtil.setValue("orgCodeTo",orgCodeTo);
		DWRUtil.setValue("empF",codeFrom);
		DWRUtil.setValue("empT",codeTo);
		
		}
	
	
</script>
</html>