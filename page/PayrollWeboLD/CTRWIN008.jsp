<%@ page language="java" contentType="text/html;charset=TIS-620" %>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>

<%
	String yearInsert = (String)request.getParameter("yearInsert");
	String periodInsert = (String)request.getParameter("periodInsert");
	String userIdInsert = (String)request.getParameter("userIdInsert");
	String ouCodeInsert = (String)request.getParameter("ouCodeInsert");
	String sectionInsert = (String)request.getParameter("sectionInsert");
	
	String orgFromIns = request.getParameter("orgFromIns");
	String orgToIns = request.getParameter("orgToIns");
	String empCodeFromIns = request.getParameter("empCodeFromIns");
	String empCodeToIns = request.getParameter("empCodeToIns");
	String pageIns = request.getParameter("pageIns");
	
	String orderFromCboIns = request.getParameter("orderFromCboIns");
	String orderToCboIns = request.getParameter("orderToCboIns");
	
	if( pageIns.trim().equals("") )
		pageIns = "-1";
	
	java.util.Date dd = new java.util.Date();
	java.text.SimpleDateFormat fmd = new java.text.SimpleDateFormat("dd/MM/yyyy",new java.util.Locale("th","TH"));
	String date = fmd.format(dd);
	
	java.util.Calendar cc = new java.util.GregorianCalendar();
	int x = cc.get(java.util.Calendar.MONTH) +1;
	//System.out.println("#$#$#$#$$#$#$# :::"+x);
	
	String periodWork = String.valueOf(x);
%>
<c:set var="org.apache.struts.taglib.html.BEAN" value="${form}" />
<html>
<head>
<title>��Ѻ��ا��¡���Ѻ /���¡�׹ (�Թ��͹)</title>
<!-- Include -->
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript" src="dwr/util.js"></script>
<!-- Javascript Script File -->
<SCRIPT type="text/javascript" src="dwr/interface/SuUserOrganizationService.js"></SCRIPT>
<script type="text/javascript" src="dwr/interface/RwModSalService.js"></script>
<SCRIPT type="text/javascript" src="dwr/interface/PnEmployeeService.js"></SCRIPT>
<script type="text/javascript" src="script/gridScript.js"></script>
<script type="text/javascript" src="script/dojo.js"></script>
<script type="text/javascript" src="script/dateCalendar.js"></script>
<script type="text/javascript" src="script/payroll_util.js"></script>

<script type="text/javascript">
	//Load Dojo's code relating to widget managing functions
	dojo.require("dojo.widget.*");
	dojo.require("dojo.widget.Menu2");
	dojo.require("dojo.widget.Button");
	dojo.require("dojo.widget.ComboBox");
	dojo.require("dojo.widget.DropDownButton");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.ContentPane");
	dojo.require("dojo.widget.LayoutContainer");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.Toolbar");
	dojo.require("dojo.widget.html.*");
	dojo.require("dojo.widget.Menu2");
	dojo.hostenv.writeIncludes();
	
	//Event
	dojo.require("dojo.event.*");
</script>
<script type="text/javascript">
<!--
	
	var rwModSal = {keySeq:null, ouCode:null, yearPr:null, periodPr:null,empCode:null,
					yearWork:null,periodWork:null,codeSeq:null,startDateNew:null,fiscalYear:null,
					startDateEdun:null,endDateEdun:null,newSal:null,oldSal:null,confirmFlag:null,
					startDateBack:null,endDateBack:null,startDateJob:null,endDateJob:null,
					multipleLevel:null,backStep:null,confirmFlag:null,updBy:null,updDate:null,creBy:null,creDate:null};
	
	function ClearData(){
		alert("�ѹ�֡���������º����");
		
		var table = document.getElementById("table");
		var tdName;
		var chkName;
		
		
		var oRows = table.rows;
		if(tdName == null)tdName="flag";
		if(chkName == null)chkName="chk";
		//alert(oRows.length);
		if(oRows.length > 3){
			for(i=oRows.length-1;i > 2;i--){
				table.deleteRow(i);			
			}
		}
		
		
		$("empCbo").value = '';
		$("name").value = '';
		$("positionShort").value = '';
		$("orgCode").value = '';
		$("orgDesc").value = '';
	}

	function insertNewData()
	{
		
			var table = document.getElementById("table");
			var tdName;
			var chkName;
			
			var oRows = table.rows;
			if(tdName == null)tdName="flag";
			if(chkName == null)chkName="chk";
			if(oRows.length > 3)
			{	
				DWREngine.beginBatch();
				for(var i=oRows.length-1;i > 2;i--)
				{
					rwModSal.ouCode = '<%=ouCodeInsert%>';
					rwModSal.yearPr = '<%=yearInsert%>';
					rwModSal.periodPr = '<%=periodInsert%>';
					rwModSal.empCode = DWRUtil.getValue("empCbo");
					rwModSal.codeSeq = $("codeSeq").value;
					rwModSal.flagPr= oRows[i].cells["flagPr"].childNodes[0].value;
				//	rwModSal.yearWork = oRows[i].cells["yearWork"].childNodes[0].value;
				//	rwModSal.periodWork = oRows[i].cells["periodWork"].childNodes[0].value;
					if(oRows[i].cells["startDateNew"].childNodes[0].value != null && oRows[i].cells["startDateNew"].childNodes[0].value != ''){
						rwModSal.startDateNew = getDateFromFormat(oRows[i].cells["startDateNew"].childNodes[0].value,"dd/MM/yyyy");
						
						var ddt = oRows[i].cells["startDateNew"].childNodes[0].value;
						var endMonth = ddt.substr(3, 2);
						var endYear = ddt.substr(6);
						
						rwModSal.yearWork = endYear;
						rwModSal.periodWork = endMonth;
					}else{
						rwModSal.startDateNew = null;
						rwModSal.yearWork = '';
						rwModSal.periodWork = '';
					}
					rwModSal.fiscalYear = oRows[i].cells["fiscalYear"].childNodes[0].value;
					if(oRows[i].cells["startDateEdun"].childNodes[0].value != null && oRows[i].cells["startDateEdun"].childNodes[0].value != ''){
						rwModSal.startDateEdun= getDateFromFormat(oRows[i].cells["startDateEdun"].childNodes[0].value,"dd/MM/yyyy");
					}else{
						rwModSal.startDateEdun = null;
					}
					if(oRows[i].cells["endDateEdun"].childNodes[0].value != null && oRows[i].cells["endDateEdun"].childNodes[0].value != ''){
						rwModSal.endDateEdun = getDateFromFormat(oRows[i].cells["endDateEdun"].childNodes[0].value,"dd/MM/yyyy");
					}else{
						rwModSal.endDateEdun = null;
					}
					if(oRows[i].cells["newSal"].childNodes[0].value != null && oRows[i].cells["newSal"].childNodes[0].value != ''){
						rwModSal.newSal = oRows[i].cells["newSal"].childNodes[0].value;
					}else{
						rwModSal.newSal = null;
					}
					if(oRows[i].cells["oldSal"].childNodes[0].value != null && oRows[i].cells["oldSal"].childNodes[0].value != ''){
						rwModSal.oldSal = oRows[i].cells["oldSal"].childNodes[0].value;
					}else{
						rwModSal.oldSal = null;
					}
				//	rwModSal.newSal = oRows[i].cells["newSal"].childNodes[0].value;
				//	rwModSal.oldSal = oRows[i].cells["oldSal"].childNodes[0].value;
					
					if(oRows[i].cells["startDateBack"].childNodes[0].value != null && oRows[i].cells["startDateBack"].childNodes[0].value != ''){
						rwModSal.startDateBack = getDateFromFormat(oRows[i].cells["startDateBack"].childNodes[0].value,"dd/MM/yyyy");
					}else{
						rwModSal.startDateBack = null;
						//alert('���á���');
					}
					if(oRows[i].cells["endDateBack"].childNodes[0].value != null && oRows[i].cells["endDateBack"].childNodes[0].value != ''){
						rwModSal.endDateBack= getDateFromFormat(oRows[i].cells["endDateBack"].childNodes[0].value,"dd/MM/yyyy");
					}else{
						rwModSal.endDateBack= null;
					}
					if(oRows[i].cells["startDateJob"].childNodes[0].value != null && oRows[i].cells["startDateJob"].childNodes[0].value != ''){
						rwModSal.startDateJob = getDateFromFormat(oRows[i].cells["startDateJob"].childNodes[0].value,"dd/MM/yyyy");
					}else{
						rwModSal.startDateJob = null;
					}
					if(oRows[i].cells["endDateJob"].childNodes[0].value != null && oRows[i].cells["endDateJob"].childNodes[0].value != ''){
						rwModSal.endDateJob = getDateFromFormat(oRows[i].cells["endDateJob"].childNodes[0].value,"dd/MM/yyyy");
					}else{
						rwModSal.endDateJob = null;
					}
					rwModSal.multipleLevel = oRows[i].cells["multipleLevel"].childNodes[0].value;
					rwModSal.backStep = oRows[i].cells["backStep"].childNodes[0].value;
					rwModSal.confirmFlag = 'N';
					rwModSal.updBy = '<%=userIdInsert%>';
					rwModSal.creBy = '<%=userIdInsert%>';
					rwModSal.creDate = getDateFromFormat(<%=date%>,"dd/MM/yyyy");
					//RwModSalService.insertRwModSal(rwModSal, {callback:onInsertCallback});
					//RwModSalService.addList(rwModSal, {callback:onInsertCallback});
				
					if( i == 3 )
						RwModSalService.addList(rwModSal, true, {callback:ClearData,errorHandler:function(message) { alert('�������ö�ѹ�֡��');}});
					else
						RwModSalService.addList(rwModSal, false, {callback:onInsertCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
				}
				DWREngine.endBatch();	
				//RwModSalService.insertAndUpdate({callback:ClearData});
				
			}else
			{
				alert('��辺�����ŷ��кѹ�֡');
			}
		
	}
	
	function onInsertCallback(data){
	}
	
     function whenSelectEmpOption()
     {
		DWRUtil.useLoadingMessage("Loading ...");
		var empCode = DWRUtil.getValue("empCbo")
		//alert(empCode);
		//var cbo = dojo.widget.byId("empCbo");
		
		whenFetchEmployeeDetail(empCode);
	 } 
	 
	 function whenFetchEmployeeDetail(empCode)
	 {
		DWRUtil.useLoadingMessage("Loading ...");
		
		PnEmployeeService.findByEmpCodeDetail('<%=userIdInsert%>', empCode, '<%=ouCodeInsert%>', '<%=yearInsert%>', '<%=periodInsert%>', {callback:whenFetchEmployeeDetailCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
	 }

	 function whenFetchEmployeeDetailCallback(data)
	 {
	 	if(data.empCode != null && data.empCode != ''){
			$("name").value = data.name;
			$("positionShort").value = data.positionShort;
			$("orgCode").value = data.orgCode;
			$("orgDesc").value = checkNull(data.divDesc,'STRING') + ' ' + checkNull(data.orgDesc,'STRING');
			$("codeSeq").value = data.codeSeqWork;
			
					document.forms['formInsert'].elements['insertData'].disabled = false;
					document.forms['formInsert'].elements['deleteData'].disabled = false;
					document.forms['formInsert'].elements['confirmData'].disabled = false;	
		}else{
			$("name").value = '';
			$("positionShort").value = '';
			$("orgCode").value = '';
	    	$("orgDesc").value = '';
			$("codeSeq").value = '';
			
					document.forms['formInsert'].elements['insertData'].disabled = true;
					document.forms['formInsert'].elements['deleteData'].disabled = true;
					document.forms['formInsert'].elements['confirmData'].disabled = true;
			alert('�Ţ��Шӵ�����١��ͧ');	
		}
	 }
	
	
	function init()
	{
		$("year").value = '<%=yearInsert%>';
	 	$("section").value = '<%=sectionInsert%>';
		$("period").value = '<%=periodInsert%>';

	}
	
	function backFormMain(){
		var frm=document.forms['backForm'];
		
		document.getElementById("orgFromEdit").value = '<%= orgFromIns %>';
		document.getElementById("orgToEdit").value = '<%= orgToIns %>';
		document.getElementById("empCodeFromEdit").value = '<%= empCodeFromIns %>';
		document.getElementById("empCodeToEdit").value = '<%= empCodeToIns %>';
		document.getElementById("pageEdit").value = '<%= Integer.parseInt(pageIns) - 1 %>';
		
		document.getElementById("orderFromCboEdit").value = '<%= orderFromCboIns %>';
		document.getElementById("orderToCboEdit").value = '<%= orderToCboIns %>';
		
		frm.submit();
	}
 	 
	
 	 dojo.addOnLoad(init);
 	// dojo.addOnLoad(onLoadEmployee);
 	
 	function compareStDateEdun(object){
	 	 while (object.tagName !=  'TR')
		 {
		 object = object.parentNode
		 }
	
		 var rowToCompare = object.rowIndex
		
     	DWRUtil.useLoadingMessage("Loading ...");
		var table = document.getElementById("table");
		//alert(table.);
		var tdName;
		var chkName;
		
		var oRows = table.rows;
	
		var stDate = oRows[rowToCompare].cells["startDateEdun"].childNodes[0].value;
		var endDate = oRows[rowToCompare].cells["endDateEdun"].childNodes[0].value;
	
		
		var strDay = stDate.substr(0, 2);
		var strMonth = stDate.substr(3, 2);
		var strYear = stDate.substr(6);
		
		if(stDate != null && stDate != '' && endDate != null && endDate != ''){
		 	if (compareDates(stDate,"dd/MM/yyyy",endDate,"dd/MM/yyyy") == 1) {
				alert("�ѹ���������鹵�ͧ���¡��� �ѹ�������ش");
				oRows[rowToCompare].cells["endDateEdun"].childNodes[0].value = '';
				oRows[rowToCompare].cells["startDateEdun"].childNodes[0].focus();
			}else{
				oRows[rowToCompare].cells["endDateEdun"].childNodes[0].focus();
			}
	 	}
	 	
	 	if(stDate != null && stDate != '' && (endDate == null || endDate == '')){
				oRows[rowToCompare].cells["endDateEdun"].childNodes[0].focus();	
		}
	 
	 }
	 
	 function compareEndDateEdun(object){
	 	 while (object.tagName !=  'TR')
		 {
		 object = object.parentNode
		 }
	
		 var rowToCompare = object.rowIndex
		
     	DWRUtil.useLoadingMessage("Loading ...");
		var table = document.getElementById("table");
		//alert(table.);
		var tdName;
		var chkName;
		
		var oRows = table.rows;
	
		var stDate = oRows[rowToCompare].cells["startDateEdun"].childNodes[0].value;
		var endDate = oRows[rowToCompare].cells["endDateEdun"].childNodes[0].value;
		
		
		var endDay = endDate.substr(0, 2);
		var endMonth = endDate.substr(3, 2);
		var endYear = endDate.substr(6);
		
		if(stDate != null && stDate != '' && endDate != null && endDate != ''){
		 	if (compareDates(stDate,"dd/MM/yyyy",endDate,"dd/MM/yyyy") == 1) {
				alert("�ѹ���������鹵�ͧ���¡��� �ѹ�������ش");
				oRows[rowToCompare].cells["startDateEdun"].childNodes[0].value = '';
				oRows[rowToCompare].cells["endDateEdun"].childNodes[0].focus();
			}
	 	}
	}
	
	function compareStDateBack(object){
	 	 while (object.tagName !=  'TR')
		 {
		 object = object.parentNode
		 }
	
		 var rowToCompare = object.rowIndex
		
     	DWRUtil.useLoadingMessage("Loading ...");
		var table = document.getElementById("table");
		//alert(table.);
		var tdName;
		var chkName;
		
		var oRows = table.rows;
	
		var stDate = oRows[rowToCompare].cells["startDateBack"].childNodes[0].value;
		var endDate = oRows[rowToCompare].cells["endDateBack"].childNodes[0].value;
	
		
		var strDay = stDate.substr(0, 2);
		var strMonth = stDate.substr(3, 2);
		var strYear = stDate.substr(6);
		
		if(stDate != null && stDate != '' && endDate != null && endDate != ''){
		 	if (compareDates(stDate,"dd/MM/yyyy",endDate,"dd/MM/yyyy") == 1) {
				alert("�ѹ���������鹵�ͧ���¡��� �ѹ�������ش");
				oRows[rowToCompare].cells["endDateBack"].childNodes[0].value = '';
				oRows[rowToCompare].cells["startDateBack"].childNodes[0].focus();
			}else{
			
				oRows[rowToCompare].cells["endDateBack"].childNodes[0].focus();
			}
	 	}
	 	
	 	if(stDate != null && stDate != '' && (endDate == null || endDate == '')){
				oRows[rowToCompare].cells["endDateBack"].childNodes[0].focus();
		}
	 
	 }
	 
	 function compareEndDateBack(object){
	 	 while (object.tagName !=  'TR')
		 {
		 object = object.parentNode
		 }
	
		 var rowToCompare = object.rowIndex
		
     	DWRUtil.useLoadingMessage("Loading ...");
		var table = document.getElementById("table");
		//alert(table.);
		var tdName;
		var chkName;
		
		var oRows = table.rows;
	
		var stDate = oRows[rowToCompare].cells["startDateBack"].childNodes[0].value;
		var endDate = oRows[rowToCompare].cells["endDateBack"].childNodes[0].value;
		
		
		var endDay = endDate.substr(0, 2);
		var endMonth = endDate.substr(3, 2);
		var endYear = endDate.substr(6);
		
		if(stDate != null && stDate != '' && endDate != null && endDate != ''){
		 	if (compareDates(stDate,"dd/MM/yyyy",endDate,"dd/MM/yyyy") == 1) {
				alert("�ѹ���������鹵�ͧ���¡��� �ѹ�������ش");
				oRows[rowToCompare].cells["startDateBack"].childNodes[0].value = '';
				oRows[rowToCompare].cells["endDateBack"].childNodes[0].focus();
			}
	 	}
	}
	
	function compareStDateJob(object){
	 	 while (object.tagName !=  'TR')
		 {
		 object = object.parentNode
		 }
	
		 var rowToCompare = object.rowIndex
		
     	DWRUtil.useLoadingMessage("Loading ...");
		var table = document.getElementById("table");
		//alert(table.);
		var tdName;
		var chkName;
		
		var oRows = table.rows;
	
		var stDate = oRows[rowToCompare].cells["startDateJob"].childNodes[0].value;
		var endDate = oRows[rowToCompare].cells["endDateJob"].childNodes[0].value;
	
		
		var strDay = stDate.substr(0, 2);
		var strMonth = stDate.substr(3, 2);
		var strYear = stDate.substr(6);
		
		if(stDate != null && stDate != '' && endDate != null && endDate != ''){
		 	if (compareDates(stDate,"dd/MM/yyyy",endDate,"dd/MM/yyyy") == 1) {
				alert("�ѹ���������鹵�ͧ���¡��� �ѹ�������ش");
				oRows[rowToCompare].cells["endDateJob"].childNodes[0].value = '';
				oRows[rowToCompare].cells["startDateJob"].childNodes[0].focus();
			}else{
				oRows[rowToCompare].cells["endDateJob"].childNodes[0].focus();
			
			}
	 	}
	 	
	 	if(stDate != null && stDate != '' && (endDate == null || endDate == '')){
				oRows[rowToCompare].cells["endDateJob"].childNodes[0].focus();
		}
	 
	 }
	 
	 function compareEndDateJob(object){
	 	 while (object.tagName !=  'TR')
		 {
		 object = object.parentNode
		 }
	
		 var rowToCompare = object.rowIndex
		
     	DWRUtil.useLoadingMessage("Loading ...");
		var table = document.getElementById("table");
		//alert(table.);
		var tdName;
		var chkName;
		
		var oRows = table.rows;
	
		var stDate = oRows[rowToCompare].cells["startDateJob"].childNodes[0].value;
		var endDate = oRows[rowToCompare].cells["endDateJob"].childNodes[0].value;
		
		
		var endDay = endDate.substr(0, 2);
		var endMonth = endDate.substr(3, 2);
		var endYear = endDate.substr(6);
		
		if(stDate != null && stDate != '' && endDate != null && endDate != ''){
		 	if (compareDates(stDate,"dd/MM/yyyy",endDate,"dd/MM/yyyy") == 1) {
				alert("�ѹ���������鹵�ͧ���¡��� �ѹ�������ش");
				oRows[rowToCompare].cells["startDateJob"].childNodes[0].value = '';
				oRows[rowToCompare].cells["endDateJob"].childNodes[0].focus();
			}
	 	}
	}
	
-->
</script>
</head>

<body>
<form name="formInsert"  action="security.htm?reqCode=CTRWMT008" method="post">
<input type="hidden" name="dataLength">
<input type="hidden" name="period">
<input type="hidden" name="codeSeq">
<table width="100%">
	<tr>
		<td class="font-head">
			[ CTRWIN008 ] ��Ѻ��ا��¡���Ѻ/���¡�׹ (�Թ��͹)
		</td>
	</tr>
</table>
<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
          <td  align="center">
				<table width="750" border="1" cellpadding="1" bordercolor="#6699CC" cellspacing="1"  align="center">
        			<tr>
          				<td class="font-field"  align="right" width="100">��Шӻ�</td>
          				<td align="left"><input type="text" name="year" value="" readonly="readonly" style="width:70;text-align:center;background-color:silver;"/></td>
          				<td colspan="3" class="font-field" align="left" width="250">
          					&nbsp;&nbsp;�Ǵ&nbsp;&nbsp;
          					<input type="text" name="section" value="" readonly="readonly" style="width:70;text-align:center;background-color:silver;"/>
          				</td>
        			</tr>
        			<tr>
         				<td class="font-field"  align="right">�Ţ��Шӵ�� </td>
          				<td align="left" width="15"><input type="text" name="empCbo" maxlength="6" style="width:150" onchange="whenSelectEmpOption();"></td>
          				<td align="left"><input type="text" name="name" readonly="readonly" style="width:260;background-color:silver;"/></td>
        				<td  class="font-field"  align="right" width="65" >���˹�</td>
          				<td  align="left"><input type="text" name="positionShort" value="" readonly="readonly" style="width: 150;text-align: center;background-color:silver;"  /></td>
          				
       			 	</tr>
       			 	<tr>
          				<td  class="font-field"  align="right">�ѧ�Ѵ��Ժѵԧҹ</td>
          				<td  align="left" width="30"><INPUT type="text" name="orgCode" maxlength="20" readonly="readonly" style="width:150;background-color: silver;" onBlurInput="whenFindOrganization();" /></td>
          				<td colspan="3"align="left"><input type="text" name="orgDesc" readonly="readonly" style="width:485;background-color: silver;"/></td>
       			 	</tr>
     		 </table>
          </td>
      </tr>
    	&nbsp;
	 	&nbsp;
	  	&nbsp;
	  <tr>
          <td align="center">
				<div style="height:345px;width:889;overflow:auto;">
				 &nbsp;
	  			&nbsp;
	  			&nbsp;
				<table id="table"  bordercolor="#4A4A4A" width="1500" align="center" style="text-align: center" border="1" cellpadding="0" cellspacing="0" >
					<thead style="text-align: center">
						<tr CLASS="TABLEBULE2">
							<th CLASS="TABLEBULE2" style="width:50px" rowspan="2" align="center">ź</th>
							<th CLASS="TABLEBULE2" rowspan="2" align="center">��������¡��</th>
							<th CLASS="TABLEBULE2" style="width:140px" rowspan="2" align="center">���ԡ��è�����/��Ѻ�ҷӧҹ���� �����</th>
							<th CLASS="TABLEBULE2" colspan="5" rowspan="1" align="center"><center>���ԡ��Ѻ�ز�</center></th>
							<th CLASS="TABLEBULE2" style="width:250px" colspan="2" rowspan="1" align="center"><center>���ԡ��Ѻ�ҷӧҹ����������Ѻ�ز�</center></th>
							<th CLASS="TABLEBULE2" colspan="3" rowspan="1" align="center"><center>���ԡ�ѡ�ҹ</center></th>
							<th CLASS="TABLEBULE2" style="width:250px" rowspan="1" align="center"><center>���ԡ�Ԩ�óҤ����դ����ͺ��͹��ѧ</center></th>
						</tr>
						<tr CLASS="TABLEBULE2"  >
							<th CLASS="TABLEBULE2" align="center">�է�����ҳ</th>
							<th CLASS="TABLEBULE2" align="center">�����</th>
							<th CLASS="TABLEBULE2" align="center">�֧</th>
							<th CLASS="TABLEBULE2" style="width:150px" align="center">�Թ��͹���</th>
							<th CLASS="TABLEBULE2" style="width:150px" align="center">�Թ��͹����</th>
							<th CLASS="TABLEBULE2" align="center">�����</th>
							<th CLASS="TABLEBULE2" align="center">�֧</th>
							<th CLASS="TABLEBULE2" align="center">�����</th>
							<th CLASS="TABLEBULE2" align="center">�֧</th>
							<th CLASS="TABLEBULE2" align="center">�ӹǹ���</th>
							<th CLASS="TABLEBULE2" align="center">�ӹǹ���</th>
						</tr>
					</thead>
											
						<tr id="temprow" style="visibility:hidden;position:absolute">
							<td id="flag"><input type="checkbox" name="chk" /></td>
							<td id="flagPr">
										<select name="type">
												<!-- <option value="N" >����</option> -->
												<option value="A" >��Ѻ��ا��¡���Ѻ</option>
												<!--<option value="R" >��¡���Ѻ���¡�׹</option>-->
												<!-- <option value="B" >���ԡ��Ѻ��ا��¡���Ѻ</option>
												<option value="S" >���ԡ��¡���Ѻ���¡�׹</option> -->
										</select>
							</td>
							<td id="startDateNew" ><input type="text"   name="startDateNew" size="20" maxlength="10" align="center" style="text-align:center;" onchange="validateDate(this);" ></td>
							<td id="fiscalYear" ><input type="text"  name="fiscalYear" value="<%=yearInsert%>" size="6"  maxlength="4" style="text-align:center;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);" /></td>
							<td id="startDateEdun" ><input type="text"  name="startDateEdun" size="12"  maxlength="10" align="center" style="text-align:center;" onchange="return validateDate(this) && compareStDateEdun(this);" ></td>
							<td id="endDateEdun" ><input type="text"  name="endDateEdun" size="12" maxlength="10" align="center" style="text-align:center;" onchange="return validateDate(this) && compareEndDateEdun(this);"></td>
							
							<td id="oldSal" ><select name="type" style="width:100%;scroll-y:true" >
												<option value=""  ></option>
												
												<c:forEach items="${SalaryList}" var="result">
													<option  value="<c:out value='${result.salary}' />" ><c:out value='${result.salary}' /></option>
												</c:forEach>
												
										</select></td>
							<td id="newSal" ><select name="type" style="width:100%;scroll-y:true" >
												<option value=""  ></option>
												
												<c:forEach items="${SalaryList}" var="result">
													<option  value="<c:out value='${result.salary}' />" ><c:out value='${result.salary}' /></option>
												</c:forEach>
												
										</select>
							</td>
							<td id="startDateBack" ><input type="text"  name="startDateBack" size="14" maxlength="10" align="center" style="text-align:center;" onchange="return validateDate(this) && compareStDateBack(this);" ></td>
							<td id="endDateBack" ><input type="text"  name="endDateBack" size="14" maxlength="10" align="center" style="text-align:center;" onchange="return validateDate(this) && compareEndDateBack(this);"></td>
							<td id="startDateJob" ><input type="text"  name="startDateJob" size="12" maxlength="10" align="center" style="text-align:center;" onchange="return validateDate(this) && compareStDateJob(this);" ></td>
							<td id="endDateJob" ><input type="text"   name="endDateJob" size="12" maxlength="10" align="center" style="text-align:center;" onchange="return validateDate(this) && compareEndDateJob(this);"><input type="hidden"  name="keySeq" /></td>
							<td id="multipleLevel">
										<select name="type">
												<option value="" ></option>
												<option value="1" >0.5 ���</option>
												<option value="2" >1.0 ���</option>
										</select>
							</td>
							<td id="backStep">
										<select name="type" style="width:100%">
												<option value="" ></option>
												<option value="1" >0.5 ���</option>
												<option value="2" >1.0 ���</option>
												<option value="3" >1.5 ���</option>
												<option value="4" >2.0 ���</option>
										</select>
							</td>
							</tr>
					</table>
				</div>
				
           </td>
      </tr>
          
</table>&nbsp;

<input type="hidden" name="pageEdit" value="">
<table width="100%" CLASS="TABLEBULE2" >
					<tr CLASS="TABLEBULE2" >
						<td align="left" >&nbsp;
							<input type="Button" class=" button " value="����������" name="insertData" onclick="addVisualRow();" disabled="disabled"/>						
							<input type="Button" class=" button " value="ź������" name="deleteData" onclick="removeVisualRow();" disabled="disabled"/>						
							<input type="Button" class=" button " value="��ŧ" name="confirmData" onclick="insertNewData();" disabled="disabled"/>
							<input type="Button" class=" button " name="back" value=" �͡ " onclick="backFormMain();"/>
						</td>
					</tr>
				</table>
</form>
	<form name="backForm" action="security.htm?reqCode=CTRWMT008" method="post">
		<input type="hidden" name="orgFromEdit" />
		<input type="hidden" name="orgToEdit" />
		<input type="hidden" name="empCodeFromEdit" />
		<input type="hidden" name="empCodeToEdit" />
		<input type="hidden" name="pageEdit" />
		<input type="hidden" name="orderFromCboEdit" />
		<input type="hidden" name="orderToCboEdit" />	
	</form>
</body>
<script type="text/javascript">
		
</script>
</html>

<SCRIPT LANGUAGE="JavaScript">

				
function addVisualRow(){

	var table = document.getElementById("table");
	var tempRow = document.getElementById("temprow");

	// Insert two rows.
   	var oTable = table;
   	var idx = oTable.rows.length;
   	var oRow1=oTable.insertRow(idx);
   	var str=" " ;
	var from = "[0]";
	var to = "("+(oRow1.rowIndex+tempRow.rowIndex)+")";

	// Insert cells into row.
	oRow1.id = tempRow.id;
	for(i=0;i<tempRow.cells.length;i++){
		var oCell=oRow1.insertCell(i);
		// Add regular HTML values to the cells.
		oCell.id = tempRow.cells[i].id;
		oCell.innerHTML=tempRow.cells[i].innerHTML;
		//alert(i +' : '+tempRow.cells[i].innerHTML.name);
		
		  
		str = oCell.innerHTML;
		if(str.indexOf(from)>0){
			oCell.innerHTML=str.replace(from, to);
		}
	}
	
}

	function removeVisualRow(){
		//alert('delete');
		var table = document.getElementById("table");
		var tdName;
		var chkName;
		
		
		var oRows = table.rows;
		if(tdName == null)tdName="flag";
		if(chkName == null)chkName="chk";
		//alert(oRows.length);
		if(oRows.length > 3){
			var answer = confirm("��ͧ���ź������ �������?");
			if( answer ){
				for(i=oRows.length-1;i > 2;i--){
						if (oRows[i].cells[tdName].childNodes[0].checked){
							table.deleteRow(i);			
						}
				}
				alert('ź���������º����');
			}
			
		}
		
	}
//***********************

//-->
</SCRIPT>