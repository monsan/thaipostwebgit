<%@ page language="java" contentType="text/html;charset=TIS-620"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="javax.swing.text.Document"%>
<%@ page import="com.ss.tp.security.UserInfo" %>
<%@ page import="sun.security.krb5.internal.i" %>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>
<%
	UserInfo userInfo = (UserInfo)session.getAttribute("UserLogin");
	String userId = userInfo.getUserId();
	String ouCode = userInfo.getOuCode();
	
	String flagQY = request.getParameter("flagEdit");
	
	String orgFromEdit = ""; 
	String orgToEdit = "";
	String empCodeFromEdit = ""; 
	String empCodeToEdit = "";
	String pageEdit = "";
	
	System.out.println("flag : " + flagQY);
	
	if( flagQY != null && flagQY.equals("MT") ){
		orgFromEdit = request.getParameter("orgFromEdit");
		orgToEdit = request.getParameter("orgToEdit");
		empCodeFromEdit = request.getParameter("empCodeFromEdit");
		empCodeToEdit = request.getParameter("empCodeToEdit");
		pageEdit = request.getParameter("pageEdit");
	}else if( flagQY != null && flagQY.equals("DT2") ){
		orgFromEdit = request.getParameter("orgFromEdit2");
		orgToEdit = request.getParameter("orgToEdit2");
		empCodeFromEdit = request.getParameter("empCodeFromEdit2");
		empCodeToEdit = request.getParameter("empCodeToEdit2");
		pageEdit = request.getParameter("pageEdit2");
	}
	
	System.out.println("orgFromEdit : " + orgFromEdit);
	System.out.println("orgToEdit : " + orgToEdit);
	System.out.println("empCodeFromEdit : " + empCodeFromEdit);
	System.out.println("empCodeToEdit : " + empCodeToEdit);
	
	if( pageEdit.trim().equals("") )
		pageEdit = "-1";
		
	System.out.println("pageEdit : " + pageEdit);
		
	String empCode = (String)request.getParameter("empCodeQuery");
%>
<html>
	<head>
		<title>�ͺ��������Ż�Ш���͹�ͧ��ѡ�ҹ/�١��ҧ (��������ѡ)</title>
		<!-- Include -->
		<script type="text/javascript" src="dwr/engine.js"></script>
		<script type="text/javascript" src="dwr/util.js"></script>
		<!-- Javascript Script File -->
		<SCRIPT type="text/javascript" src="dwr/interface/PrPeriodLineService.js"></SCRIPT>
		<SCRIPT type="text/javascript" src="dwr/interface/PnEmployeeService.js"></SCRIPT>
		<SCRIPT type="text/javascript" src="dwr/interface/PrEmployeeService.js"></SCRIPT>
		<script type="text/javascript" src="script/gridScript.js"></script>
		<script type="text/javascript" src="page/NavigatePage.jsp"></script>
		<script type="text/javascript" src="script/dojo.js"></script>
		<script type="text/javascript" src="script/json.js"></script>
		<script type="text/javascript" src="script/payroll_util.js"></script>

		<script type="text/javascript">
			//Load Dojo's code relating to widget managing functions
			dojo.require("dojo.widget.*");
			dojo.require("dojo.widget.Menu2");
			dojo.require("dojo.widget.Button");
			dojo.require("dojo.widget.ComboBox");
			dojo.require("dojo.widget.DropDownButton");
			dojo.require("dojo.widget.SortableTable");
			dojo.require("dojo.widget.ContentPane");
			dojo.require("dojo.widget.LayoutContainer");
			dojo.require("dojo.widget.SortableTable");
			dojo.require("dojo.widget.Toolbar");
			dojo.require("dojo.widget.html.*");
			dojo.require("dojo.widget.Menu2");
			dojo.hostenv.writeIncludes();
			
			//Event
			dojo.require("dojo.event.*");
		</script>
		<script type="text/javascript">
			
		    function onLoadYearSectionCallback()
			{
				$("year").value =   "<c:out value='${DefaultYearAndSection.year}' /> "  ;
				$("section").value = "<c:out value='${DefaultYearAndSection.section}' /> "  ;//data.section;
				$("period").value = ""+"<c:out value='${DefaultYearAndSection.period}' /> "  ;//data.period;
				$("empCode").value = '<%=empCode%>';
				
				whenEmpBlur();
			}

			function whenEmpBlur()
			{
		    	if( '<%=empCode%>' != '' ){
			    	DWRUtil.useLoadingMessage("Loading ...");
			    	PnEmployeeService.findPnEmpDetailInSecue('<%=userId%>', '<%=ouCode%>', '<%=empCode%>', $("year").value, $("period").value,{callback:whenPnEmployeeDeatilCallback});			    	
		    	}
		    }
		    
		    function whenPnEmployeeDeatilCallback(data)
		    {
		    	$("empName").value = checkNull(data.prefixName,'STRING') + ' ' + checkNull(data.firstName,'STRING') + ' ' + checkNull(data.lastName,'STRING');
		    	$("account").value = checkNull(data.account,'STRING');
		    	$("positionName").value = checkNull(data.position,'STRING');
		    	$("adminDesc").value = checkNull(data.adminDesc,'STRING');
		    	$("levelCode").value = checkNull(data.levelCode,'STRING');
		    	$("pDate").value = checkNull(data.PDate,'STRING');
		    	$("codeSeqName").value = checkNull(data.orgDesc,'STRING');
		    	$("orgCode").value = checkNull(data.orgCode,'STRING');
		    	$("orgDesc").value = checkNull(data.orgDesc,'STRING');
		    	
		    	PrEmployeeService.findPrEmployee('<%=ouCode%>', $("year").value, $("period").value, '<%=empCode%>', '<%=userId%>',{callback:whenPrEmployeeBlurCallback});
		    }
		    
		    function whenPrEmployeeBlurCallback(data)
		    {
		    	var empName = $("empName").value.replace(' ','space');
		    	if( empName != 'space' )
		    	{
			    	// �Ţ��Шӵ�Ǽ����������
			    	$("taxId").value = checkNull(data.taxId,'STRING');
			    	$("bankId").value = checkNull(data.bankId,'STRING');
			    	
			    	// ʶҹС���Ѻ�Թ��͹
	 		    	var payStatus = document.forms["searchForm"].elements["payStatus"];
	 		    	if( data.payStatus == '1' ){
	 		    		payStatus[0].checked = true;
	 		    	}else if( data.payStatus == '2' ){
	 		    		payStatus[1].checked = true;
	 		    	}else{
	 		    		payStatus[0].checked = true;
	 		    	}
	 		    	
	 		    	// ʶҹС�è����Թ��͹
	 		    	var flagPr = document.getElementById("flagPr");
	 		    	if( data.flagPr == '0' ){
	 		    		$("flagPr").value = '����';
	 		    	}else if( data.flagPr == '1' ){
	 		    		$("flagPr").value = '�ЧѺ��è��·�����';
	 		    	}else if( data.flagPr == '2' ){
	 		    		$("flagPr").value = '�������੾���Թ��';
	 		    	}
									
	 		    	// ʶҹ��ҾŴ���͹
	 		    	var marriedStatus = document.getElementById("marriedStatus");
	 		    	if( data.marriedStatus == '1' ){
	 		    		$("marriedStatus").value = '�ʴ';
	 		    	}else if( data.marriedStatus == '2' ){
	 		    		$("marriedStatus").value = '�������������Թ��';
	 		    	}else if( data.marriedStatus == '3' ){
	 		    		$("marriedStatus").value = '����������Թ��';
	 		    	}else if( data.marriedStatus == '4' ){
	 		    		$("marriedStatus").value = '�����-����-�ʴ�պصúح����';
	 		    	}
			    	
			    	$("costChild").value = checkNull(data.costChild,'STRING');
			    	$("childStudy").value = checkNull(data.childStudy,'STRING');
			    	$("childNoStudy").value = checkNull(data.childNoStudy,'STRING');
			    	
			    	var gundanFlag = document.getElementById("gundanFlag");
		    		if( data.gundanFlag == 'Y' )
		    			gundanFlag.checked = true;
		    		else
		    			gundanFlag.checked = false;
			    	
			    	var flagFather = document.getElementById("flagFather");
			    	if( data.flagFather == 'Y' ){
			    		flagFather.checked = true;
			    	}else{
			    		flagFather.checked = false;
			    	}
			    		
			    	var flagFatherSpouse = document.getElementById("flagFatherSpouse");
			    	if( data.flagFatherSpouse == 'Y' ){
			    		flagFatherSpouse.checked = true;
			    	}else{
			    		flagFatherSpouse.checked = false;
			    	}
			    	
			    	var flagMother = document.getElementById("flagMother");
			    	if( data.flagMother == 'Y' ){
			    		flagMother.checked = true;
			    	}else{
			    		flagMother.checked = false;
			    	}
			    	
			    	var flagMotherSpouse = document.getElementById("flagMotherSpouse");
			    	if( data.flagMotherSpouse == 'Y' ){
			    		flagMotherSpouse.checked = true;
			    	}else{
			    		flagMotherSpouse.checked = false;
			    	}
			    	
			    	$("costLife").value = checkNull(data.costLife,'STRING');
			    	$("debtLife").value = checkNull(data.debtLife,'STRING');
			    	$("debtLoan").value = checkNull(data.debtLoan,'STRING');
			    	$("donate").value = checkNull(data.donate,'STRING');
			    	$("incomeTax").value = checkNull(data.incomeTax,'STRING');
			    	$("pensionFund").value = checkNull(data.pensionFund,'STRING');
			    	$("rmf").value = checkNull(data.rmf,'STRING');
			    	$("ltf").value = checkNull(data.ltf,'STRING');
			    	$("teacherFund").value = checkNull(data.teacherFund,'STRING');
			    	$("compensateLabour").value = checkNull(data.compensateLabour,'STRING');
			    	$("overage").value = checkNull(data.overage,'STRING');
			    	$("overageSpouse").value = checkNull(data.overageSpouse,'STRING');
			    }
		    }
		    
		    function gotoCTRWQY001DT02Page()
		    { 
				var frm=document.forms["editForm"];
				DWRUtil.setValue("empCodeQuery",'<%=empCode%>');
				
				document.getElementById("orgFromEdit2").value = '<%= orgFromEdit %>';
				document.getElementById("orgToEdit2").value = '<%= orgToEdit %>';
				document.getElementById("empCodeFromEdit2").value = '<%= empCodeFromEdit %>';
				document.getElementById("empCodeToEdit2").value = '<%= empCodeToEdit %>';
				document.getElementById("pageEdit2").value = '<%= Integer.parseInt(pageEdit) - 1 %>';
				
				frm.submit();
			}
		    
		    function gotoQYPage()
		    {
		    	document.getElementById("orgFromEdit").value = '<%= orgFromEdit %>';
				document.getElementById("orgToEdit").value = '<%= orgToEdit %>';
				document.getElementById("empCodeFromEdit").value = '<%= empCodeFromEdit %>';
				document.getElementById("empCodeToEdit").value = '<%= empCodeToEdit %>';
				document.getElementById("pageEdit").value = '<%= Integer.parseInt(pageEdit) - 1 %>';
		    
		    	document.forms["goQY"].submit();
		    }
			
		    dojo.addOnLoad(onLoadYearSectionCallback);
		
		</script>

	</head>
	<body>
		<table width="100%">
			<tr>
				<td class="font-head">
					[ CTRWQY001DT01 ] �ͺ��������Ż�Ш���͹�ͧ��ѡ�ҹ/�١��ҧ��Ш� (��������ѡ)
				</td>
			</tr>
		</table>
		<form name="searchForm" action="" method="post">
			<!-- Begin Declare Paging -->
			<!-- End Declare Paging -->
		<table width="100%" align="center">
		<tr><td align="center">	
			<TABLE border="0" align="left" cellspacing="1">
				<TR>
					<TD width="354px" class="font-field" align="right">��Шӻ�&nbsp;</TD>
					<TD align="left"><INPUT type="text" name="year" value="" readonly="readonly" style="width: 70px;text-align: center;background-color:silver;"></TD>
				    <TD width="46px" class="font-field" align="right">�Ǵ&nbsp;</TD>
				    <TD align="left">
				    	<INPUT type="text" name="section" value="" readonly="readonly" style="width: 70px;text-align: center;background-color:silver;">
				    	<INPUT type="hidden" name="period" value="">
				    </TD>
				</TR>
			</TABLE>
		</td></tr>
		<tr><td align="center">
			<TABLE border="0" align="center" width="920px" cellspacing="1">
				<TR>
					<TD class="font-field" style="text-align:right;" width="120px">�Ţ��Шӵ��</TD>
					<TD width="130px"><INPUT type="text" id="empCode" readonly="readonly" style="width:130px;text-align: left;background-color:silver;" /></TD>
					<TD colspan="4" width="470px"><INPUT type="text" id="empName" readonly="readonly" style="width:100%;text-align: left;background-color:silver;" /></TD>
					<TD class="font-field" style="text-align:right;" >�ѵ���Ţ���</TD>
					<TD width="100px"><INPUT type="text" id="account" readonly="readonly" style="width:100px;text-align: left;background-color:silver;" /></TD>
				</TR>
				<TR>
					<TD class="font-field" style="text-align:right;" width="120px">���˹�</TD>
					<TD width="130px"><INPUT type="text" id="positionName" readonly="readonly" style="width:130px;text-align: left;background-color:silver;" /></TD>
					<TD colspan="4" width="470px">
						<TABLE width="100%" border="0" cellspacing="0">
						<TR>
							<TD class="font-field" style="width:65px;text-align:right;">�дѺ&nbsp;</TD>
							<TD width="70px"><INPUT type="text" id="levelCode" readonly="readonly" style="width: 70px;text-align: left;background-color:silver;" /></TD>
							<TD class="font-field" width="49px" style="text-align:right;">�ҹ�&nbsp;</TD>
							<TD width="230px"><INPUT type="text" id="adminDesc"  readonly="readonly" style="width:100%;text-align: left;background-color:silver;"/></TD>
						</TR>
						</TABLE>
					</TD>
					<TD class="font-field" style="text-align:right;">�ѹ����è�</TD>
					<TD><INPUT type="text" id="pDate" readonly="readonly" style="width: 100px;text-align: left;background-color:silver;" /></TD>
				</TR>
				<TR>
					<TD class="font-field" style="text-align:right;" width="120px">�ѧ�Ѵ�觵��</TD>
					<TD colspan="7" width="800px">
						<INPUT type="text" id="codeSeqName" readonly="readonly" style="width:100%;text-align: left;background-color:silver;" />
					</TD>
				</TR>
				<TR>
					<TD class="font-field" style="text-align:right;" width="120px">�ѧ�Ѵ��Ժѵԧҹ</TD>
					<TD width="130px">
						<INPUT type="text" id="orgCode" readonly="readonly" style="width:130px;text-align: left;background-color:silver;" />
					</TD>
					<TD colspan="6" width="670px"><INPUT type="text" id="orgDesc" readonly="readonly" style="width:100%;text-align: left;background-color:silver;" /></TD>
				</TR>
				<TR>
					<TD class="font-field" style="text-align:right;" width="120px">�Ţ��Шӵ�Ǽ����������</TD>
					<TD width="130px">
						<INPUT type="text" id="taxId" readonly="readonly" style="width:130px;text-align: left;background-color:silver;" />
					</TD>
					<TD colspan="6" width="670px">
						<TABLE width="100%" border="0" cellspacing="0">
						<TR>
							<TD class="font-field" style="text-align:right;" width="185px">ʶҹС�è����Թ��͹</TD>
							<TD >
								<INPUT type="text" id="flagPr" readonly="readonly" style="width:153px;text-align: left;background-color:silver;" />
							</TD>
							<TD class="font-field" style="text-align:right;width:165px">ʶҹС��Ŵ���͹</TD>
							<TD >
								<INPUT type="text" id="marriedStatus" readonly="readonly" style="width:153px;text-align: left;background-color:silver;" />
							</TD>
						</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD class="font-field" style="text-align:right;" width="120px">ʶҹС���Ѻ�Թ�Դ�͹ </TD>
					<TD><INPUT type="radio" value="1" name="payStatus" disabled="disabled"/><FONT class="font-field">���¼�ҹ��Ҥ��</FONT></TD>
					<TD><INPUT type="radio" value="2" name="payStatus" disabled="disabled"/><FONT class="font-field">�Թʴ</FONT></TD>
					<TD colspan="3"></TD>
					<TD class="font-field" style="text-align:right;">�Ţ���ѭ�ո�Ҥ��</TD>
					<TD width="155px">
						<input type="text" id="bankId" readonly="readonly" style="width:153px;text-align: left;background-color:silver;" />
					</TD>
				</TR>
				<TR>
					<TD class="font-field" style="text-align:right;">�Թ�ѧ�վ</TD>
					<TD width="130px">
						<INPUT id="costLife" type="text" maxlength="9" readonly="readonly" style="width:130px;text-align:right;background-color:silver;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);" >
						<INPUT type="hidden" id="hCostLife">
					</TD>
					<TD class="font-field" style="text-align:left;">&nbsp;&nbsp;&nbsp;�ӹǹ�ص÷�����Ѻ��ê�������� 
						<INPUT type="text" name="costChild" maxlength="5" readonly="readonly" style="width:123px;text-align:right;background-color:silver;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);" />
						<INPUT type="hidden" id="hCostChild">
					</TD>
					<TD colspan="3"></TD>
					<TD width="140px" class="font-field" style="text-align:right;">���¡ѹ���</TD>
					<TD width="154px">
						<input type="checkbox" disabled="disabled" name="gundanFlag" />
						<INPUT type="hidden" id="hGundanFlag">
					</TD>
				</TR>
			</TABLE>
		</td></tr>
		</TABLE>	
			<BR/>
			<TABLE border="0" align="center" width="950px" cellspacing="1">
				<TR>
					<TD colspan="8">
						<FIELDSET><LEGEND class="font-field">���������´�ӹǳ����</LEGEND>
						<TABLE border="0" width="100%">
							<TR><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD></TR>
							<TR>
								<TD class="font-field" style="text-align:right;">�ص÷���֡��</TD>
								<TD>
									<INPUT id="childStudy" type="text" readonly="readonly" style="width:100px;text-align: right;background-color:silver;" >
								</TD>
								<TD class="font-field" style="text-align: right;" width="200px">�ص÷������֡��</TD>
								<TD colspan="3">
									<INPUT id="childNoStudy" type="text" readonly="readonly" style="width:100px;text-align: right;background-color:silver;" >
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="left">
									<INPUT id="flagFather" type="checkbox" disabled="disabled"><FONT class="font-field">�ѡŴ���͹�Դ�</FONT>
								</TD>
								<TD colspan="4" align="left">
									<INPUT id="flagFatherSpouse" type="checkbox" disabled="disabled"><FONT class="font-field">�ѡŴ���͹�ԴҢͧ������ʷ�����Թ�� ����ӹǳ��������������Թ��</FONT>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="left">
									<INPUT id="flagMother" type="checkbox" disabled="disabled"><FONT class="font-field">�ѡŴ���͹��ô�</FONT>
								</TD>
								<TD colspan="4" align="left">
									<INPUT id="flagMotherSpouse" type="checkbox" disabled="disabled"><FONT class="font-field">�ѡŴ���͹��ôҢͧ������ʷ�����Թ�� ����ӹǳ��������������Թ��</FONT>
								</TD>
							</TR>
							<TR>
								<TD style="text-align: right;" class="font-field">&nbsp;</TD>
								<TD>
									<!-- <INPUT id="costLife" type="text" readonly="readonly" style="width:100px;text-align: right;background-color:silver;" >-->
									&nbsp;
								</TD>
								<TD style="text-align: right;" class="font-field" width="200px">���»�Сѹ���Ե</TD>
								<TD>
									<INPUT id="debtLife" type="text" readonly="readonly" style="width:100px;text-align: right;background-color:silver;" >
								</TD>
								<TD style="text-align: right;" class="font-field">�͡�����Թ������͡������</TD>
								<TD>
									<INPUT id="debtLoan" type="text" readonly="readonly" style="width:100px;text-align: right;background-color:silver;" >
								</TD>
							</TR>
							<TR>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<TD style="text-align: right;" class="font-field">�Թ��ԨҤ</TD>
								<TD>
									<INPUT id="donate" type="text" readonly="readonly" style="width:100px;text-align: right;background-color:silver;" >
								</TD>
								
								<TD style="text-align: right;" class="font-field">�Թ����� � ����ͧ��������</TD>
								<TD>
									<INPUT id="incomeTax" type="text" readonly="readonly" style="width:100px;text-align: right;background-color:silver;" >
								</TD>
							</TR>
							<TR>
								<TD style="text-align:right;" class="font-field" width="410px" colspan="3" align="right">��ҫ���˹���ŧ�ع㹡ͧ�ع������͡������§�վ (RMF)</TD>
								<TD>
									<INPUT id="rmf" type="text" readonly="readonly" style="width:100px;text-align: right;background-color:silver;" >
								</TD>
								<TD style="text-align:right;" class="font-field">��ҫ���˹���ŧ�ع㹡ͧ�ع������������� (LTF)</TD>
								<TD>
									<INPUT id="ltf" type="text" readonly="readonly" style="width:100px;text-align: right;background-color:silver;" >
								</TD>
							</TR>
						</TABLE>
						<hr/>
						<TABLE border="0" width="100%">
							<TR>
								<TD style="text-align: right; width: 200px;" class="font-field">��� � 1</TD>
								<TD>
									<INPUT id="teacherFund" type="text" maxlength="9" readonly="readonly" style="width:100px;text-align:right;background-color:silver;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hTeacherFund">
								</TD>
								<TD style="text-align: right;" class="font-field">��� � 2</TD>
								<TD>
									<INPUT id="compensateLabour" type="text" maxlength="9" readonly="readonly" style="width:100px;text-align:right;background-color:silver;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hCompensateLabour">
								</TD>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</TR>
							<TR>
								<TD style="text-align: right; width: 200px;" class="font-field">��� � 3</TD>
								<TD>
									<INPUT id="overage" type="text" maxlength="9" readonly="readonly" style="width:100px;text-align:right;background-color:silver;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hOverage">
								</TD>
								<TD style="text-align: right;" class="font-field">��� � 4</TD>
								<TD>
									<INPUT id="overageSpouse" type="text" maxlength="9" readonly="readonly" style="width:100px;text-align:right;background-color:silver;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hOverageSpouse">
								</TD>
								<TD style="text-align:right;" class="font-field" >��� � 5</TD>
								<TD>
									<INPUT id="pensionFund" type="text" readonly="readonly" style="width:100px;text-align: right;background-color:silver;" >
								</TD>
							</TR>
						</TABLE>
						</FIELDSET>
					</TD>
				</TR>
			</TABLE>
		<br><br>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" class=" button "   value="Pay Slip" name="paySlip" onclick="gotoCTRWQY001DT02Page();"
         disabled="disabled" />
		&nbsp;&nbsp;
		<input type="button" class=" button "  value="˹���á" name="gotoQyPage" onclick="gotoQYPage();"/>
		<br><br>				
	</form>
	<FORM name="goQY" action="security.htm?reqCode=CTRWQY001" method="post">
		<input type="hidden" name="orgFromEdit" />
		<input type="hidden" name="orgToEdit" />
		<input type="hidden" name="empCodeFromEdit" />
		<input type="hidden" name="empCodeToEdit" />
		<input type="hidden" name="pageEdit" />
		
	</FORM>
	<form name="editForm" action="security.htm?reqCode=CTRWQY001DT02" method="post">
		<input type="hidden" name="empCodeQuery" />
		
		<input type="hidden" name="orgFromEdit2" />
		<input type="hidden" name="orgToEdit2" />
		<input type="hidden" name="empCodeFromEdit2" />
		<input type="hidden" name="empCodeToEdit2" />
		<input type="hidden" name="pageEdit2" />
		
	</form>
	</body>
</html>
