<%@ page language="java" contentType="text/html;charset=TIS-620"%>
<%@ page import="com.ss.tp.security.UserInfo" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>

<%
	UserInfo userInfo = (UserInfo)session.getAttribute("UserLogin");
	String userId = userInfo.getUserId();
	String ouCode = userInfo.getOuCode();
	Calendar now = Calendar.getInstance(Locale.US);	
	String year =String.valueOf(now.get(Calendar.YEAR)+543);
%>
<html>
<head>
<title>View Personal Evaluation</title>
<!-- You have to include these two JavaScript files from DWR -->
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript" src="dwr/util.js"></script>
<!-- This JavaScript file is generated specifically for your application -->
<SCRIPT type="text/javascript" src="dwr/interface/SuUserOrganizationService.js"></SCRIPT>
<SCRIPT type="text/javascript" src="dwr/interface/WgEmployeeService.js"></SCRIPT>
<script type="text/javascript" src="dwr/interface/VPeWgEmployeeScoreService.js"></script>
<script type="text/javascript" src="script/payroll_util.js"></script>

<script type="text/javascript" src="page/NavigatePage.jsp"></script>
<script type="text/javascript" src="script/dojo.js"></script>

<script type="text/javascript" src="script/chkInputData.js"></script>

<script type="text/javascript">
	//Load Dojo's code relating to widget managing functions
	dojo.require("dojo.widget.*");
	dojo.require("dojo.widget.Menu2");
	dojo.require("dojo.widget.Button");
	dojo.require("dojo.widget.ComboBox");
	dojo.require("dojo.widget.DropDownButton");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.ContentPane");
	dojo.require("dojo.widget.LayoutContainer");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.Toolbar");
	dojo.require("dojo.widget.html.*");
	dojo.require("dojo.widget.Menu2");
	dojo.hostenv.writeIncludes();
	
	//Event
	dojo.require("dojo.event.*");
	var orgName=[] ;
	var codeSeqData =[];
	var empCodeDataFrom=[];
	var empCodeDataTo=[];
</script>

<script type="text/javascript">
	//=============== DropDown Menu ==============
			 function onLoadOrganization(){
				var cboOrgFrom = dojo.widget.byId("orgFromCbo");
				var cboOrgTo = dojo.widget.byId("orgToCbo");
				var cboSource = [];
				<c:forEach items="${OrganizationInSecurity}" var="result" >		 
					cboSource.push(["<c:out value='${result.orgCode}' />"+" "+"<c:out value='${result.orgShowDesc}' />","<c:out value='${result.orgCode}' />"]);
					orgName.push(["<c:out value='${result.orgCode}' />"+" "+"<c:out value='${result.orgShowDesc}' />"]);
					codeSeqData.push(["<c:out value='${result.codeSeq}' />"]);				
				</c:forEach>
				cboOrgFrom.dataProvider.setData(cboSource);
				cboOrgTo.dataProvider.setData(cboSource);
		     }

		     // Even ComboBox valueChange
		     function onLoadEmployee(){

				var frm=document.forms[0];
				var comB1=dojo.widget.byId("empFromCbo");
				var comB2=dojo.widget.byId("empToCbo");
				var arrData=[]
					<c:forEach items="${WgEmployeeInSecurity}" var="result" >		 
						arrData.push(["<c:out value='${result}' />","<c:out value='${result}' />"]);
						empCodeDataFrom.push(["<c:out value='${result}' />"]);
					</c:forEach>
				comB1.dataProvider.setData(arrData);
				comB2.dataProvider.setData(arrData);		     
		     } 
			// Load page
		  	dojo.addOnLoad(onLoadOrganization);
		  	dojo.addOnLoad(onLoadEmployee);	
			
</script>

<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
</head>
<body>
<table width="100%" >
<tr>
<td class="font-head">[ CTPEQY102 ] �ͺ��������Ż���ѵԤ�ṹ�����Թ�Ţͧ�١��ҧ</td>
</tr>
</table>
<form method="post" name="mainform" >
	<div style="height:400px;">
<table  width="100%" align="center" border="0">
<tr>
	<td aling ="center"> <input type="hidden" name="ouCode" value="<%=ouCode%>">
		<input type="hidden" name="userId" value="<%=userId%>">
 		<table align="center" border="0" cellpadding="2" cellspacing="2">
			
			<tr>
			 	  <td class="font-field"  align="right">������Шӻ�</td>
          		  <td colspan="1"><input type="text" name="evaYear" size="4" maxlength="4" style="text-align: center;" onKeyPress="return chkInputNum()"  /></td>
          		  <td class="font-field"  align="right">�֧��Шӻ�</td>
          		  <td colspan="1"><input type="text" name="evaYearTo" size="4" maxlength="4" style="text-align: center;" onKeyPress="return chkInputNum()" /></td>
		  </tr>
		  <tr>
			  <td class="font-field"  align="right">������ѧ�Ѵ��Ժѵԧҹ</td>
			  <td colspan="3">
			  <select  dojotype="ComboBox"  widgetid="orgFromCbo" style="width:527"></select>
              </td>
			  <td class="font-field">&nbsp;</td>
		  </tr>
		  <tr>
			  <td class="font-field"  align="right">�֧�ѧ�Ѵ��Ժѵԧҹ</td>
			  <td colspan="3">
			  <select  dojotype="ComboBox"  widgetid="orgToCbo" style="width:527"></select>
              </td>
			  <td class="font-field">&nbsp;</td>
		  </tr>
			<tr>
				<td class="font-field"  align="right">������Ţ��Шӵ��</td>
			  <td>
					<SELECT  dojoType="ComboBox" widgetId="empFromCbo" style="width:200"></SELECT>
			  </td>
				<td class="font-field"  align="center">�֧�Ţ��Шӵ��</td>
			  <td>
					<SELECT  dojoType="ComboBox" widgetId="empToCbo" style="width:200"></SELECT>
			  </td>
				<td class="font-field"><input type="button" class=" button "  value="����" onclick="return whenClickSearch();" /></td>
			</tr>
 		</table>
	</td>
</tr>
<tr>
	<td><div style="height:400px;">

<table  width="600"  align="center" border="1" bordercolor="#6699CC"  cellpadding="2" cellspacing="0">
	<thead>
  		<tr CLASS="TABLEBULE2" >
  		  <th rowspan="2" CLASS="TABLEBULE2" >��ѡ�ҹ</th>
    		<th rowspan="2" CLASS="TABLEBULE2" >�շ������Թ</th>
    		<th rowspan="2" CLASS="TABLEBULE2" >��ṹ�����</th>
  		</tr>
	</thead>
	<tbody id="vPeWgEmployeeScore" align="center">
					
	</tbody>
</table>
</div>	
	</td>
</tr>
<tr>
	<td>
		<table width="100%" CLASS="TABLEBULE2" >
			<tr CLASS="TABLEBULE2" >
				<td align="left" >&nbsp;</td>
			</tr>
		</table
	></td>
</tr>
</table>
</form>
</body>
<script>
	var year = new Date().getYear()+543;
	//document.forms[0].evaYear.value=year;

	function whenClickSearch(){
		var frm = document.forms[0];
		var evaYear = DWRUtil.getValue("evaYear");
		var evaYearTo = DWRUtil.getValue("evaYearTo");
		DWRUtil.useLoadingMessage("Loading ...");
		var empFrom=dojo.widget.byId("empFromCbo").textInputNode.value;
		var empTo=dojo.widget.byId("empToCbo").textInputNode.value;
		var orgFrom = splitCombo(dojo.widget.byId("orgFromCbo").textInputNode.value);
		var orgTo = splitCombo(dojo.widget.byId("orgToCbo").textInputNode.value);
		var userId = DWRUtil.getValue("userId");
		var ouCode = DWRUtil.getValue("ouCode");
		
		if (evaYear == '')	{
			evaYear = '0';
		}
		if(evaYearTo == ''){
			evaYearTo ='9999';
		} 
		/* if (evaYear == "")	{
			alert ("��سҡ�͡��");
			return false;
		}else if (evaYear.length != 4)	{
			alert("�վ.�.��ͧ�� 4 ��ѡ");
			return false;	
		}else */ 
		if (orgTo != '' && orgFrom == ''){
			alert("��سҡ�͡�ѧ�Ѵ��Ժѵ�ҹ�������");
			return false;
		}else if(empFrom != "" && !isNumber(empFrom)){
			alert("�Ţ��Шӵ�ǵ�ͧ�繵���Ţ");
			return false;
		}else if(empTo != "" && !isNumber(empTo)){
			alert("�Ţ��Шӵ�ǵ�ͧ�繵���Ţ");
			return false;
		}else if(empTo != "" && empFrom == ""){
			alert("��سҡ�͡ �Ţ��Шӵ��������� ");
			return false;
		}		
		VPeWgEmployeeScoreService.findVPeWgEmployeeScore(userId, ouCode, evaYear,evaYearTo, orgFrom, orgTo, empFrom, empTo, {callback:onListView});
	}

	var cellFuncs = [	
		function(data) { return "<div align='left'>" + data.empCode + " " + data.empName + "</div>"; },
		function(data) { var evaYear= data.evaYear;
						if(evaYear==0){	evaYear='&nbsp;';}
						return evaYear; },
		function(data) { return "<div align='right'>"+data.scoreNet+ "</div>"; }
	];

	function onListView(data){
		DWRUtil.removeAllRows("vPeWgEmployeeScore");
		DWRUtil.addRows("vPeWgEmployeeScore", data, cellFuncs);
	}
	
	function isNumber(num){
		var check = true;
		var realNumber = "1234567890";
		var numlength = 0;
		if (num != null)	{
			var numlength = num.length;		
		}
		var temp;
		for(var i=0;i<numlength;i++){
			temp = num.substring(i,i+1);
			if ( realNumber.indexOf(temp) == -1 ){
				check = false;
				break;
			}
		}
		return check;
	}
</script>
</html>