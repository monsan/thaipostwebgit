<%@ page language="java" contentType="text/html;charset=TIS-620" %>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Locale"%>
<%@ page import="com.ss.tp.security.UserInfo" %>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>
<html>
<head>
<title>�٢����Ť�ṹ�����Թ�š�û�Ժѵԧҹ</title>
<!-- Include -->
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript" src="dwr/util.js"></script>
<script type="text/javascript" src="script/dojo.js"></script>
<!-- Javascript Script File -->
<script type="text/javascript" src="dwr/interface/PeEmployeeScoreService.js"></script>
<script type="text/javascript" src="dwr/interface/PeEmployeeScoreDetailService.js"></script>
<script type="text/javascript" src="dwr/interface/SuUserOrganizationService.js"></script>
<script type="text/javascript" src="script/gridScript.js"></script>
<script type="text/javascript" src="page/NavigatePage.jsp"></script>
<script type="text/javascript" src="script/payroll_util.js"></script>
<script type="text/javascript">
	dojo.require("dojo.widget.*");
	dojo.require("dojo.widget.Menu2");
	dojo.require("dojo.widget.Button");
	dojo.require("dojo.widget.ComboBox");
	dojo.require("dojo.widget.DropDownButton");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.ContentPane");
	dojo.require("dojo.widget.LayoutContainer");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.Toolbar");
	dojo.require("dojo.widget.html.*");
	dojo.require("dojo.widget.Menu2");
	dojo.hostenv.writeIncludes();
//Event
	dojo.require("dojo.event.*");
</script>	
<%
	Calendar now = Calendar.getInstance(Locale.US);
	String year = (String)(request.getAttribute("maxEvaYear")+"");
	String time = (String)request.getAttribute("maxEvaTime");
	UserInfo uf =  (UserInfo)request.getSession().getAttribute("UserLogin");
	String userId =  uf.getUserId();
	String ouCode =  uf.getOuCode();
	String evaYearTmp = request.getParameter("evaYear")==null?year:request.getParameter("evaYear");
	String evaTimeTmp = request.getParameter("evaTime")==null?time:request.getParameter("evaTime");
	String pageTmp = request.getParameter("pageTmp")==null?"-1":request.getParameter("pageTmp");
	String areaCodeTmp = request.getParameter("areaCodeTmp")==null?"":request.getParameter("areaCodeTmp");
	String secCodeTmp = request.getParameter("secCodeTmp")==null?"":request.getParameter("secCodeTmp");
	String workCodeTmp = request.getParameter("workCodeTmp")==null?"":request.getParameter("workCodeTmp");
	String areaDescTmp = request.getParameter("areaDescTmp")==null?"":request.getParameter("areaDescTmp");
	String secDescTmp = request.getParameter("secDescTmp")==null?"":request.getParameter("secDescTmp");
	String workDescTmp = request.getParameter("workDescTmp")==null?"":request.getParameter("workDescTmp");

%>
<script type="text/javascript">
	var orgDataDesc=[];
	var orgDataValue=[];
	
function whenfindOrganization(){
	var comB1=dojo.widget.byId("areaCode");
	var arrData=[]
	var arrValue=[];
	var arrName=[];

		<c:forEach items="${OrganizationInSecurity31}" var="result" >		 
			arrData.push(["<c:out value='${result.areaCode}' />"+" "+"<c:out value='${result.orgShowDesc}' />","<c:out value='${result.areaCode}' />"]);
			arrValue.push(["<c:out value='${result.areaCode}' />"]);
			arrName.push(["<c:out value='${result.areaCode}' />"+" "+"<c:out value='${result.orgShowDesc}' />"]);
		</c:forEach>
				
		comB1.dataProvider.setData(arrData);
		
		if (DWRUtil.getValue("page") >= 0){
			var areaCode = DWRUtil.getValue("areaCodeTmp");
			var areaLabel = DWRUtil.getValue("areaDescTmp");
			var secCode = DWRUtil.getValue("secCodeTmp");
			var secLabel = DWRUtil.getValue("secDescTmp");
			var workCode = DWRUtil.getValue("workCodeTmp");
			var workLabel = DWRUtil.getValue("workDescTmp");
			if (areaCode != ''){ 
				dojo.widget.byId("areaCode").comboBoxSelectionValue.value = areaCode;
				dojo.widget.byId("areaCode").textInputNode.value = areaLabel
				assignSecCode(comB1);
			}
			if(secCode != ''){
				var comB2 = dojo.widget.byId("secCode");
				comB2.comboBoxSelectionValue.value = secCode;
				comB2.textInputNode.value  = secLabel;
				assignWorkCode(comB2);
			}
			if (workCode != ''){
				var comB3 = dojo.widget.byId("workCode");
				comB3.comboBoxSelectionValue.value = workCode;
				comB3.textInputNode.value  = workLabel;
			}
			whenShowDataTable();
		}
	
	}
dojo.addOnLoad(whenfindOrganization);
dojo.addOnLoad(setWork);
	function setSec(){
		var ouCode  = DWRUtil.getValue("ouCode");
		var userId   = DWRUtil.getValue("userId");
		SuUserOrganizationService.findSecByDiv(ouCode, null,userId, {callback:whenshowSecHandler});
	}
	function setWork(){
		var ouCode  = DWRUtil.getValue("ouCode");
		var userId   = DWRUtil.getValue("userId");
		SuUserOrganizationService.findWorkCodeByAreaAndSecCode(ouCode, null, null,userId, {callback:whenShowWorkCodeHandler});
	}
	function countData(){
		var evaYear  = DWRUtil.getValue("evaYear");
		var evaTime  = DWRUtil.getValue("evaTime");
		var areaCode = splitCombo(dojo.widget.byId("areaCode").textInputNode.value);
		var secCode  = splitCombo(dojo.widget.byId("secCode").textInputNode.value);
		var workCode = splitCombo(dojo.widget.byId("workCode").textInputNode.value);
		var userId   = DWRUtil.getValue("userId");
		var ouCode  = DWRUtil.getValue("ouCode");
		
		PeEmployeeScoreService.countData(ouCode,evaYear,evaTime,areaCode,secCode,workCode,userId,{callback:countDataHandler});
	}
	function countDataHandler(data){
		DWRUtil.setValue("countData",data);
		onCheckButt("searchForm");
	    
	}
	
	function whenShowDataTable(){
		var evaYear  = DWRUtil.getValue("evaYear");
		var evaTime  = DWRUtil.getValue("evaTime");
		var areaCode = splitCombo(dojo.widget.byId("areaCode").textInputNode.value);
		var secCode  = splitCombo(dojo.widget.byId("secCode").textInputNode.value);
		var workCode = splitCombo(dojo.widget.byId("workCode").textInputNode.value);
		var userId   = DWRUtil.getValue("userId");
		var maxRowPerPage = DWRUtil.getValue("dataPerPage");
		var page    = DWRUtil.getValue("page");
		var ouCode  = DWRUtil.getValue("ouCode");
		
		/*
		if (orgDesc!=''&&codeSeq==""){DWRUtil.removeAllRows("dataTable");alert('�������ö���Ң����šͧ/�ӹѡ�ҹ�����');}
		else{
		*/
		PeEmployeeScoreService.findByCriteria(ouCode,evaYear,evaTime,areaCode,secCode,workCode,userId,page,maxRowPerPage,{callback:whenListDataTableHandler});
										 
	}
	
	var cellFuncs = [
		function(data) { return "<div align='center'>"+data.empCode+"</div>";},
		function(data) { return data.ename;},
		function(data) { return "<div align='right'>"+data.formScore+"</div>";},
		function(data) { return "<div align='right'>"+data.evaTotal+"</div>";},
		function(data) { return writeButton("edit",data.empCode);}
	];
	
	function whenListDataTableHandler(data){
		var evaYear  = DWRUtil.getValue("evaYear");
		var evaTime  = DWRUtil.getValue("evaTime");
		var areaCode = splitCombo(dojo.widget.byId("areaCode").textInputNode.value);
		var secCode  = splitCombo(dojo.widget.byId("secCode").textInputNode.value);
		var workCode = splitCombo(dojo.widget.byId("workCode").textInputNode.value);
		
		DWRUtil.removeAllRows("dataTable");
		DWRUtil.addRows("dataTable",data,cellFuncs);
		
		DWRUtil.setValue("evaYearEdit",DWRUtil.getValue("evaYear"));
		DWRUtil.setValue("evaTimeEdit",DWRUtil.getValue("evaTime"));
		DWRUtil.setValue("areaCodeEdit",areaCode);
		DWRUtil.setValue("secCodeEdit",secCode);
		DWRUtil.setValue("workCodeEdit",workCode);
		DWRUtil.setValue("areaDescEdit",dojo.widget.byId("areaCode").textInputNode.value);
		DWRUtil.setValue("secDescEdit",dojo.widget.byId("secCode").textInputNode.value);
		DWRUtil.setValue("workDescEdit",dojo.widget.byId("workCode").textInputNode.value);
		if (DWRUtil.getValue("showMaxPage")==""){countData();}
		else{onCheckButt("searchForm");}
	}	
	
	
	function writeButton(inname,emp){
		return "<div align='center'><input type='button' class='button' name = '"+inname+"' value='...' onclick='preEdit(this.empId);' empId='"+emp+"' /></div>";
	}
	
	function preEdit(empId){ 
	var frm=document.forms["editForm"];
	DWRUtil.setValue("pageEdit",DWRUtil.getValue("page"));
	DWRUtil.setValue("empCodeEdit",empId);
	frm.submit();
	}
	
	function checkNAN(value){
		if(isNaN(Number(value))) {
			value = value.substring(0,value.length - 1);
		DWRUtil.setValue("evaYear",value);
		}
		
	}

	function assignSecCode(comBo){
		var ouCode = DWRUtil.getValue("ouCode");
		var areaCode = splitCombo(comBo.textInputNode.value);
		var userId   = DWRUtil.getValue("userId");
	//	dojo.widget.byId("secCode").textInputNode.value = "";
	//	dojo.widget.byId("secCode").comboBoxSelectionValue.value = "";
	//	dojo.widget.byId("workCode").textInputNode.value = "";
	//	dojo.widget.byId("workCode").comboBoxSelectionValue.value = "";
		SuUserOrganizationService.findSecByDiv(ouCode, areaCode,userId, {callback:whenshowSecHandler});
	}
	function whenshowSecHandler(data){
		var frm=document.forms[0];
		var comB2=dojo.widget.byId("secCode");
		var arrSecCode = [];
		if (data != ''){
			for(i=0;i<data.length;i++){
				arrSecCode.push([data[i].secCode + " " + data[i].secDesc, data[i].secCode]);
			}	
			comB2.dataProvider.setData(arrSecCode);
		}else{
			var arrTmp = [];
			dojo.widget.byId("secCode").textInputNode.value = "";
			dojo.widget.byId("secCode").comboBoxSelectionValue.value = "";			
			dojo.widget.byId("secCode").dataProvider.setData(arrTmp);
			dojo.widget.byId("workCode").textInputNode.value = "";
			dojo.widget.byId("workCode").comboBoxSelectionValue.value = "";			
			dojo.widget.byId("workCode").dataProvider.setData(arrTmp);
		}
		assignWorkCode(comB2);
	}
	function assignWorkCode(comBo){
			var ouCode = DWRUtil.getValue("ouCode");
			var areaCode = splitCombo(dojo.widget.byId("areaCode").textInputNode.value);
			var secCode = splitCombo(dojo.widget.byId("secCode").textInputNode.value);
			var userId   = DWRUtil.getValue("userId");
		//	dojo.widget.byId("workCode").textInputNode.value = "";
		//	dojo.widget.byId("workCode").comboBoxSelectionValue.value = "";
			SuUserOrganizationService.findWorkCodeByAreaAndSecCode(ouCode, areaCode, secCode,userId, {callback:whenShowWorkCodeHandler});
	
	}
	function whenShowWorkCodeHandler(data){
		var frm = document.forms[0];
		var comB2 = dojo.widget.byId("workCode");
		var arrWorkCode = [];
		if (data != ''){
			for(i=0;i<data.length;i++){
				arrWorkCode.push([data[i].workCode+" "+data[i].workDesc, data[i].workCode]);
			}	
			comB2.dataProvider.setData(arrWorkCode);
		}else{
			var arrTmp = [];
			dojo.widget.byId("workCode").textInputNode.value = "";
			dojo.widget.byId("workCode").comboBoxSelectionValue.value = "";			
			dojo.widget.byId("workCode").dataProvider.setData(arrTmp);
		}
	}
</script>
</head>
<body>
<table width="100%" >
<tr>
<td class="font-head">[ CTPEQY001 ]  �ͺ��������Ť�ṹ�����Թ�š�û�Ժѵԧҹ�ͧ��ѡ�ҹ/�١��ҧ��Ш�</td>
</tr>
<tr>
<td>
<form name="searchForm" action="" method="post">
<table width="800" border="0" align="center" cellspacing="1" >
<input type="hidden" name="userId" value="<%=userId %>"/>
<input type="hidden" name="ouCode" value="<%=ouCode %>"/>
<input type="hidden" name="areaCodeTmp" value="<%=areaCodeTmp%>"/>
<input type="hidden" name="secCodeTmp" value="<%=secCodeTmp%>"/>
<input type="hidden" name="workCodeTmp" value="<%=workCodeTmp%>"/>
<input type="hidden" name="areaDescTmp" value="<%=areaDescTmp%>"/>
<input type="hidden" name="secDescTmp" value="<%=secDescTmp%>"/>
<input type="hidden" name="workDescTmp" value="<%=workDescTmp%>"/>
  <tr>
    <td width="200" class="font-field" align="right">��Шӻ�&nbsp;</td>
    <td align="left" width="170">
    <input type="text" name="evaYear" maxlength="4" value="<%=evaYearTmp%>" style="width: 70px;text-align: center;" onkeyup="checkNAN(this.value)"/>
    </td>
    
    <td align="left">
    <font class="font-field">���駷��&nbsp;</font>
	    <select name="evaTime" style="width: 100">
		<option value="1" <%if(evaTimeTmp.equals("1")){%>selected="selected"<% } %>>1</option>
		<option value="2" <%if(evaTimeTmp.equals("2")){%>selected="selected"<% } %>>2</option>
		</select>
	</td>
  </tr>
	<tr>
  		<td width="200" class="font-field" align="right">ʾ./��.&nbsp;</td>
  		<td colspan="4">
  			<select widgetId="areaCode" dojoType="ComboBox"  style="width:450;" onBlurInput="assignSecCode(this);" onChange="assignSecCode(this);"></select>
		</td>
  	</tr>
	<tr>
  		<td width="200" class="font-field" align="right">��ǹ/���ӡ��&nbsp;</td>
  		<td colspan="4">
  			<select widgetId="secCode" dojoType="ComboBox"  style="width:450;" onBlurInput="assignWorkCode(this);" onChange="assignWorkCode(this);"></select>
		</td>
  	</tr>
	<tr>
  		<td width="200" class="font-field" align="right">Ἱ�&nbsp;</td>
  		<td colspan="3">
  			<select widgetId="workCode" dojoType="ComboBox"  style="width:450;" ></select>
		</td>
	  	<td align="center"><input type="button" class=" button "  value="����" onclick="onQuery(whenShowDataTable);" /></td>
  	</tr>
</table>
<div style="height:310px;">
<table width="600"  border="1" bordercolor="#6699CC"  align="center"  cellpadding="2" cellspacing="0">
	<thead>
		<tr CLASS="TABLEBULE2">
		<th CLASS="TABLEBULE2" width="100">�Ţ��Шӵ��</th>
		<th CLASS="TABLEBULE2">���� - ���ʡ��</th>
		<th CLASS="TABLEBULE2" width="70">��ṹ���</th>
		<th CLASS="TABLEBULE2" width="70">��ṹ�����</th>
		<th CLASS="TABLEBULE2" width="100">��������´</th>
		<tr>
	</thead>
	<tbody id="dataTable">
	</tbody>
</table>
</div>
<div>
<table width="600" align="center"  cellpadding="2" cellspacing="0" >
	<tr>
		<td align="right">
			<input type="hidden" name="page" value="<%=pageTmp%>">
			<input type="hidden" name="maxPage">
			<input type="hidden" name="countData" >
			<input type="hidden" name="dataPerPage" value="10">
			<input type="button" disabled="disabled" class=" button " value="First" name="first" onclick="onFirst(whenShowDataTable);"/>
			<input type="button" disabled="disabled" class=" button " value="<<" name="previous" onclick="onPrevious(whenShowDataTable);"/>
			<input type="text"  name="showPage" style="text-align:right;width: 30;" 
				    onkeyup="onCheckPageNAN(this.value);" onchange="onChangeGoPage(whenShowDataTable);" onkeypress="onKeyGoPage(event,whenShowDataTable);" 
			/>
			/
			<input type="text"  name="showMaxPage" readonly="readonly" style="width: 30;border-style : none;background-color : transparent;text-align:right;font-weight:bold;"/>
			<input type="button" disabled="disabled" class=" button " value=">>" name="next" onclick="onNext(whenShowDataTable);" />
			<input type="button" disabled="disabled" class=" button " value="Last" name="last" onclick="onLast(whenShowDataTable);"/>
		</td>
	</tr>
</table>
</div>
<table width="100%" CLASS="TABLEBULE2">
	<tr CLASS="TABLEBULE2" >
		<td align="left" >&nbsp;</td>
	</tr>
</table>
</form>
<form name="editForm" action="security.htm?reqCode=CTPEQY001_1" method="post">
	<input type="hidden" name="evaYearEdit">
	<input type="hidden" name="evaTimeEdit">
	<input type="hidden" name="areaCodeEdit">
	<input type="hidden" name="secCodeEdit">
	<input type="hidden" name="workCodeEdit">
	<input type="hidden" name="areaDescEdit">
	<input type="hidden" name="secDescEdit">
	<input type="hidden" name="workDescEdit">
	<input type="hidden" name="workDescEdit">
	<input type="hidden" name="empCodeEdit">
	<input type="hidden" name="pageEdit">
</form>
</td>
</tr>
</table>
</body>
</html>