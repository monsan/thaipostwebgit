<%@ page language="java" contentType="text/html;charset=TIS-620"%>
<%@ page import="com.ss.tp.security.UserInfo" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>

<%
	UserInfo userInfo = (UserInfo)session.getAttribute("UserLogin");
	String userId = userInfo.getUserId();
	String ouCode = userInfo.getOuCode();
	Calendar now = Calendar.getInstance(Locale.US);	
	String year =String.valueOf(now.get(Calendar.YEAR)+543);
%>

<html>
<head>
<title>��§ҹ��ºѹ�֡�����Ť�ṹ�����Թ�š�û�Ժѵԧҹ</title>
<!-- You have to include these two JavaScript files from DWR -->
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript" src="dwr/util.js"></script>
<script type="text/javascript" src="script/dojo.js"></script>
<script type="text/javascript" src="script/payroll_util.js"></script>
<script type="text/javascript" src="script/chkInputData.js"></script>
<!-- This JavaScript file is generated specifically for your application -->
<SCRIPT type="text/javascript" src="dwr/interface/SuUserOrganizationService.js"></SCRIPT>
<SCRIPT type="text/javascript" src="dwr/interface/PnEmployeeService.js"></SCRIPT>
<script type="text/javascript" src="dwr/interface/PeEmployeeScoreService.js"></script>
<script type="text/javascript" src="dwr/interface/VPeEmpScoreReportService.js"></script>
<script type="text/javascript">
	//Load Dojo's code relating to widget managing functions
	dojo.require("dojo.widget.*");	
	dojo.require("dojo.widget.Menu2");
	dojo.require("dojo.widget.Button");
	dojo.require("dojo.widget.ComboBox");
	dojo.require("dojo.widget.DropDownButton");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.ContentPane");
	dojo.require("dojo.widget.LayoutContainer");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.Toolbar");
	dojo.require("dojo.widget.html.*");
	dojo.require("dojo.widget.Menu2");
	dojo.hostenv.writeIncludes();
	
	//Event
	dojo.require("dojo.event.*");

	var empCodeDataFrom=[];
	var empCodeDataTo=[];
	var orgCodeShowDataF=[];
	var orgCodeShowDataT=[];

</script>


<script type="text/javascript">
		//=============== DropDown Menu ==============
		// Load ComboBox From base
      		 function onLoadOrganization(){
				var cboOrgFrom = dojo.widget.byId("orgFromCbo");
				var cboOrgTo = dojo.widget.byId("orgToCbo");
				var cboSource = [];
				<c:forEach items="${OrganizationInSecurity}" var="result" >		 
					cboSource.push(["<c:out value='${result.orgCode}' />"+" "+"<c:out value='${result.orgShowDesc}' />","<c:out value='${result.orgCode}' />"]);
					orgCodeShowDataF.push(["<c:out value='${result.orgCode}' />"]);
				</c:forEach>
				cboOrgFrom.dataProvider.setData(cboSource);
				cboOrgTo.dataProvider.setData(cboSource);		     	
		     }
		     
		     function onLoadEmployee(){
				var comB1=dojo.widget.byId("empFromCbo");
				var comB2=dojo.widget.byId("empToCbo");
				var arrData=[]
				<c:forEach items="${PnEmployeeInSecurity}" var="result" >		 
					arrData.push(["<c:out value='${result}' />","<c:out value='${result}' />"]);
					empCodeDataFrom.push(["<c:out value='${result}' />"]);
				</c:forEach>
				comB1.dataProvider.setData(arrData);
				comB2.dataProvider.setData(arrData);		     
		     }		     
		     
		     
		     // Even ComboBox valueChange

		    function whenSelectOrgOption(){
		    	DWRUtil.useLoadingMessage("Loading ...");
				var cbo = dojo.widget.byId("orgFromCbo");
				whenFetchOrganizationTo(cbo.textInputNode.value);
			}
			function whenFetchOrganizationTo(orgCode){
		     	DWRUtil.useLoadingMessage("Loading ...");
				var cboSource = [];
				var cboTo = dojo.widget.byId("orgToCbo");
				if(orgCode >cboTo.comboBoxSelectionValue.value){
					cboTo.textInputNode.value = '';
					cboTo.comboBoxSelectionValue.value='';
				}
				var cboTo = dojo.widget.byId("orgToCbo");
			     	
			     	<c:forEach items="${OrganizationInSecurity}" var="result" >	
			     		if ( "<c:out value='${result.orgCode}' />"+" "+"<c:out value='${result.orgDesc}' />" >= orgCode)
						cboSource.push(["<c:out value='${result.orgCode}' />"+" "+"<c:out value='${result.orgDesc}' />","<c:out value='${result.orgCode}' />"]);
					</c:forEach>
			     	cboTo.dataProvider.setData(cboSource);	
		     	//SuUserOrganizationService.findOrganizationByUserIdAndOuCodeToOrgCode('<%=userId%>','<%=ouCode%>',orgCode , {callback:whenFetchOrganizationToCallback});
		    }

		    function whenFetchOrganizationToCallback(data){
		    	try{
			     	var cboSource = [];
			     	var cboTo = dojo.widget.byId("orgToCbo");
			     	for(var i=0; i<data.length; i++){
			     		var org = data[i];
			     		cboSource.push([org.orgCode, org.orgCode]);
			     		orgCodeShowDataT.push([org.orgCode]);			     		
			     	}
			     	cboTo.dataProvider.setData(cboSource);
		     	}catch(e){
		     		alert(e.message);
		     	}
		    }
		    
			function whenSelectEmpOption(){
		    	DWRUtil.useLoadingMessage("Loading ...");
				var cbo = dojo.widget.byId("empFromCbo");
				whenFetchEmployeeTo(cbo.textInputNode.value);
			}
			function whenFetchEmployeeTo(empCode){
		     	DWRUtil.useLoadingMessage("Loading ...");
		     	var cboSource = [];
				var cboTo = dojo.widget.byId("empToCbo");
				if( empCode > cboTo.comboBoxSelectionValue.value ){
			     	cboTo.textInputNode.value = '';
			     	cboTo.comboBoxSelectionValue.value = '';
				}
	
			     	<c:forEach items="${PnEmployeeInSecurity}" var="result" >		
			     		if ( "<c:out value='${result}' />" >= empCode)
						cboSource.push(["<c:out value='${result}' />","<c:out value='${result}' />"]);
					</c:forEach>			     	

			     	cboTo.dataProvider.setData(cboSource);		     	
		     	//PnEmployeeService.findToEmpBySecurity('<%=userId%>','<%=ouCode%>',empCode , {callback:whenFetchEmployeeToCallback});
		    }
		    function whenFetchEmployeeToCallback(data){
		    	try{
			     	var cboSource = [];
			     	var cboTo = dojo.widget.byId("empToCbo");
			     	for(i=0; i<data.length; i++){
			     		cboSource.push([data[i].empCode, data[i].empCode]);
			     		empCodeDataTo.push([data[i].empCode]);
			     	}
			     	cboTo.dataProvider.setData(cboSource);
		     	}catch(e){
		     		alert(e.message);
		     	}
		    }	  
		    
			function init(){
				var cboOrgFrom = dojo.widget.byId("orgFromCbo");
				var cboEmpFrom = dojo.widget.byId("empFromCbo");
				dojo.event.connect(cboOrgFrom, "selectOption", "whenSelectOrgOption");
				dojo.event.connect(cboEmpFrom, "selectOption", "whenSelectEmpOption");
			}
			
			// Load page
		  	//dojo.addOnLoad(init);
			dojo.addOnLoad(onLoadOrganization);
			dojo.addOnLoad(onLoadEmployee);
</script>

</head>
<body>
<table width="100%" >
<tr>
<td class="font-head">[ CTPERP001 ] ��§ҹ�����Ż���ѵԡ�û����Թ�Ţͧ��ѡ�ҹ/�١��ҧ��Ш�</td>
</tr>
<tr>
<td>
<form action="evaluationReport.htm?reqCode=doPrintReport" method="post" name="mainform" target="_blank"><div style="height:400px;">

<table  width="100%" align="center" border="0">
	<tr>
		<td aling ="center"> <input type="hidden" name="ouCode" value="<%=ouCode%>"><input type="hidden" name="userId" value="<%=userId%>">
			
	    	<table width="770" align="center"   border="0" cellpadding="2" cellspacing="2" >
          		<tr>
          		  <td class="font-field"  align="right">������Шӻ�</td>
          		  <td colspan="1"><input type="text" name="evaYear" size="4" maxlength="4" style="text-align: center;" onKeyPress="return chkInputNum()"  /></td>
          		  <td class="font-field"  align="right">�֧��Шӻ�</td>
          		  <td colspan="1"><input type="text" name="evaYearTo" size="4" maxlength="4" style="text-align: center;" onKeyPress="return chkInputNum()" /></td>
       		    </tr>
          		<tr>
            		<td class="font-field" >������ѧ�Ѵ��Ժѵԧҹ</td>
            		<td colspan="3"><select  dojotype="ComboBox"  widgetid="orgFromCbo" style="width:"570"></select></td>
				</tr>
				<tr>
            		<td class="font-field">�֧�ѧ�Ѵ��Ժѵԧҹ</td>
            		<td colspan="3"><select  dojotype="ComboBox" widgetid="orgToCbo" style="width:"570"></select></td>
           		</tr>
          		<tr >
					<td width="120"  class="font-field"  align="right">������Ţ��Шӵ��</td>
					<td width="150"><select  dojotype="ComboBox" widgetid="empFromCbo" style="width:120" ></select></td>
					<td  width="100"  class="font-field"  align="center">�֧�Ţ��Шӵ��</td>
					<td width="350" align="left"><select  dojotype="ComboBox" widgetid="empToCbo" style="width:120"></select></td>
            		
            		<input type="hidden" name="orgCodeFrom" ><input type="hidden" name="orgCodeTo" >
            		<input type="hidden" name="empF" ><input type="hidden" name="empT">
           		</tr>
           		<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
          		<tr>
          		  <td colspan="4" align="center">
          		    <input type="button" class=" button "  value="�ʴ���§ҹ" onClick="return whenClickSearch();" />
       		      </td>
       		  	</tr>
        	</table>
        </td>
	</tr>
</table>
</form>
</td>
</tr>
</table>
</body>

<script>
	var year = new Date().getYear()+543;
	//document.forms[0].evaYear.value = year;
	//document.forms[0].evaYearTo.value = year;
	function whenClickSearch(){
		var frm = document.forms[0];
		var comB1=dojo.widget.byId("empFromCbo").textInputNode.value;
		var comB2=dojo.widget.byId("empToCbo").textInputNode.value;
		var orgCodeF=dojo.widget.byId("orgFromCbo").textInputNode.value;
		var orgCodeT=dojo.widget.byId("orgToCbo").textInputNode.value;
		var evaYear=frm.evaYear.value;
		var evaYearTo=frm.evaYearTo.value;
		var orgCodeTempFrom='';
		var orgCodeTempTo='';
		var empCF='';
		var empCT='';
		
		
		if (evaYear == '')	{
			evaYear = '0';
		}
		if(evaYearTo == ''){
			evaYearTo ='0';
		}

		/* if(!isNumber(evaYear)){
			alert("�յ�ͧ�繵���Ţ");
			return false;
		}

		if (evaYear.length != 4)	{
			alert ("�վ.�.��ͧ�� 4 ��ѡ");
			return false;
		}
		*/
		if(orgCodeF!=''){
			dojo.widget.byId("orgFromCbo").comboBoxSelectionValue.value = splitCombo(dojo.widget.byId("orgFromCbo").textInputNode.value);
			orgCodeTempFrom = splitCombo(dojo.widget.byId("orgFromCbo").textInputNode.value);
			
			
		}
		if(orgCodeT !=''){
			dojo.widget.byId("orgToCbo").comboBoxSelectionValue.value = splitCombo(dojo.widget.byId("orgToCbo").textInputNode.value);
			orgCodeTempTo =  splitCombo(dojo.widget.byId("orgToCbo").textInputNode.value);
			
		}
		
		if(dojo.widget.byId("orgToCbo").textInputNode.value != ""){
			if(dojo.widget.byId("orgFromCbo").textInputNode.value == ""){
				alert("��سҡ�͡ ���ʡͧ/�ӹѡ�ҹ������� ");
				return false;
			}
		}
		
		
	

		if(dojo.widget.byId("empFromCbo").textInputNode.value != ""){
			if(!isNumber(dojo.widget.byId("empFromCbo").textInputNode.value)){
				alert("�Ţ��Шӵ�ǵ�ͧ�繵���Ţ");
				return false;
			}
		}
		if(dojo.widget.byId("empToCbo").textInputNode.value != ""){
			if(!isNumber(dojo.widget.byId("empToCbo").textInputNode.value)){
				alert("�Ţ��Шӵ�ǵ�ͧ�繵���Ţ");
				return false;
			}
		}
		
		if(comB1!=''){
			dojo.widget.byId("empFromCbo").comboBoxSelectionValue.value = splitCombo(dojo.widget.byId("empFromCbo").textInputNode.value);
			empCF = splitCombo(dojo.widget.byId("empFromCbo").textInputNode.value);
			
		}
		if(comB2 !=''){
			dojo.widget.byId("empToCbo").comboBoxSelectionValue.value = splitCombo(dojo.widget.byId("empToCbo").textInputNode.value);
			empCT =  splitCombo(dojo.widget.byId("empToCbo").textInputNode.value);
			
		}
		if(dojo.widget.byId("empToCbo").textInputNode.value != ""){
			if(dojo.widget.byId("empFromCbo").textInputNode.value == ""){
				alert("��سҡ�͡ �Ţ��Шӵ��������� ");
				return false;
			}
		}
		
		forController(orgCodeTempFrom,orgCodeTempTo,empCF,empCT);
		frm.target = "_blank";
		frm.submit();	
	}
	
	function forController(orgCodeTempFrom,orgCodeTempTo,empCF,empCT){
		DWRUtil.setValue("orgCodeFrom",orgCodeTempFrom);
		DWRUtil.setValue("orgCodeTo",orgCodeTempTo);
		DWRUtil.setValue("empF",empCF);
		DWRUtil.setValue("empT",empCT);
	}
	
	
	
	function isNumber(num){
		var check = true;
		var realNumber = "1234567890";
		var numlength = 0;
		if (num != null)	{
			var numlength = num.length;		
		}
		var temp;
		for(var i=0;i<numlength;i++){
			temp = num.substring(i,i+1);
			if ( realNumber.indexOf(temp) == -1 ){
				check = false;
				break;
			}
		}
		return check;
	}
</script>

</html>