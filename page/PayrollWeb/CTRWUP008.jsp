<%@ page language="java" contentType="text/html;charset=TIS-620" %>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>

<%
	String empID = (String)request.getParameter("empCodeEdit");
	String yearEdit = (String)request.getParameter("yearEdit");
	String periodEdit = (String)request.getParameter("periodEdit");
	String ouCodeEdit = (String)request.getParameter("ouCodeEdit");
	String confirm = (String)request.getParameter("confirmEdit");
	String sectionEdit = (String)request.getParameter("sectionEdit");
	String userId = (String)request.getParameter("userIdEdit");
	
	String orgFromEdit = request.getParameter("orgFromEdit");
	String orgToEdit = request.getParameter("orgToEdit");
	String empCodeFromEdit = request.getParameter("empCodeFromEdit");
	String empCodeToEdit = request.getParameter("empCodeToEdit");
	String pageEdit = request.getParameter("pageEdit");
	
	String orderFromCboEdit = request.getParameter("orderFromCboEdit");
	String orderToCboEdit = request.getParameter("orderToCboEdit");
	
	if( pageEdit.trim().equals("") )
		pageEdit = "-1";	
	
	java.util.Date dd = new java.util.Date();
	java.text.SimpleDateFormat fmd = new java.text.SimpleDateFormat("dd/MM/yyyy",new java.util.Locale("th","TH"));
	String date = fmd.format(dd);
	
	java.util.Calendar cc = new java.util.GregorianCalendar();
	int x = cc.get(java.util.Calendar.MONTH) +1;
	//System.out.println("#$#$#$#$$#$#$# :::"+x);
	
	String periodWork = String.valueOf(x);
	
%>

<c:set var="org.apache.struts.taglib.html.BEAN" value="${form}" />
<html>
<head>
<title>��Ѻ��ا��¡���Ѻ (�Թ��͹)</title>
<!-- Include -->
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript" src="dwr/util.js"></script>
<!-- Javascript Script File -->
<script type="text/javascript" src="dwr/interface/RwModSalService.js"></script>

<script type="text/javascript" src="script/gridScript.js"></script>
<script type="text/javascript" src="script/dojo.js"></script>
<script type="text/javascript" src="script/dateCalendar.js"></script>

<script type="text/javascript">
	//Load Dojo's code relating to widget managing functions
	dojo.require("dojo.widget.*");
	dojo.require("dojo.widget.Menu2");
	dojo.require("dojo.widget.Button");
	dojo.require("dojo.widget.ComboBox");
	dojo.require("dojo.widget.DropDownButton");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.ContentPane");
	dojo.require("dojo.widget.LayoutContainer");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.Toolbar");
	dojo.require("dojo.widget.html.*");
	dojo.require("dojo.widget.Menu2");
	dojo.hostenv.writeIncludes();
	
	//Event
	dojo.require("dojo.event.*");
</script>
<script type="text/javascript">
<!--

	var myUpdate = new Array();
	var count = 0;
	var specRow = 0;
	var salaryTmp = new Array();
	var rowModify ;
	
	var specRowNew = 0;
	var salaryTmpNew = new Array();
	
	function whenQueryData()
	{
		DWRUtil.useLoadingMessage("Loading ...");
		RwModSalService.findByEmpCodeDetail('<%=empID%>','<%=ouCodeEdit%>','<%=yearEdit%>','<%=periodEdit%>',{callback:whenQueryDetailHandler,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
		RwModSalService.findByEmpCodeList('<%=userId%>','<%=ouCodeEdit%>','<%=yearEdit%>','<%=periodEdit%>','<%=empID%>',{callback:whenQueryListDataHandler,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
		
		
	}
	function whenQueryDetailHandler(data){
		$("year").value = '<%=yearEdit%>';
		$("period").value = '<%=periodEdit%>';
		$("section").value = '<%=sectionEdit%>';
		$("empCode").value = data.empCode;
		$("name").value = data.name;
		$("positionShort").value = data.positionShort;
		$("orgCode").value = data.orgCode;
		$("orgDesc").value = data.orgDesc;
		$("codeSeq").value = data.codeSeqWork;
		
		if(<%=confirm%>){
			//document.forms['formEdit'].elements['insertData'].disabled = true;
			document.forms['formEdit'].elements['deleteData'].disabled = true;
			document.forms['formEdit'].elements['confirmData'].disabled = true;
		}
	}
	
	function setDefaultSalary()
	{
		var tab = $('dataTable');
		var row;
		var frm   = document.forms["formEdit"];
		
		var newSalary 		= frm.elements["oldSal"];
		
		
		
		for(var i=0; i<tab.rows.length; i++)
		{	
			
			row = tab.rows[i];
			if(tab.rows.length == 1){
				newSalary.value = salaryTmp[i];
			}else if(tab.rows.length > 1){
				newSalary[i].value = salaryTmp[i];
			}
		}
	}
	
	function setDefaultSalaryNew()
	{
		var tab = $('dataTable');
		var row;
		var frm   = document.forms["formEdit"];
		
		var newSalary 		= frm.elements["newSal"];
		
		
		
		for(var i=0; i<tab.rows.length; i++)
		{	
			
			row = tab.rows[i];
			if(tab.rows.length == 1){
				newSalary.value = salaryTmpNew[i];
			}else if(tab.rows.length > 1){
				newSalary[i].value = salaryTmpNew[i];
			}
		}
	}
	
	var cellFuncs = [
		function(data) { return writeCheckBox();},
		function(data) { return writeSelect("flagPr",data.flagPr,data.keySeq);},
		function(data) { 
						if(data.startDateNew != null && data.startDateNew != ''){
							return writeTextDate("startDateNew",formatDate(data.startDateNew,"dd/MM/yyyy"),10,"center",data.keySeq,20);
						}else{
							return writeTextDate("startDateNew","",10,"center",data.keySeq,20);
						}
						},
		function(data) { return writeHidden("fiscalYear",data.fiscalYear,4,"center","keySeq",data.keySeq);},	
		function(data) { 
						if(data.startDateEdun != null && data.startDateEdun != ''){
							return writeTextStDateEdun("startDateEdun",formatDate(data.startDateEdun,"dd/MM/yyyy"),10,"center",data.keySeq,12);
						}else{
							return writeTextStDateEdun("startDateEdun","",10,"center",data.keySeq,12);
						}
						},
		function(data) { 
						if(data.endDateEdun != null & data.endDateEdun != ''){
							return writeTextEndDateEdun("endDateEdun",formatDate(data.endDateEdun,"dd/MM/yyyy"),10,"center",data.keySeq,12);
						}else{
							return writeTextEndDateEdun("endDateEdun","",10,"center",data.keySeq,12);
						}
						},
		
		function(data) { return writeNewSalary("oldSal",data.oldSal,data.keySeq);},
		function(data) { return writeNewSalaryNew("newSal",data.newSal,data.keySeq);},
		function(data) { return writeSelect("eduWut",data.eduWut,data.keySeq);},
		function(data) { 
						 if(data.startDateBack != null && data.startDateBack != ''){
						 	return writeTextStDateBack("startDateBack",formatDate(data.startDateBack,"dd/MM/yyyy"),10,"center",data.keySeq,14);
						 }else{
						 	return writeTextStDateBack("startDateBack","",10,"center",data.keySeq,14);
						 }
						 },
		function(data) { 
						if(data.endDateBack != null && data.endDateBack != ''){
							return writeTextEndDateBack("endDateBack",formatDate(data.endDateBack,"dd/MM/yyyy"),10,"center",data.keySeq,14);
						}else{
							return writeTextEndDateBack("endDateBack","",10,"center",data.keySeq,14);
						}	
						},
		function(data) { 
						if(data.startDateJob != null && data.startDateJob != ''){
							return writeTextStDateJob("startDateJob",formatDate(data.startDateJob,"dd/MM/yyyy"),10,"center",data.keySeq,12);
						}else{
							return writeTextStDateJob("startDateJob","",10,"center",data.keySeq,12);
						}	
						},
		function(data) { 
						if(data.endDateJob != null && data.endDateJob != ''){
							return writeTextEndDateJob("endDateJob",formatDate(data.endDateJob,"dd/MM/yyyy"),10,"center",data.keySeq,12);
						}else{
							return writeTextEndDateJob("endDateJob","",10,"center",data.keySeq,12);
						}	
						},
		function(data) { return writeSelect("multipleLevel",data.multipleLevel,data.keySeq);},
		function(data) { return writeSelect("backStep",data.backStep,data.keySeq);}
	];
	
	function whenQueryListDataHandler(data){
		$("dataLength").value = data.length;
		if(data.length > 0){
			DWRUtil.removeAllRows("dataTable");
			DWRUtil.addRows("dataTable",data,cellFuncs);
			
			setDefaultSalary();
			setDefaultSalaryNew();
		}else{
			
			DWRUtil.removeAllRows("dataTable");
			alert('��辺������');
		}
	}

	function writeCheckBox()
	{
		if(<%=confirm%>){
			return "<div align='center'><input type='checkbox' disabled='true' name ='chk'  /></div>";
		}else{
			return "<div align='center'><input type='checkbox' name ='chk'  /></div>";
		}
	}
	
	function writeNewSalary(inname,emp,key)
	{
		if(<%=confirm%>){
		var tmpStr = "<div align='center' style='background-color:#CCCCCC;'><select name='"+inname+"' disabled='true' style='background-color:transparent;color:#000000;width:100%;'><option value=''></option>"+
												<c:forEach items='${SalaryList}' var='result'>
												"<option  value='<c:out value='${result.salary}' />' ><c:out value='${result.salary}' /></option>" +
												</c:forEach>
												"</select></div>";
			salaryTmp[specRow] = emp;
			specRow++;
			return tmpStr;
		}else {
		var tmpStr = "<div align='center' ><select onchange='addListUpdate("+key+")' name='"+inname+"' style='width:100%;'><option value=''></option>" +
												<c:forEach items='${SalaryList}' var='result'>
												"<option  value='<c:out value='${result.salary}' />'><c:out value='${result.salary}' /></option>" +
												</c:forEach>
												"</select></div>";
			salaryTmp[specRow] = emp;
			specRow++;
			return tmpStr;
		}
		if(<%=confirm%>){
			var tmpStr = "<div align='center' style='background-color:#CCCCCC;'><select name='"+inname+"' disabled='true' style='background-color:transparent;color:#000000;width:100%;'><option value=''></option>"+
													<c:forEach items='${SalaryList}' var='result'>
													"	<option  value='<c:out value='${result.salary}' />' ><c:out value='${result.salary}' /></option>" +
													</c:forEach>
													"</select></div>";
				salaryTmp[specRow] = emp;
				specRow++;
				return tmpStr;
			}else {
			var tmpStr = "<div align='center' ><select onchange='addListUpdate("+key+")' name='"+inname+"' style='width:100%;'><option value=''></option>" +
													<c:forEach items='${SalaryList}' var='result'>
														"	<option  value='<c:out value='${result.salary}' />'><c:out value='${result.salary}' /></option>" +
													</c:forEach>
													"</select></div>";
				salaryTmp[specRow] = emp;
				specRow++;
				return tmpStr;
			}
		
	}
	
	function writeNewSalaryNew(inname,emp,key)
	{
		if(<%=confirm%>){
		var tmpStr = "<div align='center' style='background-color:#CCCCCC;'><select name='"+inname+"' disabled='true' style='background-color:transparent;color:#000000;width:100%;'><option value=''></option>"+
												<c:forEach items='${SalaryList}' var='result'>
												"	<option  value='<c:out value='${result.salary}' />' ><c:out value='${result.salary}' /></option>" +
												</c:forEach>
												"</select></div>";
			salaryTmpNew[specRowNew] = emp;
			specRowNew++;
			return tmpStr;
		}else {
		var tmpStr = "<div align='center' ><select onchange='addListUpdate("+key+")' name='"+inname+"' style='width:100%;'><option value=''></option>" +
												<c:forEach items='${SalaryList}' var='result'>
													"	<option  value='<c:out value='${result.salary}' />'><c:out value='${result.salary}' /></option>" +
												</c:forEach>
												"</select></div>";
			salaryTmpNew[specRowNew] = emp;
			specRowNew++;
			return tmpStr;
		}
	}
	
	function writeButton(inname,emp)
	{
		return "<div align='center'><input type='button' name = '"+inname+"' value='"+emp+"'  /></div>";
	}
	
	function writeSelectMonth(inname,emp,key){
		var mm1 = '';
		var mm2 = '';
		var mm3 = '';
		var mm4 = '';
		var mm5 = '';
		var mm6 = '';
		var mm7 = '';
		var mm8 = '';
		var mm9 = '';
		var mm10 = '';
		var mm11 = '';
		var mm12 = '';
		
		if(emp=='1'){
			mm1 = 'selected';
		} else if(emp=='2'){
			mm2 = 'selected';		
		}else if(emp=='3'){
			mm3 = 'selected';		
		}else if(emp=='4'){
			mm4 = 'selected';		
		}else if(emp=='5'){
			mm5 = 'selected';		
		}else if(emp=='6'){
			mm6 = 'selected';		
		}else if(emp=='7'){
			mm7 = 'selected';		
		}else if(emp=='8'){
			mm8 = 'selected';		
		}else if(emp=='9'){
			mm9 = 'selected';		
		}else if(emp=='10'){
			mm10 = 'selected';		
		}else if(emp=='11'){
			mm11 = 'selected';		
		}else if(emp=='12'){
			mm12 = 'selected';		
		}
		
		if(<%=confirm%>){
		return "<div align='center' style='background-color:#CCCCCC;'><select name='"+inname+"' disabled='true' style='background-color:transparent;color:#000000'>" +
													"<option value='1' "+mm1+" >���Ҥ�</option>" +
													"<option value='2' "+mm2+">����Ҿѹ��</option>" +
													"<option value='3' "+mm3+">�չҤ�</option>" +
													"<option value='4' "+mm4+">����¹</option>" +
													"<option value='5' "+mm5+">����Ҥ�</option>" +
													"<option value='6' "+mm6+" >�Զع�¹</option>" +
													"<option value='7' "+mm7+">�á�Ҥ�</option>" +
													"<option value='8' "+mm8+">�ԧ�Ҥ�</option>" +
													"<option value='9' "+mm9+">�ѹ��¹</option>" +
													"<option value='10' "+mm10+" >���Ҥ�</option>" +
													"<option value='11' "+mm11+">��Ȩԡ�¹</option>" +
													"<option value='12' "+mm12+" >�ѹ�Ҥ�</option>" +
												"</select></div>";
		}else {
		return "<div align='center' ><select onchange='addListUpdate("+key+")' name='"+inname+"'>" +
												"<option value='1' "+mm1+" >���Ҥ�</option>" +
													"<option value='2' "+mm2+">����Ҿѹ��</option>" +
													"<option value='3' "+mm3+">�չҤ�</option>" +
													"<option value='4' "+mm4+">����¹</option>" +
													"<option value='5' "+mm5+">����Ҥ�</option>" +
													"<option value='6' "+mm6+" >�Զع�¹</option>" +
													"<option value='7' "+mm7+">�á�Ҥ�</option>" +
													"<option value='8' "+mm8+">�ԧ�Ҥ�</option>" +
													"<option value='9' "+mm9+">�ѹ��¹</option>" +
													"<option value='10' "+mm10+" >���Ҥ�</option>" +
													"<option value='11' "+mm11+">��Ȩԡ�¹</option>" +
													"<option value='12' "+mm12+" >�ѹ�Ҥ�</option>" +
												"</select></div>";
		
		
		}
	}
	
	function writeSelect(inname,emp,key)
	{
		if(inname=="flagPr")
		{
			var temp1 = '';
			var temp2 = '';
			var temp3 = '';
			var temp4 = '';
			var temp5 = '';
			if(emp=='N'){
				temp1 = 'selected';
			} else if(emp=='A'){
				temp2 = 'selected';		
			}else if(emp=='R'){
				temp3 = 'selected';		
			}else if(emp=='B'){
				temp4 = 'selected';		
			}else if(emp=='S'){
				temp5 = 'selected';		
			}
			if(<%=confirm%>){
			return "<div align='center' style='background-color:#CCCCCC;'><select name='"+inname+"' disabled='true' style='background-color:transparent;color:#000000'><!--<option value='N' "+temp1+" >����</option>-->"+
													"<option value='A' "+temp2+">��Ѻ��ا��¡���Ѻ</option>" +
													"<!--<option value='R' "+temp3+">��¡���Ѻ���¡�׹</option>" +
													"<option value='B' "+temp4+">���ԡ��Ѻ��ا��¡���Ѻ</option>" +
													"<option value='S' "+temp5+">���ԡ��¡���Ѻ���¡�׹</option>--></select></div>";
			}else {
			return "<div align='center' ><select onchange='addListUpdate("+key+")' name='"+inname+"'><!--<option value='N' "+temp1+" >����</option>-->"+
													"<option value='A' "+temp2+">��Ѻ��ا��¡���Ѻ</option>" +
													"<!--<option value='R' "+temp3+">��¡���Ѻ���¡�׹</option>" +
													"<option value='B' "+temp4+">���ԡ��Ѻ��ا��¡���Ѻ</option>" +
													"<option value='S' "+temp5+">���ԡ��¡���Ѻ���¡�׹</option>--></select></div>";
			
			
			}
		}
		if(inname=="multipleLevel")
		{
			var tempNull = '';
			var temp6 = '';
			var temp7 = '';
			if(emp==''){
				tempNull = 'selected';
			} else if(emp=='1'){
				temp6 = 'selected';
			} else if(emp=='2'){
				temp7 = 'selected';		
			}
			if(<%=confirm%>){
				return "<div align='center'><select name='"+inname+"' disabled='true'><option value='' "+tempNull+" ></option><option value='1' "+temp6+" >0.5 ���</option>"+
													"<option value='2' "+temp7+">1.0 ���</option></select></div>";
			}else{
				return "<div align='center'><select onchange='addListUpdate("+key+")' name='"+inname+"'><option value='' "+tempNull+" ></option><option value='1' "+temp6+" >0.5 ���</option>"+
													"<option value='2' "+temp7+">1.0 ���</option></select></div>";
			
			}
		}
		if(inname=="backStep")
		{
			var ttNull = '';
			var temp8 = '';
			var temp9 = '';
			var temp10 = '';
			var temp11 = '';
			if(emp==''){
				ttNull = 'selected';
			} else if(emp=='1'){
				temp8 = 'selected';
			} else if(emp=='2'){
				temp9 = 'selected';		
			}else if(emp=='3'){
				temp10 = 'selected';		
			}else if(emp=='4'){
				temp11 = 'selected';		
			}
			if(<%=confirm%>){
				return "<div align='center'><select name='"+inname+"' disabled='true' style='width:100%'><option value='' "+ttNull+" ></option><option value='1' "+temp8+" >0.5 ���</option>"+
													"<option value='2' "+temp9+">1.0 ���</option>" +
													"<option value='3' "+temp10+">1.5 ���</option>" +
													"<option value='4' "+temp11+">2.0 ���</option></select></div>";
			}else{
			
				return "<div align='center'><select onchange='addListUpdate("+key+")' name='"+inname+"'style='width:100%'><option value='' "+ttNull+" ></option><option value='1' "+temp8+" >0.5 ���</option>"+
													"<option value='2' "+temp9+">1.0 ���</option>" +
													"<option value='3' "+temp10+">1.5 ���</option>" +
													"<option value='4' "+temp11+">2.0 ���</option></select></div>";
			
			}
		}
		if(inname=="eduWut")
		{
			var tempNull = '';
			var temp12 = '';
			var temp13 = '';
			if(emp==''){
				tempNull = 'selected';
			} else if(emp=='5'){
				temp12 = 'selected';
			} else if(emp=='6'){
				temp13 = 'selected';		
			}
			if(<%=confirm%>){
				return "<div align='center'><select name='"+inname+"' disabled='true'><option value='' "+tempNull+" ></option><option value='5' "+temp12+" >��ӡ��� �.���</option>"+
													"<option value='6' "+temp13+">�.��բ�ѹ�</option></select></div>";
			}else{
				return "<div align='center'><select onchange='addListUpdate("+key+")' name='"+inname+"'><option value='' "+tempNull+" ></option><option value='5' "+temp12+" >��ӡ��� �.���</option>"+
													"<option value='6' "+temp13+">�.��բ���</option></select></div>";
			
			}
		}
		
	}
	function writeSelectWut(inname,emp,key)
	{
		
		if(inname=="eduWut")
		{
			var tempNull = '';
			var temp12 = '';
			var temp13 = '';
			if(emp==''){
				tempNull = 'selected';
			} else if(emp=='5'){
				temp12 = 'selected';
			} else if(emp=='6'){
				temp13 = 'selected';		
			}
			if(<%=confirm%>){
				return "<div align='center'><select name='"+inname+"' disabled='true'><option value='' "+tempNull+" ></option><option value='5' "+temp12+" >��ӡ��� �.���</option>"+
													"<option value='6' "+temp13+">�.��բ�ѹ�</option></select></div>";
			}else{
				return "<div align='center'><select onchange='addListUpdate("+key+")' name='"+inname+"'><option value='' "+tempNull+" ></option><option value='5' "+temp12+" >��ӡ��� �.���</option>"+
													"<option value='6' "+temp13+">�.��բ���</option></select></div>";
			
			}
		}
		
		
	}
	function writeText(inname,emp,maxlength,textalign,key)
	{
		if(<%=confirm%>){
			return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='"+emp+"' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' onkeyup='if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);'/></div>";
		}else{
			return "<div align='center' ><input type='text' onchange='addListUpdate("+key+")' name = '"+inname+"' value='"+emp+"' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;' onkeyup='if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);'/></div>";
		}
	}
	
	function writeTextDate(inname,emp,maxlength,textalign,key,size)
	{
		if(<%=confirm%>){
			return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='"+emp+"' align='center' size='"+size+"' maxlength='"+maxlength+"' style='text-align:"+textalign+";background-color:transparent;' onblur='validateDate(this);'/></div>";
		}else{
			return "<div align='center' ><input type='text' onchange='addListUpdate("+key+")' name = '"+inname+"' value='"+emp+"' align='center' size='"+size+"' maxlength='"+maxlength+"' style='text-align:"+textalign+";' onblur='validateDate(this);'/></div>";
		}
	}
	
	function writeTextStDateEdun(inname,emp,maxlength,textalign,key,size)
	{
		if(<%=confirm%>){
			return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='"+emp+"' align='center' size='"+size+"' maxlength='"+maxlength+"' style='text-align:"+textalign+";background-color:transparent;' onblur='validateDate(this);'/></div>";
		}else{
			return "<div align='center' ><input type='text' onchange='addListUpdate("+key+");return validateDate(this) && compareStDateEdunRowUpdate("+key+");' name = '"+inname+"' value='"+emp+"' align='center' size='"+size+"' maxlength='"+maxlength+"' style='text-align:"+textalign+";' /></div>";
		}
	}
	function writeTextEndDateEdun(inname,emp,maxlength,textalign,key,size)
	{
		if(<%=confirm%>){
			return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='"+emp+"' align='center' size='"+size+"' maxlength='"+maxlength+"' style='text-align:"+textalign+";background-color:transparent;' onblur='validateDate(this);'/></div>";
		}else{
			return "<div align='center' ><input type='text' onchange='addListUpdate("+key+");return validateDate(this) && compareEndDateEdunRowUpdate("+key+");' name = '"+inname+"' value='"+emp+"' align='center' size='"+size+"' maxlength='"+maxlength+"' style='text-align:"+textalign+";' /></div>";
		}
	}
	function writeTextStDateBack(inname,emp,maxlength,textalign,key,size)
	{
		if(<%=confirm%>){
			return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='"+emp+"' align='center' size='"+size+"' maxlength='"+maxlength+"' style='text-align:"+textalign+";background-color:transparent;' onblur='validateDate(this);'/></div>";
		}else{
			return "<div align='center' ><input type='text' onchange='addListUpdate("+key+");return validateDate(this) && compareStDateBackRowUpdate("+key+");' name = '"+inname+"' value='"+emp+"' align='center' size='"+size+"' maxlength='"+maxlength+"' style='text-align:"+textalign+";' /></div>";
		}
	}
	function writeTextEndDateBack(inname,emp,maxlength,textalign,key,size)
	{
		if(<%=confirm%>){
			return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='"+emp+"' align='center' size='"+size+"' maxlength='"+maxlength+"' style='text-align:"+textalign+";background-color:transparent;' onblur='validateDate(this);'/></div>";
		}else{
			return "<div align='center' ><input type='text' onchange='addListUpdate("+key+");return validateDate(this) && compareEndDateBackRowUpdate("+key+");' name = '"+inname+"' value='"+emp+"' align='center' size='"+size+"' maxlength='"+maxlength+"' style='text-align:"+textalign+";' /></div>";
		}
	}
	function writeTextStDateJob(inname,emp,maxlength,textalign,key,size)
	{
		if(<%=confirm%>){
			return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='"+emp+"' align='center' size='"+size+"' maxlength='"+maxlength+"' style='text-align:"+textalign+";background-color:transparent;' onblur='validateDate(this);'/></div>";
		}else{
			return "<div align='center' ><input type='text' onchange='addListUpdate("+key+");return validateDate(this) && compareStDateJobRowUpdate("+key+");' name = '"+inname+"' value='"+emp+"' align='center' size='"+size+"' maxlength='"+maxlength+"' style='text-align:"+textalign+";' /></div>";
		}
	}
	function writeTextEndDateJob(inname,emp,maxlength,textalign,key,size)
	{
		if(<%=confirm%>){
			return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='"+emp+"' align='center' size='"+size+"' maxlength='"+maxlength+"' style='text-align:"+textalign+";background-color:transparent;' onblur='validateDate(this);'/></div>";
		}else{
			return "<div align='center' ><input type='text' onchange='addListUpdate("+key+");return validateDate(this) && compareEndDateJobRowUpdate("+key+");' name = '"+inname+"' value='"+emp+"' align='center' size='"+size+"' maxlength='"+maxlength+"' style='text-align:"+textalign+";' /></div>";
		}
	}
	
	function writeHidden(inname,emp,maxlength,textalign,nameHide,empHide)
	{
		if(<%=confirm%>){
			return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='"+emp+"' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' onkeyup='if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);'/><input type='hidden' name = '"+nameHide+"' value='"+empHide+"'  /></div>";
		}else{
			return "<div align='center'><input type='text' onchange='addListUpdate("+empHide+")' name = '"+inname+"' value='"+emp+"' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%' onkeyup='if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);'/><input type='hidden' name = '"+nameHide+"' value='"+empHide+"'  /></div>";
		}
	}
	
	var rwModSal = {keySeq:null, ouCode:null, yearPr:null, periodPr:null,empCode:null,
					yearWork:null,periodWork:null,codeSeq:null,startDateNew:null,fiscalYear:null,
					startDateEdun:null,endDateEdun:null,oldSal:null,newSal:null,eduWut:null,confirmFlag:null,
					startDateBack:null,endDateBack:null,startDateJob:null,endDateJob:null,
					multipleLevel:null,backStep:null,flagPr:null,updBy:null,updDate:null,creBy:null,creDate:null};
					
	var allRowUpdate = 0;
	
	function onUpdate()
	{
		var table = document.getElementById("table");
		var aRows = table.rows;
		var num = 2 + parseInt(DWRUtil.getValue("dataLength"));
		
		var tab = $('dataTable');
		var update;
		var row;
		var empList=[];
		var frm   = document.forms["formEdit"];
		
		var keySeq 			= frm.elements["keySeq"];
		var flagPr 			= frm.elements["flagPr"];
		var startDateNew 	= frm.elements["startDateNew"];
		var fiscalYear 		= frm.elements["fiscalYear"];
		var startDateEdun 	= frm.elements["startDateEdun"];
		var endDateEdun 	= frm.elements["endDateEdun"];
		var oldSal 			= frm.elements["oldSal"];
		var newSal 			= frm.elements["newSal"];
		var eduWut			= frm.elements["eduWut"];
		var startDateBack 	= frm.elements["startDateBack"];
		var endDateBack 	= frm.elements["endDateBack"];
		var startDateJob 	= frm.elements["startDateJob"];
		var endDateJob 		= frm.elements["endDateJob"];
		var multipleLevel 	= frm.elements["multipleLevel"];
		var backStep 		= frm.elements["backStep"];
		
		DWREngine.beginBatch();

		for(var i=0; i<tab.rows.length; i++)
		{	
			update = false;
			row = tab.rows[i];
			rwModSal.keySeq = parseInt(keySeq[i].value);
			
			if(tab.rows.length == 1){
				rwModSal.flagPr = flagPr.value;
				rwModSal.multipleLevel  = multipleLevel.value;
				rwModSal.backStep  = backStep.value;
				if (newSal.value !=''){
					rwModSal.newSal  = newSal.value;
				}
				else{
					rwModSal.newSal  = null;
				}
				
				if (oldSal.value !=''){
					rwModSal.oldSal  = oldSal.value;
				}
				else{
					rwModSal.oldSal  = null;
				}
				 if (eduWut.value !=''){
						rwModSal.eduWut  = eduWut.value;
					}
					else{
						rwModSal.eduWut  = null;
					} 
				
				
			}else{
				rwModSal.flagPr = flagPr[i].value;
				rwModSal.multipleLevel  = multipleLevel[i].value;
				rwModSal.backStep  = backStep[i].value;
				if (newSal[i].value !=''){
					rwModSal.newSal  = newSal[i].value;
				}
				else{
					rwModSal.newSal  = null;
				}
				
				if (oldSal[i].value !=''){
					rwModSal.oldSal  = oldSal[i].value;
				}
				else{
					rwModSal.oldSal  = null;
				}
				 if (eduWut[i].value !=''){
						rwModSal.eduWut  = eduWut[i].value;
					}
					else{
						rwModSal.eduWut  = null;
					} 
				
			}
			
			if(startDateNew[i].value != null && startDateNew[i].value != ''){
				rwModSal.startDateNew  = getDateFromFormat(startDateNew[i].value,"dd/MM/yyyy");
				
				var ddt = startDateNew[i].value;
				var endMonth = ddt.substr(3, 2);
				var endYear = ddt.substr(6);
						
				rwModSal.yearWork = endYear;
				rwModSal.periodWork = endMonth;
			}else{
				rwModSal.startDateNew = null;
				rwModSal.yearWork = '';
				rwModSal.periodWork = '';
			}
			
			if(startDateEdun[i].value != null && startDateEdun[i].value != ''){
				rwModSal.startDateEdun  = getDateFromFormat(startDateEdun[i].value,"dd/MM/yyyy");
			}else{
				rwModSal.startDateEdun  = null;
			}
			
			if(endDateEdun[i].value != null && endDateEdun[i].value != ''){
				rwModSal.endDateEdun  = getDateFromFormat(endDateEdun[i].value,"dd/MM/yyyy");
			}else{
				rwModSal.endDateEdun  = null;
			}
			
			if(startDateBack[i].value != null && startDateBack[i].value != ''){
				rwModSal.startDateBack  = getDateFromFormat(startDateBack[i].value,"dd/MM/yyyy");
			}else{
				rwModSal.startDateBack  = null;
			}
			
			if(endDateBack[i].value != null && endDateBack[i].value != ''){
				rwModSal.endDateBack  = getDateFromFormat(endDateBack[i].value,"dd/MM/yyyy");
			}else{
				rwModSal.endDateBack  = null;
			}
			
			if(startDateJob[i].value != null && startDateJob[i].value != ''){
				rwModSal.startDateJob  = getDateFromFormat(startDateJob[i].value,"dd/MM/yyyy");
			}else{
				rwModSal.startDateJob  = null;
			}
			
			if(endDateJob[i].value != null && endDateJob[i].value != ''){
				rwModSal.endDateJob  = getDateFromFormat(endDateJob[i].value,"dd/MM/yyyy");
			}else{
				rwModSal.endDateJob  = null;
			}
			
		
			
			
			
			
			if (fiscalYear[i].value !=''){
				rwModSal.fiscalYear  = parseInt(fiscalYear[i].value);
			}
			else{
				rwModSal.fiscalYear  = null;
			}
			
					
		/* 	if (oldSal[i].value !=''){
				rwModSal.oldSal  = oldSal[i].value;
			}
			else{
				rwModSal.oldSal  = null;
			}
			if (newSal[i].value !=''){
				rwModSal.newSal  = newSal[i].value;
			}
			else{
				rwModSal.newSal  = null;
			}
			 if (eduWut[i].value !=''){
				rwModSal.eduWut  = eduWut[i].value;
			}
			else{
				rwModSal.eduWut  = null;
			}  */
			rwModSal.updBy = '<%=userId%>';
			for(var x = 0 ; x < myUpdate.length ;x++){
				if(myUpdate[x] == parseInt(keySeq[i].value)){
					update = true;
					break;
				}
			}
			
			if(update){
				allRowUpdate++;
				if(aRows.length  > num + 1 ){
					if( allRowUpdate == myUpdate.length )
						RwModSalService.addList(rwModSal, false, {callback:onInsertCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
					else
						RwModSalService.addList(rwModSal, false);
					
				}else{
					if( allRowUpdate == myUpdate.length )
							RwModSalService.addList(rwModSal, true, {callback:ClearData,errorHandler:function(message) { alert('�������ö�ѹ�֡��');}});
						else
							RwModSalService.addList(rwModSal, false);
					
				}
				
			}
			
			
		}
		
		DWREngine.endBatch();
		if(myUpdate.length == 0){
			insertNewData();
		}
	}
	
	function insertNewData(){
		//alert("new data");
		var table = document.getElementById("table");
		var tdName;
		var chkName;
		var num = 2 + parseInt(DWRUtil.getValue("dataLength"));
		
		var oRows = table.rows;
		if(tdName == null)tdName="flag";
		if(chkName == null)chkName="chk";
		//alert(oRows.length);
		DWREngine.beginBatch();
			for(var i=oRows.length-1;i > num;i--){
				//alert(oRows[i].cells[tdName].childNodes[0].checked);
				//var row = oRows[i];
			
						rwModSal.keySeq = null;
						rwModSal.ouCode = '<%=ouCodeEdit%>';
						rwModSal.yearPr = '<%=yearEdit%>';
						rwModSal.periodPr = '<%=periodEdit%>';
						rwModSal.empCode = '<%=empID%>';
						rwModSal.codeSeq = $("codeSeq").value;
						rwModSal.flagPr= oRows[i].cells["flagPr"].childNodes[0].value;
						if(oRows[i].cells["startDateNew"].childNodes[0].value != null && oRows[i].cells["startDateNew"].childNodes[0].value != ''){
							rwModSal.startDateNew = getDateFromFormat(oRows[i].cells["startDateNew"].childNodes[0].value,"dd/MM/yyyy");
							var ddt = oRows[i].cells["startDateNew"].childNodes[0].value;
							var endMonth = ddt.substr(3, 2);
							var endYear = ddt.substr(6);
							
							rwModSal.yearWork = endYear;
							rwModSal.periodWork = endMonth;
						}else{
							rwModSal.startDateNew = null;
							rwModSal.yearWork = '';
							rwModSal.periodWork = '';
						}
						rwModSal.fiscalYear = oRows[i].cells["fiscalYear"].childNodes[0].value;
						if(oRows[i].cells["startDateEdun"].childNodes[0].value != null && oRows[i].cells["startDateEdun"].childNodes[0].value != ''){
							rwModSal.startDateEdun= getDateFromFormat(oRows[i].cells["startDateEdun"].childNodes[0].value,"dd/MM/yyyy");
						}else{
							rwModSal.startDateEdun= null;
						}
						
						if(oRows[i].cells["endDateEdun"].childNodes[0].value != null && oRows[i].cells["endDateEdun"].childNodes[0].value != ''){
							rwModSal.endDateEdun = getDateFromFormat(oRows[i].cells["endDateEdun"].childNodes[0].value,"dd/MM/yyyy");
						}else{
							rwModSal.endDateEdun = null;
						}
						rwModSal.oldSal = oRows[i].cells["oldSal"].childNodes[0].value;
						rwModSal.newSal = oRows[i].cells["newSal"].childNodes[0].value;
						rwModSal.eduWut = oRows[i].cells["eduWut"].childNodes[0].value;
						if(oRows[i].cells["startDateBack"].childNodes[0].value != null && oRows[i].cells["startDateBack"].childNodes[0].value != ''){
							rwModSal.startDateBack = getDateFromFormat(oRows[i].cells["startDateBack"].childNodes[0].value,"dd/MM/yyyy");
						}else{
							rwModSal.startDateBack = null;
						}
						if(oRows[i].cells["endDateBack"].childNodes[0].value != null && oRows[i].cells["endDateBack"].childNodes[0].value != ''){
							rwModSal.endDateBack= getDateFromFormat(oRows[i].cells["endDateBack"].childNodes[0].value,"dd/MM/yyyy");
						}else{
							rwModSal.endDateBack= null;
						}
						if(oRows[i].cells["startDateJob"].childNodes[0].value != null && oRows[i].cells["startDateJob"].childNodes[0].value != ''){
							rwModSal.startDateJob = getDateFromFormat(oRows[i].cells["startDateJob"].childNodes[0].value,"dd/MM/yyyy");
						}else{
							rwModSal.startDateJob = null;
						}	
						if(oRows[i].cells["endDateJob"].childNodes[0].value != null && oRows[i].cells["endDateJob"].childNodes[0].value != ''){
							rwModSal.endDateJob = getDateFromFormat(oRows[i].cells["endDateJob"].childNodes[0].value,"dd/MM/yyyy");
						}else{
							rwModSal.endDateJob = null;
						}
						
						rwModSal.multipleLevel = oRows[i].cells["multipleLevel"].childNodes[0].value;
						rwModSal.backStep = oRows[i].cells["backStep"].childNodes[0].value;
						rwModSal.confirmFlag = 'N';
						rwModSal.updBy = '<%=userId%>';
						rwModSal.creBy = '<%=userId%>';
						rwModSal.creDate = getDateFromFormat(<%=date%>,"dd/MM/yyyy");
						//RwModSalService.addList(rwModSal, {callback:onInsertCallback});
						
						if( i == (num + 1) )
							RwModSalService.addList(rwModSal, true, {callback:ClearData,errorHandler:function(message) { alert('�������ö�ѹ�֡��');}});
						else
							RwModSalService.addList(rwModSal, false);
				
		}
		DWREngine.endBatch();
	}
	
	function ClearData(){
		alert("�ѹ�֡���������º����");
		var frm=document.forms["backForm"];
		
		document.getElementById("orgFromEdit").value = '<%= orgFromEdit %>';
		document.getElementById("orgToEdit").value = '<%= orgToEdit %>';
		document.getElementById("empCodeFromEdit").value = '<%= empCodeFromEdit %>';
		document.getElementById("empCodeToEdit").value = '<%= empCodeToEdit %>';
		document.getElementById("pageEdit").value = '<%= Integer.parseInt(pageEdit) - 1 %>';
		
		document.getElementById("orderFromCboEdit").value = '<%= orderFromCboEdit %>';
		document.getElementById("orderToCboEdit").value = '<%= orderToCboEdit %>';
		document.getElementById("mustQuery").value = 'true';
		frm.submit();
	}
	
	function onInsertCallback(){
		insertNewData();
		allRowUpdate = 0;
	}
	
	function addListUpdate(data){
		var add = true;
		for(var i = 0 ; i < myUpdate.length ;i++){
			if(myUpdate[i] == data){
				add = false;
				break;
			}
		}
		if(add){
			myUpdate[count] = data;
			count++;
		}
	}
	
	function backMain(){
		var frm=document.forms["backForm"];
		
		document.getElementById("orgFromEdit").value = '<%= orgFromEdit %>';
		document.getElementById("orgToEdit").value = '<%= orgToEdit %>';
		document.getElementById("empCodeFromEdit").value = '<%= empCodeFromEdit %>';
		document.getElementById("empCodeToEdit").value = '<%= empCodeToEdit %>';
		document.getElementById("pageEdit").value = '<%= Integer.parseInt(pageEdit) - 1 %>';
		
		document.getElementById("orderFromCboEdit").value = '<%= orderFromCboEdit %>';
		document.getElementById("orderToCboEdit").value = '<%= orderToCboEdit %>';
		document.getElementById("mustQuery").value = 'true';
		frm.submit();
	}
	
	dojo.addOnLoad(whenQueryData);
	
	
	function compareStDateEdun(object){
	 	 while (object.tagName !=  'TR')
		 {
		 object = object.parentNode
		 }
	
		 var rowToCompare = object.rowIndex
		
     	DWRUtil.useLoadingMessage("Loading ...");
		var table = document.getElementById("table");
		//alert(table.);
		var tdName;
		var chkName;
		
		var oRows = table.rows;
	
		var stDate = oRows[rowToCompare].cells["startDateEdun"].childNodes[0].value;
		var endDate = oRows[rowToCompare].cells["endDateEdun"].childNodes[0].value;
	
		
		var strDay = stDate.substr(0, 2);
		var strMonth = stDate.substr(3, 2);
		var strYear = stDate.substr(6);
		
		if(stDate != null && stDate != '' && endDate != null && endDate != ''){
		 	if (compareDates(stDate,"dd/MM/yyyy",endDate,"dd/MM/yyyy") == 1) {
				alert("�ѹ���������鹵�ͧ���¡��� �ѹ�������ش");
				oRows[rowToCompare].cells["endDateEdun"].childNodes[0].value = '';
				oRows[rowToCompare].cells["startDateEdun"].childNodes[0].focus();
			}
	 	}
	 
	 }
	 
	 function compareEndDateEdun(object){
	 	 while (object.tagName !=  'TR')
		 {
		 object = object.parentNode
		 }
	
		 var rowToCompare = object.rowIndex
		
     	DWRUtil.useLoadingMessage("Loading ...");
		var table = document.getElementById("table");
		//alert(table.);
		var tdName;
		var chkName;
		
		var oRows = table.rows;
	
		var stDate = oRows[rowToCompare].cells["startDateEdun"].childNodes[0].value;
		var endDate = oRows[rowToCompare].cells["endDateEdun"].childNodes[0].value;
		
		
		var endDay = endDate.substr(0, 2);
		var endMonth = endDate.substr(3, 2);
		var endYear = endDate.substr(6);
		
		if(stDate != null && stDate != '' && endDate != null && endDate != ''){
		 	if (compareDates(stDate,"dd/MM/yyyy",endDate,"dd/MM/yyyy") == 1) {
				alert("�ѹ���������鹵�ͧ���¡��� �ѹ�������ش");
				oRows[rowToCompare].cells["startDateEdun"].childNodes[0].value = '';
				oRows[rowToCompare].cells["endDateEdun"].childNodes[0].focus();
			}
	 	}
	}
	
	function compareStDateBack(object){
	 	 while (object.tagName !=  'TR')
		 {
		 object = object.parentNode
		 }
	
		 var rowToCompare = object.rowIndex
		
     	DWRUtil.useLoadingMessage("Loading ...");
		var table = document.getElementById("table");
		//alert(table.);
		var tdName;
		var chkName;
		
		var oRows = table.rows;
	
		var stDate = oRows[rowToCompare].cells["startDateBack"].childNodes[0].value;
		var endDate = oRows[rowToCompare].cells["endDateBack"].childNodes[0].value;
	
		
		var strDay = stDate.substr(0, 2);
		var strMonth = stDate.substr(3, 2);
		var strYear = stDate.substr(6);
		
		if(stDate != null && stDate != '' && endDate != null && endDate != ''){
		 	if (compareDates(stDate,"dd/MM/yyyy",endDate,"dd/MM/yyyy") == 1) {
				alert("�ѹ���������鹵�ͧ���¡��� �ѹ�������ش");
				oRows[rowToCompare].cells["endDateBack"].childNodes[0].value = '';
				oRows[rowToCompare].cells["startDateBack"].childNodes[0].focus();
			}
	 	}
	 
	 }
	 
	 function compareEndDateBack(object){
	 	 while (object.tagName !=  'TR')
		 {
		 object = object.parentNode
		 }
	
		 var rowToCompare = object.rowIndex
		
     	DWRUtil.useLoadingMessage("Loading ...");
		var table = document.getElementById("table");
		//alert(table.);
		var tdName;
		var chkName;
		
		var oRows = table.rows;
	
		var stDate = oRows[rowToCompare].cells["startDateBack"].childNodes[0].value;
		var endDate = oRows[rowToCompare].cells["endDateBack"].childNodes[0].value;
		
		
		var endDay = endDate.substr(0, 2);
		var endMonth = endDate.substr(3, 2);
		var endYear = endDate.substr(6);
		
		if(stDate != null && stDate != '' && endDate != null && endDate != ''){
		 	if (compareDates(stDate,"dd/MM/yyyy",endDate,"dd/MM/yyyy") == 1) {
				alert("�ѹ���������鹵�ͧ���¡��� �ѹ�������ش");
				oRows[rowToCompare].cells["startDateBack"].childNodes[0].value = '';
				oRows[rowToCompare].cells["endDateBack"].childNodes[0].focus();
			}
	 	}
	}
	
	function compareStDateJob(object){
	 	 while (object.tagName !=  'TR')
		 {
		 object = object.parentNode
		 }
	
		 var rowToCompare = object.rowIndex
		
     	DWRUtil.useLoadingMessage("Loading ...");
		var table = document.getElementById("table");
		//alert(table.);
		var tdName;
		var chkName;
		
		var oRows = table.rows;
	
		var stDate = oRows[rowToCompare].cells["startDateJob"].childNodes[0].value;
		var endDate = oRows[rowToCompare].cells["endDateJob"].childNodes[0].value;
	
		
		var strDay = stDate.substr(0, 2);
		var strMonth = stDate.substr(3, 2);
		var strYear = stDate.substr(6);
		
		if(stDate != null && stDate != '' && endDate != null && endDate != ''){
		 	if (compareDates(stDate,"dd/MM/yyyy",endDate,"dd/MM/yyyy") == 1) {
				alert("�ѹ���������鹵�ͧ���¡��� �ѹ�������ش");
				oRows[rowToCompare].cells["endDateJob"].childNodes[0].value = '';
				oRows[rowToCompare].cells["startDateJob"].childNodes[0].focus();
			}
	 	}
	 
	 }
	 
	 function compareEndDateJob(object){
	 	 while (object.tagName !=  'TR')
		 {
		 object = object.parentNode
		 }
	
		 var rowToCompare = object.rowIndex
		
     	DWRUtil.useLoadingMessage("Loading ...");
		var table = document.getElementById("table");
		//alert(table.);
		var tdName;
		var chkName;
		
		var oRows = table.rows;
	
		var stDate = oRows[rowToCompare].cells["startDateJob"].childNodes[0].value;
		var endDate = oRows[rowToCompare].cells["endDateJob"].childNodes[0].value;
		
		
		var endDay = endDate.substr(0, 2);
		var endMonth = endDate.substr(3, 2);
		var endYear = endDate.substr(6);
		
		if(stDate != null && stDate != '' && endDate != null && endDate != ''){
		 	if (compareDates(stDate,"dd/MM/yyyy",endDate,"dd/MM/yyyy") == 1) {
				alert("�ѹ���������鹵�ͧ���¡��� �ѹ�������ش");
				oRows[rowToCompare].cells["startDateJob"].childNodes[0].value = '';
				oRows[rowToCompare].cells["endDateJob"].childNodes[0].focus();
			}
	 	}
	}
	
	function compareStDateEdunRowUpdate(data){
	 	var tab = $('dataTable');
		var row;
		var update ;
		var empList=[];
		var frm   = document.forms["formEdit"];
		//alert('leng  :'+tab.rows.length);	
		
		if(tab.rows.length > 0){
			var keySeq 		= frm.elements["keySeq"];
			var startDateEdun 	= frm.elements["startDateEdun"];
			var endDateEdun 	= frm.elements["endDateEdun"];
		
			for(var c=0; c<tab.rows.length; c++){
				//alert(c+' :: '+keySeq[c].value + ':::'+ data);
				if(keySeq[c].value == data ){
					rowModify = c;
				//	alert('value in row :'+keySeq[c].value+' : '+flagPr[c].value+' : '+yearWork[c].value+' : '+periodWork[c].value+' : '+fiscalYear[c].value+' : '+startDate[c].value+' : '+endDate[c].value+' : '+totDay1[c].value+' : '+totDay15[c].value+' : '+totDay3[c].value+' : '+seqData[c].value+' : '+codeSeqNew[c].value+' : '+flagWork[c].value+' : '+orgCode[c+1].value);
					break;
				}
			}
		//	alert(rowModify);
		
			var stDate = startDateEdun[rowModify].value;
			var enDate = endDateEdun[rowModify].value;
		
	
		
		
			var endDay = enDate.substr(0, 2);
			var endMonth = enDate.substr(3, 2);
			var endYear = enDate.substr(6);
			
		
		 	if(enDate != null && enDate != '' && stDate != null && stDate != ''){
		 		if (compareDates(stDate,"dd/MM/yyyy",endDate,"dd/MM/yyyy") == 1) {
					alert("�ѹ���������鹵�ͧ���¡��� �ѹ�������ش");
					
					endDateEdun[rowModify].value = '';
					startDateEdun[rowModify].focus();
				}
		 	}
	 
	 }
	 }
	
	function compareEndDateEdunRowUpdate(data){
	 	var tab = $('dataTable');
		var row;
		var update ;
		var empList=[];
		var frm   = document.forms["formEdit"];
		//alert('leng  :'+tab.rows.length);	
		
		if(tab.rows.length > 0){
			var keySeq 		= frm.elements["keySeq"];
			var startDateEdun 	= frm.elements["startDateEdun"];
			var endDateEdun 	= frm.elements["endDateEdun"];
		
			for(var c=0; c<tab.rows.length; c++){
				//alert(keySeq[c].value + ':::'+ data);
				if(keySeq[c].value == data ){
					rowModify = c;
				//	alert('value in row :'+keySeq[c].value+' : '+flagPr[c].value+' : '+yearWork[c].value+' : '+periodWork[c].value+' : '+fiscalYear[c].value+' : '+startDate[c].value+' : '+endDate[c].value+' : '+totDay1[c].value+' : '+totDay15[c].value+' : '+totDay3[c].value+' : '+seqData[c].value+' : '+codeSeqNew[c].value+' : '+flagWork[c].value+' : '+orgCode[c+1].value);
					break;
				}
			}
			
		
			var stDate = startDateEdun[rowModify].value;
			var enDate = endDateEdun[rowModify].value;
		
	
		
		
			var endDay = enDate.substr(0, 2);
			var endMonth = enDate.substr(3, 2);
			var endYear = enDate.substr(6);
			
	
		 	
		 	if(enDate != null && enDate != '' && stDate != null && stDate != ''){
		 		if (compareDates(stDate,"dd/MM/yyyy",endDate,"dd/MM/yyyy") == 1) {
					alert("�ѹ���������鹵�ͧ���¡��� �ѹ�������ش");
					startDateEdun[rowModify].value = '';
					endDateEdun[rowModify].focus();
				}
		 	}
	 
	 }
	 }
	 
	 function compareStDateBackRowUpdate(data){
	 	var tab = $('dataTable');
		var row;
		var update ;
		var empList=[];
		var frm   = document.forms["formEdit"];
		//alert('leng  :'+tab.rows.length);	
		
		if(tab.rows.length > 0){
			var keySeq 		= frm.elements["keySeq"];
			var startDateBack 	= frm.elements["startDateBack"];
			var endDateBack 	= frm.elements["endDateBack"];
		
			for(var c=0; c<tab.rows.length; c++){
				//alert(keySeq[c].value + ':::'+ data);
				if(keySeq[c].value == data ){
					rowModify = c;
				//	alert('value in row :'+keySeq[c].value+' : '+flagPr[c].value+' : '+yearWork[c].value+' : '+periodWork[c].value+' : '+fiscalYear[c].value+' : '+startDate[c].value+' : '+endDate[c].value+' : '+totDay1[c].value+' : '+totDay15[c].value+' : '+totDay3[c].value+' : '+seqData[c].value+' : '+codeSeqNew[c].value+' : '+flagWork[c].value+' : '+orgCode[c+1].value);
					break;
				}
			}
			
		
			var stDate = startDateBack[rowModify].value;
			var enDate = endDateBack[rowModify].value;
		
	
		
		
			var endDay = enDate.substr(0, 2);
			var endMonth = enDate.substr(3, 2);
			var endYear = enDate.substr(6);
			
	
		 	
		 	if(enDate != null && enDate != '' && stDate != null && stDate != ''){
		 		if (compareDates(stDate,"dd/MM/yyyy",endDate,"dd/MM/yyyy") == 1) {
					alert("�ѹ���������鹵�ͧ���¡��� �ѹ�������ش");
					startDateBack[rowModify].focus();
					endDateBack[rowModify].value = '';
				}
		 	}
	 
	 }
	 }
	
	function compareEndDateBackRowUpdate(data){
	 	var tab = $('dataTable');
		var row;
		var update ;
		var empList=[];
		var frm   = document.forms["formEdit"];
		//alert('leng  :'+tab.rows.length);	
		
		if(tab.rows.length > 0){
			var keySeq 		= frm.elements["keySeq"];
			var startDateBack 	= frm.elements["startDateBack"];
			var endDateBack 	= frm.elements["endDateBack"];
		
			for(var c=0; c<tab.rows.length; c++){
				//alert(keySeq[c].value + ':::'+ data);
				if(keySeq[c].value == data ){
					rowModify = c;
				//	alert('value in row :'+keySeq[c].value+' : '+flagPr[c].value+' : '+yearWork[c].value+' : '+periodWork[c].value+' : '+fiscalYear[c].value+' : '+startDate[c].value+' : '+endDate[c].value+' : '+totDay1[c].value+' : '+totDay15[c].value+' : '+totDay3[c].value+' : '+seqData[c].value+' : '+codeSeqNew[c].value+' : '+flagWork[c].value+' : '+orgCode[c+1].value);
					break;
				}
			}
			
		
			var stDate = startDateBack[rowModify].value;
			var enDate = endDateBack[rowModify].value;
		
	
		
		
			var endDay = enDate.substr(0, 2);
			var endMonth = enDate.substr(3, 2);
			var endYear = enDate.substr(6);
			
	
		 	
		 	if(enDate != null && enDate != '' && stDate != null && stDate != ''){
		 		if (compareDates(stDate,"dd/MM/yyyy",endDate,"dd/MM/yyyy") == 1) {
					alert("�ѹ���������鹵�ͧ���¡��� �ѹ�������ش");
					startDateBack[rowModify].value = '';
					endDateBack[rowModify].focus();
				}
		 	}
	 
	 }
	 }
	 
	 
	 function compareStDateJobRowUpdate(data){
	 	var tab = $('dataTable');
		var row;
		var update ;
		var empList=[];
		var frm   = document.forms["formEdit"];
		//alert('leng  :'+tab.rows.length);	
		
		if(tab.rows.length > 0){
			var keySeq 		= frm.elements["keySeq"];
			var startDateBack 	= frm.elements["startDateJob"];
			var endDateBack 	= frm.elements["endDateJob"];
		
			for(var c=0; c<tab.rows.length; c++){
				//alert(keySeq[c].value + ':::'+ data);
				if(keySeq[c].value == data ){
					rowModify = c;
				//	alert('value in row :'+keySeq[c].value+' : '+flagPr[c].value+' : '+yearWork[c].value+' : '+periodWork[c].value+' : '+fiscalYear[c].value+' : '+startDate[c].value+' : '+endDate[c].value+' : '+totDay1[c].value+' : '+totDay15[c].value+' : '+totDay3[c].value+' : '+seqData[c].value+' : '+codeSeqNew[c].value+' : '+flagWork[c].value+' : '+orgCode[c+1].value);
					break;
				}
			}
			
		
			var stDate = startDateJob[rowModify].value;
			var enDate = endDateJob[rowModify].value;
		
	
		
		
			var endDay = enDate.substr(0, 2);
			var endMonth = enDate.substr(3, 2);
			var endYear = enDate.substr(6);
			
	
		 	
		 	if(enDate != null && enDate != '' && stDate != null && stDate != ''){
		 		if (compareDates(stDate,"dd/MM/yyyy",endDate,"dd/MM/yyyy") == 1) {
					alert("�ѹ���������鹵�ͧ���¡��� �ѹ�������ش");
					startDateJob[rowModify].focus();
					endDateJob[rowModify].value = '';
				}
		 	}
	 
	 }
	 }
	
	function compareEndDateJobRowUpdate(data){
	 	var tab = $('dataTable');
		var row;
		var update ;
		var empList=[];
		var frm   = document.forms["formEdit"];
		//alert('leng  :'+tab.rows.length);	
		
		if(tab.rows.length > 0){
			var keySeq 		= frm.elements["keySeq"];
			var startDateBack 	= frm.elements["startDateJob"];
			var endDateBack 	= frm.elements["endDateJob"];
		
			for(var c=0; c<tab.rows.length; c++){
				//alert(keySeq[c].value + ':::'+ data);
				if(keySeq[c].value == data ){
					rowModify = c;
				//	alert('value in row :'+keySeq[c].value+' : '+flagPr[c].value+' : '+yearWork[c].value+' : '+periodWork[c].value+' : '+fiscalYear[c].value+' : '+startDate[c].value+' : '+endDate[c].value+' : '+totDay1[c].value+' : '+totDay15[c].value+' : '+totDay3[c].value+' : '+seqData[c].value+' : '+codeSeqNew[c].value+' : '+flagWork[c].value+' : '+orgCode[c+1].value);
					break;
				}
			}
			
		
			var stDate = startDateJob[rowModify].value;
			var enDate = endDateJob[rowModify].value;
		
	
		
		
			var endDay = enDate.substr(0, 2);
			var endMonth = enDate.substr(3, 2);
			var endYear = enDate.substr(6);
			
	
		 	
		 	if(enDate != null && enDate != '' && stDate != null && stDate != ''){
		 		if (compareDates(stDate,"dd/MM/yyyy",endDate,"dd/MM/yyyy") == 1) {
					alert("�ѹ���������鹵�ͧ���¡��� �ѹ�������ش");
					startDateJob[rowModify].value = '';
					endDateJob[rowModify].focus();
				}
		 	}
	 
	 }
	 }
-->
</script>
</head>
 
<body>
<form name="formEdit"  action="security.htm?reqCode=CTRWMT008" method="post">
<input type="hidden" name="dataLength">
<input type="hidden" name="codeSeq">
<input type="hidden" name="period">
<table width="100%">
	<tr>
		<td class="font-head">
			[ CTRWUP008 ] ��Ѻ��ا��¡���Ѻ (�Թ��͹)
		</td>
	</tr>
</table>
<table width="800" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
		
		<td>
			
		</td>
		</tr>
      <tr>
          <td  align="center">
				<table width="750" border="1" cellpadding="1" bordercolor="#6699CC" cellspacing="1"  align="center">
        			<tr>
          				<td class="font-field"  align="right" width="100">��Шӻ�</td>
          				<td align="left"><input type="text" name="year" value="" readonly="readonly" style="width:70;text-align:center;background-color:silver;"/></td>
          				<td colspan="3" class="font-field" align="left" width="250">
          					&nbsp;&nbsp;�Ǵ&nbsp;&nbsp;
          					<input type="text" name="section" value="" readonly="readonly" style="width:70;text-align:center;background-color:silver;"/>
          				</td>
        			</tr>
        			<tr>
         				<td class="font-field"  align="right">�Ţ��Шӵ��</td>
          				<td align="left" width="15"><input type="text" name="empCode" readonly="readonly" style="width:150;text-align:center;background-color:silver;"/></td>
          				<td align="left"><input type="text" name="name" readonly="readonly" style="width:260;background-color:silver;"/></td>
        				<td class="font-field"  align="right" width="65">���˹�</td>
          				<td align="left" ><input type="text" name="positionShort" value="" readonly="readonly" style="width: 150;text-align: center;background-color:silver;"  /></td>
          				
       			 	</tr>
       			 	<tr>
          				<td class="font-field"  align="right">�ѧ�Ѵ��Ժѵԧҹ</td>
          				<td align="left" width="30"><input type="text" name="orgCode" value="" readonly="readonly" style="width: 150;background-color: silver;text-align: center;"  /></td>
          				<td colspan="3" align="left"><input type="text" name="orgDesc" readonly="readonly" style="width:485;background-color: silver;"/></td>
       			 	</tr>
     		 </table>
          </td>
      </tr>
    	&nbsp;
	 	&nbsp;
	  	&nbsp;
	  <tr>
          <td align="center">
				<div style="height:345px;width:892;overflow:auto;">
				 &nbsp;
	  			&nbsp;
	  			&nbsp;
				<table id="table"  bordercolor="#4A4A4A" width="1500" align="center" style="text-align: center" border="1" cellpadding="0" cellspacing="0" >
					<thead style="text-align: center">
						<tr CLASS="TABLEBULE2">
							<th CLASS="TABLEBULE2" style="width:50px" rowspan="2" align="center">ź</th>
							<th CLASS="TABLEBULE2" rowspan="2" align="center">��������¡��</th>
							<th CLASS="TABLEBULE2" style="width:140px" rowspan="2" align="center">���ԡ��è�����/��Ѻ�ҷӧҹ���� �����</th>
							<th CLASS="TABLEBULE2" colspan="6" rowspan="1" align="center"><center>���ԡ��Ѻ�ز�</center></th>
							<th CLASS="TABLEBULE2" style="width:250px" colspan="2" rowspan="1" align="center"><center>���ԡ��Ѻ�ҷӧҹ����������Ѻ�ز�</center></th>
							<th CLASS="TABLEBULE2" colspan="3" rowspan="1" align="center"><center>���ԡ�ѡ�ҹ</center></th>
							<th CLASS="TABLEBULE2" style="width:250px" rowspan="1" align="center"><center>���ԡ�Ԩ�óҤ����դ����ͺ��͹��ѧ</center></th>
						</tr>
						<tr CLASS="TABLEBULE2"  >
							<th CLASS="TABLEBULE2" align="center">�է�����ҳ</th>
							<th CLASS="TABLEBULE2" align="center">�����</th>
							<th CLASS="TABLEBULE2" align="center">�֧</th>
							<th CLASS="TABLEBULE2" style="width:150px" align="center">�Թ��͹���</th>
							<th CLASS="TABLEBULE2" style="width:150px" align="center">�Թ��͹����</th>
							<th CLASS="TABLEBULE2" style="width:150px" align="center">�س�زԷ���Ѻ</th>
							<th CLASS="TABLEBULE2" align="center">�����</th>
							<th CLASS="TABLEBULE2" align="center">�֧</th>
							<th CLASS="TABLEBULE2" align="center">�����</th>
							<th CLASS="TABLEBULE2" align="center">�֧</th>
							<th CLASS="TABLEBULE2" align="center">�ӹǹ���</th>
							<th CLASS="TABLEBULE2" align="center">�ӹǹ���</th>
						</tr>
					</thead>
					<tbody id="dataTable">
					</tbody>					
						<tr id="temprow" style="visibility:hidden;position:absolute">
							<td id="flag"><input type="checkbox" name="chk" /></td>
							<td id="flagPr">
										<select name="type">
												<!-- <option value="N" >����</option> -->
												<option value="A" >��Ѻ��ا��¡���Ѻ</option>
												<!--<option value="R" >��¡���Ѻ���¡�׹</option>-->
												<!-- <option value="B" >���ԡ��Ѻ��ا��¡���Ѻ</option>
												<option value="S" >���ԡ��¡���Ѻ���¡�׹</option> -->
										</select>
							</td>
							<td id="startDateNew" ><input type="text"  name="startDateNew" size="20" maxlength="10" align="center" style="text-align:center;width:100%" onchange="validateDate(this);" ></td>
							<td id="fiscalYear" ><input type="text"  name="fiscalYear" value="<%=yearEdit%>" size="6" maxlength="4" style="text-align:center;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"/></td>
							<td id="startDateEdun" ><input type="text"  name="startDateEdun" size="12" maxlength="10" align="center" style="text-align:center;" onchange="return validateDate(this) && compareStDateEdun(this);"></td>
							<td id="endDateEdun" ><input type="text"  name="endDateEdun" size="12" maxlength="10" align="center" style="text-align:center;" onchange="return validateDate(this) && compareEndDateEdun(this);"></td>
							
							<td id="oldSal" ><select name="type" style="width:100%;scroll-y:true" >
												<option value=""  ></option>
												
												<c:forEach items="${SalaryList}" var="result">
													<option  value="<c:out value='${result.salary}' />" ><c:out value='${result.salary}' /></option>
												</c:forEach>
												
										</select></td>
							<td id="newSal" ><select name="type" style="width:100%;scroll-y:true" >
												<option value=""  ></option>
												
												<c:forEach items="${SalaryList}" var="result">
													<option  value="<c:out value='${result.salary}' />" ><c:out value='${result.salary}' /></option>
												</c:forEach>
												
										</select>
							</td>
							<td id="eduWut">
										<select name="type" style="width:100%;">
												<option value="" ></option>
												<option value="5" >��ӡ��� �.���</option>
												<option value="6" >�.��բ���</option>
												<!-- <option value="3" >1.5 ���</option>
												<option value="4" >2.0 ���</option> -->
										</select>
							</td>
							
							<td id="startDateBack" ><input type="text"  name="startDateBack" size="14" maxlength="10" align="center" style="text-align:center;" onchange="return validateDate(this) && compareStDateBack(this);"></td>
							<td id="endDateBack" ><input type="text"  name="endDateBack" size="14" maxlength="10" align="center" style="text-align:center;" onchange="return validateDate(this) && compareEndDateBack(this);"></td>
							<td id="startDateJob" ><input type="text"  name="startDateJob" size="12" maxlength="10" align="center" style="text-align:center;" onchange="return validateDate(this) && compareStDateJob(this);"></td>
							<td id="endDateJob" ><input type="text"   name="endDateJob" size="12" maxlength="10" align="center" style="text-align:center;" onchange="return validateDate(this) && compareEndDateJob(this);"><input type="hidden"  name="keySeq" /></td>
							<td id="multipleLevel">
										<select name="type">
												<option value="" ></option>
												<option value="1" >0.5 ���</option>
												<option value="2" >1.0 ���</option>
										</select>
							</td>
							<td id="backStep">
										<select name="type" style="width:100%;">
												<option value="" ></option>
												<option value="1" >0.5 ���</option>
												<option value="2" >1.0 ���</option>
												<option value="3" >1.5 ���</option>
												<option value="4" >2.0 ���</option>
										</select>
							</td>
							</tr>
					</table>
				</div>
		</td>
      </tr>
          
</table>&nbsp;

<input type="hidden" name="pageEdit" value="">
<table width="100%" CLASS="TABLEBULE2" >
					<tr CLASS="TABLEBULE2" >
						<td align="left" >&nbsp;
							<input type="Button" class=" button " value="����������" name="insertData" onclick="addVisualRow();"/>						
							<input type="Button" class=" button " value="ź������" name="deleteData" onclick="removeVisualRow();"/>						
							<input type="Button" class=" button " value="��ŧ" name="confirmData" onclick="onUpdate();"/>
							<input type="Button" class=" button " value="  �͡  " name="back" onclick="backMain();"/>
						</td>
					</tr>
				</table>
</form>
	<form name="backForm" action="security.htm?reqCode=CTRWMT008" method="post">
		<input type="hidden" name="orgFromEdit" />
		<input type="hidden" name="orgToEdit" />
		<input type="hidden" name="empCodeFromEdit" />
		<input type="hidden" name="empCodeToEdit" />
		<input type="hidden" name="pageEdit" />
		<input type="hidden" name="orderFromCboEdit" />
		<input type="hidden" name="orderToCboEdit" />
		<input type="hidden" name="mustQuery" />
	</form>
</body>
<script type="text/javascript">
		
</script>
</html>

<SCRIPT LANGUAGE="JavaScript">
<!--
function addVisualRow(){

	var table = document.getElementById("table");
	var tempRow = document.getElementById("temprow");

	// Insert two rows.
   	var oTable = table;
   	var idx = oTable.rows.length;
   	var oRow1=oTable.insertRow(idx);
   	var str=" " ;
	var from = "[0]";
	var to = "("+(oRow1.rowIndex+tempRow.rowIndex)+")";

	// Insert cells into row.
	oRow1.id = tempRow.id;
	for(i=0;i<tempRow.cells.length;i++){
		var oCell=oRow1.insertCell(i);
		// Add regular HTML values to the cells.
		
		
		oCell.id = tempRow.cells[i].id;
		oCell.innerHTML=tempRow.cells[i].innerHTML;  
		str = oCell.innerHTML;
		if(str.indexOf(from)>0){
			oCell.innerHTML=str.replace(from, to);
		}
	}
	
}

//this function removeVisualRow() used page UPDATE ONLY!!!
	var chDelete = false;
	var chcCon = false;
	function removeVisualRow(){
		var tab = $('dataTable');
		var row;
		var empList=[];
		var frm = document.forms["formEdit"];
		var chk = frm.elements["chk"];
		
		var table = document.getElementById("table");
		var tdName;
		var chkName;
		var num = 2 + parseInt(DWRUtil.getValue("dataLength"));
		
		var oRows = table.rows;
		if(tdName == null)tdName="flag";
		if(chkName == null)chkName="chk";
		//alert(oRows.length+" : "+num);
		
		
		for(x=oRows.length-1;x > num;x--){
						if (oRows[x].cells[tdName].childNodes[0].checked){
								chcCon = true;	
							}
					}
		if(tab.rows.length>1){
						for(i=0; i<tab.rows.length; i++){
							row = tab.rows[i];	
							if (chk[i].checked){
									chcCon = true;
								}
							
								
						}
					}else{
						if(tab.rows.length==1){
							row = tab.rows[0];	
							if (chk[0].checked){
									chcCon = true;
								}
						  
						}	
					}
					
		
		
		
		
		if(chcCon){
			var answer = confirm("��ͧ���ź������ �������?");
			if( answer ){
		
				for(i=oRows.length-1;i > num;i--){
						if (oRows[i].cells[tdName].childNodes[0].checked){
								table.deleteRow(i);	
								chDelete = true;		
							}
					}
					
				DWREngine.beginBatch();
					//alert('123435  :'+tab.rows.length);
				    if(tab.rows.length>1){
						for(i=0; i<tab.rows.length; i++){
							row = tab.rows[i];	
							if (chk[i].checked){
									
									rwModSal.keySeq = parseInt(frm.elements["keySeq"][i].value);
									RwModSalService.deleteRwModSal(rwModSal, {callback:onDeleteCallback});
									chDelete = true;
								}
							
								
						}
					}else{
						if(tab.rows.length==1){
							row = tab.rows[0];	
							if (chk[0].checked){
									
									rwModSal.keySeq = parseInt(frm.elements["keySeq"][0].value);
									RwModSalService.deleteRwModSal(rwModSal, {callback:onDeleteCallback});
									chDelete = true;
								}
						  
						}	
					}
					
					DWREngine.endBatch();
		
			}
		}
		
		
		
		if(chDelete){
			alert('ź���������º����');
			whenQueryData();
		}
	}
	
	function onDeleteCallback()
	{
		//whenQueryData();
	}

//-->
</SCRIPT>