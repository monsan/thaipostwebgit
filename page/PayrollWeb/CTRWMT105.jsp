<%@ page language="java" contentType="text/html;charset=TIS-620" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Locale"%>
<%@ page import="com.ss.tp.security.UserInfo" %>
<%@ page import="com.ss.tp.dto.DefaultYearSectionVO" %>

<%
	UserInfo userInfo = (UserInfo)session.getAttribute("UserLogin");
	String userId = userInfo.getUserId();
	String ouCode = userInfo.getOuCode();
	
	DefaultYearSectionVO defaultYear = (DefaultYearSectionVO)session.getAttribute("DefaultYearAndSection");
	boolean canDeleteData = ((Boolean)session.getAttribute("canDeleteData")).booleanValue();
	boolean isConfirm = ((Boolean)session.getAttribute("isConfirm")).booleanValue();
	
	System.out.println("canDeleteData : " + canDeleteData);
	System.out.println("isConfirm : " + isConfirm);
%>
<html>
<head>
<title>ź�����š�èѴ���Թ��͹</title>
<!-- Include -->
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript" src="dwr/util.js"></script>
<!-- Javascript Script File -->
<SCRIPT type="text/javascript" src="dwr/interface/PrPeriodLineService.js"></SCRIPT>
<SCRIPT type="text/javascript" src="dwr/interface/PrEmployeeTextService.js"></SCRIPT>
<script type="text/javascript" src="script/gridScript.js"></script>
<script type="text/javascript" src="script/payrollComboBox.js"></script>
<script type="text/javascript" src="page/NavigatePage.jsp"></script>
<script type="text/javascript" src="script/dojo.js"></script>

<script type="text/javascript">
	//Load Dojo's code relating to widget managing functions
	dojo.require("dojo.widget.*");
	dojo.require("dojo.widget.Menu2");
	dojo.require("dojo.widget.Button");
	dojo.require("dojo.widget.ComboBox");
	dojo.require("dojo.widget.DropDownButton");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.ContentPane");
	dojo.require("dojo.widget.LayoutContainer");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.Toolbar");
	dojo.require("dojo.widget.html.*");
	dojo.require("dojo.widget.Menu2");
	dojo.hostenv.writeIncludes();
	
	//Event
	dojo.require("dojo.event.*");
</script>

<script type="text/javascript">

	function init()
	{
		$("year").value = '<%= defaultYear.getYear() %>';
		$("section").value = '<%= defaultYear.getSection() %>';
		$("period").value = '<%= defaultYear.getPeriod() %>';
		$("isConfirm").value = '<%= isConfirm %>';
	
		if( $("isConfirm").value == 'false' )
			document.forms['searchForm'].elements['add'].disabled = true;
		
		if( '<%= canDeleteData %>' == 'false' ){
			document.forms['searchForm'].elements['add'].disabled = true;
			alert("����ź ���ͧ��ǹ��ҧ�ѧ������Ǻ���������");
		}
	
		//PrPeriodLineService.getDefaultYearAndSection('<%=ouCode%>', '<%=userId%>', {callback:onLoadYearSectionCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
	}
	
    /*function onLoadYearSectionCallback(data)
	{
		$("year").value = data.year;
		$("section").value = data.section;
		$("period").value = data.period;
	}*/
	
	function whenShowDataTable()
	{
		//alert( 'yes' );
		
		allOrder = 0;
		
		PrEmployeeTextService.deleteConfirmIncome
		(
			'<%=userId%>',
			'<%=ouCode%>',
			DWRUtil.getValue("year"),
			DWRUtil.getValue("period"),
			{callback:whenListDataTableHandler,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}}
		);
	}
	
	var allOrder = 0;
	
	var cellFuncs = [
		function(data) { return "<div align='left'>"+data.incDecName+"</div>";},
		function(data) { allOrder += parseInt(data.countOt); return "<div align='right'>"+data.countOt+"&nbsp;</div>";}
	];
	
	function whenListDataTableHandler(data)
	{
		//alert('dataSize ' + data.length);
		//alert('xxx');
		if(data.length > 0){
			DWRUtil.removeAllRows("dataTable1");
			DWRUtil.addRows("dataTable1",data,cellFuncs);
		}else{
			//alert('No Data');
			DWRUtil.removeAllRows("dataTable1");
		}
		
		whenShowDataTable2();
	}
	
	function whenShowDataTable2()
	{
		PrEmployeeTextService.deleteConfirmDeduct
		(
			'<%=userId%>',
			'<%=ouCode%>',
			DWRUtil.getValue("year"),
			DWRUtil.getValue("period"),
			{callback:whenListDataTable2Handler,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}}
		);
	}
	
	var cellFuncs2 = [
		function(data) { return "<div align='left'>"+data.incDecName+"</div>";},
		function(data) { allOrder += parseInt(data.countOt); return "<div align='right'>"+data.countOt+"&nbsp;</div>";}
	];
	
	function whenListDataTable2Handler(data)
	{
		//alert('dataSize ' + data.length);
		//alert('yyy');
		if(data.length > 0){
			DWRUtil.removeAllRows("dataTable2");
			DWRUtil.addRows("dataTable2",data,cellFuncs2);
		}else{
			var tab = $('dataTable1');
			if(tab.rows.length == 0){
				alert('��辺������');
			}
			DWRUtil.removeAllRows("dataTable2");
		}
		
		$("allData").value = allOrder;
	}
			
	// Load page
  	dojo.addOnLoad(init);
	
</script>
<%
	Calendar now = Calendar.getInstance(Locale.US);
	String year = ((now.get(Calendar.YEAR)+543)+"");
	String keySeq  = request.getParameter("keySeq");
%>
</head>
<body>
<table width="100%">
	<tr>
		<td class="font-head">
			[ CTRWMT105 ] ź�����Ũҡ��÷��Թ��͹
		</td>
	</tr>
</table>
<form name="searchForm" action="" method="post">
<input type="hidden" name="period"> 
<input type="hidden" name="confirm"> 
<br><br>
<table border="1" align="center" cellpadding="1" bordercolor="#6699CC" cellspacing="1">
			<tr>
				<td>
					<table width="350" border="0" align="center" cellspacing="1">
					  <tr>
					    <td class="font-field" align="right">��Шӻ�&nbsp;&nbsp;&nbsp;</td>
					    <td align="left"><input type="text" name="year"   value="" readonly="readonly" style="width:70;text-align:center;background-color:silver;"/></td>
					    <td  class="font-field" align="right">�Ǵ&nbsp;&nbsp;&nbsp;</td>
					    <td align="left">
					    	<input type="text" name="section"  value="" readonly="readonly" style="width:70;text-align:center;background-color:silver;"/>
					    	<INPUT type="hidden" name="isConfirm" />
					    </td>
					  </tr>
					</table>
					<center>__________________________________________________________________ </center>
					<br>
					<table width="400" border="0" align="left" cellspacing="1">
					  <tr>
					    <td class="font-field" align="right">�ӹǹ�����ŷ�����&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					    <td align="left"><input type="text" name="allData"   value="" readonly="readonly" style="width:100;text-align:center;background-color:silver;"/></td>
					    <td class="font-field" align="left">��¡��</td>
					  </tr>
					</table>
				</td>
			</tr>
		</table>
<br><br>
<table width="840" align="center" cellpadding="2" cellspacing="2">
<tr>
	<td valign="top">
	<FIELDSET>
    		<LEGEND>�Թ��</LEGEND>
    		<br>
    		<table width="350"  border="1" bordercolor="#6699CC"  align="center"  cellpadding="2" cellspacing="0">
			  	<thead>
					<tr CLASS="TABLEBULE2">
						<th CLASS="TABLEBULE2">����</th>
						<th CLASS="TABLEBULE2">�ӹǹ��¡��</th>
					<tr>
				</thead>
				<tbody id="dataTable1">
				</tbody>
			</table>
			<br>
    </FIELDSET>
	</td>
	<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td valign="top">
	<FIELDSET>
    		<LEGEND>�Թ�ѡ</LEGEND>
    		<br>
    		<table width="350"  border="1" bordercolor="#6699CC"  align="center"  cellpadding="2" cellspacing="0">
			  	<thead>
					<tr CLASS="TABLEBULE2">
						<th CLASS="TABLEBULE2">����</th>
						<th CLASS="TABLEBULE2">�ӹǹ��¡��</th>
					<tr>
				</thead>
				<tbody id="dataTable2">
				</tbody>
			</table>
			<br>
    </FIELDSET>	
	</td>
</tr>
</table>
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" class=" button "  value="��ŧ" name="add" onclick="whenShowDataTable();"/>

</body>
</html>