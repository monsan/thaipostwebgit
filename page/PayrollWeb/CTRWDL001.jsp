<%@ page language="java" contentType="text/html;charset=TIS-620"%>
<%@ page import="com.ss.tp.security.UserInfo" %>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>
<%
	UserInfo userInfo = (UserInfo)session.getAttribute("UserLogin");
	String userId = userInfo.getUserId();
	String ouCode = userInfo.getOuCode();
	
	String year = (String)request.getParameter("yearDel");
	String period = (String)request.getParameter("periodDel");
	String section = (String)request.getParameter("sectionDel");
	String isConfirm = (String)request.getParameter("isConfirmDel");
	//String empCode = (String)request.getParameter("empCodeEdit");
	
	String orgFromEdit = request.getParameter("orgFromDel");
	String orgToEdit = request.getParameter("orgToDel");
	String empCodeFromEdit = request.getParameter("empCodeFromDel");
	String empCodeToEdit = request.getParameter("empCodeToDel");
	String pageEdit = request.getParameter("pageDel");
	
	if( pageEdit.trim().equals("") )
		pageEdit = "-1";
	
	//isConfirm = "true";
	System.out.println("isConfirm : " + isConfirm);
%>
<html>
	<head>
		<title>�ѹ�֡��������ѡ㹡�èѴ���Թ��͹</title>
		<!-- Include -->
		<script type="text/javascript" src="dwr/engine.js"></script>
		<script type="text/javascript" src="dwr/util.js"></script>
		<!-- Javascript Script File -->
		<SCRIPT type="text/javascript" src="dwr/interface/SuUserOrganizationService.js"></SCRIPT>
		<SCRIPT type="text/javascript" src="dwr/interface/VPnOrganizationSecurityService.js"></SCRIPT>
		<SCRIPT type="text/javascript" src="dwr/interface/PrPeriodLineService.js"></SCRIPT>
		<SCRIPT type="text/javascript" src="dwr/interface/PnEmployeeService.js"></SCRIPT>
		<SCRIPT type="text/javascript" src="dwr/interface/PrEmployeeService.js"></SCRIPT>
		<SCRIPT type="text/javascript" src="dwr/interface/PrEmployeeTextService.js"></SCRIPT>
		
		<script type="text/javascript" src="script/gridScript.js"></script>
		<script type="text/javascript" src="page/NavigatePage.jsp"></script>
		<script type="text/javascript" src="script/dojo.js"></script>
		<script type="text/javascript" src="script/json.js"></script>
		<script type="text/javascript" src="script/payroll_util.js"></script>

		<script type="text/javascript">
			//Load Dojo's code relating to widget managing functions
			dojo.require("dojo.widget.*");
			dojo.require("dojo.widget.Menu2");
			dojo.require("dojo.widget.Button");
			dojo.require("dojo.widget.ComboBox");
			dojo.require("dojo.widget.DropDownButton");
			dojo.require("dojo.widget.SortableTable");
			dojo.require("dojo.widget.ContentPane");
			dojo.require("dojo.widget.LayoutContainer");
			dojo.require("dojo.widget.SortableTable");
			dojo.require("dojo.widget.Toolbar");
			dojo.require("dojo.widget.html.*");
			dojo.require("dojo.widget.Menu2");
			dojo.hostenv.writeIncludes();
			
			//Event
			dojo.require("dojo.event.*");
		</script>
		<script type="text/javascript">
		    
			
			/**** BEGIN When Employee Select ****/
		    function onLoad(){
		    	DWRUtil.useLoadingMessage("Loading ...");
		    	$("year").value = '<%=year%>';
		    	$("period").value = '<%=period%>';
		    	$("section").value = '<%=section%>';
		    	$("isConfirm").value = '<%=isConfirm%>';
		    	
		    	
		    }
		    
		    function whenEmpBlur()
		    {
		    	//alert("test");
		    	if( $("empCode").value != '' ){
			    	DWRUtil.useLoadingMessage("Loading ...");
			    	//var cboEmp = dojo.widget.byId("empCode");
			    	PnEmployeeService.findByCriteriaInSecueEmpText('<%=userId%>', '<%=ouCode%>', $("empCode").value, $("year").value, $("period").value,{callback:whenLoadPnEmployeeDeatilCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ�������������');}});
		    	}
		    }
		    
		    function whenLoadPnEmployeeDeatilCallback(data){
		    	//alert( data.toJSONString() );
		    
		    	if( data.firstName != null )
		    	{
			    	$("empName").value = checkNull(data.prefixName,'STRING') + ' ' + checkNull(data.firstName,'STRING') + ' ' + checkNull(data.lastName,'STRING');
			    	$("account").value = checkNull(data.account,'STRING');
			    	$("positionName").value = checkNull(data.position,'STRING');
			    	$("adminDesc").value = checkNull(data.adminDesc,'STRING');
			    	$("levelCode").value = checkNull(data.levelCode,'STRING');
			    	$("pDate").value = checkNull(data.PDate,'STRING');
			    	$("codeSeq").value = checkNull(data.codeSeq,'STRING');
			    	$("codeSeqName").value = checkNull(data.orgActDesc,'STRING');
			    	
			    	dojo.widget.byId("orgCode").textInputNode.value = checkNull(data.orgCode,'STRING') + ' ' + checkNull(data.orgDesc,'STRING');
			    	$("hOrgCode").value = checkNull(data.orgCode,'STRING');
			    	
			    	//$("orgDesc").value = checkNull(data.orgDesc,'STRING'); // + ' ' + checkNull(data.orgDesc,'STRING');
			    	
			    	$("codeSeqAct").value = checkNull(data.codeSeqAct,'STRING');
			    	$("hCodeSeqAct").value = checkNull(data.codeSeqAct,'STRING');
			    	
			    	PrEmployeeService.findPrEmployee('<%=ouCode%>', $("year").value, $("period").value, $("empCode").value, '<%=userId%>',{callback:whenPrEmployeeLoadCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ���������þ��');}});
		    	}else
		    		alert("��辺���������ʾ�ѡ�ҹ");
		    }
		    function whenPrEmployeeLoadCallback(data){
		    
		    	var empName = $("empName").value.replace(' ','space');
		    	if( empName != 'space' ){
			    	
			    	$("delete").disabled = false;
			    	
			    	// �Ţ��Шӵ�Ǽ����������
			    	$("taxId").value = checkNull(data.taxId,'STRING');
			    	$("hTaxId").value = checkNull(data.taxId,'STRING');
			    	
			    	/* $("bankBranch").value = checkNull(data.bankBranch,'STRING');
			    	$("hBankBranch").value = checkNull(data.bankBranch,'STRING');
			    	
			    	$("bankId").value = checkNull(data.bankId,'STRING');
			    	$("hBankId").value = checkNull(data.bankId,'STRING');
			    	
			    	  	// ���͸�Ҥ��
	 		    	var bankCode = document.getElementById("bankCode");
	 		    	//alert("bankCode : " + data.bankCode);
	 		    	if( data.bankCode == '001' ){
	 		    		bankCode.selectedIndex = 0;
	 		    		$("hBankCode").value = data.bankCode;
	 		    	}else if (data.bankCode=='002'){
	 		    		bankCode.selectedIndex = 1;
	 		    		$("hBankCode").value = data.bankCode;
	 		    	} */
			    	
			    	// ʶҹС���Ѻ�Թ��͹
	 		    	var payStatus = document.forms["searchForm"].elements["payStatus"];
	 		    	//alert("payStatus : " + data.payStatus );
	 		    	if( data.payStatus == '1' ){
	 		    		payStatus[0].checked = true;
	 		    		$("hPayStatus").value = data.payStatus;
	 		    	}else if( data.payStatus == '2' ){
	 		    		payStatus[1].checked = true;
	 		    		$("hPayStatus").value = data.payStatus;
	 		    	}else{
	 		    		payStatus[0].checked = true;
	 		    	}
	 		    	
	 		    	// ʶҹС�è����Թ��͹
	 		    	var flagPr = document.getElementById("flagPr");
	 		    	if( data.flagPr == '0' ){
	 		    		flagPr.selectedIndex = 0;
	 		    		$("hFlagPr").value = data.flagPr;
	 		    	}else if( data.flagPr == '1' ){
	 		    		flagPr.selectedIndex = 1;
	 		    		$("hFlagPr").value = data.flagPr;
	 		    	}else if( data.flagPr == '2' ){
	 		    		flagPr.selectedIndex = 2;
	 		    		$("hFlagPr").value = data.flagPr;
	 		    	}
	 		    	
	 		    	// ʶҹ��ҾŴ���͹
	 		    	var marriedStatus = document.getElementById("marriedStatus");
	 		    	//alert("marriedStatus : " + data.marriedStatus);
	 		    	if( data.marriedStatus == '1' ){
	 		    		marriedStatus.selectedIndex = 0;
	 		    		$("hMarriedStatus").value = data.marriedStatus;
	 		    	}else if( data.marriedStatus == '2' ){
	 		    		marriedStatus.selectedIndex = 1;
	 		    		$("hMarriedStatus").value = data.marriedStatus;
	 		    	}else if( data.marriedStatus == '3' ){
	 		    		marriedStatus.selectedIndex = 2;
	 		    		$("hMarriedStatus").value = data.marriedStatus;
	 		    	}else if( data.marriedStatus == '4' ){
	 		    		marriedStatus.selectedIndex = 3;
	 		    		$("hMarriedStatus").value = data.marriedStatus;
	 		    	}
			    	
			    	$("costChild").value = checkNull(data.costChild,'STRING');
			    	$("hCostChild").value = checkNull(data.costChild,'STRING');
			    	
			    	$("childStudy").value = checkNull(data.childStudy,'STRING');
			    	$("hChildStudy").value = checkNull(data.childStudy,'STRING');
			    	
			    	$("childNoStudy").value = checkNull(data.childNoStudy,'STRING');
			    	$("hChildNoStudy").value = checkNull(data.childNoStudy,'STRING');

			    
			    
			    	
			    	var gundanFlag = document.getElementById("gundanFlag");
			    	if( data.gundanFlag == 'Y' ){
			    		gundanFlag.checked = true;
			    		$("hGundanFlag").value = data.gundanFlag;
			    	}else{
			    		gundanFlag.checked = false;
			    		$("hGundanFlag").value = data.gundanFlag;
			    	}
			    	
			    	var flagFather = document.getElementById("flagFather");
			    	if( data.flagFather == 'Y' ){
			    		flagFather.checked = true;
			    		$("hFlagFather").value = data.flagFather;
			    	}else{
			    		flagFather.checked = false;
			    		$("hFlagFather").value = data.flagFather;
			    	}
			    	
			    	var flagFatherSpouse = document.getElementById("flagFatherSpouse");
			    	if( data.flagFatherSpouse == 'Y' ){
			    		flagFatherSpouse.checked = true;
			    		$("hFlagFatherSpouse").value = data.flagFatherSpouse;
			    	}else{
			    		flagFatherSpouse.checked = false;
			    		$("hFlagFatherSpouse").value = data.flagFatherSpouse;
			    	}
			    	
			    	var flagMother = document.getElementById("flagMother");
			    	if( data.flagMother == 'Y' ){
			    		flagMother.checked = true;
			    		$("hFlagMother").value = data.flagMother;
			    	}else{
			    		flagMother.checked = false;
			    		$("hFlagMother").value = data.flagMother;
			    	}
			    	
			    	var flagMotherSpouse = document.getElementById("flagMotherSpouse");
			    	if( data.flagMotherSpouse == 'Y' ){
			    		flagMotherSpouse.checked = true;
			    		$("hFlagMotherSpouse").value = data.flagMotherSpouse;
			    	}else{
			    		flagMotherSpouse.checked = false;
			    		$("hFlagMotherSpouse").value = data.flagMotherSpouse;
			    	}
			    	
			    	$("costLife").value = checkNull(data.costLife,'STRING');
			    	$("hCostLife").value = checkNull(data.costLife,'STRING');
			    	
			    	var gundanFlag = document.getElementById("gundanFlag");
		    		if( data.gundanFlag == 'Y' )
		    			gundanFlag.checked = true;
		    		else
		    			gundanFlag.checked = false;
			    	
			    	$("debtLife").value = checkNull(data.debtLife,'STRING');
			    	$("hDebtLife").value = checkNull(data.debtLife,'STRING');
			    	
			    	$("debtLifeSpouse").value = checkNull(data.debtLifeSpouse,'STRING');
			    	$("hDebtLifeSpouse").value = checkNull(data.debtLifeSpouse,'STRING');
			    	
			    	
			    	$("debtLoan").value = checkNull(data.debtLoan,'STRING');
			    	$("hDebtLoan").value = checkNull(data.debtLoan,'STRING');
			    	
			    	$("donate").value = checkNull(data.donate,'STRING');
			    	$("hDonate").value = checkNull(data.donate,'STRING');
			    	
			    	$("incomeTax").value = checkNull(data.incomeTax,'STRING');
			    	$("hIncomeTax").value = checkNull(data.incomeTax,'STRING');
			    	
			    	$("pensionFund").value = checkNull(data.pensionFund,'STRING');
			    	$("hPensionFund").value = checkNull(data.pensionFund,'STRING');
			    	
			    	$("rmf").value = checkNull(data.rmf,'STRING');
			    	$("hRmf").value = checkNull(data.rmf,'STRING');
			    	
			    	$("ltf").value = checkNull(data.ltf,'STRING');
			    	$("hLtf").value = checkNull(data.ltf,'STRING');
			    	
			    	//$("teacherFund").value = checkNull(data.teacherFund,'STRING');
			    	//$("hTeacherFund").value = checkNull(data.teacherFund,'STRING');
			    	
			    	$("compensateLabour").value = checkNull(data.compensateLabour,'STRING');
			    	$("hCompensateLabour").value = checkNull(data.compensateLabour,'STRING');
			    	
			    	//$("overage").value = checkNull(data.overage,'STRING');
			    	//$("hOverage").value = checkNull(data.overage,'STRING');
			    	
			    	$("overageSpouse").value = checkNull(data.overageSpouse,'STRING');
			    	$("hOverageSpouse").value = checkNull(data.overageSpouse,'STRING');
			    	
			    	$("healthFather").value = checkNull(data.healthFather,'STRING');
			    	$("hHealthFather").value = checkNull(data.healthFather,'STRING');
			    	
			    	$("handicappedDec").value = checkNull(data.handicappedDec,'STRING');
			    	$("hHandicappedDec").value = checkNull(data.handicappedDec,'STRING');
			    
			    }
		    }
		     function clearScreen(){
		    
		    	$("empName").value = '';
		    	$("account").value = '';
		    	$("positionName").value = '';
		    	$("adminDesc").value = '';
		    	$("levelCode").value = '';
		    	$("pDate").value = '';
		    	$("codeSeq").value = '';
		    	$("codeSeqName").value = '';
		    	var cbo = dojo.widget.byId("orgCode");
		    	cbo.textInputNode.value = '';
		    	//$("orgDesc").value = '';
		    
		    	$("taxId").value = '';
		    	
 		    	var payStatus = document.forms["searchForm"].elements["payStatus"];
 		    	payStatus[0].checked = false;
 		    	payStatus[1].checked = false;
 		    	//$("bankBranch").value = '';
 		    	//$("bankId").value = '';
 		    	
 		    	// ʶҹС�è����Թ��͹
 		    	var flagPr = document.getElementById("flagPr");
 		    	flagPr.selectedIndex = 0;
 		    	
 		    	//var bankCode = document.getElementById("bankCode");
 		    	//bankCode.selectedIndex = 0;
 		    	
 		    	// ʶҹ��ҾŴ���͹
 		    	var marriedStatus = document.getElementById("marriedStatus");
 		    	marriedStatus.selectedIndex = 0;
 		    	
 		    	$("costChild").value = '';
		    	$("childStudy").value = '';
		    	$("childNoStudy").value = '';
		    	
		    	var gundanFlag = document.getElementById("gundanFlag");
		    	gundanFlag.checked = false;
		    	
		    	var flagFather = document.getElementById("flagFather");
		    	flagFather.checked = false;
		    		
		    	var flagFatherSpouse = document.getElementById("flagFatherSpouse");
		    	flagFatherSpouse.checked = false;
		    	
		    	var flagMother = document.getElementById("flagMother");
		    	flagMother.checked = false;
		    	
		    	var flagMotherSpouse = document.getElementById("flagMotherSpouse");
		    	flagMotherSpouse.checked = false;
		    	
		    	$("costLife").value = '';
		    	$("debtLife").value = '';
		    	$("debtLifeSpouse").value = '';
		    	$("debtLoan").value = '';
		    	$("donate").value = '';
		    	$("incomeTax").value = '';
		    	$("pensionFund").value = '';
		    	$("rmf").value = '';
		    	$("ltf").value = '';
		    	//$("teacherFund").value = '';
		    	$("compensateLabour").value = '';
		    	//$("overage").value = '';
		    	$("overageSpouse").value = '';
		    	//$("oldKlongChev").value = '';
		    	//$("newKlongChev").value = '';
		    	$("healthFather").value = '';
		    	$("handicappedDec").value = '';
		    	$("delete").disabled = true;
		    }
		    /**** END When Employee Select ****/
		    
		    
		    
		    function whenFindOrganization(){
		    	DWRUtil.useLoadingMessage("Loading ...");
		    	SuUserOrganizationService.findPrOrganizationByCriteria('<%=userId%>','<%=ouCode%>', splitCombo( dojo.widget.byId("orgCode").textInputNode.value ), $("year").value, $("period").value, {callback:whenFindOrganizationCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
		    }
		    function whenFindOrganizationCallback(data){
		    	$("codeSeqAct").value = data.codeSeq;
		    	//alert(data.toJSONString());
		    	//$("orgDesc").value = checkNull(data.divDesc,'STRING') + ' ' + checkNull(data.secDesc,'STRING');
		    }
		    	
		    var prEmpTextVo = {	keySeq:null, 
		    					ouCode:null,
								year:null,
								period:null,
								empCode:null,
								codeSeqWork:null,
								taxId:null,
								marriedStatus:null,
								payStatus:null,
								bankId:null,
								costChild:null,
								childStudy:null,
								childNoStudy:null,
								costLife:null,
								gundanFlag:null,
								debtLife:null,
								debtLifeSpouse:null,
								debtLoan:null,
								donate:null,
								other:null,
								incomeTax:null,
								oldSalary:null,
								newSalary:null,
								adjOldsal:null,
								adjNewsal:null,
								gundanAmt:null,
								flagPr:null,
								deductAmt:null,
								flagStatus:null,
								seqData:null,
								creBy:'<%=userId%>',
								updBy:'<%=userId%>',
								flagFather:null,
								flagMother:null,
								flagFatherSpouse:null,
								flagMotherSpouse:null,
								rmf:null,
								ltf:null,
								pensionFund:null,
								teacherFund:null,
								overage:null,
								overageSpouse:null,
								compensateLabour:null,
								confirmFlag:'N',
								newKlongChev:null,
								bankBranch:null,
								bankCode:null,
								healthFather:null,
								handicappedDec:null};
		    
		    function save()
		    {
		    	var empName = Trim($("empName").value);
		    	//var orgDesc = Trim($("orgDesc").value);
		    	
		    	var cbo = dojo.widget.byId("orgCode");
		    	
				if( cbo.textInputNode.value == '' )
		    	{
			    	alert("�ô��͡�ѧ�Ѵ��Ժѵԧҹ���١��ͧ");
		    	}else
		    	{
		    		VPnOrganizationSecurityService.findByKeyPK('<%=ouCode%>','<%=userId%>', splitCombo( dojo.widget.byId("orgCode").textInputNode.value ),{callback:whenCheckCanSaveCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
				}
		    }
		    
		    function whenCheckCanSaveCallback(data)
		    {
		    	var cbo = dojo.widget.byId("orgCode");
		    	
		    	if(data == null)
		    	{
		    		alert("�ѧ�Ѵ��Ժѵԧҹ���١��ͧ");
		    	}else
		    	{
		    		var empName = Trim($("empName").value);
		    		//alert($("empName").value);
		    		if( empName != '' && cbo.textInputNode.value != '' )
			    	{
				    	prEmpTextVo.keySeq = '';
				    	prEmpTextVo.ouCode = '<%=ouCode%>';
						prEmpTextVo.year = $("year").value; 
						prEmpTextVo.period = $("period").value;
						prEmpTextVo.empCode = $("empCode").value;
						
						//if( !isEqualsValue($("codeSeqAct").value, $("hCodeSeqAct").value) )
						prEmpTextVo.codeSeqWork = $("codeSeqAct").value;
						
						if( !isEqualsValue($("taxId").value, $("hTaxId").value) )
							prEmpTextVo.taxId = $("taxId").value;
						
						if( !isEqualsValue($("marriedStatus").value, $("hMarriedStatus").value) )
							prEmpTextVo.marriedStatus = $("marriedStatus").value;
							
						if( !isEqualsValue($("payStatus").value, $("hPayStatus").value) )
							prEmpTextVo.payStatus = $("payStatus").value;
							
						
	
						/* if( !isEqualsValue($("bankId").value, $("hBankId").value) )
							prEmpTextVo.bankId = $("bankId").value;
						 */
						if( !isEqualsValue($("costChild").value, $("hCostChild").value) )
							prEmpTextVo.costChild = $("costChild").value;
						
						if( !isEqualsValue($("childStudy").value, $("hChildStudy").value) )
							prEmpTextVo.childStudy = $("childStudy").value;
							
						if( !isEqualsValue($("childNoStudy").value, $("hChildNoStudy").value) )
							prEmpTextVo.childNoStudy = $("childNoStudy").value;
							
						if( !isEqualsValue($("costLife").value, $("hCostLife").value) )
							prEmpTextVo.costLife = $("costLife").value;
	
						//if( !isEqualsValue($("gundanFlag").value, $("hGundanFlag").value) )
						//	prEmpTextVo.gundanFlag = $("gundanFlag").value;
						
						if( !isEqualsValue($("debtLife").value, $("hDebtLife").value) )
							prEmpTextVo.debtLife = $("debtLife").value;
						
						if( !isEqualsValue($("debtLifeSpouse").value, $("hDebtLifeSpouse").value) )
							prEmpTextVo.debtLifeSpouse = $("debtLifeSpouse").value;
	
						if( !isEqualsValue($("debtLoan").value, $("hDebtLoan").value) )
							prEmpTextVo.debtLoan = $("debtLoan").value;
	
						if( !isEqualsValue($("donate").value, $("hDonate").value) )
							prEmpTextVo.donate = $("donate").value;
	
						//if( !isEqualsValue($("other").value, $("hOther").value) )
						//	prEmpTextVo.other = $("other").value;
	
						if( !isEqualsValue($("incomeTax").value, $("hIncomeTax").value) )
							prEmpTextVo.incomeTax = $("incomeTax").value;
	
						//if( !isEqualsValue($("oldSalary").value, $("oldSalary").value) )
						//	prEmpTextVo.oldSalary = '';
	
						//if( !isEqualsValue($("debtLoan").value, $("hDebtLoan").value) )
						//	prEmpTextVo.newSalary = '';
	
						//if( !isEqualsValue($("debtLoan").value, $("hDebtLoan").value) )
						//	prEmpTextVo.adjOldsal = '';
	
						//if( !isEqualsValue($("debtLoan").value, $("hDebtLoan").value) )
						//	prEmpTextVo.adjNewsal = '';
	
						//prEmpTextVo.gundanAmt = '';
						
						if( !isEqualsValue($("flagPr").value, $("hFlagPr").value) )
							prEmpTextVo.flagPr = $("flagPr").value;
	
						//prEmpTextVo.deductAmt = '';
						//prEmpTextVo.flagStatus = '';
						
						//if( !isEqualsValue($("flagPr").value, $("hFlagPr").value) )
						//	prEmpTextVo.seqData = '';
	
						if( $("gundanFlag").checked ){
							//alert($("hGundanFlag").value);
							if( !isEqualsValue('Y', $("hGundanFlag").value) )
								prEmpTextVo.gundanFlag = 'Y';
						}else{
							if( !isEqualsValue('N', $("hGundanFlag").value) )
								prEmpTextVo.gundanFlag = 'N';
						}
	
						if( $("flagFather").checked ){
							if( !isEqualsValue('Y', $("hFlagFather").value) )
								prEmpTextVo.flagFather = 'Y';
						}else{
							if( !isEqualsValue('N', $("hFlagFather").value) )
								prEmpTextVo.flagFather = 'N';
						}
						
						if( $("flagMother").checked ){
							if( !isEqualsValue('Y', $("hFlagMother").value) )
								prEmpTextVo.flagMother = 'Y';
						}else{
							if( !isEqualsValue('N', $("hFlagMother").value) )
								prEmpTextVo.flagMother = 'N';
						}
						
						if( $("flagFatherSpouse").checked ){
							if( !isEqualsValue('Y', $("hFlagFatherSpouse").value) )
								prEmpTextVo.flagFatherSpouse = 'Y';
						}else{	
							if( !isEqualsValue('N', $("hFlagFatherSpouse").value) )
								prEmpTextVo.flagFatherSpouse = 'N';
						}
							
						if( $("flagMotherSpouse").checked ){
							if( !isEqualsValue('Y', $("hFlagMotherSpouse").value) )
								prEmpTextVo.flagMotherSpouse = 'Y';
						}else{
							if( !isEqualsValue('N', $("hFlagMotherSpouse").value) )
								prEmpTextVo.flagMotherSpouse = 'N';
						}
	
						if( !isEqualsValue($("rmf").value, $("hRmf").value) )
							prEmpTextVo.rmf = $("rmf").value;
	
						if( !isEqualsValue($("ltf").value, $("hLtf").value) )
							prEmpTextVo.ltf = $("ltf").value;
	
						if( !isEqualsValue($("pensionFund").value, $("hPensionFund").value) )
							prEmpTextVo.pensionFund = $("pensionFund").value;
						
						//if( !isEqualsValue($("teacherFund").value, $("hTeacherFund").value) )
						//	prEmpTextVo.teacherFund = $("teacherFund").value;
						
						//if( !isEqualsValue($("overage").value, $("hOverage").value) )
						//	prEmpTextVo.overage = $("overage").value;
						
						if( !isEqualsValue($("overageSpouse").value, $("hOverageSpouse").value) )
							prEmpTextVo.overageSpouse = $("overageSpouse").value;
						
						if( !isEqualsValue($("compensateLabour").value, $("hCompensateLabour").value) )
							prEmpTextVo.compensateLabour = $("compensateLabour").value;
						
						//alert( prEmpTextVo.toJSONString() );
						if( !isEqualsValue($("healthFather").value, $("hHealthFather").value) )
							prEmpTextVo.healthFather = $("healthFather").value;
						
						
						if( !isEqualsValue($("handicappedDec").value, $("hHandicappedDec").value) )
							prEmpTextVo.handicappedDec = $("handicappedDec").value;
						
						prEmpTextVo.flagStatus = 'A';
						
						PrEmployeeTextService.insertPrEmployeeText(prEmpTextVo, {callback:whenInsertPrEmployeeText,errorHandler:function(message) { alert('�������ö�ѹ�֡��');}});
					}else{
						alert("�ô��͡ �Ţ��Шӵ�� ���� �ѧ�Ѵ��Ժѵԧҹ���١��ͧ");
					}
			    }
		    }
		    
		   
		    
		    function whenInsertPrEmployeeText(data)
		    {
		    	alert("�ѹ�֡���������º����");
		    	//gotoMTPage();
		    	
		    	clearScreen();
		    	
		    	prEmpTextVo = {	keySeq:null, 
		    					ouCode:null,
								year:null,
								period:null,
								empCode:null,
								codeSeqWork:null,
								taxId:null,
								marriedStatus:null,
								payStatus:null,
								bankId:null,
								costChild:null,
								childStudy:null,
								childNoStudy:null,
								costLife:null,
								gundanFlag:null,
								debtLife:null,
								debtLifeSpouse:null,
								debtLoan:null,
								donate:null,
								other:null,
								incomeTax:null,
								oldSalary:null,
								newSalary:null,
								adjOldsal:null,
								adjNewsal:null,
								gundanAmt:null,
								flagPr:null,
								deductAmt:null,
								flagStatus:null,
								seqData:null,
								creBy:'<%=userId%>',
								updBy:'<%=userId%>',
								flagFather:null,
								flagMother:null,
								flagFatherSpouse:null,
								flagMotherSpouse:null,
								rmf:null,
								ltf:null,
								pensionFund:null,
								teacherFund:null,
								overage:null,
								overageSpouse:null,
								compensateLabour:null,
								confirmFlag:'N',
								newKlongChev:null,
								bankBranch:null,
								bankCode:null,
								healthFather:null,
								handicappedDec:null};
							  
					document.getElementById("empCode").value = '';
					document.getElementById("empCode").focus();
		    }
		    
		    function onDelete(){
		    	var answer = confirm("��ͧ���ź������ �������?");
		    	if( answer ){
			    	var empName = $("empName").value.replace(' ','space');
			    	//alert( empName );
			    	if( empName != 'space' ){
				    	prEmpTextVo.keySeq = '';
				    	prEmpTextVo.ouCode = '<%=ouCode%>';
						prEmpTextVo.year = $("year").value; 
						prEmpTextVo.period = $("period").value;
						prEmpTextVo.empCode = $("empCode").value;
						
						//if( !isEqualsValue($("codeSeqAct").value, $("hCodeSeqAct").value) )
						prEmpTextVo.codeSeqWork = $("codeSeqAct").value;
						
						//if( !isEqualsValue($("taxId").value, $("hTaxId").value) )
							prEmpTextVo.taxId = $("taxId").value;
						
						//if( !isEqualsValue($("marriedStatus").value, $("hMarriedStatus").value) )
							prEmpTextVo.marriedStatus = $("marriedStatus").value;
							
						//if( !isEqualsValue($("payStatus").value, $("hPayStatus").value) )
							prEmpTextVo.payStatus = $("payStatus").value;
	
						//if( !isEqualsValue($("bankId").value, $("hBankId").value) )
						
						/*     prEmpTextVo.bankBranch = $("bankBranch").value;
							prEmpTextVo.bankId = $("bankId").value;
							prEmpTextVo.bankCode = $("bankCode").value;
						 */
						//if( !isEqualsValue($("costChild").value, $("hCostChild").value) )
							prEmpTextVo.costChild = $("costChild").value;
						
						//if( !isEqualsValue($("childStudy").value, $("hChildStudy").value) )
							prEmpTextVo.childStudy = $("childStudy").value;
							
						//if( !isEqualsValue($("childNoStudy").value, $("hChildNoStudy").value) )
							prEmpTextVo.childNoStudy = $("childNoStudy").value;
							
						//if( !isEqualsValue($("costLife").value, $("hCostLife").value) )
							prEmpTextVo.costLife = $("costLife").value;
	
						//if( !isEqualsValue($("gundanFlag").value, $("hGundanFlag").value) )
							prEmpTextVo.gundanFlag = $("gundanFlag").value;
						
						//if( !isEqualsValue($("debtLife").value, $("hDebtLife").value) )
							prEmpTextVo.debtLife = $("debtLife").value;
						
							prEmpTextVo.debtLifeSpouse = $("debtLifeSpouse").value;
	
						//if( !isEqualsValue($("debtLoan").value, $("hDebtLoan").value) )
							prEmpTextVo.debtLoan = $("debtLoan").value;
	
						//if( !isEqualsValue($("donate").value, $("hDonate").value) )
							prEmpTextVo.donate = $("donate").value;
	
						//if( !isEqualsValue($("other").value, $("hOther").value) )
						//	prEmpTextVo.other = $("other").value;
	
						//if( !isEqualsValue($("incomeTax").value, $("hIncomeTax").value) )
							prEmpTextVo.incomeTax = $("incomeTax").value;
	
						//if( !isEqualsValue($("oldSalary").value, $("oldSalary").value) )
						//	prEmpTextVo.oldSalary = '';
	
						//if( !isEqualsValue($("flagPr").value, $("hFlagPr").value) )
							prEmpTextVo.flagPr = $("flagPr").value;
						
	
						if( $("gundanFlag").checked ){
							//alert($("hGundanFlag").value);
							//if( !isEqualsValue('Y', $("hGundanFlag").value) )
								prEmpTextVo.gundanFlag = 'Y';
						}else{
							//if( !isEqualsValue('N', $("hGundanFlag").value) )
								prEmpTextVo.gundanFlag = 'N';
						}
	
						if( $("flagFather").checked ){
							//if( !isEqualsValue('Y', $("hFlagFather").value) )
								prEmpTextVo.flagFather = 'Y';
						}else{
							//if( !isEqualsValue('N', $("hFlagFather").value) )
								prEmpTextVo.flagFather = 'N';
						}
						
						if( $("flagMother").checked ){
							//if( !isEqualsValue('Y', $("hFlagMother").value) )
								prEmpTextVo.flagMother = 'Y';
						}else{
							//if( !isEqualsValue('N', $("hFlagMother").value) )
								prEmpTextVo.flagMother = 'N';
						}
						
						if( $("flagFatherSpouse").checked ){
							//if( !isEqualsValue('Y', $("hFlagFatherSpouse").value) )
								prEmpTextVo.flagFatherSpouse = 'Y';
						}else{	
							//if( !isEqualsValue('N', $("hFlagFatherSpouse").value) )
								prEmpTextVo.flagFatherSpouse = 'N';
						}
							
						if( $("flagMotherSpouse").checked ){
							//if( !isEqualsValue('Y', $("hFlagMotherSpouse").value) )
								prEmpTextVo.flagMotherSpouse = 'Y';
						}else{
							//if( !isEqualsValue('N', $("hFlagMotherSpouse").value) )
								prEmpTextVo.flagMotherSpouse = 'N';
						}
	
						//if( !isEqualsValue($("rmf").value, $("hRmf").value) )
							prEmpTextVo.rmf = $("rmf").value;
	
						//if( !isEqualsValue($("ltf").value, $("hLtf").value) )
							prEmpTextVo.ltf = $("ltf").value;
	
						//if( !isEqualsValue($("pensionFund").value, $("hPensionFund").value) )
							prEmpTextVo.pensionFund = $("pensionFund").value;
						
						//if( !isEqualsValue($("teacherFund").value, $("hTeacherFund").value) )
						//	prEmpTextVo.teacherFund = $("teacherFund").value;
						
						//if( !isEqualsValue($("overage").value, $("hOverage").value) )
						//	prEmpTextVo.overage = $("overage").value;
						
						//if( !isEqualsValue($("overageSpouse").value, $("hOverageSpouse").value) )
							prEmpTextVo.overageSpouse = $("overageSpouse").value;
						
						//if( !isEqualsValue($("compensateLabour").value, $("hCompensateLabour").value) )
							prEmpTextVo.compensateLabour = $("compensateLabour").value;
							
							prEmpTextVo.healthFather = $("healthFather").value;
						
						prEmpTextVo.handicappedDec = $("handicappedDec").value;
						
						prEmpTextVo.flagStatus = 'D';
						
						//alert( prEmpTextVo.toJSONString() );
						
						PrEmployeeTextService.insertPrEmployeeText(prEmpTextVo, {callback:whenDeletePrEmployeeText,errorHandler:function(message) { alert('�������ö�ѹ�֡��');}});
					}else{
						alert("�ô��͡ �Ţ��Шӵ�� ���� �ѧ�Ѵ��Ժѵԧҹ���١��ͧ");
					}
				}
		    }
		    
		    // comment this because move update to service
		   	//function whenDeletePrEmployeeText(data){
		    //	PrEmployeeService.updatePrEmpByPrEmpText(prEmpTextVo, {callback:whenUpdatePrEmployee,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
			//}
		    
		    function whenDeletePrEmployeeText(data){
		    	alert("�ѹ�֡���������º����");
		    	
		    	clearScreen();
		    	
		    	prEmpTextVo = {	keySeq:null, 
		    					ouCode:null,
								year:null,
								period:null,
								empCode:null,
								codeSeqWork:null,
								taxId:null,
								marriedStatus:null,
								payStatus:null,
								bankId:null,
								costChild:null,
								childStudy:null,
								childNoStudy:null,
								costLife:null,
								gundanFlag:null,
								debtLife:null,
								debtLifeSpouse:null,
								debtLoan:null,
								donate:null,
								other:null,
								incomeTax:null,
								oldSalary:null,
								newSalary:null,
								adjOldsal:null,
								adjNewsal:null,
								gundanAmt:null,
								flagPr:null,
								deductAmt:null,
								flagStatus:null,
								seqData:null,
								creBy:'<%=userId%>',
								updBy:'<%=userId%>',
								flagFather:null,
								flagMother:null,
								flagFatherSpouse:null,
								flagMotherSpouse:null,
								rmf:null,
								ltf:null,
								pensionFund:null,
								teacherFund:null,
								overage:null,
								overageSpouse:null,
								compensateLabour:null,
								confirmFlag:'N',
								newKlongChev:null,
								bankBranch:null,
								bankCode:null,
								healthFather:null,
								handicappedDec:null};
							  
					document.getElementById("empCode").value = '';
					document.getElementById("empCode").focus();
		    	
		    	//gotoMTPage();
		    }		    
		    
		    function gotoMTPage(){
		    	document.getElementById("orgFromEdit").value = '<%= orgFromEdit %>';
				document.getElementById("orgToEdit").value = '<%= orgToEdit %>';
				document.getElementById("empCodeFromEdit").value = '<%= empCodeFromEdit %>';
				document.getElementById("empCodeToEdit").value = '<%= empCodeToEdit %>';
				document.getElementById("pageEdit").value = '<%= Integer.parseInt(pageEdit) - 1 %>';
				
				document.forms["goMT"].submit();
		    }
			
			// this function use for compare 2 value is Equals ?
			function isEqualsValue(val1, val2){
				if( val1 == val2 )
					return true;
				else
					return false;
			}
			
			function initOrgCodeCbo()
			{
				try
				{
			     	var cboSource = [];
			     	var orgCodeCbo = dojo.widget.byId("orgCode");
			     	
			     	<c:forEach items="${OrganizationInSecurity}" var="result" >		 
						cboSource.push(["<c:out value='${result.orgCode} ${result.divShort} ${result.areaDesc} ${result.secDesc} ${result.workDesc}' />","<c:out value='${result.orgCode}' />"]);
					</c:forEach>
			     	
			     	orgCodeCbo.dataProvider.setData(cboSource);
			     	
		     	}catch(e){
		     		alert(e.message);
		     	}
			}
			
			function init(){
				//var cboEmp = dojo.widget.byId("empCode");
				//dojo.event.connect(cboEmp, "selectOption", "whenEmpSelectOption");
				//initOrgCodeCbo(); BY AMORN
				
				//var cbo = dojo.widget.byId("orgCode"); BY AMORN
				//dojo.event.connect(cbo, "selectOption", "whenFindOrganization"); BY AMORN
				
				onLoad();
				// ADD BY AMORN
				var cbo = dojo.widget.byId("orgCode");
				cbo.setDisable(true);
				document.forms['searchForm'].elements['empCode'].focus();
				document.forms['searchForm'].elements['taxId'].readOnly = true;
				document.forms['searchForm'].elements['taxId'].style.background = 'silver';
					document.forms['searchForm'].elements['flagPr'].disabled = true;
					document.forms['searchForm'].elements['flagPr'].style.background = 'silver';
				document.forms['searchForm'].elements['marriedStatus'].disabled = true;
					document.forms['searchForm'].elements['marriedStatus'].style.background = 'silver';
					
					
					document.forms['searchForm'].elements['payStatus'][0].disabled = true;
					document.forms['searchForm'].elements['payStatus'][1].disabled = true;
					/* document.forms['searchForm'].elements['bankBranch'].readOnly = true;
					document.forms['searchForm'].elements['bankBranch'].style.background = 'silver';
					
					document.forms['searchForm'].elements['bankId'].readOnly = true;
					document.forms['searchForm'].elements['bankId'].style.background = 'silver';
					
					document.forms['searchForm'].elements['bankCode'].readOnly = true;
					document.forms['searchForm'].elements['bankCode'].style.background = 'silver'; */
					
					document.forms['searchForm'].elements['costChild'].readOnly = true;
					document.forms['searchForm'].elements['costChild'].style.background = 'silver';
					
					document.forms['searchForm'].elements['childStudy'].readOnly = true;
					document.forms['searchForm'].elements['childStudy'].style.background = 'silver';
					
					document.forms['searchForm'].elements['childNoStudy'].readOnly = true;
					document.forms['searchForm'].elements['childNoStudy'].style.background = 'silver';
					
					document.forms['searchForm'].elements['gundanFlag'].disabled = true;
					document.forms['searchForm'].elements['gundanFlag'].style.background = 'silver';
					
					document.forms['searchForm'].elements['flagFather'].disabled = true;
					document.forms['searchForm'].elements['flagFatherSpouse'].disabled = true;
					document.forms['searchForm'].elements['flagMother'].disabled = true;
					document.forms['searchForm'].elements['flagMotherSpouse'].disabled = true;
					
					document.forms['searchForm'].elements['costLife'].readOnly = true;
					document.forms['searchForm'].elements['costLife'].style.background = 'silver';
					
					document.forms['searchForm'].elements['debtLife'].readOnly = true;
					document.forms['searchForm'].elements['debtLife'].style.background = 'silver';
					
					document.forms['searchForm'].elements['debtLifeSpouse'].readOnly = true;
					document.forms['searchForm'].elements['debtLifeSpouse'].style.background = 'silver';
					
					document.forms['searchForm'].elements['debtLoan'].readOnly = true;
					document.forms['searchForm'].elements['debtLoan'].style.background = 'silver';
					
					document.forms['searchForm'].elements['donate'].readOnly = true;
					document.forms['searchForm'].elements['donate'].style.background = 'silver';
					
					document.forms['searchForm'].elements['pensionFund'].readOnly = true;
					document.forms['searchForm'].elements['pensionFund'].style.background = 'silver';
					
					document.forms['searchForm'].elements['incomeTax'].readOnly = true;
					document.forms['searchForm'].elements['incomeTax'].style.background = 'silver';
					
					document.forms['searchForm'].elements['rmf'].readOnly = true;
					document.forms['searchForm'].elements['rmf'].style.background = 'silver';
					
					document.forms['searchForm'].elements['ltf'].readOnly = true;
					document.forms['searchForm'].elements['ltf'].style.background = 'silver';
					
					
					
					document.forms['searchForm'].elements['compensateLabour'].readOnly = true;
					document.forms['searchForm'].elements['compensateLabour'].style.background = 'silver';
					
					//document.forms['searchForm'].elements['overage'].readOnly = true;
					//document.forms['searchForm'].elements['overage'].style.background = 'silver';
					
					document.forms['searchForm'].elements['overageSpouse'].readOnly = true;
					document.forms['searchForm'].elements['overageSpouse'].style.background = 'silver';
					
					document.forms['searchForm'].elements['healthFather'].readOnly = true;
					document.forms['searchForm'].elements['healthFather'].style.background = 'silver';
					
					document.forms['searchForm'].elements['handicappedDec'].readOnly = true;
					document.forms['searchForm'].elements['handicappedDec'].style.background = 'silver';

		
					
				if( <%= isConfirm %> ){ 
					//document.forms['searchForm'].elements['empCode'].readOnly = true;
					//document.forms['searchForm'].elements['empCode'].style.background = 'silver';
					
					document.forms['searchForm'].elements['taxId'].focus();
					
					document.forms['searchForm'].elements['ok'].disabled = true;
					//document.forms['searchForm'].elements['delete'].disabled = true;
					
					var cbo = dojo.widget.byId("orgCode");
					cbo.setDisable(true);
					
					document.forms['searchForm'].elements['taxId'].readOnly = true;
					document.forms['searchForm'].elements['taxId'].style.background = 'silver';
					
					document.forms['searchForm'].elements['flagPr'].disabled = true;
					document.forms['searchForm'].elements['flagPr'].style.background = 'silver';
					//document.forms['searchForm'].elements['flagPr'].style.color = 'black';
										
					document.forms['searchForm'].elements['marriedStatus'].disabled = true;
					document.forms['searchForm'].elements['marriedStatus'].style.background = 'silver';
					
					document.forms['searchForm'].elements['payStatus'][0].disabled = true;
					document.forms['searchForm'].elements['payStatus'][1].disabled = true;
					
					document.forms['searchForm'].elements['bankId'].readOnly = true;
					document.forms['searchForm'].elements['bankId'].style.background = 'silver';
					
					document.forms['searchForm'].elements['incomeTax'].readOnly = true;
					document.forms['searchForm'].elements['incomeTax'].style.background = 'silver';
					
					document.forms['searchForm'].elements['costChild'].readOnly = true;
					document.forms['searchForm'].elements['costChild'].style.background = 'silver';
					
					document.forms['searchForm'].elements['childStudy'].readOnly = true;
					document.forms['searchForm'].elements['childStudy'].style.background = 'silver';
					
					document.forms['searchForm'].elements['childNoStudy'].readOnly = true;
					document.forms['searchForm'].elements['childNoStudy'].style.background = 'silver';
					
					document.forms['searchForm'].elements['gundanFlag'].disabled = true;
					document.forms['searchForm'].elements['gundanFlag'].style.background = 'silver';
					
					document.forms['searchForm'].elements['flagFather'].disabled = true;
					document.forms['searchForm'].elements['flagFatherSpouse'].disabled = true;
					document.forms['searchForm'].elements['flagMother'].disabled = true;
					document.forms['searchForm'].elements['flagMotherSpouse'].disabled = true;
					
					document.forms['searchForm'].elements['costLife'].readOnly = true;
					document.forms['searchForm'].elements['costLife'].style.background = 'silver';
					
					document.forms['searchForm'].elements['debtLife'].readOnly = true;
					document.forms['searchForm'].elements['debtLife'].style.background = 'silver';
					
					document.forms['searchForm'].elements['debtLifeSpouse'].readOnly = true;
					document.forms['searchForm'].elements['debtLifeSpouse'].style.background = 'silver';
					
					
					document.forms['searchForm'].elements['debtLoan'].readOnly = true;
					document.forms['searchForm'].elements['debtLoan'].style.background = 'silver';
					
					document.forms['searchForm'].elements['donate'].readOnly = true;
					document.forms['searchForm'].elements['donate'].style.background = 'silver';
					
					document.forms['searchForm'].elements['pensionFund'].readOnly = true;
					document.forms['searchForm'].elements['pensionFund'].style.background = 'silver';
					
					document.forms['searchForm'].elements['incomeTax'].readOnly = true;
					document.forms['searchForm'].elements['incomeTax'].style.background = 'silver';
					
					document.forms['searchForm'].elements['rmf'].readOnly = true;
					document.forms['searchForm'].elements['rmf'].style.background = 'silver';
					
					document.forms['searchForm'].elements['ltf'].readOnly = true;
					document.forms['searchForm'].elements['ltf'].style.background = 'silver';
					
				
					
					document.forms['searchForm'].elements['compensateLabour'].readOnly = true;
					document.forms['searchForm'].elements['compensateLabour'].style.background = 'silver';
					
					//document.forms['searchForm'].elements['overage'].readOnly = true;
					//document.forms['searchForm'].elements['overage'].style.background = 'silver';
					
					document.forms['searchForm'].elements['overageSpouse'].readOnly = true;
					document.forms['searchForm'].elements['overageSpouse'].style.background = 'silver';
					
					document.forms['searchForm'].elements['healthFather'].readOnly = true;
					document.forms['searchForm'].elements['healthFather'].style.background = 'silver';
					
					document.forms['searchForm'].elements['handicappedDec'].readOnly = true;
					document.forms['searchForm'].elements['handicappedDec'].style.background = 'silver';
					
					
				}
			}
		    
		    dojo.addOnLoad(init);
		    
		    function checkMoney(obj){
		    	
		    	if(obj.value > 100000){
		    		alert('�ӹǹ�Թ�����Թ 100,000 �ҷ');
		    		document.getElementById("debtLife").value = '';
		    	}
		    }
		    
           function checkMoneySpouse(obj){
		    	
		    	if(obj.value > 10000){
		    		alert('�ӹǹ�Թ�����Թ 10,000 �ҷ');
		    		document.getElementById("debtLifeSpouse").value = '';
		    	}
		    }
		
		</script>

	</head>
	<body>
		<table width="100%">
			<tr>
				<td class="font-head">
					[ CTRWDL001 ] ź��������ѡ��ѡ�ҹ/�١��ҧ��Ш��͡�ҡ�ҹ�Թ��͹
				</td>
			</tr>
		</table>
				<form name="searchForm" action="" method="post">
			<!-- Begin Declare Paging -->
			<!-- End Declare Paging -->
		<table width="100%" align="center">
		<tr><td align="center">
			<TABLE border="0" align="left" cellspacing="1">
				<TR>
					<TD width="354px" class="font-field" align="right">��Шӻ�&nbsp;</TD>
					<TD align="left"><INPUT type="text" name="year" value="<%= year %>" readonly="readonly" style="width: 70px;text-align: center;background-color:silver;"></TD>
				    <TD width="46px" class="font-field" align="right">�Ǵ&nbsp;</TD>
				    <TD align="left">
				    	<INPUT type="text" name="section" value="<%= section %>" readonly="readonly" style="width: 70px;text-align: center;background-color:silver;">
				    	<INPUT type="hidden" name="period" value="<%= period %>">
				    	<INPUT type="hidden" name="isConfirm" value="<%=isConfirm %>">
				    </TD>
				</TR>
			</TABLE>
		</td></tr>
		<tr><td align="center">
			<TABLE border="0" align="center" width="920px" cellspacing="1">
				<TR>
					<TD class="font-field" style="text-align:right;" width="200px">�Ţ��Шӵ��</TD>
					<TD width="130px"><INPUT type="text" id="empCode" style="width:130px" maxlength="6" onblur="whenEmpBlur();" onkeypress="clearScreen();" onkeydown="clearScreen();" /></TD>
					<TD colspan="4"><INPUT type="text" id="empName" readonly="readonly" style="width:385px;text-align: left;background-color:silver;" /></TD>
					<TD class="font-field" style="text-align:right;" width="150px">�ѵ���Ţ���</TD>
					<TD width="100px"><INPUT type="text" id="account" readonly="readonly" style="width:100px;text-align: left;background-color:silver;" /></TD>
				</TR>
				<TR>
					<TD class="font-field" style="text-align:right;" width="200px">���˹�</TD>
					<TD width="130px"><INPUT type="text" id="positionName" readonly="readonly" style="width:130px;text-align: left;background-color:silver;" /></TD>
					<TD colspan="4" width="500px">
						<TABLE width="100%" border="0" cellspacing="0">
						<TR>
							<TD class="font-field" style="width:45px;text-align:right;">�дѺ&nbsp;</TD>
							<TD width="70px"><INPUT type="text" id="levelCode" readonly="readonly" style="width: 70px;text-align: left;background-color:silver;" /></TD>
							<TD class="font-field" width="50px" style="text-align:right;">�ҹ�&nbsp;</TD>
							<TD width="230px"><INPUT type="text" id="adminDesc"  readonly="readonly" style="width:221px;text-align: left;background-color:silver;"/></TD>
						</TR>
						</TABLE>
					</TD>
					<TD class="font-field" style="text-align:right;" width="150px">�ѹ����è�</TD>
					<TD><INPUT type="text" id="pDate" readonly="readonly" style="width: 100px;text-align: left;background-color:silver;" /></TD>
				</TR>
				<TR>
					<TD class="font-field" style="text-align:right;" width="200px">�ѧ�Ѵ�觵��</TD>
					<TD colspan="7" width="800px">
						<INPUT type="hidden" id="codeSeq">
						<INPUT type="text" id="codeSeqName" readonly="readonly" style="width:100%;text-align: left;background-color:silver;" />
					</TD>
				</TR>
				<TR>
					<TD class="font-field" style="text-align:right;" width="200px">�ѧ�Ѵ��Ժѵԧҹ</TD>
					<TD width="780px" colspan="8">
						<table border="0" style="width:100%;border:none" cellpadding="0" cellspacing="0"><tr>
							<TD>
								<INPUT type="text" widgetId="orgCode" maxlength="20" dojoType="ComboBox" style="width:730px" onBlurInput="whenFindOrganization();" />
								<INPUT type="hidden" id="codeSeqAct" />
								
								<INPUT type="hidden" id="hOrgCode" />
            <INPUT type="hidden" id="hCodeSeqAct" />
							</TD>
							<!-- <TD  width="620px"><INPUT type="text" id="orgDesc" readonly="readonly" style="width:100%;text-align: left;background-color:silver;" /></TD> -->
						</tr></table>
					</TD>
				
				</TR>
			
				<TABLE border="1" align="center" width="950px" cellspacing="1">
				<TR>
						<TD colspan="8">
						<FIELDSET><LEGEND class="font-field">��������´��è����Թ
     
       </LEGEND>
						<TABLE border="01" width="100%" colspan="3">
							<TR><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD></TR>
					<TD class="font-field" style="text-align:right;" width="400px">�Ţ�����������</TD>
					<TD width="130px">
						<INPUT type="text" id="taxId" maxlength="13" style="width:130px" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);" />
						<INPUT type="hidden" id="hTaxId">
					</TD>
				 <TD class="font-field" style="text-align:right;" width="400px" >ʶҹ��Ѻ�Թ��͹<INPUT type="hidden" id="hPayStatus"> </TD>
					<TD width="130px"><INPUT type="radio" value="1" name="payStatus" checked="checked"/><FONT class="font-field">���¼�ҹ��Ҥ��</FONT></TD>
					<TD width="130px"><input type="radio" value="2" name="payStatus"/><FONT class="font-field">�Թʴ</FONT></TD>
										
							<TD class="font-field" style="text-align:right;" width="450px">ʶҹШ����Թ��͹</TD>
							<TD width="260px">
								<SELECT id="flagPr">
									<OPTION value="0">����</OPTION>
									<OPTION value="1">�ЧѺ��è��·�����</OPTION>
									<OPTION value="2">�������੾���Թ��</OPTION>
								</SELECT>
								<INPUT type="hidden" id="hFlagPr">
							</TD>						
					
				</TR>
				<TR>		       
					
					<!--<TD width="150px" class="font-field" width="350px" align="right">�Ţ���ѭ�ո�Ҥ��</TD>
					<TD width="250px">
						<input type="text" id="bankId" maxlength="10" style="width=150px;"onblur="validBankBranch();" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"/>
						<INPUT type="hidden" id="hBankId">
					</TD>
					<TD width="100px" class="font-field" width="350px" align="right">�����ҢҸ�Ҥ��</TD>
					<TD width="50px">
						<input type="text" id="bankBranch" maxlength="4" style="width=50px;" onblur="validBankBranch();" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"/>
						<INPUT type="hidden" id="hBankBranch">
					</TD>
						<TD class="font-field" style="text-align:right;width:159px">��Ҥ��</TD>
							<TD width="150px">
								<SELECT id="bankCode" style="width:150px">
									<OPTION value="001">��ا��</OPTION>																
								</SELECT>
								<INPUT type="hidden" id="hBankCode">
						</TD>-->
				</TR>
				<TR>
					<TD class="font-field" style="text-align:right;" width="200px">�Թ�ѧ�վ</TD>
					<TD width="130px">
						<!-- <SELECT id="costLife" onblur="validateCostLife();">
							<OPTION value=''></OPTION>
							<OPTION value='800'>800</OPTION>
							<OPTION value='1600'>1600</OPTION>
							<OPTION value='2400'>2400</OPTION>
							<OPTION value='3200'>3200</OPTION>
						</SELECT> -->
						<INPUT id="costLife" type="text" maxlength="9" style="width:100px;text-align:right;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);" />
						<INPUT type="hidden" id="hCostLife">
					</TD>
					<TD class="font-field" style="width:600px;text-align:right;">�ص÷�����Ѻ��Ҫ�������ͺص� </TD>
					<TD width="130px">
						<INPUT type="text" name="costChild" maxlength="5" style="width:123px;text-align:right;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);" />
						<INPUT type="hidden" id="hCostChild">
					</TD>
					<TD width="240px" class="font-field" style="text-align:right;">���¡ѹ���</TD>
					<TD width="30px">
						<input type="checkbox" name="gundanFlag" style="width:30px;disabled ="true" onclick="onGundanChecked();" />
						<INPUT type="hidden" id="hGundanFlag">
					</TD>
					
				</TR>
				<TR>
				  <!-- <TD class="font-field" style="text-align:right;" width="200px">��Ҥ�ͧ�վ���</TD>
					<TD width="130px">
						<INPUT id="oldKlongChev" type="text" maxlength="9" disable="true" style="width:100px;text-align:right; background-color:silver;" readonly="readonly"/>
						<INPUT type="hidden" id="hOldKlongChev">
					</TD>
					<TD class="font-field" style="text-align:right;">��Ҥ�ͧ�վ�Ѩ�غѹ</TD>
					<TD width="130px">
						<INPUT id="newKlongChev" type="text" maxlength="9" disable="true" style="width:100px;text-align:left; background-color:silver;" readonly="readonly"/>
						<INPUT type="hidden" id="hNewKlongChev">
					</TD>-->
					<%-- <TD width="150px" class="font-err" style="text-align:right;">�Ţ���ѭ�ո�Ҥ��</TD>
					<TD width="150px">
						<input type="text" id="bankId" maxlength="10" style="width=150px;"onblur="validBankBranch();" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"/>
						<INPUT type="hidden" id="hBankId">
					</TD>
					<TD width="150px" class="font-err" style="text-align:right;">�����ҢҸ�Ҥ��</TD>
					<TD width="50px">
						<input type="text" id="bankBranch" maxlength="4" style="width=50px;" onblur="validBankBranch();" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"/>
						<INPUT type="hidden" id="hBankBranch">
					</TD>
						<TD class="font-err" style="text-align:right;width:159px">��Ҥ��</TD>
							<TD width="150px">
								<SELECT id="bankCode" style="width:150px">
									<OPTION value="001">��ا��</OPTION>	
									<OPTION value="002">�¾ҳԪ��</OPTION>															
								</SELECT>
								<INPUT type="hidden" id="hBankCode">
						</TD> --%>
				</TR>
			</TABLE>
		</td></tr>
		</TABLE>	
			<BR/>
			<TABLE border="1" align="center" width="950px" cellspacing="0">
				<TR>
					<TD colspan="9">
						<FIELDSET><LEGEND class="font-field">��������´�ӹǳ����
     
       </LEGEND>
						<TABLE border="1" style="text-align:right;width="100%">
							
							<TR>
							<TABLE border="1" width="100%" colspan="3">
							<TD class="font-field" style="text-align:right;width:190px">ʶҹС��Ŵ���͹</TD>
							<TD width="120px">
								<SELECT id="marriedStatus" style="width:150px" >
									<OPTION value="1">�ʴ</OPTION>
									<OPTION value="2">�������������Թ��</OPTION>
									<OPTION value="3">����������Թ��</OPTION>
									<OPTION value="4">�����-����-�ʴ�պصúح����</OPTION>
								</SELECT>
								<INPUT type="hidden" id="hMarriedStatus">
								
							</TD>			
									 				
								<TD class="font-field" style="text-align:right;" >�ӹǹ�ص÷���֡��</TD>
								<TD>
								    <INPUT id="childStudy" type="text" maxlength="5" style="width:100px;text-align:right;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hChildStudy">
								</TD>
								<TD class="font-field" style="text-align:right;" >�ӹǹ�ص÷������֡��</TD>
								<TD colspan="2">
									<INPUT id="childNoStudy" type="text" maxlength="5" style="width:100px;text-align:right;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT id="hChildNoStudy" type="hidden">	
								</TD>													
							</TR>
								<TR>
								<TD></TD>
								<TD align="left">
									<INPUT id="flagFather" type="checkbox" onclick="onFatherChecked();"><FONT class="font-field">�ѡŴ���͹�Դ�</FONT>
									<INPUT type="hidden" id="hFlagFather">
								</TD>
								<TD colspan="4" align="left">
									<INPUT id="flagFatherSpouse" type="checkbox" onclick="onFatherSpouseChecked();"><FONT class="font-field">�ѡŴ���͹�ԴҢͧ������ʷ�����Թ�� ����ӹǳ��������������Թ��</FONT>
									<INPUT type="hidden" id="hFlagFatherSpouse">
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="left">
									<INPUT id="flagMother" type="checkbox" onclick="onMotherChecked();"><FONT class="font-field">�ѡŴ���͹��ô�</FONT>
									<INPUT type="hidden" id="hFlagMother">
								</TD>
								<TD colspan="4" align="left">
									<INPUT id="flagMotherSpouse" type="checkbox" onclick="onMotherSpouseChecked();"><FONT class="font-field">�ѡŴ���͹��ôҢͧ������ʷ�����Թ�� ����ӹǳ��������������Թ��</FONT>
									<INPUT type="hidden" id="hFlagMotherSpouse">
								</TD>
							</TR>
							
								
								<TABLE border="1" width="100%" colspan="3" >
								<TR>
								<TD style="text-align: right; width: 280px;" class="font-field">�ػ��������§�ټ��ԡ��</TD>
								<TD>
									<INPUT id="handicappedDec" type="text" maxlength="9" style="width:100px;text-align:right;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hHandicappedDec">
								</TD>
													
								<TD style="text-align: right; width: 260px;" class="font-field">���»�Сѹ�آ�Ҿ�Դ���ô�</TD>
								<TD>
									<INPUT id="healthFather" type="text" maxlength="9" style="width:100px;text-align:right;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1) ;">
									<INPUT type="hidden" id="hHealthFather">
								</TD>
								</TR>
								<TD style="text-align: right;" class="font-field" width="280px">���»�Сѹ���Ե</TD>
								<TD>
									<INPUT id="debtLife" type="text" maxlength="9" style="width:100px;text-align:right;" onblur="checkMoney(this);" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hDebtLife">
								</TD>
								<TD style="text-align: right;" class="font-field" width="260px">���»�Сѹ���Ե������� �ó�������Թ�� </TD>
								<TD>
									<INPUT id="debtLifeSpouse" type="text" maxlength="9" style="width:100px;text-align:right;" onblur="checkMoneySpouse(this);" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hDebtLifeSpouse">
								</TD>
							
							</TR>
							<TR>
								<TD style="text-align:right;" class="font-field" width="280px">��ҫ���˹���ŧ�ع㹡ͧ�ع������͡������§�վ (RMF)</TD>
								<TD>
									<INPUT id="rmf" type="text" maxlength="9" style="width:100px;text-align:right;" onblur="checkRMFMoney(this);" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hRmf">
								</TD>
								<TD style="text-align:right;" class="font-field" width="260px">��ҫ���˹���ŧ�ع㹡ͧ�ع������������� (LTF)</TD>
								<TD>
									<INPUT id="ltf" type="text" maxlength="9" style="width:100px;text-align:right;" onblur="checkLTFMoney(this);" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hLtf">
								</TD>
							</TR>
							<TR>
							   	<TD style="text-align: right;" class="font-field" width="280px">�͡�����Թ������͡������</TD>
								<TD>
									<INPUT id="debtLoan" type="text" maxlength="9" style="width:100px;text-align:right;" onblur="checkDebtLoanMoney(this);" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hDebtLoan">
								</TD>
								<TD style="text-align: right;" class="font-field" width="260px" > �Թ��ԨҤ</TD>
								<TD>
									<INPUT id="donate" type="text" maxlength="9" style="width:100px;text-align:right;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hDonate">
								</TD>						
							</TR>	
							<TR>	
								<TD style="text-align: right;" class="font-field" >�Թ����� � ����ͧ��������</TD>
								<TD>
									<INPUT id="incomeTax" type="text" maxlength="9" style="width:100px;text-align:right;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hIncomeTax">
								</TD>		
							</TR>			
						</TABLE>
						
						<TABLE border="1" width="100%">
							
							<hr/>
							<TR>
								<TD style="text-align: right;" class="font-field">��� �</TD>
								<TD>
									<INPUT id="compensateLabour" type="text" maxlength="9" style="width:100px;text-align:right;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hCompensateLabour">
								</TD>
								<TD style="text-align: right;" class="font-field">��� �</TD>
								<TD>
									<INPUT id="overageSpouse" type="text" maxlength="9" style="width:100px;text-align:right;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hOverageSpouse">
								</TD>	
									<TD style="text-align:right;" class="font-field" >��� � </TD>
								<TD>
									<INPUT id="pensionFund" type="text" maxlength="9" style="width:100px;text-align:right;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hPensionFund">
								</TD>							
							</TR>
						</TABLE>
						<!--<TABLE border="0" width="100%">
							<TR>
								<TD style="text-align: right; width: 200px;" class="font-field">��� � 1</TD>
								<TD>
									<INPUT id="teacherFund" type="text" maxlength="9" style="width:100px;text-align:right;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hTeacherFund">
								</TD>
								<TD style="text-align: right;" class="font-field">��� � 2</TD>
								<TD>
									<INPUT id="compensateLabour" type="text" maxlength="9" style="width:100px;text-align:right;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hCompensateLabour">
								</TD>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</TR>
							<TR>
								<TD style="text-align: right; width: 200px;" class="font-field">��� � 3</TD>
								<TD>
									<INPUT id="overage" type="text" maxlength="9" style="width:100px;text-align:right;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hOverage">
								</TD>
								<TD style="text-align: right;" class="font-field">��� � 4</TD>
								<TD>
									<INPUT id="overageSpouse" type="text" maxlength="9" style="width:100px;text-align:right;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hOverageSpouse">
								</TD>
								
								<TD style="text-align:right;" class="font-field" >��� � 5</TD>
								<TD>
									<INPUT id="pensionFund" type="text" maxlength="9" style="width:100px;text-align:right;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);">
									<INPUT type="hidden" id="hPensionFund">
								</TD>
							</TR>
						</TABLE>-->
						</FIELDSET>
					</TD>
				</TR>
			</TABLE>
			<TABLE width="100%" CLASS="TABLEBULE2">
				<TR CLASS="TABLEBULE2" >
					<TD align="left" >&nbsp;
						<input type="button" class=" button " style="width: 60px" value="ź" id="delete" name="delete" onclick="onDelete();"/>
						<!-- <input type="button" class=" button " style="width: 60px" value="��ŧ" name="ok" onclick="save();"/> -->
						<input type="button" class=" button " style="width: 60px" value="�͡" name="back" onclick="gotoMTPage();"/>
						<!-- <input type="button" class=" button "  value="Clear" name="Clear" onclick="clearScreen();"/> -->
					</TD>
				</TR>
			</TABLE>
		</form>
		<FORM name="goMT" action="security.htm?reqCode=CTRWMT001" method="post">
			<input type="hidden" name="orgFromEdit" />
			<input type="hidden" name="orgToEdit" />
			<input type="hidden" name="empCodeFromEdit" />
			<input type="hidden" name="empCodeToEdit" />
			<input type="hidden" name="pageEdit" />
		</FORM>
	</body>
</html>
