<%@ page language="java" contentType="text/html;charset=TIS-620"%>
<%@ page import="com.ss.tp.security.UserInfo" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>

<%
	UserInfo userInfo = (UserInfo)session.getAttribute("UserLogin");
	String userId = userInfo.getUserId();
	String ouCode = userInfo.getOuCode();
	Calendar now = Calendar.getInstance(Locale.US);	
	String year =String.valueOf(now.get(Calendar.YEAR));
%>
<html>
<head>
<title>Report New Employees</title>
<!-- You have to include these two JavaScript files from DWR -->
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript" src="dwr/util.js"></script>
<script type="text/javascript" src="script/dojo.js"></script>
<script type="text/javascript" src="script/payroll_util.js"></script>
<!-- This JavaScript file is generated specifically for your application -->
<script type="text/javascript" src="dwr/interface/PnEmpImpService.js"></script>

</head>
<script type="text/javascript">
	dojo.require("dojo.widget.*");	
	dojo.require("dojo.widget.Menu2");
	dojo.require("dojo.widget.Button");
	dojo.require("dojo.widget.ComboBox");
	dojo.require("dojo.widget.DropDownButton");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.ContentPane");
	dojo.require("dojo.widget.LayoutContainer");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.Toolbar");
	dojo.require("dojo.widget.html.*");
	dojo.require("dojo.widget.Menu2");
	dojo.hostenv.writeIncludes();
//Event
	dojo.require("dojo.event.*");
	var empCodeDataFrom=[];
	var empCodeDataTo=[];
	var orgCodeShowDataF=[];
	var orgCodeShowDataT=[];
	var tempWorkCode=[];
	

function initMonth(){
		var workYear = DWRUtil.getValue("workYear");
		var monthFrom='';
		var monthTo='';
		var tempMonth='';
		
		tempMonth=workYear;
		monthTo ='01/'+(parseInt(tempMonth));
		monthFrom = '12/'+tempMonth;
		DWRUtil.setValue("monthFrom",monthTo);
		DWRUtil.setValue("monthTo",monthFrom);
}



dojo.addOnLoad(initMonth);




 // Even ComboBox valueChange

</script>
<body>
<table width="100%" >
<tr>
<td class="font-head">[ CTPPPRP002 ]  ��§ҹ�����ž�ѡ�ҹ/�١��ҧ��ШӾ���Ҿ</td>
</tr>
<tr>  
<td>
<form action="pnEmpImpEngNameOldReport.htm?reqCode=doPrintReport" method="post" name="mainform"  ><div style="height:400px;">
	<table  width="100%" align="center" border="0">
	<tr>
		<td aling ="center"> <input type="hidden" name="ouCode" value="<%=ouCode%>"><input type="hidden" name="userId" value="<%=userId%>">
 			<table width="800" align="center" border="0" cellpadding="2" cellspacing="2">
				<tr>
					<td align="right" class="font-field">��Шӻ�&nbsp;</td>
					<td colspan="3" ><input type="text" name="workYear" style="text-align: center;"  value="<%=year%>" size="4" maxlength="4" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"  onChange="onChangeMonth(this.value);" /></td>
				</tr>
		
				<tr>
					<td class="font-field"  align="right">�������͹/�� (MM/YYYY)&nbsp;</td>
					<td><input type="text" name="monthFrom" style="text-align: center;"  size="7" maxlength="7"/></td>
					<td class="font-field">�֧��͹/�� (MM/YYYY)</td>
					<td ><input type="text" name="monthTo" style="text-align: center;"  size="7" maxlength="7"/></td>
					<input type="hidden" name="workYearFrom" ><input type="hidden" name="workYearTo" >
					<input type="hidden" name="monthTempFrom" ><input type="hidden" name="monthTempTo" >
				</tr>
			
				
				<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
				
					<td colspan="4" align="center"><input type="button" class=" button "  value=" �ʴ���§ҹ " onclick="whenLoadTaReport();" /></td>
				
				</tr>
 			</table>
		</td>
	</tr>

	<table>
	</table>
		
	</form>
</td>
</tr>
</table>
</body>
<script>
	function onChangeMonth(month){
		var monthFrom='';
		var monthTo='';
		var tempMonth='';
		if (month.length < 4){
			alert("�վ.�.��ͧ�� 4 ��ѡ");
		}else{
			tempMonth=month;
			monthTo ='01/'+(parseInt(tempMonth));
			monthFrom = '12/'+tempMonth;
			DWRUtil.setValue("monthFrom",monthTo);
			DWRUtil.setValue("monthTo",monthFrom);
			
		}
	}
	function whenLoadTaReport(){
		var frm = document.forms[0];
		var workYear = DWRUtil.getValue("workYear");
		var monthFrom = DWRUtil.getValue("monthFrom");
		var monthTo  =DWRUtil.getValue("monthTo");
		
		var workYear=frm.workYear.value;
	
		
	
//   for check Month
		var tempMonthFrom ='';
		var tempMonthTo = '';
		
		var tempYearFrom;
		var tempYearTo;
		var tempYear ='';
		var tempYearFront ='';
		var calYear = '';
		
		var calMonthFrom =0;
		var calMonthTo =0;
		var sumMonth = 0;
//     fix choice sql

		if(workYear==''){
 			alert("��سҡ�͡��");
			return false;
		}else if (workYear.length < 4){
			alert("�վ.�.��ͧ�� 4 ��ѡ");
			return false;
		}
		if(monthFrom == '' || monthTo == ''){
			alert("��سҡ�͡���  ��͹/��");
			return false;
		}else{
			tempYear=workYear;
			tempYearFront = workYear;
			if( checkMonth(monthFrom)){
				alert("��سҡ�͡ ��͹/�� ���� ");
				return false;
			}
			
			if( checkMonth(monthTo)){
				alert("��سҡ�͡ ��͹/�� ���� ");
				return false;
			}
			
			if(monthFrom.length ==7 ){
				tempYearFrom = monthFrom.substring(3,monthFrom.length);
				tempMonthFrom = monthFrom.substring(0,monthFrom.length-5);
			
				if(!isNumber(tempMonthFrom)){
					alert("��͹/�� ��ͧ�繵���Ţ ");
					return false;
				}
				if(!isNumber(tempYearFrom)){
					alert("��͹/�� ��ͧ�繵���Ţ");
					
					return false;
				}
				
			}else if(monthFrom.length == 6){
				tempYearFrom = monthFrom.substring(2,monthFrom.length);
				tempMonthFrom = monthFrom.substring(0,monthFrom.length-5);
				alert(tempMonthFrom);
				alert(tempYearFrom);
				if(!isNumber(tempMonthFrom)){
					alert("��͹/�� ��ͧ�繵���Ţ");
					
					return false;
				}
				if(!isNumber(tempYearFrom)){
					alert("��͹/�� ��ͧ�繵���Ţ");
					return false;
				}
			}
			
			if(monthTo.length ==7 ){
				tempYearTo = monthTo.substring(3,monthTo.length);
				tempMonthTo = monthTo.substring(0,monthTo.length-5);
			
				if(!isNumber(tempMonthTo)){
					alert("��͹/�� ��ͧ�繵���Ţ ");
					return false;
				}
				if(!isNumber(tempYearTo)){
					alert("��͹/�� ��ͧ�繵���Ţ");
					return false;
				}
				
			}else if(monthTo.length == 6){
				tempYearTo = monthTo.substring(2,monthTo.length);
				tempMonthTo = monthTo.substring(0,monthTo.length-5);
				if(!isNumber(tempMonthTo)){
					alert("��͹/�� ��ͧ�繵���Ţ");
					return false;
				}
				if(!isNumber(tempYearTo)){
					alert("��͹/�� ��ͧ�繵���Ţ");
					return false;
				}
			}
			
			if(tempYearFrom > tempYearTo){
				alert("��͹/�� ������鹵�ͧ���¡��� ��͹�� ����ش");
				
				
				return false;
			}
			if(tempYearFrom == tempYearTo){
				if(tempYearTo != tempYear){
					alert("�״�͹/�� ��ͧ�繻����ǡѹ�Ѻ ��Шӻ�");
					return false;
				}
				if((tempMonthTo-tempMonthFrom)< 0  ){
					alert("��͹ ������鹵�ͧ���¡�����͹����ش");
					return false;
				}else{
					for(var i=tempMonthFrom;i <= tempMonthTo;i++){
						sumMonth++;
						
					}
				}
				
			}
			if(tempYearTo > tempYearFrom ){
				calYear = tempYear  - tempYearFrom;
			
				if(calYear>1){
					alert("��͹/�� ������鹵�ҧ�Ѻ ��Шӻ� ������Թ 1 ��");	
					return false;
				}
				calYear = tempYearTo - tempYear;
				if(calYear>1){
					alert("��͹/�� ����ش��ҧ�Ѻ ��Шӻ� ������Թ 1 ��");	
					return false;
				}
				calYear = tempYearTo - tempYearFrom;
				if(calYear>1){
					alert("��͹/�� ��ҧ�ѹ������Թ 1 ��");
					return false;
				}else{
					for(var k = tempMonthFrom; k < 13;k++){
						calMonthFrom ++;
					}
					 for(var y =1 ;y <= tempMonthTo;y++){
						calMonthTo++;
					}
					sumMonth = 	calMonthFrom + calMonthTo;
					if(sumMonth>12){
						alert("�ӹǹ ��͹ ������ǵ�ͧ����Թ 12 ��͹");
						return false;
					}
				}
				
			}
			tempYearFrom =tempYearFront+tempYearFrom; 
			tempYearTo = tempYearFront+tempYearTo; 
			
		}
		


		
		
		
			
			
			
		
		forController(workYear,tempYearFrom,tempYearTo,tempMonthFrom,tempMonthTo);
		frm.target = "_blank";
		frm.submit();	
	}
	function forController(workYear,tempYearFrom,tempYearTo,tempMonthFrom,tempMonthTo){
	    DWRUtil.setValue("workYear",workYear);
		DWRUtil.setValue("workYearFrom",workYear);
		DWRUtil.setValue("workYearTo",tempYearTo);
		DWRUtil.setValue("monthTempFrom",tempMonthFrom);
		DWRUtil.setValue("monthTempTo",tempMonthTo);
	
		}
	
	
	function isNumber(num){
		var realNumber = "1234567890";
		var length = num.length;		
		var temp;
		var check = true;
		for(var i=0;i<length;i++){
			temp = num.substring(i,i+1);
			if ( realNumber.indexOf(temp) == -1 ){
				check = false;
				break;
			}
		}
		return check;
	}
	
	function checkMonth(str){
		var tempStr ="/";
		var sizeIs = str.length;
		var temp;
		var check = true;
		for(var i=0;i<sizeIs;i++){
			temp = str.substring(i,i+1);
			if( temp == '/'){
				check = false;
				break;
			}
		}
		
		return check;
	}
	
</script>
</html>
