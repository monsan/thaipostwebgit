<%@ page language="java" contentType="text/html;charset=TIS-620" %>
<%@page import="java.text.DecimalFormat"%>
<%@page import="javax.swing.text.Document"%>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Date"%>
<%@ page import="com.ss.tp.security.ProcessResult" %>
<%@ page import="com.ss.tp.security.UserInfo" %>

<%
	Calendar now = Calendar.getInstance(Locale.US);	
	UserInfo uf =  (UserInfo)request.getSession().getAttribute("UserLogin");	
	
	
	java.util.Date dd = new java.util.Date();
	java.text.SimpleDateFormat fmd = new java.text.SimpleDateFormat("dd/MM/yyyy",new java.util.Locale("th","TH"));
	String date = fmd.format(dd);
	String userId =  uf.getUserId();
	String ouCode =  uf.getOuCode();
	String hidPage = request.getParameter("hidPage")==null?"":request.getParameter("hidPage");
	String pageEdit = request.getParameter("pageEdit")==null?"0":request.getParameter("pageEdit");
	String year = request.getParameter("hidWorkYear")==null?String.valueOf(now.get(Calendar.YEAR)+543):request.getParameter("hidWorkYear");
	String month = request.getParameter("hidWorkMonth")==null?String.valueOf(now.get(Calendar.MONTH)+1):request.getParameter("hidWorkMonth");
	
	if (request.getSession().getAttribute("processResult") != null){
		ProcessResult processResult = (ProcessResult) request.getSession().getAttribute("processResult");
		year =  request.getParameter("hidYear")==null?String.valueOf(now.get(Calendar.YEAR)+543):request.getParameter("hidYear");
		month =  request.getParameter("hidMonth")==null?String.valueOf(now.get(Calendar.MONTH)+1):request.getParameter("hidMonth");
	}
%>
<html>
<head>
<title>�ѹ�֡��¡���͡���Ṻ������觵��</title>
<!-- Include -->
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript" src="dwr/util.js"></script>
<script type="text/javascript" src="script/payroll_util.js"></script>
<!-- Javascript Script File -->
<SCRIPT type="text/javascript" src="dwr/interface/SuUserOrganizationService.js"></SCRIPT>
<SCRIPT type="text/javascript" src="dwr/interface/PnEmployeeService.js"></SCRIPT>
<SCRIPT type="text/javascript" src="dwr/interface/WePnPromoteInstService.js"></SCRIPT>


<script type="text/javascript" src="script/gridScript.js"></script>
<script type="text/javascript" src="script/payrollComboBox.js"></script>
<script type="text/javascript" src="page/NavigatePage.jsp"></script>
<script type="text/javascript" src="script/dojo.js"></script>
<script type="text/javascript" src="script/dateCalendar.js"></script>
<script type="text/javascript" src="script/json.js"></script>

<script type="text/javascript">
	//Load Dojo's code relating to widget managing functions
	dojo.require("dojo.widget.*");
	dojo.require("dojo.widget.Menu2");
	dojo.require("dojo.widget.Button");
	dojo.require("dojo.widget.ComboBox");
	dojo.require("dojo.widget.DropDownButton");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.ContentPane");
	dojo.require("dojo.widget.LayoutContainer");
	dojo.require("dojo.widget.SortableTable");
	dojo.require("dojo.widget.Toolbar");
	dojo.require("dojo.widget.html.*");
	dojo.require("dojo.widget.Menu2");
	dojo.hostenv.writeIncludes();
	
	//Event
	dojo.require("dojo.event.*");
</script>

<script type="text/javascript">

	// =========================== Start LOV ===========================
    var myUpdate = new Array();
	var count = 0;
	var rowModify ;
	var rowModifyAMT ;
	var lRowNumber;
	var canSave = true;
	
   
    
    function onLoadEmployeeCallback()
    {
    	try
    	{
	    	var cboSource = [];
	     	var cboFrom = dojo.widget.byId("empFromCbo");
	     	var cboTo = dojo.widget.byId("empToCbo");
			
	     	/*for(var i=0; i<data.length; i++){
			     		var emp = data[i];
			     		cboSource.push([emp.empCode, emp.empCode]);
			     	}*/
			     	
			     	<c:forEach items="${PnEmployeeInSecurity}" var="result" >		 
						cboSource.push(["<c:out value='${result}' />","<c:out value='${result}' />"]);
					</c:forEach>
	     	
	     	cboFrom.dataProvider.setData(cboSource);
	     	cboTo.dataProvider.setData(cboSource);
	   
     	}catch(e)
     	{
     		alert(e.message);
    	}
	} 
     
   
    function whenChengVolume(){
		
		var x=document.getElementById("volumeCbo")
		//alert(x.selectedIndex)
		
		//alert(document.forms['searchForm'].elements['incDecCbo'].options[x.selectedIndex].text);
		var str = document.forms['searchForm'].elements['volumeCbo'].value;
		
		//if(str == '�Թ����������ԪҪվ੾��'){
		
		
		
		var table = document.getElementById("table");
		var tdName;
		var chkName;
		var num;
		if(parseInt(DWRUtil.getValue("dataLength")) != null && parseInt(DWRUtil.getValue("dataLength")) > 0){
			num = 1 + parseInt(DWRUtil.getValue("dataLength"));
		}else{
			num = 1;
		}
		
		var oRows = table.rows;
		if(tdName == null)tdName="flag";
		if(chkName == null)chkName="chk";
		//alert(oRows.length+" : "+num);
		for(i=oRows.length-1;i > num;i--){
				table.deleteRow(i);		
		}
		
		// clear global variable
		$("dataLength").value = 0;
		
		DWRUtil.removeAllRows("dataTable");
	}
  
    
	
    
   
	function whenSelectEmpOption()
	{
    	DWRUtil.useLoadingMessage("Loading ...");
		var cbo = dojo.widget.byId("empFromCbo");
		whenFetchEmployeeTo(cbo.textInputNode.value);
	}
	
	function whenFetchEmployeeTo(empCode)
	{
     	DWRUtil.useLoadingMessage("Loading ...");
     	var cboTo = dojo.widget.byId("empToCbo");
		     	
		     	if( empCode > cboTo.comboBoxSelectionValue.value ){
			     	cboTo.textInputNode.value = '';
			     	cboTo.comboBoxSelectionValue.value = '';
		     	}
		     	
		     	var cboSource = [];
		     	
		     	<c:forEach items="${PnEmployeeInSecurity}" var="result" >	
		     		 
		     		var tmp = '<c:out value="${result}" />'+'.00';
		     		if( parseFloat(tmp.toString()) >= parseFloat(empCode) )
						cboSource.push(["<c:out value='${result}' />","<c:out value='${result}' />"]);
				</c:forEach>	
		     	
		     	cboTo.dataProvider.setData(cboSource);
     	
    }
  
 
	function init()
	{
		
		var cboEmpFrom = dojo.widget.byId("empFromCbo");
		
		
		dojo.event.connect(cboEmpFrom, "selectOption", "whenSelectEmpOption");
		
		
	}
	
  	dojo.addOnLoad(init);

	//dojo.addOnLoad(onLoadVolumeCallback);
	dojo.addOnLoad(onLoadEmployeeCallback);

	
	// =========================== End LOV ===========================
	
	
	
	
	function whenShowDataTable()
	{	
		myUpdate = new Array();
		if(document.forms['searchForm'].elements['volumeCbo'].value != null && document.forms['searchForm'].elements['volumeCbo'].value != ''){
	    	var frm = document.forms[0];
			var workYear = DWRUtil.getValue("workYear");
			var workMonth = DWRUtil.getValue("workMonth");
			var	empFromCbo = '';
			var	empToCbo = '';
			var volumeFromCbo = '';
			
		
			
			if(dojo.widget.byId("empFromCbo").textInputNode.value != '')
			{
				empFromCbo = dojo.widget.byId("empFromCbo").textInputNode.value;
			}else
			{
				empFromCbo = '';
			}
			
			if(dojo.widget.byId("empToCbo").textInputNode.value != '')
			{
				empToCbo = dojo.widget.byId("empToCbo").textInputNode.value;
			}else
			{
				empToCbo = '';
			}
			if(dojo.widget.byId("empToCbo").textInputNode.value != '')
			{
				empToCbo = dojo.widget.byId("empToCbo").textInputNode.value;
			}else
			{
				empToCbo = '';
			}
			if(document.forms['searchForm'].elements['volumeCbo'].value != '')
			{
				volumeFromCbo = document.forms['searchForm'].elements['volumeCbo'].value;
			}else
			{
				volumeFromCbo = '';
			}
			//alert('query');
			// Query by Criteria
			WePnPromoteInstService.findByCriteriaList
			(
				'<%=userId%>',
				'<%=ouCode%>',
				workYear,
				workMonth,
				volumeFromCbo,
				empFromCbo,
				empToCbo,
				{callback:whenListDataTableHandler,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ���������� c');}}
			);
		}else{
			alert('��س����͡�����Ţ�ش');
			document.forms['searchForm'].elements['volumeCbo'].focus();
		}
		
	}
	
	var cellFuncs = [
		function(data) { return writeCheckBox();},
		function(data) { return writeTextID("empCode",data.empCode,10,"left",data.keySeq);},
		function(data) { return writeTextDisplay("name",data.name,200,"left","codeSeq",data.codeSeq);},
		function(data) { return writeTextDisplay("oldDuty",data.oldDuty,200,"left","codeSeq",data.codeSeq);},
		function(data) { return writeTextDisplay("oldPositionShort",data.oldPositionShort,200,"left","codeSeq",data.codeSeq);},
		function(data) { return writeTextDisplay("orgCode",data.orgCode,200,"left","codeSeq",data.codeSeq);},
		function(data) { return writeTextDisplay("orgDesc",data.orgDesc,200,"left","codeSeq",data.codeSeq);},
		function(data) { 
						if(data.newOldDuty != null && data.newOldDuty != ''){
							return writeDuty("newOldDuty",data.newOldDuty,data.keySeq);
						}else{
							return writeDuty("",data.keySeq);
						}
						},
		function(data) { 
						if(data.newPositionCode != null && data.newPositionCode != ''){
							return writePosition("newPositionCode",data.newPositionCode,data.keySeq);
						}else{
							return writePosition("",data.keySeq);
						}
						},
					
		function(data) { 
						if(data.newLevelCode != null && data.newLevelCode != ''){
							return writeLevel("newLevelCode",data.newLevelCode,data.keySeq);
						}else{
							return writeLevel("",data.keySeq);
						}	
						},
		function(data) { 
						if(data.newDuty != null && data.newDuty != ''){
							return writeDuty("newDuty",data.newDuty,data.keySeq);
						}else{
							return writeDuty("",data.keySeq);
						}
						},
		function(data) { 
						if(data.newOrgCode != null && data.newOrgCode != ''){
							return writeTextOrgCode("newOrgCode",data.newOrgCode,14,"right",data.keySeq);
						}else{
							return writeTextOrgCode("",date.keySeq);
						}
						}, 						
		function(data) { if(data.newOrgDesc != null && data.newOrgDesc != ''){
							return writeTextDisplay("newOrgDesc",data.newOrgDesc,200,"left","newOrgCode",data.newOrgCode);
						}else{
							return writeTextDisplay("",date.newOrgCode);
						}
						}, 	
		function(data) { return writeHidden("seqData",data.seqData,4,3,"right","keySeq",data.keySeq);}		
	];
	
	function writeCheckBox()
	{	
		return "<div align='center'><input type='checkbox' name ='chk'  /></div>";	
	}

	function writeText(inname,emp,maxlength,textalign,key)
	{
		return "<div align='center' ><input type='text' name = '"+inname+"' onChange='addListUpdate("+key+");checkValueInRowUpdate("+key+");'  value='"+emp+"' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;' onkeyup='if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);'/></div>";
	}
	function writeTextDisplay(inname,emp,maxlength,textalign,nameSeq,codeseq)
	{
		return "<div align='center' style='background-color:#CCCCCC;'><input type='text' readonly='true' name = '"+inname+"' value='"+emp+"' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;background-color:transparent;' /><input type='hidden' name = '"+nameSeq+"' value='"+codeseq+"'  /></div>";
	}
	function writeTextID(inname,emp,maxlength,textalign,key)
	{
		return "<div align='center' ><input type='text' name = '"+inname+"' maxlength='6' onChange='whenSelectEmpOptionInRowUpdate("+key+");addListUpdate("+key+")' value='"+emp+"' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;'  onkeyup='if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);'/></div>";
	}
	function writeTextOrgCode(inname,org,maxlength,textalign,key)
	{
		return "<div align='center' ><input type='text' name = '"+inname+"' maxlength='14' onChange='whenSelectOrgOptionInRowUpdate("+key+");addListUpdate("+key+")' value='"+org+"' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%;'  onkeyup='if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);'/></div>";
	}
	
	function writeDuty(inname,emp,key)
	{
		var mm0 = '';
		var mm1 = '';
		var mm2 = '';
		var mm3 = '';
		var mm4 = '';
		var mm5 = '';
		var mm6 = '';
		var mm7 = '';
		var mm8 = '';
		var mm9 = '';
		var mm10 = '';
		var mm11 = '';
		var mm12 = '';
		var mm13 = '';
		var mm14 = '';
		var mm15 = '';
		var mm16 = '';
		var mm17 = '';
		var mm18 = '';
		var mm19 = '';
		var mm20 = '';
		var mm21 = '';
		var mm22 = '';
		var mm23 = '';
		var mm24 = '';
		var mm25 = '';
		var mm26 = '';
		var mm27 = '';
		var mm28 = '';
		var mm29 = '';
		var mm30 = '';
		var mm31 = '';
		var mm32 = '';
		
	
		if(emp==''){
			mm0 = 'selected';
		} else if(emp=='�'){
			mm1 = 'selected';
		} else if(emp=='�'){
			mm2 = 'selected';		
		}else if(emp=='�'){
			mm3 = 'selected';		
		}else if(emp=='�'){
			mm4 = 'selected';		
		}else if(emp=='�'){
			mm5 = 'selected';		
		}else if(emp=='�'){
			mm6 = 'selected';		
		}else if(emp=='�'){
			mm7 = 'selected';		
		}else if(emp=='�'){
			mm8 = 'selected';		
		}else if(emp=='�'){
			mm9 = 'selected';		
		}else if(emp=='�'){
			mm10 = 'selected';		
		}else if(emp=='�'){
			mm11 = 'selected';		
		}else if(emp=='�'){
			mm12 = 'selected';		
		} else if(emp=='�'){
			mm13 = 'selected';
		} else if(emp=='�'){
			mm14 = 'selected';		
		}else if(emp=='�'){
			mm15 = 'selected';		
		}else if(emp=='�'){
			mm16 = 'selected';		
		}else if(emp=='�'){
			mm17 = 'selected';		
		}else if(emp=='�'){
			mm18 = 'selected';		
		}else if(emp=='�'){
			mm19 = 'selected';		
		}else if(emp=='�'){
			mm20 = 'selected';		
		}else if(emp=='�'){
			mm21 = 'selected';		
		}else if(emp=='�'){
			mm22 = 'selected';		
		}else if(emp=='�'){
			mm23 = 'selected';		
		}else if(emp=='�'){
			mm24 = 'selected';		
		} else if(emp=='�'){
			mm25 = 'selected';
		} else if(emp=='�'){
			mm26 = 'selected';		
		}else if(emp=='�'){
			mm27 = 'selected';		
		}else if(emp=='�'){
			mm28 = 'selected';		
		}else if(emp=='�'){
			mm29 = 'selected';		
		}else if(emp=='�'){
			mm30 = 'selected';		
		}
		return "<div align='center' ><select onchange='addListUpdate("+key+")' name='"+inname+"'>" +
													"<option value='' "+mm0+" ></option>" +
													"<option value='�' "+mm1+" >�ǡ./���.</option>" +
												    "<option value='�' "+mm2+" >�/� �ǡ./���.</option>" +
												    "<option value='�' "+mm3+" >���.�ʷ./���</option>" +
												    "<option value='�' "+mm4+" >�ͧ �ǡ./è�.</option>" +
												    "<option value='�' "+mm5+" >�/� �ͧ �ǡ./è�.</option>" +
												    "<option value='�' "+mm6+" >��.�ǡ./���.</option>" +
												    "<option value='�' "+mm7+" >�/� ��.�ǡ./���.</option>" +
												    "<option value='�' "+mm8+" >�ͽ.</option>" +
												    "<option value='�' "+mm9+" >���ͧ���˹� �ͧ �ͽ.</option>" +
												    "<option value='�' "+mm10+" >�./���./���./�ȳ.</option>" +
												    "<option value='�' "+mm11+" >�/� �./���./���./�ȳ.</option>" +
												    "<option value='�' "+mm12+" >��.Ȼ.��./��.ʴ.����Ҫ�</option>" +
												    "<option value='�' "+mm13+" >���ͧ �/� ��.Ȼ.��./��.ʴ.����Ҫ�</option>" +
												 	"<option value='�' "+mm14+" >��.�./��.���./��.���./��.�ȳ.</option>" +
												 	"<option value='�' "+mm15+" >�/� ��.�./��.���./��.���./��.�ȳ.</option>" +
												 	"<option value='�' "+mm16+" >��.��.Ȼ.��./��.��.ʴ.����Ҫ�</option>" +
												 	"<option value='�' "+mm17+" >�ͧ �/� ��.��.Ȼ.��./��.��.ʴ.����Ҫ�</option>" +
												 	"<option value='�' "+mm18+" >˹.Ȼ./˹.Ƚ./˹.ʾ./˹.��.</option>" +
												 	"<option value='�' "+mm19+" >�/� ˹.Ȼ./�/� ˹.Ƚ./�/� ˹.ʾ./�/� ˹.��.</option>" +
												 	"<option value='�' "+mm20+" >��.˹.Ȼ./��.˹.Ƚ./��.˹.ʾ./��.˹.��.</option>" +
												 	"<option value='�' "+mm21+" >�/� ��.˹.Ȼ./�/� ��.˹.Ƚ./�/� ��.˹.ʾ./�/� ��.˹.��.</option>" +
												    "<option value='�' "+mm22+" >˹�./˹.��./˹�.</option>" +
												    "<option value='�' "+mm23+" >�/� ˹�./˹.��./˹�.</option>" +
												    "<option value='�' "+mm24+" >��.˹.��./��.˹�.</option>" +
												    "<option value='�' "+mm25+" >�/� ��.˹.��./��.˹�.</option>" +
												    "<option value='�' "+mm26+" >��ѡ�ҹ�дѺ 8 ��С�����ä�ѧ�� ˹�./˹�. Ȼ.��./ʴ.����Ҫ�</option>" +
												    "<option value='�' "+mm27+" >���˹��Ἱ�</option>" +
												    "<option value='�' "+mm28+" >����հҹ�  �Ѩ�غѹ����հҹ�</option>" +
												    "<option value='�' "+mm29+" >˹.��. �/� ˹.��.</option>" +
												    "<option value='�' "+mm30+" >�/� ˹�.</option>" +											
							"</select></div>";		
	}
	function writeLevel(inname,emp,key)
	{
		var mm0 = '';
		var mm1 = '';
		var mm2 = '';
		var mm3 = '';
		var mm4 = '';
		var mm5 = '';
		var mm6 = '';
		var mm7 = '';
		var mm8 = '';
		var mm9 = '';
		var mm10 = '';
		var mm11 = '';
		var mm12 = '';
		var mm13 = '';
		
		if(emp==''){
			mm0 = 'selected';
		} else if(emp=='1'){
			mm1 = 'selected';
		} else if(emp=='2'){
			mm2 = 'selected';		
		}else if(emp=='3'){
			mm3 = 'selected';		
		}else if(emp=='4'){
			mm4 = 'selected';		
		}else if(emp=='5'){
			mm5 = 'selected';		
		}else if(emp=='6'){
			mm6 = 'selected';		
		}else if(emp=='7'){
			mm7 = 'selected';		
		}else if(emp=='8'){
			mm8 = 'selected';		
		}else if(emp=='9'){
			mm9 = 'selected';		
		}else if(emp=='10'){
			mm10 = 'selected';		
		}else if(emp=='11'){
			mm11 = 'selected';		
		}else if(emp=='12'){
			mm12 = 'selected';		
		} else if(emp=='13'){
			mm13 = 'selected';		
		}
		
		
		return "<div align='center' ><select onchange='addListUpdate("+key+")' name='"+inname+"'>" +
													"<option value='' "+mm0+" ></option>" +
													"<option value='1' "+mm1+" >1</option>" +
												    "<option value='2' "+mm2+" >2</option>" +
												    "<option value='3' "+mm3+" >3</option>" +
												    "<option value='4' "+mm4+" >4</option>" +
												    "<option value='5' "+mm5+" >5</option>" +
												    "<option value='6' "+mm6+" >6</option>" +
												    "<option value='7' "+mm7+" >7</option>" +
												    "<option value='8' "+mm8+" >8</option>" +
												    "<option value='9' "+mm9+" >9</option>" +
												    "<option value='10' "+mm10+" >10</option>" +
												    "<option value='11' "+mm11+" >11</option>" +
												    "<option value='12' "+mm12+" >12</option>" +
												    "<option value='13' "+mm13+" >13</option>" +												 							
							"</select></div>";		
	}
	function writePosition(inname,emp,key)
	{
		var mm0 = '';
		var mm1 = '';
		var mm2 = '';
		var mm3 = '';
		var mm4 = '';
		var mm5 = '';
		var mm6 = '';
		var mm7 = '';
		var mm8 = '';
		var mm9 = '';
		var mm10 = '';
		var mm11 = '';
		var mm12 = '';
		var mm13 = '';
		var mm14 = '';
		var mm15 = '';
		var mm16 = '';
		var mm17 = '';
		var mm18 = '';
		var mm19 = '';
		var mm20 = '';
		var mm21 = '';
		var mm22 = '';
		var mm23 = '';
		var mm24 = '';
		var mm25 = '';
		var mm26 = '';
		var mm27 = '';
		var mm28 = '';
		var mm29 = '';
		var mm30 = '';
		var mm31 = '';
		var mm32 = '';
		var mm33 = '';
		var mm34 = '';
		var mm35 = '';
		var mm36 = '';
		var mm37 = '';
		var mm38 = '';
		var mm39 = '';
		var mm40 = '';
		var mm41 = '';
		var mm42 = '';
		var mm43 = '';
		var mm44 = '';
		var mm45 = '';
		var mm46 = '';
		var mm47 = '';
		var mm48 = '';
		var mm49 = '';
		var mm50 = '';
		var mm51 = '';
		var mm52 = '';
		var mm53 = '';
		var mm54 = '';
		var mm55 = '';
		var mm56 = '';
		var mm57 = '';
		var mm58 = '';
		var mm59 = '';
		var mm60 = '';
		var mm61 = '';
		var mm62 = '';
		var mm63 = '';
		var mm64 = '';
		var mm65 = '';
		var mm66 = '';
		var mm67 = '';
		var mm68 = '';
		var mm69 = '';
		var mm70 = '';
		var mm71 = '';
		var mm72 = '';
		var mm73 = '';
		var mm74 = '';
		var mm75 = '';
		var mm76 = '';
		var mm77 = '';
		var mm78 = '';
		var mm79 = '';
		var mm80 = '';
		var mm81 = '';
		var mm82 = '';
		var mm83 = '';
		var mm84 = '';
		var mm85 = '';
		var mm86 = '';
		var mm87 = '';
		var mm88 = '';
		var mm89 = '';
		var mm90 = '';
		var mm91 = '';
		var mm92 = '';
	
		
		if(emp==''){
			mm0 = 'selected';
		} else if(emp=='301'){
			mm1 = 'selected';
		} else if(emp=='302'){
			mm2 = 'selected';		
		}else if(emp=='303'){
			mm3 = 'selected';		
		}else if(emp=='304'){
			mm4 = 'selected';		
		}else if(emp=='305'){
			mm5 = 'selected';		
		}else if(emp=='306'){
			mm6 = 'selected';		
		}else if(emp=='307'){
			mm7 = 'selected';		
		}else if(emp=='308'){
			mm8 = 'selected';		
		}else if(emp=='309'){
			mm9 = 'selected';		
		}else if(emp=='310'){
			mm10 = 'selected';		
		}else if(emp=='311'){
			mm11 = 'selected';		
		}else if(emp=='312'){
			mm12 = 'selected';		
		} else if(emp=='313'){
			mm13 = 'selected';		
		} else if(emp=='314'){
			mm14 = 'selected';
		} else if(emp=='315'){
			mm15 = 'selected';		
		}else if(emp=='316'){
			mm16 = 'selected';		
		}else if(emp=='317'){
			mm17 = 'selected';		
		}else if(emp=='318'){
			mm18 = 'selected';		
		}else if(emp=='319'){
			mm19 = 'selected';		
		}else if(emp=='320'){
			mm20 = 'selected';		
		}else if(emp=='321'){
			mm21 = 'selected';		
		}else if(emp=='322'){
			mm22 = 'selected';		
		}else if(emp=='323'){
			mm23 = 'selected';		
		}else if(emp=='324'){
			mm24 = 'selected';		
		}else if(emp=='325'){
			mm25 = 'selected';		
		} else if(emp=='326'){
			mm26 = 'selected';		
		} else if(emp=='327'){
			mm27 = 'selected';
		} else if(emp=='328'){
			mm28 = 'selected';		
		}else if(emp=='329'){
			mm29 = 'selected';		
		}else if(emp=='330'){
			mm30 = 'selected';		
		}else if(emp=='331'){
			mm31 = 'selected';		
		}else if(emp=='332'){
			mm32 = 'selected';		
		}else if(emp=='333'){
			mm33 = 'selected';		
		}else if(emp=='334'){
			mm34 = 'selected';		
		}else if(emp=='335'){
			mm35 = 'selected';		
		}else if(emp=='336'){
			mm36 = 'selected';		
		}else if(emp=='337'){
			mm37 = 'selected';		
		}else if(emp=='338'){
			mm38 = 'selected';		
		}else if(emp=='339'){
			mm39 = 'selected';		
		}else if(emp=='340'){
			mm40 = 'selected';		
		}else if(emp=='341'){
			mm41 = 'selected';		
		}else if(emp=='342'){
			mm42 = 'selected';		
		}else if(emp=='343'){
			mm43 = 'selected';		
		}else if(emp=='344'){
			mm44 = 'selected';		
		}else if(emp=='345'){
			mm45 = 'selected';		
		} else if(emp=='346'){
			mm46 = 'selected';		
		} else if(emp=='347'){
			mm47 = 'selected';
		} else if(emp=='348'){
			mm48 = 'selected';		
		}else if(emp=='349'){
			mm49 = 'selected';		
		}else if(emp=='350'){
			mm50 = 'selected';		
		}else if(emp=='351'){
			mm51 = 'selected';		
		}else if(emp=='352'){
			mm52 = 'selected';		
		}else if(emp=='353'){
			mm53 = 'selected';		
		}else if(emp=='354'){
			mm54 = 'selected';		
		}else if(emp=='355'){
			mm55 = 'selected';		
		}else if(emp=='356'){
			mm56 = 'selected';		
		}else if(emp=='357'){
			mm57 = 'selected';		
		}else if(emp=='358'){
			mm58 = 'selected';		
		} else if(emp=='359'){
			mm59 = 'selected';		
		}else if(emp=='360'){
			mm60 = 'selected';		
		}else if(emp=='361'){
			mm61 = 'selected';		
		}else if(emp=='362'){
			mm62 = 'selected';		
		}else if(emp=='363'){
			mm63 = 'selected';		
		}else if(emp=='364'){
			mm64 = 'selected';		
		}else if(emp=='365'){
			mm65 = 'selected';		
		} else if(emp=='366'){
			mm66 = 'selected';		
		} else if(emp=='367'){
			mm67 = 'selected';
		} else if(emp=='368'){
			mm68 = 'selected';		
		}else if(emp=='369'){
			mm69 = 'selected';		
		}else if(emp=='370'){
			mm70 = 'selected';		
		}else if(emp=='371'){
			mm71 = 'selected';		
		}else if(emp=='372'){
			mm72 = 'selected';		
		}else if(emp=='373'){
			mm73 = 'selected';		
		}else if(emp=='374'){
			mm74 = 'selected';		
		}else if(emp=='375'){
			mm75 = 'selected';		
		}else if(emp=='376'){
			mm76 = 'selected';		
		}else if(emp=='377'){
			mm77 = 'selected';		
		}else if(emp=='378'){
			mm78 = 'selected';		
		} else if(emp=='379'){
			mm79 = 'selected';
		}else if(emp=='380'){
			mm80 = 'selected';		
		}else if(emp=='381'){
			mm81 = 'selected';		
		}else if(emp=='382'){
			mm82 = 'selected';		
		}else if(emp=='383'){
			mm83 = 'selected';		
		}else if(emp=='384'){
			mm84 = 'selected';		
		}else if(emp=='385'){
			mm85 = 'selected';		
		} else if(emp=='386'){
			mm86 = 'selected';		
		} else if(emp=='387'){
			mm87 = 'selected';
		} else if(emp=='388'){
			mm88 = 'selected';		
		}else if(emp=='389'){
			mm89 = 'selected';		
		}else if(emp=='390'){
			mm90 = 'selected';		
		}else if(emp=='391'){
			mm91 = 'selected';		
		}else if(emp=='392'){
			mm92 = 'selected';		
		}
						
		return "<div align='center' ><select onchange='addListUpdate("+key+")' name='"+inname+"'>" +
													"<option value='' "+mm0+" ></option>" +
													"<option value='301' "+mm1+">è�.</option>" +
													"<option value='302' "+mm2+">���.</option>" +
													"<option value='303' "+mm3+">�.</option>" +
													"<option value='304' "+mm4+">��.�.</option>" +
													"<option value='305' "+mm5+">˹.ʾ.</option>" +
													"<option value='306' "+mm6+">˹.��.</option>" +
													"<option value='307' "+mm7+">˹.Ȼ.</option>" +
													"<option value='308' "+mm8+">˹.Ƚ.</option>" +
													"<option value='309' "+mm9+">˹.��.</option>" +
													"<option value='310' "+mm10+">˹�.</option>" +
													"<option value='311' "+mm11+">˹�.</option>" +
													"<option value='312' "+mm12+">��.˹.ʾ.</option>" +
													"<option value='313' "+mm13+">��.˹.��.</option>" +
													"<option value='314' "+mm14+">��.˹.Ȼ.</option>" +
													"<option value='315' "+mm15+">��.˹.Ƚ.</option>" +
													"<option value='316' "+mm16+">��.˹.��.</option>" +
													"<option value='317' "+mm17+">���.</option>" +
													"<option value='318' "+mm18+">���.</option>" +
													"<option value='319' "+mm19+">���.</option>" +
													"<option value='320' "+mm20+">���.</option>" +
													"<option value='321' "+mm21+">���.</option>" +
													"<option value='322' "+mm22+">���.</option>" +
													"<option value='323' "+mm23+">�Ȥ.</option>" +
													"<option value='324' "+mm24+">���.</option>" +
													"<option value='325' "+mm25+">�ȿ.</option>" +
													"<option value='326' "+mm26+">�Ⱦ.</option>" +
													"<option value='327' "+mm27+">ʶ�.</option>" +
													"<option value='328' "+mm28+">���.</option>" +
													"<option value='329' "+mm29+">���.</option>" +
													"<option value='330' "+mm30+">���.</option>" +
													"<option value='331' "+mm31+">���.</option>" +
													"<option value='332' "+mm32+">��.</option>" +
													"<option value='333' "+mm33+">��.</option>" +
													"<option value='334' "+mm34+">��.��.</option>" +
													"<option value='335' "+mm35+">�ʡ.</option>" +
													"<option value='336' "+mm36+">Ƿ�.</option>" +
													"<option value='337' "+mm37+">���.</option>" +
													"<option value='338' "+mm38+">���.</option>" +
													"<option value='339' "+mm39+">�º.</option>" +
													"<option value='340' "+mm40+">���.</option>" +
													"<option value='341' "+mm41+">���.</option>" +
													"<option value='342' "+mm42+">Ȱ�..</option>" +
													"<option value='343' "+mm43+">���.</option>" +
													"<option value='344' "+mm44+">���.</option>" +
													"<option value='345' "+mm45+">�Ǿ.</option>" +
													"<option value='346' "+mm46+">�ʶ.</option>" +
													"<option value='347' "+mm47+">�ʶ.</option>" +
													"<option value='348' "+mm48+">���.</option>" +
													"<option value='349' "+mm49+">���.</option>" +
													"<option value='350' "+mm50+">���.</option>" +
													"<option value='351' "+mm51+">���.</option>" +
													"<option value='352' "+mm52+">���.</option>" +
													"<option value='353' "+mm53+">���.</option>" +
													"<option value='354' "+mm54+">���.</option>" +
													"<option value='355' "+mm55+">���.</option>" +
													"<option value='356' "+mm56+">�Ȼ.</option>" +
													"<option value='357' "+mm57+">�Ȼ.</option>" +
													"<option value='358' "+mm58+">���.</option>" +
													"<option value='359' "+mm59+">���.</option>" +
													"<option value='360' "+mm60+">���.</option>" +
													"<option value='361' "+mm61+">���.</option>" +
													"<option value='362' "+mm62+">���.</option>" +
													"<option value='363' "+mm63+">���.</option>" +
													"<option value='364' "+mm64+">���.</option>" +
													"<option value='365' "+mm65+">���.</option>" +
													"<option value='366' "+mm66+">���.</option>" +
													"<option value='367' "+mm67+">˹�.</option>" +
													"<option value='368' "+mm68+">���.</option>" +
													"<option value='369' "+mm69+">���.</option>" +
													"<option value='370' "+mm70+">���.</option>" +
													"<option value='371' "+mm71+">���.</option>" +
													"<option value='372' "+mm72+">���.</option>" +
													"<option value='373' "+mm73+">���.</option>" +
													"<option value='374' "+mm74+">��.</option>" +
													"<option value='375' "+mm75+">���.</option>" +
													"<option value='376' "+mm76+">���.</option>" +
													"<option value='377' "+mm77+">���.</option>" +
													"<option value='378' "+mm78+">���.</option>" +
													"<option value='379' "+mm79+">���.</option>" +
													"<option value='380' "+mm80+">���.</option>" +
													"<option value='381' "+mm81+">���.</option>" +
													"<option value='382' "+mm82+">���.</option>" +
													"<option value='383' "+mm83+">���.</option>" +
													"<option value='384' "+mm84+">���.</option>" +
													"<option value='385' "+mm85+">���.</option>" +
													"<option value='386' "+mm86+">���.</option>" +
													"<option value='387' "+mm87+">���.</option>" +
													"<option value='388' "+mm88+">���.</option>" +
													"<option value='389' "+mm89+">���.</option>" +
													"<option value='390' "+mm90+">���.</option>" +
													"<option value='391' "+mm91+">���.</option>" +
													"<option value='392' "+mm92+">���.</option>" +														 							
							"</select></div>";		
	}
	
	function writeHidden(inname,emp,size,maxlength,textalign,nameHide,empHide)
	{
				return "<div align='center'><input type='text' name = '"+inname+"' onchange='addListUpdate("+empHide+")' value='"+emp+"' align='center' maxlength='"+maxlength+"' style='text-align:"+textalign+";width:100%' onkeyup='if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);'/><input type='hidden' name = '"+nameHide+"' value='"+empHide+"'  /></div>";
	}
	
	function whenListDataTableHandler(data)
	{
		$("dataLength").value = data.length;
		if(data.length > 0){
			DWRUtil.removeAllRows("dataTable");
			DWRUtil.addRows("dataTable",data,cellFuncs);
		}else{
			alert('��辺������');
			DWRUtil.removeAllRows("dataTable");
		}
	}
	
	
	var wePnPromoteInst = {keySeq:null, ouCode:null, yearPn:null, monthPn:null,volumeSet:null,empCode:null,codeSeq:null,newOldDuty:null,newPositionCode:null,newLevelCode:null,newDuty:null,
	                       newOrgCode:null,seqData:null,updBy:null,updDate:null,creBy:null,creDate:null};
	
	var allRowUpdate = 0;
	
	function onUpdate(){
		
		var table = document.getElementById("table");
		var aRows = table.rows;
		var num = 1 + parseInt(DWRUtil.getValue("dataLength"));
		
		var empNull = true;
		canSave = true ;
		var tab = $('dataTable');
		var row;
		var update ;
		var empList=[];
		var frm   = document.forms["searchForm"];
		
		
		
		if(parseInt(DWRUtil.getValue("dataLength")) != null && parseInt(DWRUtil.getValue("dataLength")) > 0){
			num = 1 + parseInt(DWRUtil.getValue("dataLength"));
		}else{
			num = 1 ;
		}
		
	
		for(var a=aRows.length-1;a > num;a--){
				if (aRows[a].cells["empCode"].childNodes[0].value == null || aRows[a].cells["empCode"].childNodes[0].value == '' || aRows[a].cells["name"].childNodes[0].value == null || aRows[a].cells["name"].childNodes[0].value == ''){
					empNull = false;
					break;
				}
						
		}
		
		
		if(tab.rows.length > 0){
		
			var keySeq 		= frm.elements["keySeq"];
			var empCode 	= frm.elements["empCode"];
			var name		= frm.elements["name"];
			var codeSeq 	= frm.elements["codeSeq"];
			var oldDuty		= frm.elements["oldDuty"];
			var oldPositionShort = frm.elements["oldPositionShort"];
			var orgCode		= frm.elements["orgCode"];
			var orgDesc		= frm.elements["orgDesc"];
			var newOldDuty  = frm.elements["newOldDuty"];
			var newPositionCode = frm.elements["newPositionCode"];
			var newLevelCode = frm.elements["newLevelCode"];
			var newDuty = frm.elements["newDuty"];
			var newOrgCode = frm.elements["newOrgCode"];
			var newOrgDesc = frm.elements["newOrgDesc"];
			var seqData 	= frm.elements["seqData"];
			var sst = "";
			var sed = "";
			
			for(var c=0; c<tab.rows.length; c++){
				if (empCode[c].value == null || empCode[c].value == ''){
					empNull = false;
				}
				
			
			}

			
			if(canSave){
				if(empNull){
				
					DWREngine.beginBatch();
					//alert('UPDATE '+tab.rows.length);
					for(var i=0; i<tab.rows.length; i++){
							update = false;
						row = tab.rows[i];
						wePnPromoteInst.keySeq = parseInt(keySeq[i].value);
					    wePnPromoteInst.volumeSet = document.forms['searchForm'].elements['volumeCbo'].value;
												
						wePnPromoteInst.codeSeq = codeSeq[i].value;
						
						
						if (empCode[i].value != ''){
							wePnPromoteInst.empCode  = empCode[i].value;
						}
						else{
							wePnPromoteInst.empCode  = null;
						}
						
						if (name[i].value != ''){
							wePnPromoteInst.name  = name[i].value;
						}
						else{
							wePnPromoteInst.name  = null;
						}
						
						
						
						if (orgCode[i].value != ''){
							wePnPromoteInst.orgCode  = orgCode[i].value;
						}
						else{
							wePnPromoteInst.orgCode  = null;
						}
						
						if (newOldDuty[i].value != ''){
							wePnPromoteInst.newOldDuty  = newOldDuty[i].value;
						}
						else{
							wePnPromoteInst.newOldDuty  = null;
						}
					
						
						if (newPositionCode[i].value != ''){
							wePnPromoteInst.newPositionCode  = newPositionCode[i].value;
						}
						else{
							wePnPromoteInst.newPositionCode  = null;
						}
						if (newLevelCode[i].value != ''){
							wePnPromoteInst.newLevelCode  = newLevelCode[i].value;
						}
						else{
							wePnPromoteInst.newLevelCode  = null;
						}
						if (newDuty[i].value != ''){
							wePnPromoteInst.newDuty  = newDuty[i].value;
						}
						else{
							wePnPromoteInst.newDuty  = null;
						}
						if (newOrgCode[i].value != ''){
							wePnPromoteInst.newOrgCode  = newOrgCode[i].value;
							
						}
						else{
							wePnPromoteInst.newOrgCode  = null;
							
						}
						
						if (seqData[i].value != ''){
							wePnPromoteInst.seqData  = parseInt(seqData[i].value);
						}
						else{
							wePnPromoteInst.seqData  = null;
						}
						
						wePnPromoteInst.updBy = '<%=userId%>';
						
						for(var x = 0 ; x < myUpdate.length ;x++){
							if(myUpdate[x] == parseInt(keySeq[i].value)){
								update = true;
								break;
							}
						}
						
						if(update){
							allRowUpdate++;
							if(aRows.length  > num + 1 ){
								if( allRowUpdate == myUpdate.length )
									WePnPromoteInstService.addList(wePnPromoteInst, false, {callback:onInsertCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
								else
									WePnPromoteInstService.addList(wePnPromoteInst, false);
					
								}else{
								if( allRowUpdate == myUpdate.length )
									WePnPromoteInstService.addList(wePnPromoteInst, true, {callback:ClearData,errorHandler:function(message) { alert('�������ö�ѹ�֡��');}});
								else
									WePnPromoteInstService.addList(wePnPromoteInst, false);
								
							}
						}
						
						
					}

				
					DWREngine.endBatch();
				}
			}
		}
		

		if(empNull){
			if(canSave){
				if(myUpdate.length == 0){
						insertNewData();
				}
			}
		}else{
					alert('�Ţ��Шӵ�����١��ͧ cansave');
				}
	}
	

	function ClearData(){
		alert("�ѹ�֡���������º����");
			var table = document.getElementById("table");
			var tdName;
			var chkName;
			var num;
			if(parseInt(DWRUtil.getValue("dataLength")) != null && parseInt(DWRUtil.getValue("dataLength")) > 0){
				num = 1 + parseInt(DWRUtil.getValue("dataLength"));
			}else{
				num = 1;
			}
			
			var oRows = table.rows;
			if(tdName == null)tdName="flag";
			if(chkName == null)chkName="chk";
			for(i=oRows.length-1;i > num;i--){
					table.deleteRow(i);		
			}
		DWRUtil.removeAllRows("dataTable");
		whenShowDataTable();
	}
	
	function onInsertCallback(){
		insertNewData();
		allRowUpdate = 0;
	}
	
	function insertNewData()
	{
		var frm   = document.forms[0];
		var table = document.getElementById("table");
		var tdName;
		var chkName;
		var num;
		var empNull = true;
		var workMonth = DWRUtil.getValue("workMonth");
		var sst = "";
		var sed = "";
		var volumeFromCbo = '';
		if(parseInt(DWRUtil.getValue("dataLength")) != null && parseInt(DWRUtil.getValue("dataLength")) > 0){
			num = 1 + parseInt(DWRUtil.getValue("dataLength"));
		}else{
			num = 1 ;
		}
		var oRows = table.rows;
		var insertStatus = false;
		if(tdName == null)tdName="flag";
		if(chkName == null)chkName="chk";
		
		
		if(dojo.widget.byId("volumeCbo").textInputNode.value != '')
			{
				volumeFromCbo = dojo.widget.byId("volumeCbo").textInputNode.value;
			}else
			{
				volumeFromCbo = '';
			}
		
		for(var c=oRows.length-1;c > num;c--){
				if (oRows[c].cells["empCode"].childNodes[0].value == null || oRows[c].cells["empCode"].childNodes[0].value == '' || oRows[c].cells["name"].childNodes[0].value == null || oRows[c].cells["name"].childNodes[0].value == ''){
					empNull = false;
				}
		}
		
		//alert( empNull );
		
		if(empNull){
		
			DWREngine.beginBatch();
			//alert( oRows.length + ' ' + num );
			for(var i=oRows.length-1;i > num;i--)
			{
					insertStatus = true;
					wePnPromoteInst.keySeq = null;
					wePnPromoteInst.ouCode = '<%=ouCode%>';
					wePnPromoteInst.yearPn = '<%=year%>';
					wePnPromoteInst.monthPn = workMonth;
					wePnPromoteInst.volumeSet = document.forms['searchForm'].elements['volumeCbo'].value;
					wePnPromoteInst.empCode = oRows[i].cells["empCode"].childNodes[0].value;
					wePnPromoteInst.name = oRows[i].cells["name"].childNodes[0].value;
					wePnPromoteInst.oldDuty = oRows[i].cells["oldDuty"].childNodes[0].value;
					wePnPromoteInst.oldPositionShort = oRows[i].cells["oldPositionShort"].childNodes[0].value;
					wePnPromoteInst.orgCode = oRows[i].cells["orgCode"].childNodes[0].value;
					wePnPromoteInst.orgDesc = oRows[i].cells["orgDesc"].childNodes[0].value;
					
					if(oRows[i].cells["newOldDuty"].childNodes[0].value != ''){
						wePnPromoteInst.newOldDuty = oRows[i].cells["newOldDuty"].childNodes[0].value;
					}else{
						wePnPromoteInst.newOldDuty = null;
					}
					if(oRows[i].cells["newPositionCode"].childNodes[0].value != ''){
						wePnPromoteInst.newPositionCode = oRows[i].cells["newPositionCode"].childNodes[0].value;
					}else{
						wePnPromoteInst.newPositionCode = null;						
					}
					if(oRows[i].cells["newLevelCode"].childNodes[0].value != ''){
						wePnPromoteInst.newLevelCode = oRows[i].cells["newLevelCode"].childNodes[0].value;
					}else{
						wePnPromoteInst.newLevelCode = null;
					}
					if(oRows[i].cells["newDuty"].childNodes[0].value != ''){
						wePnPromoteInst.newDuty = oRows[i].cells["newDuty"].childNodes[0].value;
					}else{
						wePnPromoteInst.newDuty = null;
					}
					if(oRows[i].cells["newOrgCode"].childNodes[0].value != ''){
					 	wePnPromoteInst.newOrgCode = oRows[i].cells["newOrgCode"].childNodes[0].value;
					 }else{
					 	wePnPromoteInst.newOrgCode = null;
					 }
					wePnPromoteInst.codeSeq = oRows[i].cells["name"].childNodes[1].value;
					wePnPromoteInst.seqData = oRows[i].cells["seqData"].childNodes[0].value;
					wePnPromoteInst.updBy = '<%=userId%>';
					wePnPromoteInst.creBy = '<%=userId%>';
					wePnPromoteInst.creDate = getDateFromFormat(<%=date%>,"dd/MM/yyyy");
				  		if( i == (num + 1) )
							WePnPromoteInstService.addList(wePnPromoteInst, true, {callback:ClearData,errorHandler:function(message) { alert('�������ö�ѹ�֡��');}});
						else
							WePnPromoteInstService.addList(wePnPromoteInst, false);
			}
			
			DWREngine.endBatch();
			if (!insertStatus){alert("�ѹ�֡���������º����");}
			
		}else{
			alert('�Ţ��Шӵ�����١��ͧ');
		}
	
	}
	
	function onDeleteCallback()
	{
		alert("Delete Complete");
		whenShowDataTable();
	}
	
	
	 
	 function addListUpdate(data){
		var add = true;
		for(var i = 0 ; i < myUpdate.length ;i++){
			if(myUpdate[i] == data){
				add = false;
				break;
			}
		}
		if(add){
			myUpdate[count] = data;
			count++;
		}
	}
	

	function whenSelectEmpOptionInRow(object)
     {
     
     	 while (object.tagName !=  'TR')
		 {
		 object = object.parentNode;
		 }
	
		 lRowNumber = object.rowIndex;
		 
		DWRUtil.useLoadingMessage("Loading ...");
		var table = document.getElementById("table");
		var tdName;
		var chkName;
		
		var oRows = table.rows;
		var i = oRows.length-1;
		//alert(oRows[i].cells["empCode"].childNodes[0].value);
		var empCode = oRows[lRowNumber].cells["empCode"].childNodes[0].value;
		//alert(empCode);
		//var cbo = dojo.widget.byId("empCbo");
		
		if(empCode != null && empCode != ''){
			whenFetchEmployeeDetailInRow(empCode);
		}else{
			oRows[lRowNumber].cells["name"].childNodes[0].value = '';
			oRows[lRowNumber].cells["name"].childNodes[1].value = '';
		}
	 } 
	  function whenFetchEmployeeDetailInRow(empCode)
	 {
		DWRUtil.useLoadingMessage("Loading ...");
		//alert(empCode+' : '+<%=ouCode%>+' : ' + DWRUtil.getValue("year")+' : '+ DWRUtil.getValue("period"))
		PnEmployeeService.findByWeEmpCodeDetail(empCode,'<%=ouCode%>', {callback:whenFetchEmployeeDetailInRowCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ���������� wee ');}});
	 }
	 
	 function whenFetchEmployeeDetailInRowCallback(data)
	 {
		// alert(data.empCode);
		var table = document.getElementById("table");
		var tdName;
		var chkName;
		
		var oRows = table.rows;
		var i = oRows.length-1;
	 	if(data.empCode != null && data.empCode != ''){
	 				oRows[lRowNumber].cells["name"].childNodes[0].value = data.name;
					oRows[lRowNumber].cells["oldDuty"].childNodes[0].value = data.oldDuty;
					oRows[lRowNumber].cells["oldPositionShort"].childNodes[0].value = data.oldPositionShort;
					oRows[lRowNumber].cells["orgCode"].childNodes[0].value = data.orgCode;
					oRows[lRowNumber].cells["orgDesc"].childNodes[0].value = data.orgDesc;
					oRows[lRowNumber].cells["newOrgCode"].childNodes[0].value = oRows[lRowNumber].cells["orgCode"].childNodes[0].value;
					oRows[lRowNumber].cells["newOrgDesc"].childNodes[0].value = oRows[lRowNumber].cells["orgDesc"].childNodes[0].value;
					oRows[lRowNumber].cells["name"].childNodes[1].value = data.codeSeq ;
		}else{
			oRows[lRowNumber].cells["name"].childNodes[0].value = '';
			oRows[lRowNumber].cells["oldDuty"].childNodes[0].value = '';
			oRows[lRowNumber].cells["oldPositionShort"].childNodes[0].value = '';
			oRows[lRowNumber].cells["orgCode"].childNodes[0].value = '';
			oRows[lRowNumber].cells["orgDesc"].childNodes[0].value ='';
			oRows[lRowNumber].cells["newOrgCode"].childNodes[0].value ='';
			oRows[lRowNumber].cells["newOrgDesc"].childNodes[0].value ='';
			oRows[lRowNumber].cells["name"].childNodes[1].value = '';
			alert('�Ţ��Шӵ�����١��ͧ');
			oRows[lRowNumber].cells["empCode"].childNodes[0].focus();	
		}
	 }
	 
	 function whenSelectEmpOptionInRowUpdate(data){
	 	
	 	var tab = $('dataTable');
		var row;
		var update ;
		var empList=[];
		var frm   = document.forms["searchForm"];
		
		
		if(tab.rows.length > 0){
			var keySeq 		= frm.elements["keySeq"];
			var empCode 	= frm.elements["empCode"];
			var name		= frm.elements["name"];
			var codeSeq 	= frm.elements["codeSeq"];
			var oldDuty  	= frm.elements["oldDuty"];
			var oldPositionShort  = frm.elements["oldPositionShort"];
			var orgCode = frm.elements["orgCode"];
			var orgDesc = frm.elements["orgDesc"];
			var newOldDuty  = frm.elements["newOldDuty"];
			var newPositionCode = frm.elements["newPositionCode"];
			var newLevelCode = frm.elements["newLevelCode"];
			var newDuty = frm.elements["newDuty"];
			var newOrgCode = frm.elements["newOrgCode"];
			var newOrgDesc = frm.elements["newOrgDesc"];
			var seqData 	= frm.elements["seqData"];
			
			for(var c=0; c<tab.rows.length; c++){
				if(keySeq[c].value == data ){
					rowModify = c;
					break;
				}
			}
			var empCode = empCode[rowModify].value;
			//alert(empCode);
			if(empCode != null && empCode != ''){
				whenFetchEmployeeDetailInRowUpdate(empCode)
			}else{
				name[rowModify].value = '';
				codeSeq[rowModify].value = '';
			}
			
		}
	 	
	 }
	 
	 function whenFetchEmployeeDetailInRowUpdate(empCode)
	 {
		DWRUtil.useLoadingMessage("Loading ...");
		//alert(empCode+' : '+<%=ouCode%>+' : ' + DWRUtil.getValue("year")+' : '+ DWRUtil.getValue("period"))
		PnEmployeeService.findByWeEmpCodeDetail(empCode, '<%=ouCode%>', {callback:whenFetchEmployeeDetailInRowUpdateCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
	 }
	 
	 function whenFetchEmployeeDetailInRowUpdateCallback(data)
	 {
		// alert(data.empCode);
		
		var tab = $('dataTable');
		var row;
		var update ;
		var empList=[];
		var frm   = document.forms["searchForm"];
		var oRows = table.rows;
		
		if(tab.rows.length > 0){
			var keySeq 		= frm.elements["keySeq"];
			var empCode 	= frm.elements["empCode"];
			var name		= frm.elements["name"];
			var codeSeq     = frm.elements["codeSeq"];
			var oldDuty  	= frm.elements["oldDuty"];
			var oldPositionShort  = frm.elements["oldPositionShort"];
			var orgCode = frm.elements["orgCode"];
			var orgDesc = frm.elements["orgDesc"];
			var newOldDuty  = frm.elements["newOldDuty"];
			var newPositionCode = frm.elements["newPositionCode"];
			var newLevelCode = frm.elements["newLevelCode"];
			var newDuty = frm.elements["newDuty"];
			var newOrgCode = frm.elements["newOrgCode"];
			var newOrgDesc = frm.elements["newOrgDesc"];
			var seqData 	= frm.elements["seqData"];
			
			if(data.empCode != null && data.empCode != ''){
				name[rowModify].value = data.name;
				codeSeq[rowModify].value = data.codeSeq;
				oldDuty[rowModify].value = data.oldDuty;
				oldPositionShort[rowModify].value = data.oldPositionShort;
				orgCode[rowModify].value = data.orgCode;
				orgDesc[rowModify].value = data.orgDesc;
			
				
			    
			}else{
				name[rowModify].value = '';
				oldDuty[rowModify].value =  '';
				oldPositionShort[rowModify].value =  '';
				orgCode[rowModify].value =  '';
				orgDesc[rowModify].value =  '';
				codeSeq[rowModify].value = '';
				alert('�Ţ��Шӵ�����١��ͧ');
				empCode[rowModify].focus();	
			}
			
		}
	
	 }
	 
	
	function whenSelectOrgOptionInRow(object)
     {
     
     	 while (object.tagName !=  'TR')
		 {
		 object = object.parentNode;
		 }
	
		 lRowNumber = object.rowIndex;
		 
		DWRUtil.useLoadingMessage("Loading ...");
		var table = document.getElementById("table");
		var tdName;
		var chkName;
		
		var oRows = table.rows;
		var i = oRows.length-1;
		//alert(oRows[i].cells["empCode"].childNodes[0].value);
		var newOrgCode = oRows[lRowNumber].cells["newOrgCode"].childNodes[0].value;
		//alert(empCode);
		//var cbo = dojo.widget.byId("empCbo");
		
		if(newOrgCode != null && newOrgCode != ''){
			whenFetchOrganizationDetailInRow(newOrgCode);
		}else{
			oRows[lRowNumber].cells["newOrgCode"].childNodes[0].value = '';
			oRows[lRowNumber].cells["newOrgDesc"].childNodes[0].value = '';
		}
	 } 
	  function whenFetchOrganizationDetailInRow(newOrgCode)
	 {
		DWRUtil.useLoadingMessage("Loading ...");
		//alert(empCode+' : '+<%=ouCode%>+' : ' + DWRUtil.getValue("year")+' : '+ DWRUtil.getValue("period"))
		PnEmployeeService.findByWeOrgCodeDetail(newOrgCode, '<%=ouCode%>', {callback:whenFetchOrganizationDetailInRowCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
	 }
	 
	 function whenFetchOrganizationDetailInRowCallback(data)
	 {
		// alert(data.empCode);
		var table = document.getElementById("table");
		var tdName;
		var chkName;
		
		var oRows = table.rows;
		var i = oRows.length-1;
	 	if(data.newOrgCode != null && data.newOrgCode != ''){
	 		
					
					oRows[lRowNumber].cells["newOrgCode"].childNodes[0].value = data.newOrgCode;
					oRows[lRowNumber].cells["newOrgDesc"].childNodes[0].value = data.newOrgDesc;
					
		}else{
			
			oRows[lRowNumber].cells["newOrgCode"].childNodes[0].value = '';
			oRows[lRowNumber].cells["newOrgDesc"].childNodes[0].value = '';
			alert('�Ţ��Шӵ�����١��ͧ');
			oRows[lRowNumber].cells["newOrgCode"].childNodes[0].focus();	
		}
	 }
	 
	 function whenSelectOrgOptionInRowUpdate(data){
	 	
	 	var tab = $('dataTable');
		var row;
		var update ;
		var orgList=[];
		var frm   = document.forms["searchForm"];
		
		
		if(tab.rows.length > 0){
			var keySeq 		= frm.elements["keySeq"];
			var newOrgCode 	= frm.elements["newOrgCode"];
			var newOrgDesc		= frm.elements["newOrgDesc"];
			var seqData 	= frm.elements["seqData"];
			
			for(var c=0; c<tab.rows.length; c++){
				if(keySeq[c].value == data ){
					rowModify = c;
					break;
				}
			}
			var newOrgCode = newOrgCode[rowModify].value;
			
			if(newOrgCode != null && newOrgCode != ''){
				whenFetchOrgCodeDetailInRowUpdate(newOrgCode)
			}else{
				newOrgCode[rowModify].value = '';
				newOrgDesc[rowModify].value = '';
			}
			
		}
	 	
	 }
	 
	 function whenFetchOrgCodeDetailInRowUpdate(newOrgCode)
	 {
		DWRUtil.useLoadingMessage("Loading ...");
		PnEmployeeService.findByWeOrgCodeDetail(newOrgCode, '<%=ouCode%>', {callback:whenFetchOrgCodeDetailInRowUpdateCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
	 }
	 
	 function whenFetchOrgCodeDetailInRowUpdateCallback(data)
	 {
		// alert(data.empCode);
		
		var tab = $('dataTable');
		var row;
		var update ;
		var orgList=[];
		var frm   = document.forms["searchForm"];
		var oRows = table.rows;
		
		if(tab.rows.length > 0){
			var keySeq 		= frm.elements["keySeq"];
			var newOrgCode 	= frm.elements["newOrgCode"];
			var newOrgDesc	= frm.elements["newOrgDesc"];
		
			var seqData 	= frm.elements["seqData"];
			
			if(data.newOrgCode != null && data.newOrgCode != ''){
					newOrgDesc[rowModify].value = data.newOrgDesc;
					
				
			
			}else{
				newOrgDesc[rowModify].value = '';
				newOrgCode[rowModify].value = '';
				//codeSeq[rowModify].value = '';
				alert('����˹��§ҹ���١��ͧ');
				newOrgCode[rowModify].focus();	
			}
			
		}
	
	 }
	 
</script>
<%
	
	
	String keySeq  = request.getParameter("keySeq");
%>
</head>
<body>

<table width="100%">
	<tr>
		<td class="font-head">
			[ CTTTPM001 ] ��¡�úѹ�֡�͡���Ṻ������觵��
		</td>
	</tr>
</table>
<form name="searchForm" action="" method="post">
<input type="hidden" name="hidMonth" value="<%=month%>"/>
<input type="hidden" name="dataLength"> 
<table width="770" border="0" align="center" cellspacing="1">

  		<tr>
    	<td class="font-field" align="right">��Шӻ�&nbsp</td>
    	<td align="left">
    		<input type="text" name="workYear" value="<%=year%>" size="2" maxlength="4" style="width: 70px;text-align: center;" onkeyup="if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"/>
    	</td>
	    <td class="font-field" align="right">��͹</td>
		<td align="left">
			<select name="workMonth" >	
				<option value="0">- - - - - - - - - -</option>
				<option value="1">���Ҥ�</option>
				<option value="2">����Ҿѹ��</option>
				<option value="3">�չҤ�</option>
				<option value="4">����¹</option>
				<option value="5">����Ҥ�</option>
				<option value="6">�Զع�¹</option>
				<option value="7">�á�Ҥ�</option>
				<option value="8">�ԧ�Ҥ�</option>
				<option value="9">�ѹ��¹</option>
				<option value="10">���Ҥ�</option>
				<option value="11">��Ȩԡ�¹</option>
				<option value="12">�ѹ�Ҥ�</option>
			</select>
			<script>
				document.forms["searchForm"].workMonth.value = document.forms["searchForm"].hidMonth.value;
			</script>
		</td>
		<td>&nbsp</td>
  	</tr>
  	
   	<tr>
	    <td class="font-field" align="right">������Ţ��Шӵ��&nbsp;</td>
	    <td align="left"><SELECT  dojoType="ComboBox" widgetId="empFromCbo" style="width:200"  onBlurInput="whenSelectEmpOption();"></SELECT></td>
	    <td  class="font-field" align="right">�֧�Ţ��Шӵ��&nbsp;</td>
	    <td align="left"><SELECT  dojoType="ComboBox" widgetId="empToCbo" style="width:200"></SELECT></td>
  	    <td><input type="Button" value="����" class=" button " onclick="whenShowDataTable();" /></td>
  	    	<input type="hidden" name="hidSearchMonth"/>	
			<input type="hidden" name="hidSearchYear" />	
  	</tr>
 	<tr>
  	<td class="font-field" align="right">�ش���.&nbsp;</td>
		<td align="left">
			<select name="volumeCbo" onchange="whenChengVolume();"  style="width:200">
				<c:forEach items="${VolumeInSecurity}" var="result">
					<option value="<c:out value='${result}' />"><c:out value='${result}' /></option>
				</c:forEach>
			</select>		
		</td>
     </tr>
</table>
<br/>

<table  width="800" border="0" cellspacing="0" cellpadding="0" align="center" >
	<tr>
		<td>
		<div style="height:320px;width:950;overflow:auto;vertical-align: top;" align="center" >
		<table id="table" width="2000"  border="1" bordercolor="#6699CC" cellpadding="0" cellspacing="0">
			<thead style="text-align: center">
				<tr CLASS="TABLEBULE2" style="height: 30px;">
					<th CLASS="TABLEBULE2" style="width:40px" rowspan="1" align="center">ź</th>
					<th CLASS="TABLEBULE2" style="width:100px" align="center" rowspan="1">�Ţ��Шӵ��</th>
					<th CLASS="TABLEBULE2" style="width:200px" rowspan="1">���� - ���ʡ��</th>
					<th CLASS="TABLEBULE2" style="width:300px" rowspan="1">�ҹ��к�</th>
					<th CLASS="TABLEBULE2" style="width:90px" rowspan="1">���˹�/�дѺ</th>
					<th CLASS="TABLEBULE2" style="width:130px" rowspan="1">�����ѧ�Ѵ</th>
					<th CLASS="TABLEBULE2" style="width:250px" rowspan="1">�ѧ�Ѵ</th>
					<th CLASS="TABLEBULE2" style="width:100px" rowspan="1">��ҹ����</th>
					<th CLASS="TABLEBULE2" style="width:90px" rowspan="1">���˹�����</th>
					<th CLASS="TABLEBULE2" style="width:80px" rowspan="1">�дѺ����</th>
					<th CLASS="TABLEBULE2" style="width:100px" rowspan="1">�ҹ�����</th>
					<th CLASS="TABLEBULE2" style="width:130px" rowspan="1">�����ѧ�Ѵ����</th>
					<th CLASS="TABLEBULE2" style="width:250px" rowspan="1">�ѧ�Ѵ</th>
					<th CLASS="TABLEBULE2" style="width:50px" rowspan="1" align="center">�ӴѺ���</th>
				</tr>				
			</thead>
			<tbody id="dataTable">
			</tbody>
			<tr id="temprow" style="visibility:hidden;position:absolute">
			<td id="flag"  ><input type="checkbox" name="chk"  style="width:100%"/></td>
			<td id="empCode" align="center"><input type="text" maxlength="6" name="empCode" style="width:100%" onchange="whenSelectEmpOptionInRow(this);"  onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"></td>
			<td id="name" align="center"><input type="text"  name="name" readonly="readonly" style="width:100%;background-color:silver;"/><input type="hidden"  name="codeSeq" /></td>
			<td id="oldDuty" align="center"><input type="text"  name="oldDuty" readonly="readonly" style="width:100%;background-color:silver;"/>
			<td id="oldPositionShort" align="center"><input type="text"  name="oldPositionShort" readonly="readonly" style="width:100%;background-color:silver;"/>
			<td id="orgCode" align="center"><input type="text"  name="orgCode" readonly="readonly" style="width:100%;background-color:silver;"/>
			<td id="orgDesc" align="center"><input type="text"  name="orgDesc" readonly="readonly" style="width:100%;background-color:silver;"/>
			<td id="newOldDuty" align="center" width="300">
					<select name="newOldDuty" style="width:100%;" >
													<option value="" selected="selected"  ></option>
													<option value="�">�ǡ./���.</option>
													<option value="�">�/� �ǡ./���.</option>
													<option value="�">���.�ʷ./���</option>
													<option value="�">�ͧ �ǡ./è�.</option>
													<option value="�">�/� �ͧ �ǡ./è�.</option>
													<option value="�">��.�ǡ./���.</option>
													<option value="�">�/� ��.�ǡ./���.</option>
													<option value="�">�ͽ.</option>
													<option value="�">���ͧ���˹� �ͧ �ͽ.</option>
													<option value="�">�./���./���./�ȳ.</option>
													<option value="�">�/� �./���./���./�ȳ.</option>
													<option value="�">��.Ȼ.��./��.ʴ.����Ҫ�</option>
													<option value="�">���ͧ �/� ��.Ȼ.��./��.ʴ.����Ҫ�</option>
													<option value="�">��.�./��.���./��.���./��.�ȳ.</option>
													<option value="�">�/� ��.�./��.���./��.���./��.�ȳ.</option>
													<option value="�">��.��.Ȼ.��./��.��.ʴ.����Ҫ�</option>
													<option value="�">�ͧ �/� ��.��.Ȼ.��./��.��.ʴ.����Ҫ�</option>
													<option value="�">˹.Ȼ./˹.Ƚ./˹.ʾ./˹.��.</option>
													<option value="�">�/� ˹.Ȼ./�/� ˹.Ƚ./�/� ˹.ʾ./�/� ˹.��.</option>
													<option value="�">��.˹.Ȼ./��.˹.Ƚ./��.˹.ʾ./��.˹.��.</option>
													<option value="�">�/� ��.˹.Ȼ./�/� ��.˹.Ƚ./�/� ��.˹.ʾ./�/� ��.˹.��.</option>
													<option value="�">˹�./˹.��./˹�.</option>
													<option value="�">�/� ˹�./˹.��./˹�.</option>
													<option value="�">��.˹.��./��.˹�.</option>
													<option value="�">�/� ��.˹.��./��.˹�.</option>
													<option value="�">��ѡ�ҹ�дѺ 8 ��С�����ä�ѧ�� ˹�./˹�. Ȼ.��./ʴ.����Ҫ�</option>
													<option value="�">���˹��Ἱ�</option>
													<option value="�">����հҹ�  �Ѩ�غѹ����հҹ�</option>
													<option value="�">˹.��. �/� ˹.��.</option>
													<option value="�">�/� ˹�.</option>
						</select></td>
			<td id="newPositionCode" align="center" width="40">
						<select name="newPositionCode" style="width:100%;">
													<option value="" selected="selected"  ></option>
													<option value="301">è�.</option>
													<option value="302">���.</option>
													<option value="303">�.</option>
													<option value="304">��.�.</option>
													<option value="305">˹.ʾ.</option>
													<option value="306">˹.��.</option>
													<option value="307">˹.Ȼ.</option>
													<option value="308">˹.Ƚ.</option>
													<option value="309">˹.��.</option>
													<option value="310">˹�.</option>
													<option value="311">˹�.</option>
													<option value="312">��.˹.ʾ.</option>
													<option value="313">��.˹.��.</option>
													<option value="314">��.˹.Ȼ.</option>
													<option value="315">��.˹.Ƚ.</option>
													<option value="316">��.˹.��.</option>
													<option value="317">���.</option>
													<option value="318">���.</option>
													<option value="319">���.</option>
													<option value="320">���.</option>
													<option value="321">���.</option>
													<option value="322">���.</option>
													<option value="323">�Ȥ.</option>
													<option value="324">���.</option>
													<option value="325">�ȿ.</option>
													<option value="326">�Ⱦ.</option>
													<option value="327">ʶ�.</option>
													<option value="328">���.</option>
													<option value="329">���.</option>
													<option value="330">���.</option>
													<option value="331">���.</option>
													<option value="332">��.</option>
													<option value="333">��.</option>
													<option value="334">��.��.</option>
													<option value="335">�ʡ.</option>
													<option value="336">Ƿ�.</option>
													<option value="337">���.</option>
													<option value="338">���.</option>
													<option value="339">�º.</option>
													<option value="340">���.</option>
													<option value="341">���.</option>
													<option value="342">Ȱ�..</option>
													<option value="343">���.</option>
													<option value="344">���.</option>
													<option value="345">�Ǿ.</option>
													<option value="346">�ʶ.</option>
													<option value="347">�ʶ.</option>
													<option value="348">���.</option>
													<option value="349">���.</option>
													<option value="350">���.</option>
													<option value="351">���.</option>
													<option value="352">���.</option>
													<option value="353">���.</option>
													<option value="354">���.</option>
													<option value="355">���.</option>
													<option value="356">�Ȼ.</option>
													<option value="357">�Ȼ.</option>
													<option value="358">���.</option>
													<option value="359">���.</option>
													<option value="360">���.</option>
													<option value="361">���.</option>
													<option value="362">���.</option>
													<option value="363">���.</option>
													<option value="364">���.</option>
													<option value="365">���.</option>
													<option value="366">���.</option>
													<option value="367">˹�.</option>
													<option value="368">���.</option>
													<option value="369">���.</option>
													<option value="370">���.</option>
													<option value="371">���.</option>
													<option value="372">���.</option>
													<option value="373">���.</option>
													<option value="374">��.</option>
													<option value="375">���.</option>
													<option value="376">���.</option>
													<option value="377">���.</option>
													<option value="378">���.</option>
													<option value="379">���.</option>
													<option value="380">���.</option>
													<option value="381">���.</option>
													<option value="382">���.</option>
													<option value="383">���.</option>
													<option value="384">���.</option>
													<option value="385">���.</option>
													<option value="386">���.</option>
													<option value="387">���.</option>
													<option value="388">���.</option>
													<option value="389">���.</option>
													<option value="390">���.</option>
													<option value="391">���.</option>
													<option value="392">���.</option>													
 					</select></td>
			<td id="newLevelCode" align="center" width="40">
						<select name="newLevelCode" style="width:100%;"" >
													<option value="" selected="selected"  ></option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
													<option value="6">6</option>
													<option value="7">7</option>
													<option value="8">8</option>
													<option value="9">9</option>
													<option value="10">10</option>
													<option value="11">11</option>
						</select></td>
			<td id="newDuty" align="center" width="300">
					<select name="newDuty" style="width:100%;" >
													<option value="" selected="selected"  ></option>
													<option value="�">�ǡ./���.</option>
													<option value="�">�/� �ǡ./���.</option>
													<option value="�">���.�ʷ./���</option>
													<option value="�">�ͧ �ǡ./è�.</option>
													<option value="�">�/� �ͧ �ǡ./è�.</option>
													<option value="�">��.�ǡ./���.</option>
													<option value="�">�/� ��.�ǡ./���.</option>
													<option value="�">�ͽ.</option>
													<option value="�">���ͧ���˹� �ͧ �ͽ.</option>
													<option value="�">�./���./���./�ȳ.</option>
													<option value="�">�/� �./���./���./�ȳ.</option>
													<option value="�">��.Ȼ.��./��.ʴ.����Ҫ�</option>
													<option value="�">���ͧ �/� ��.Ȼ.��./��.ʴ.����Ҫ�</option>
													<option value="�">��.�./��.���./��.���./��.�ȳ.</option>
													<option value="�">�/� ��.�./��.���./��.���./��.�ȳ.</option>
													<option value="�">��.��.Ȼ.��./��.��.ʴ.����Ҫ�</option>
													<option value="�">���ͧ �/� ��.��.Ȼ.��./��.��.ʴ.����Ҫ�</option>
													<option value="�">˹.Ȼ./˹.Ƚ./˹.ʾ./˹.��.</option>
													<option value="�">�/� ˹.Ȼ./�/� ˹.Ƚ./�/� ˹.ʾ./�/� ˹.��.</option>
													<option value="�">��.˹.Ȼ./��.˹.Ƚ./��.˹.ʾ./��.˹.��.</option>
													<option value="�">�/� ��.˹.Ȼ./�/� ��.˹.Ƚ./�/� ��.˹.ʾ./�/� ��.˹.��.</option>
													<option value="�">˹�./˹.��./˹�.</option>
													<option value="�">�/� ˹�./˹.��./˹�.</option>
													<option value="�">��.˹.��./��.˹�.</option>
													<option value="�">�/� ��.˹.��./��.˹�.</option>
													<option value="�">��ѡ�ҹ�дѺ 8 ��С�����ä�ѧ�� ˹�./˹�. Ȼ.��./ʴ.����Ҫ�</option>
													<option value="�">���˹��Ἱ�</option>
													<option value="�">����հҹ�  �Ѩ�غѹ����հҹ�</option>
													<option value="�">˹.��. �/� ˹.��.</option>
													<option value="�">�/� ˹�.</option>
						</select></td>
			<td id="newOrgCode" align="center"><input type="text" maxlength="14"  name="newOrgCode"  onchange="whenSelectOrgOptionInRow(this);"  onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"></td>
			<td id="newOrgDesc" align="center"><input type="text"  name="newOrgDesc"   readonly="readonly" style="width:100%;background-color:silver;"/>
			<td id="seqData" align="center"><input type="text"  name="seqData"  maxlength="3" style="text-align:right;width:100%;" onkeyup="if(this.value < 0) this.value = '';if(isNaN(Number(this.value))) this.value = this.value.substring(0,this.value.length - 1);"/><input type="hidden"  name="keySeq" /></td>
		</tr>
	</table>
	</div>
	</td>
  </tr>
</table>


<table width="100%" CLASS="TABLEBULE2" >
	<tr CLASS="TABLEBULE2" >
		<td align="left" >&nbsp;
			<input type="Button" class=" button " value="����������" id="insertData" name="insertData" onclick="addVisualRow();"/>						
			<input type="Button" class=" button " value="ź������" id="deleteData" name="deleteData" onclick="removeVisualRow();"/>						
			<input type="Button" class=" button " value="��ŧ" id="confirmData" name="confirmData" onclick="onUpdate();"/>
		</td>
	</tr>
</table>
</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
<!--
function addVisualRow(){
		var tab = $('dataTable');
	
		var table = document.getElementById("table");
		var tempRow = document.getElementById("temprow");
	
		// Insert two rows.
	   	var oTable = table;
	   	var oRowsCheck = table.rows;
	   	var rrCheck = oRowsCheck.length - 1;
	   	var idx = oTable.rows.length;
	   
	   	var str=" " ;
		var from = "[0]";
		
		if(tab.rows.length > 0){
			if(idx > (2+tab.rows.length)){
				if(oRowsCheck[rrCheck].cells["empCode"].childNodes[0].value != null && oRowsCheck[rrCheck].cells["empCode"].childNodes[0].value != ''){
					// Insert cells into row.
					var oRow1=oTable.insertRow(idx);
					var to = "("+(oRow1.rowIndex+tempRow.rowIndex)+")";
					oRow1.id = tempRow.id;
					for(i=0;i<tempRow.cells.length;i++){
						var oCell=oRow1.insertCell(i);
						// Add regular HTML values to the cells.
						
						
						oCell.id = tempRow.cells[i].id;
						oCell.innerHTML=tempRow.cells[i].innerHTML;  
						str = oCell.innerHTML;
						if(str.indexOf(from)>0){
							oCell.innerHTML=str.replace(from, to);
						}
					}
				}else{
					alert('��سҡ�͡�Ţ��Шӵ�ǡ�͹���������ŵ�ǶѴ�');
				
				}
			}else{
					var oRow1=oTable.insertRow(idx);
					var to = "("+(oRow1.rowIndex+tempRow.rowIndex)+")";
					oRow1.id = tempRow.id;
					for(i=0;i<tempRow.cells.length;i++){
						var oCell=oRow1.insertCell(i);
						// Add regular HTML values to the cells.
						
						
						oCell.id = tempRow.cells[i].id;
						oCell.innerHTML=tempRow.cells[i].innerHTML;  
						str = oCell.innerHTML;
						if(str.indexOf(from)>0){
							oCell.innerHTML=str.replace(from, to);
						}
			
				}
			}
		}else{
			if(idx > 2){
				if(oRowsCheck[rrCheck].cells["empCode"].childNodes[0].value != null && oRowsCheck[rrCheck].cells["empCode"].childNodes[0].value != ''){
					// Insert cells into row.
					var oRow1=oTable.insertRow(idx);
					var to = "("+(oRow1.rowIndex+tempRow.rowIndex)+")";
					oRow1.id = tempRow.id;
					for(i=0;i<tempRow.cells.length;i++){
						var oCell=oRow1.insertCell(i);
						// Add regular HTML values to the cells.
						
						
						oCell.id = tempRow.cells[i].id;
						oCell.innerHTML=tempRow.cells[i].innerHTML;  
						str = oCell.innerHTML;
						if(str.indexOf(from)>0){
							oCell.innerHTML=str.replace(from, to);
						}
					}
				}else{
					alert('��سҡ�͡�Ţ��Шӵ�ǡ�͹���������ŵ�ǶѴ�');
				
				}
			}else{
					var oRow1=oTable.insertRow(idx);
					var to = "("+(oRow1.rowIndex+tempRow.rowIndex)+")";
					oRow1.id = tempRow.id;
					for(i=0;i<tempRow.cells.length;i++){
						var oCell=oRow1.insertCell(i);
						// Add regular HTML values to the cells.
						
						
						oCell.id = tempRow.cells[i].id;
						oCell.innerHTML=tempRow.cells[i].innerHTML;  
						str = oCell.innerHTML;
						if(str.indexOf(from)>0){
							oCell.innerHTML=str.replace(from, to);
						}
			
				}
			}
		}
	
	
}

//this function removeVisualRow() used page UPDATE ONLY!!!
	var chDelete = false;
	var chcCon = false;
	function removeVisualRow(){
		var tab = $('dataTable');
		var row;
		var empList=[];
		var frm = document.forms["searchForm"];
		var chk = frm.elements["chk"];
		
			
		var table = document.getElementById("table");
		var tdName;
		var chkName;
		var num;
		if(parseInt(DWRUtil.getValue("dataLength")) != null && parseInt(DWRUtil.getValue("dataLength")) > 0){
			num = 1 + parseInt(DWRUtil.getValue("dataLength"));
		}else{
			num = 1;
		}
		
		var oRows = table.rows;
		if(tdName == null)tdName="flag";
		if(chkName == null)chkName="chk";
		//alert(oRows.length+" : "+num);
		
		
		
		for(x=oRows.length-1;x > num;x--){
				if (oRows[x].cells[tdName].childNodes[0].checked){
						chcCon = true;	
				}
			
			}
		if(tab.rows.length>1){
				for(i=0; i<tab.rows.length; i++){
					row = tab.rows[i];	
					if (chk[i].checked){
							chcCon = true;
						}
					
						
				}
			}else{
				if(tab.rows.length==1){
					row = tab.rows[0];	
					if (chk[0].checked){
							chcCon = true;
						}
				  
				}	
			}
		
		
		
		
		
		
		
		
		if(chcCon){
			var answer = confirm("��ͧ���ź������ �������?");
			if( answer ){
				for(i=oRows.length-1;i > num;i--){
						if (oRows[i].cells[tdName].childNodes[0].checked){
								table.deleteRow(i);		
								chDelete = true;	
						}
					
					}
					
				DWREngine.beginBatch();
					//alert('123435  :'+tab.rows.length);
				    if(tab.rows.length>1){
						for(i=0; i<tab.rows.length; i++){
							row = tab.rows[i];	
							if (chk[i].checked){
									
									//alert('BB'+rowDelete);
									wePnPromoteInst.keySeq = parseInt(frm.elements["keySeq"][i].value);
									WePnPromoteInstService.deleteWePnPromoteInst(wePnPromoteInst, {callback:onDeleteCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
									chDelete = true;
								}
							
								
						}
					}else{
						if(tab.rows.length==1){
							row = tab.rows[0];	
							if (chk[0].checked){
									
									//alert('CC'+rowDelete);
									wePnPromoteInst.keySeq = parseInt(frm.elements["keySeq"][0].value);
									WePnPromoteInstService.deleteWePnPromoteInst(wePnPromoteInst, {callback:onDeleteCallback,errorHandler:function(message) { alert('�Դ��ͼԴ��Ҵ����ǡѺ��õԴ����������');}});
									chDelete = true;
								}
						  
						}	
					}
					
				DWREngine.endBatch();
					
		
			}
		}
		
		if(chDelete){
			alert('ź���������º����');
			DeletCompleaseData();
		}
	}
//***********************

	function onDeleteCallback(){
		//whenQueryData();
	}
	
	function DeletCompleaseData(){
		if(parseInt(DWRUtil.getValue("dataLength")) != null && parseInt(DWRUtil.getValue("dataLength")) > 0){
			whenShowDataTable();
		}
	}
	
	

//-->
</SCRIPT>
