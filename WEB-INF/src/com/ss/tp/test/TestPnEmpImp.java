package com.ss.tp.test;

import org.springframework.test.AbstractDependencyInjectionSpringContextTests;

import com.ss.tp.dto.PnEmpImpVO;
import com.ss.tp.service.PnEmpImpService;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TestPnEmpImp extends AbstractDependencyInjectionSpringContextTests {

	public void testPn() {
		List retList = new ArrayList();
		retList = this.getSimpleService().findByCriteriaReport(2008, 2008,
				2008, "01", "12");
		System.out.println(retList.size());
		for (Iterator it = retList.iterator(); it.hasNext();) {
			PnEmpImpVO dec = (PnEmpImpVO) it.next();
			System.out.println(dec.getEmpCode());
			System.out.println(dec.getEmpName());
			System.out.println(dec.getMPosition());
			System.out.println(dec.getMDivdesc());
			System.out.println(dec.getMSecdesc());
		}
	}

	public PnEmpImpService getSimpleService() {
		return (PnEmpImpService) this.applicationContext
				.getBean("pnEmpImpService");
	}

	protected String[] getConfigLocations() {
		return new String[] { "/com/ss/tp/test/applicationContext.xml" };
	}

}
