package com.ss.tp.test;

import org.springframework.test.AbstractDependencyInjectionSpringContextTests;

import com.ss.tp.service.TestEmpService;

public class TestTestEmp extends AbstractDependencyInjectionSpringContextTests {

	public void testloadData() {
		this.getSimpleService().loadTestEmp();
	}

	public TestEmpService getSimpleService() {
		return (TestEmpService) this.applicationContext
				.getBean("testEmpService");
	}

	protected String[] getConfigLocations() {
		return new String[] { "/com/ss/tp/test/applicationContext.xml" };
	}

}
