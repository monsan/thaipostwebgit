package com.ss.tp.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.test.AbstractDependencyInjectionSpringContextTests;

import com.ss.tp.dto.RwIncDecEmployeeVO;
import com.ss.tp.service.RwIncDecOtherService;

public class TestRwIncDecOther extends
		AbstractDependencyInjectionSpringContextTests {

	public void testRw() {
		List retList = new ArrayList();
		retList = this.getSimpleService().findByCriteria("PAY20", "001", 2550,
				22, null, null, null, null, "08", "1");
		System.out.println(retList.size());
		for (Iterator it = retList.iterator(); it.hasNext();) {
			RwIncDecEmployeeVO rwdec = (RwIncDecEmployeeVO) it.next();
			System.out.println(rwdec.getEmpCode());
		}
	}

	public RwIncDecOtherService getSimpleService() {
		return (RwIncDecOtherService) this.applicationContext
				.getBean("rwIncDecOtherService");
	}

	protected String[] getConfigLocations() {
		return new String[] { "/com/ss/tp/test/applicationContext.xml" };
	}

}
