package com.ss.tp.test;

import org.springframework.test.AbstractDependencyInjectionSpringContextTests;

import com.ss.tp.service.PnEmpMoveService;
import java.util.ArrayList;
import java.util.List;

public class TestPnEmpMove extends
		AbstractDependencyInjectionSpringContextTests {

	public void testReport() throws Exception {
		List retList = new ArrayList();
		String orgCodeFrom = "";
		String orgCodeTo = "";
		String empCodeFrom = "";
		String empCodeTo = "";
		String choiceStatus = "";
		String choiceGroup = "";
		retList = this.getSimpleService().findByCriteriaReport2(orgCodeFrom,
				orgCodeTo, empCodeFrom, empCodeTo, choiceStatus, choiceGroup);
		System.out.println(retList.size());

	}

	public PnEmpMoveService getSimpleService() {
		return (PnEmpMoveService) this.applicationContext
				.getBean("pnEmpMoveService");
	}

	protected String[] getConfigLocations() {
		return new String[] { "/com/ss/tp/test/applicationContext.xml" };
	}

}