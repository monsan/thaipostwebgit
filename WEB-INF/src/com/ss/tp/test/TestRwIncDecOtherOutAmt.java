package com.ss.tp.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.test.AbstractDependencyInjectionSpringContextTests;

import com.ss.tp.dto.RwIncDecEmployeeVO;
import com.ss.tp.dto.PrDailyOutAmtReportVO;
import com.ss.tp.service.RwIncDecOtherService;

public class TestRwIncDecOtherOutAmt extends
		AbstractDependencyInjectionSpringContextTests {

	public void testRw() {
		List retList = new ArrayList();
		retList = this.getSimpleService().decOutAmtReport("MON","001",2558,14,"50");
		System.out.println(retList.size());
		for (Iterator it = retList.iterator(); it.hasNext();) {
			PrDailyOutAmtReportVO rwdec = (PrDailyOutAmtReportVO) it.next();
			System.out.println(rwdec.getEmpCode());
			System.out.println(rwdec.getOrgDesc());
			
		}
	}

	public RwIncDecOtherService getSimpleService() {
		return (RwIncDecOtherService) this.applicationContext
				.getBean("rwIncDecOtherService");
	}

	protected String[] getConfigLocations() {
		return new String[] { "/com/ss/tp/test/applicationContext.xml" };
	}

}
