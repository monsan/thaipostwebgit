package com.ss.tp.test;

import java.util.ArrayList;
import java.util.Iterator;

import java.util.List;

import org.springframework.test.AbstractDependencyInjectionSpringContextTests;

import com.ss.tp.dto.WeEmployeeVO;
import com.ss.tp.service.WePnPromoteInstService;

public class TestWePnPromoteInst extends
		AbstractDependencyInjectionSpringContextTests {

	public void testWe() {
		List retList = new ArrayList();
		String userId = "MON";
		String evaOuCode = "001";
		String evaYear = "2551";
		String evaMonth = "9";
		String evaVolume = "1";

		retList = this.getSimpleService().findByCriteriaReport(userId,
				evaOuCode, evaYear, evaMonth, evaVolume);
		System.out.println(retList.size());
		for (Iterator it = retList.iterator(); it.hasNext();) {
			WeEmployeeVO dec = (WeEmployeeVO) it.next();
			System.out.println(dec.getName());
			System.out.println(dec.getOldPosition());
			System.out.println(dec.getOldDuty());
			System.out.println(dec.getOldWork());
			System.out.println(dec.getOldSec());
			System.out.println(dec.getOldDiv());
			System.out.println(dec.getEmpCode());
			System.out.println(dec.getAccount());
			System.out.println(dec.getNewPosition());
			System.out.println(dec.getNewGworkCode());
			System.out.println(dec.getNewSec());
			System.out.println(dec.getNewWork());
			System.out.println(dec.getNewDiv());
			System.out.println(dec.getNewDuty());

		}
	}

	public WePnPromoteInstService getSimpleService() {
		return (WePnPromoteInstService) this.applicationContext
				.getBean("wePnPromoteInstService");
	}

	protected String[] getConfigLocations() {
		return new String[] { "/com/ss/tp/test/applicationContext.xml" };
	}

}
