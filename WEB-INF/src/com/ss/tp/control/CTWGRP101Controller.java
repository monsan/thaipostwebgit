/*
 * Created on 17 ?.?. 2549
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.ss.tp.control;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Blank;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.ss.tp.dto.WgDraftReportVO;
import com.ss.tp.service.SuOrganizeService;
import com.ss.tp.service.WgDraftService;

/**
 * @author
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class CTWGRP101Controller extends MultiActionController {

	private String ChgNullToEmpty(String str1, String str2) {
		if (str1 == null) {
			str1 = str2;
		}
		return str1;
	}

	private String ConcatSpace(String str1, int size) {
		if (str1.length() < size) {
			int tmp = size - str1.length();
			for (int i = 0; i < tmp; i++) {
				str1 = " " + str1;
			}
		}
		return str1;
	}

	private WgDraftService getWgDraftService() {
		return (WgDraftService) this.getApplicationContext().getBean(
				"wgDraftService");
	}

	public SuOrganizeService getSuOrganizeService() {
		return (SuOrganizeService) this.getApplicationContext().getBean(
				"suOrganizeService");
	}

	public ModelAndView doPrintReport(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		DecimalFormat df = new DecimalFormat("###,##0.000");
		DecimalFormat dfInt = new DecimalFormat("###,##0");

		String ouCode = "";
		String userId = "";
		int year = 0;
		int period = 0;
		String flag = "";
		String section = "";
		String incDecCode = "";
		String incDecName = "";

		if (request.getParameter("ouCode") != null) {
			ouCode = request.getParameter("ouCode");
		}

		if (request.getParameter("userId") != null) {
			userId = request.getParameter("userId");
		}

		if (request.getParameter("year") != null
				&& !"".equals(request.getParameter("year"))) {
			year = Integer.parseInt(request.getParameter("year"));
		}

		List empList = this.getWgDraftService().wgDraftReport(userId, ouCode,
				new Integer(year));
		String ouDesc = this.getSuOrganizeService().findOrganizeName(ouCode);

		// ----------------- Generate Report Detail
		// -------------------------------

		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition",
				"attachment; filename=./page/report/CTWGRP101.xls");
		InputStream in = this.getServletContext().getResourceAsStream(
				"/page/report/CTWGRP101.xls");

		// ///////////////////// Set Format //////////////////////////
		WritableFont fontBold = new WritableFont(WritableFont.ARIAL);
		fontBold.setBoldStyle(WritableFont.BOLD);
		fontBold.setPointSize(9);

		WritableFont fontNormal = new WritableFont(WritableFont.ARIAL);
		fontNormal.setBoldStyle(WritableFont.NO_BOLD);
		fontNormal.setPointSize(9);

		Alignment dataAlignLeft = Alignment.LEFT;
		Alignment dataAlignRight = Alignment.RIGHT;
		Alignment dataAlignCenter = Alignment.CENTRE;

		WritableCellFormat boldLeftFormat = new WritableCellFormat();
		boldLeftFormat.setBorder(Border.LEFT, BorderLineStyle.THIN);
		boldLeftFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		boldLeftFormat.setAlignment(dataAlignLeft);
		boldLeftFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		boldLeftFormat.setFont(fontBold);

		WritableCellFormat HeadFormat = new WritableCellFormat();
		HeadFormat.setAlignment(dataAlignCenter);
		HeadFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		HeadFormat.setFont(fontBold);

		WritableCellFormat HeadLeftFormat = new WritableCellFormat();
		HeadLeftFormat.setAlignment(dataAlignLeft);
		HeadLeftFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		HeadLeftFormat.setFont(fontBold);

		WritableCellFormat normalLeftFormat = new WritableCellFormat();
		normalLeftFormat.setBorder(Border.LEFT, BorderLineStyle.THIN);
		normalLeftFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		normalLeftFormat.setAlignment(dataAlignLeft);
		normalLeftFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalLeftFormat.setFont(fontNormal);

		WritableCellFormat normalCenterFormat = new WritableCellFormat();
		normalCenterFormat.setBorder(Border.LEFT, BorderLineStyle.THIN);
		normalCenterFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		normalCenterFormat.setAlignment(dataAlignCenter);
		normalCenterFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalCenterFormat.setFont(fontNormal);

		WritableCellFormat normalRightFormat = new WritableCellFormat();
		normalRightFormat.setBorder(Border.LEFT, BorderLineStyle.THIN);
		normalRightFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		normalRightFormat.setAlignment(dataAlignRight);
		normalRightFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalRightFormat.setFont(fontNormal);

		WritableCellFormat normalLeftLastFormat = new WritableCellFormat();
		normalLeftLastFormat.setBorder(Border.LEFT, BorderLineStyle.THIN);
		normalLeftLastFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		normalLeftLastFormat.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
		normalLeftLastFormat.setAlignment(dataAlignLeft);
		normalLeftLastFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalLeftLastFormat.setFont(fontNormal);

		WritableCellFormat normalCenterLastFormat = new WritableCellFormat();
		normalCenterLastFormat.setBorder(Border.LEFT, BorderLineStyle.THIN);
		normalCenterLastFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		normalCenterLastFormat.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
		normalCenterLastFormat.setAlignment(dataAlignCenter);
		normalCenterLastFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalCenterLastFormat.setFont(fontNormal);

		WritableCellFormat normalRightLastFormat = new WritableCellFormat();
		normalRightLastFormat.setBorder(Border.LEFT, BorderLineStyle.THIN);
		normalRightLastFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		normalRightLastFormat.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
		normalRightLastFormat.setAlignment(dataAlignRight);
		normalRightLastFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalRightLastFormat.setFont(fontNormal);

		WritableCellFormat normalTotalLeftTopFormat = new WritableCellFormat();
		normalTotalLeftTopFormat.setBorder(Border.LEFT, BorderLineStyle.THIN);
		normalTotalLeftTopFormat.setBorder(Border.TOP, BorderLineStyle.THIN);
		normalTotalLeftTopFormat.setAlignment(dataAlignLeft);
		normalTotalLeftTopFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalTotalLeftTopFormat.setFont(fontNormal);

		WritableCellFormat normalTotalRightTopFormat = new WritableCellFormat();
		normalTotalRightTopFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		normalTotalRightTopFormat.setBorder(Border.TOP, BorderLineStyle.THIN);
		normalTotalRightTopFormat.setAlignment(dataAlignLeft);
		normalTotalRightTopFormat
				.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalTotalRightTopFormat.setFont(fontNormal);

		WritableCellFormat normalTotalTopFormat = new WritableCellFormat();
		normalTotalTopFormat.setBorder(Border.TOP, BorderLineStyle.THIN);
		normalTotalTopFormat.setAlignment(dataAlignLeft);
		normalTotalTopFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalTotalTopFormat.setFont(fontNormal);

		WritableCellFormat normalTotalLeftFormat = new WritableCellFormat();
		normalTotalLeftFormat.setBorder(Border.LEFT, BorderLineStyle.THIN);
		normalTotalLeftFormat.setAlignment(dataAlignLeft);
		normalTotalLeftFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalTotalLeftFormat.setFont(fontNormal);

		WritableCellFormat normalTotalRightFormat = new WritableCellFormat();
		normalTotalRightFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		normalTotalRightFormat.setAlignment(dataAlignLeft);
		normalTotalRightFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalTotalRightFormat.setFont(fontNormal);

		WritableCellFormat normalTotalLeftButtomFormat = new WritableCellFormat();
		normalTotalLeftButtomFormat
				.setBorder(Border.LEFT, BorderLineStyle.THIN);
		normalTotalLeftButtomFormat.setBorder(Border.BOTTOM,
				BorderLineStyle.THIN);
		normalTotalLeftButtomFormat.setAlignment(dataAlignLeft);
		normalTotalLeftButtomFormat
				.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalTotalLeftButtomFormat.setFont(fontNormal);

		WritableCellFormat normalTotalRightButtomFormat = new WritableCellFormat();
		normalTotalRightButtomFormat.setBorder(Border.RIGHT,
				BorderLineStyle.THIN);
		normalTotalRightButtomFormat.setBorder(Border.BOTTOM,
				BorderLineStyle.THIN);
		normalTotalRightButtomFormat.setAlignment(dataAlignLeft);
		normalTotalRightButtomFormat
				.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalTotalRightButtomFormat.setFont(fontNormal);

		WritableCellFormat normalTotalButtomFormat = new WritableCellFormat();
		normalTotalButtomFormat.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
		normalTotalButtomFormat.setAlignment(dataAlignLeft);
		normalTotalButtomFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalTotalButtomFormat.setFont(fontNormal);

		WritableCellFormat normalTotalFormat = new WritableCellFormat();
		normalTotalFormat.setAlignment(dataAlignLeft);
		normalTotalFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalTotalFormat.setFont(fontNormal);
		// /////////////////////////////////////////////////////////////

		WritableCellFormat headRight = new WritableCellFormat();
		headRight.setAlignment(Alignment.RIGHT);
		headRight.setFont(fontBold);

		GregorianCalendar gd = new GregorianCalendar();
		SimpleDateFormat sdfPrint = new SimpleDateFormat("dd/MM/yyyy HH:mm",
				new java.util.Locale("th", "TH"));

		Workbook wb = Workbook.getWorkbook(in);
		WritableWorkbook ww = Workbook.createWorkbook(
				response.getOutputStream(), wb);
		WritableSheet sheet = ww.getSheet(0);

		// sheet.addCell(new Label(0, 0, ouDesc, HeadFormat));

		if (empList.size() > 0) {

			int startRow = 5;
			String divDesc = "";
			String orgDesc = "";
			String punishDesc = "";
			int row = startRow;
			int count = 0;
			int numSheet = 1;
			int totalEmp = 0;
			int total5 = 0;
			int total4 = 0;
			int total3 = 0;
			int total2 = 0;
			int total1 = 0;
			int totalY = 0;
			int totalN = 0;
			double total5Per = 0;
			double total4Per = 0;
			double total3Per = 0;
			double total2Per = 0;
			String Percent5 = "";
			String Percent4 = "";
			String Percent3 = "";
			String Percent2 = "";

			for (Iterator itt = empList.iterator(); itt.hasNext();) {
				WgDraftReportVO vo = (WgDraftReportVO) itt.next();

				if (!divDesc.equals(vo.getDivCode())) {
					// Total Page Before Create New
					// Page//////////////////////////////////////////////////////////////////
					if (numSheet > 1) {
						sheet.setRowView(row, 320);
						sheet.addCell(new Label(1,row,"��� �ӹǹ�١��ҧ������Է��",normalTotalTopFormat));
						sheet.addCell(new Label(2,row,ConcatSpace(dfInt.format(totalY),4)+"  ���",normalTotalTopFormat));						
						sheet.addCell(new Label(3,row,"��� �ӹǹ�١��ҧ���������Է�Ի�Ѻ       "+dfInt.format(totalN)+"  ���",normalTotalTopFormat));
						sheet.addCell(new Blank(4, row,normalTotalTopFormat));
						sheet.addCell(new Blank(5, row, normalTotalTopFormat));
						sheet.addCell(new Blank(6, row, normalTotalTopFormat));
						sheet.addCell(new Blank(7, row, normalTotalTopFormat));
						sheet.addCell(new Blank(8, row, normalTotalTopFormat));
						sheet.addCell(new Blank(9, row, normalTotalTopFormat));
						sheet.addCell(new Blank(10, row, normalTotalTopFormat));
						sheet.addCell(new Blank(11, row,
								normalTotalRightTopFormat));
						row = row + 1;
						if (total5 > 0) {
							total5Per = Math.round((100 / (double) totalY)
									* 1000 * total5)
									/ (double) 1000;
						}
						if (total4 > 0) {
							total4Per = Math.round((100 / (double) totalY)
									* 1000 * total4)
									/ (double) 1000;
						}
						if (total3 > 0) {
							total3Per = Math.round((100 / (double) totalY)
									* 1000 * total3)
									/ (double) 1000;
						}
						if (total2 > 0) {
							total2Per = Math.round((100 / (double) totalY)
									* 1000 * total2)
									/ (double) 1000;
						}
						if (total5 == totalY) {
							total5Per = 100;
						}
						if (total4 == totalY) {
							total4Per = 100;
						}
						if (total3 == totalY) {
							total3Per = 100;
						}
						if (total2 == totalY) {
							total2Per = 100;
						}
						Percent5 = "( " + ConcatSpace(df.format(total5Per), 7)
								+ "% )";
						Percent4 = "( " + ConcatSpace(df.format(total4Per), 7)
								+ "% )";
						Percent3 = "( " + ConcatSpace(df.format(total3Per), 7)
								+ "% )";
						Percent2 = "( " + ConcatSpace(df.format(total2Per), 7)
								+ "% )";
						sheet.addCell(new Blank(0, row, normalTotalLeftFormat));
						sheet.addCell(new Label(1,row,"��� �ӹǹ�١��ҧ������Ѻ 5%",normalTotalFormat));
						sheet.addCell(new Label(2,row,ConcatSpace(dfInt.format(total5),4)+"  ���   "+Percent5,normalTotalFormat));
						sheet.addCell(new Blank(11, row,normalTotalRightFormat));
						row = row +1;
						sheet.addCell(new Blank(0, row,normalTotalLeftFormat));
						sheet.addCell(new Label(1,row,"��� �ӹǹ�١��ҧ������Ѻ 4%",normalTotalFormat));
						sheet.addCell(new Label(2,row,ConcatSpace(dfInt.format(total4),4)+"  ���   "+Percent4,normalTotalFormat));
						sheet.addCell(new Blank(11, row,normalTotalRightFormat));
						row = row +1;
						sheet.addCell(new Blank(0, row,normalTotalLeftFormat));
						sheet.addCell(new Label(1,row,"��� �ӹǹ�١��ҧ������Ѻ 3%",normalTotalFormat));
						sheet.addCell(new Label(2,row,ConcatSpace(dfInt.format(total3),4)+"  ���   "+Percent3,normalTotalFormat));
						sheet.addCell(new Blank(11, row,normalTotalRightFormat));
						row = row +1;
						sheet.addCell(new Blank(0, row,normalTotalLeftButtomFormat));
						sheet.addCell(new Label(1,row,"��� �ӹǹ�١��ҧ������Ѻ 2%",normalTotalButtomFormat));
						sheet.addCell(new Label(2,row,ConcatSpace(dfInt.format(total2),4)+"  ���   "+Percent2,normalTotalButtomFormat));
						sheet.addCell(new Blank(3, row,normalTotalButtomFormat));
						sheet.addCell(new Blank(4, row, normalTotalButtomFormat));
						sheet.addCell(new Blank(5, row, normalTotalButtomFormat));
						sheet.addCell(new Blank(6, row, normalTotalButtomFormat));
						sheet.addCell(new Blank(7, row, normalTotalButtomFormat));
						sheet.addCell(new Blank(8, row, normalTotalButtomFormat));
						sheet.addCell(new Blank(9, row, normalTotalButtomFormat));
						sheet.addCell(new Blank(10, row,
								normalTotalButtomFormat));
						sheet.addCell(new Blank(11, row,
								normalTotalRightButtomFormat));
						// sheet.addCell(new Blank(0,
						// row,normalTotalLeftButtomFormat));
						// sheet.addCell(new Blank(1,
						// row,normalTotalButtomFormat));
						// sheet.addCell(new Blank(2,
						// row,normalTotalButtomFormat));
						// sheet.addCell(new Blank(3,
						// row,normalTotalButtomFormat));
						// sheet.addCell(new Blank(4,
						// row,normalTotalButtomFormat));
						// sheet.addCell(new Blank(5,
						// row,normalTotalButtomFormat));
						// sheet.addCell(new Blank(6,
						// row,normalTotalButtomFormat));
						// sheet.addCell(new Blank(7,
						// row,normalTotalButtomFormat));
						// sheet.addCell(new
						// Blank(8,row,normalTotalButtomFormat));
						// sheet.addCell(new Blank(9,
						// row,normalTotalButtomFormat));
						// sheet.addCell(new
						// Blank(10,row,normalTotalButtomFormat));
						// sheet.addCell(new
						// Blank(11,row,normalTotalRightButtomFormat));
						// row = row +1;
						row = row + 1;
					}
					// //////////////////////////////////////////////////////////////

					ww.copySheet(
							0,
							ChgNullToEmpty(vo.getDivDesc(), "Sheet" + numSheet),
							numSheet);
					sheet = ww.getSheet(numSheet);
					sheet.getSettings().setPassword("028313766");
					sheet.getSettings().setProtected(true);
					row = startRow;
					totalEmp = 0;
					total5 = 0;
					total4 = 0;
					total3 = 0;
					total2 = 0;
					total1 = 0;
					totalY = 0;
					totalN = 0;
					total5Per = 0;
					total4Per = 0;
					total3Per = 0;
					total2Per = 0;
					// total1Per = 0;
					Percent5 = "";
					Percent4 = "";
					Percent3 = "";
					Percent2 = "";
					// Percent1 = "";
					numSheet = numSheet + 1;
					sheet.setRowView(row, 320);
					sheet.addCell(new Label(2,1,"                                               ��§ҹ�����š�û�Ѻ��Ҩ�ҧ�ͧ�١��ҧ��Шӻէ�����ҳ "+year+" (���§����ӴѺ���ͻ�Ѻ��Ҩ�ҧ)",HeadFormat));
					sheet.addCell(new Label(2,2,vo.getDivDesc(),HeadFormat));
					sheet.addCell(new Label(11,2,"�ѹ������� : "+sdfPrint.format(gd.getTime()), headRight));
					divDesc = vo.getDivCode();
				}
				sheet.setRowView(row, 320);
				orgDesc = "";
				if (vo.getAreaDesc() != null
						&& !vo.getAreaDesc().equals(vo.getDivDesc())) {
					orgDesc = vo.getAreaDesc();
				}
				if (vo.getSecDesc() != null) {
					if (orgDesc != "") {
						orgDesc = orgDesc + "/" + vo.getSecDesc();
					} else {
						orgDesc = vo.getSecDesc();
					}
				}
				if (vo.getWorkDesc() != null) {
					orgDesc = orgDesc + "/" + vo.getWorkDesc();
				}
				sheet.setRowView(row, 320);
				sheet.addCell(new Label(0, row, vo.getEmpCode() + "/"
						+ vo.getGworkCode(), normalCenterFormat));
				sheet.addCell(new Label(1, row, vo.getEmpName(),
						normalLeftFormat));
				sheet.addCell(new Label(2, row, orgDesc, normalLeftFormat));
				if (vo.getWage() != null) {
					sheet.addCell(new Label(3, row, String.valueOf(dfInt
							.format(vo.getWage())), normalRightFormat));
				} else {
					sheet.addCell(new Blank(3, row, normalRightFormat));
				}
				if (vo.getHisSeq() != null) {
					sheet.addCell(new Label(4, row, String.valueOf(vo
							.getHisSeq()), normalCenterFormat));
				} else {
					sheet.addCell(new Blank(4, row, boldLeftFormat));
				}
				if (vo.getPunishStatus().equals("Y")){punishDesc = "�";}else{punishDesc = "";}
				if (vo.getPerDraft() != null) {
					if (String.valueOf(dfInt.format(vo.getPerDraft())).equals(
							"5")) {
						sheet.addCell(new Label(5, row, "/" + punishDesc,
								normalCenterFormat));
						total5 = total5 + 1;
					} else {
						sheet.addCell(new Blank(5, row, boldLeftFormat));
					}
					if (String.valueOf(dfInt.format(vo.getPerDraft())).equals(
							"4")) {
						sheet.addCell(new Label(6, row, "/" + punishDesc,
								normalCenterFormat));
						total4 = total4 + 1;
					} else {
						sheet.addCell(new Blank(6, row, boldLeftFormat));
					}
					if (String.valueOf(dfInt.format(vo.getPerDraft())).equals(
							"3")) {
						sheet.addCell(new Label(7, row, "/" + punishDesc,
								normalCenterFormat));
						total3 = total3 + 1;
					} else {
						sheet.addCell(new Blank(7, row, boldLeftFormat));
					}
					if (String.valueOf(dfInt.format(vo.getPerDraft())).equals(
							"2")) {
						sheet.addCell(new Label(8, row, "/" + punishDesc,
								normalCenterFormat));
						total2 = total2 + 1;
					} else {
						sheet.addCell(new Blank(8, row, boldLeftFormat));
					}
					if (String.valueOf(dfInt.format(vo.getPerDraft())).equals(
							"1")) {
						sheet.addCell(new Label(9, row, "/" + punishDesc,
								normalCenterFormat));
						total1 = total1 + 1;
					} else {
						sheet.addCell(new Blank(9, row, boldLeftFormat));
					}
				} else {
					sheet.addCell(new Blank(5, row, normalCenterFormat));
					sheet.addCell(new Blank(6, row, normalCenterFormat));
					sheet.addCell(new Blank(7, row, normalCenterFormat));
					sheet.addCell(new Blank(8, row, normalCenterFormat));
					sheet.addCell(new Blank(9, row, normalCenterFormat));
				}
				if (vo.getDraftStatus().equals("N")) {
					sheet.addCell(new Label(10, row, vo.getNoDraft(),
							normalCenterFormat));
					sheet.addCell(new Label(8, row, punishDesc,
							normalCenterFormat));
					totalN = totalN + 1;
				} else {
					sheet.addCell(new Blank(10, row, boldLeftFormat));
				}
				if (vo.getScore() != null) {
					sheet.addCell(new Label(11, row, String.valueOf(df
							.format(vo.getScore())), normalRightFormat));
				} else {
					sheet.addCell(new Blank(11, row, boldLeftFormat));
				}
				row++;
				count = count + 1;
				totalEmp = totalEmp + 1;
				totalY = totalEmp - totalN;

				// Total Page When Last
				// Page//////////////////////////////////////////////////////////////////
				if (count == empList.size()) {
					sheet.setRowView(row, 320);
					sheet.addCell(new Blank(0, row, normalTotalLeftTopFormat));
					sheet.addCell(new Label(1,row,"��� �ӹǹ�١��ҧ������Է��",normalTotalTopFormat));
					sheet.addCell(new Label(2,row,ConcatSpace(dfInt.format(totalY),4)+"  ���",normalTotalTopFormat));
					sheet.addCell(new Label(3,row,"��� �ӹǹ�١��ҧ���������Է�Ի�Ѻ       "+dfInt.format(totalN)+"  ���",normalTotalTopFormat));
					sheet.addCell(new Blank(4, row,normalTotalTopFormat));
					sheet.addCell(new Blank(5, row, normalTotalTopFormat));
					sheet.addCell(new Blank(6, row, normalTotalTopFormat));
					sheet.addCell(new Blank(7, row, normalTotalTopFormat));
					sheet.addCell(new Blank(8, row, normalTotalTopFormat));
					sheet.addCell(new Blank(9, row, normalTotalTopFormat));
					sheet.addCell(new Blank(10, row, normalTotalTopFormat));
					sheet.addCell(new Blank(11, row, normalTotalRightTopFormat));
					row = row + 1;
					if (total5 > 0) {
						total5Per = Math.round((100 / (double) totalY) * 1000
								* total5)
								/ (double) 1000;
					}
					if (total4 > 0) {
						total4Per = Math.round((100 / (double) totalY) * 1000
								* total4)
								/ (double) 1000;
					}
					if (total3 > 0) {
						total3Per = Math.round((100 / (double) totalY) * 1000
								* total3)
								/ (double) 1000;
					}
					if (total2 > 0) {
						total2Per = Math.round((100 / (double) totalY) * 1000
								* total2)
								/ (double) 1000;
					}
					// if (total3 > 0){total1Per = Math.round((100 /
					// (double)totalY)*100*total3)/(double)100;}
					if (total5 == totalY) {
						total5Per = 100;
					}
					if (total4 == totalY) {
						total4Per = 100;
					}
					if (total3 == totalY) {
						total3Per = 100;
					}
					if (total2 == totalY) {
						total2Per = 100;
					}
					Percent5 = "( " + ConcatSpace(df.format(total5Per), 7)
							+ "% )";
					Percent4 = "( " + ConcatSpace(df.format(total4Per), 7)
							+ "% )";
					Percent3 = "( " + ConcatSpace(df.format(total3Per), 7)
							+ "% )";
					Percent2 = "( " + ConcatSpace(df.format(total2Per), 7)
							+ "% )";
					sheet.addCell(new Blank(0, row, normalTotalLeftFormat));
					sheet.addCell(new Label(1,row,"��� �ӹǹ�١��ҧ������Ѻ 5%",normalTotalFormat));
					sheet.addCell(new Label(2,row,ConcatSpace(dfInt.format(total5),4)+"  ���   "+Percent5,normalTotalFormat));
					sheet.addCell(new Blank(11, row,normalTotalRightFormat));
					row = row +1;
					sheet.addCell(new Blank(0, row,normalTotalLeftFormat));
					sheet.addCell(new Label(1,row,"��� �ӹǹ�١��ҧ������Ѻ 4%",normalTotalFormat));
					sheet.addCell(new Label(2,row,ConcatSpace(dfInt.format(total4),4)+"  ���   "+Percent4,normalTotalFormat));
					sheet.addCell(new Blank(11, row,normalTotalRightFormat));
					row = row +1;
					sheet.addCell(new Blank(0, row,normalTotalLeftFormat));
					sheet.addCell(new Label(1,row,"��� �ӹǹ�١��ҧ������Ѻ 3%",normalTotalFormat));
					sheet.addCell(new Label(2,row,ConcatSpace(dfInt.format(total3),4)+"  ���   "+Percent3,normalTotalFormat));
					sheet.addCell(new Blank(11, row,normalTotalRightFormat));
					row = row +1;
					sheet.addCell(new Blank(0, row,normalTotalLeftButtomFormat));
					sheet.addCell(new Label(1,row,"��� �ӹǹ�١��ҧ������Ѻ 2%",normalTotalButtomFormat));
					sheet.addCell(new Label(2,row,ConcatSpace(dfInt.format(total2),4)+"  ���   "+Percent2,normalTotalButtomFormat));
					sheet.addCell(new Blank(3, row,normalTotalButtomFormat));
					sheet.addCell(new Blank(4, row, normalTotalButtomFormat));
					sheet.addCell(new Blank(5, row, normalTotalButtomFormat));
					sheet.addCell(new Blank(6, row, normalTotalButtomFormat));
					sheet.addCell(new Blank(7, row, normalTotalButtomFormat));
					sheet.addCell(new Blank(8, row, normalTotalButtomFormat));
					sheet.addCell(new Blank(9, row, normalTotalButtomFormat));
					sheet.addCell(new Blank(10, row, normalTotalButtomFormat));
					sheet.addCell(new Blank(11, row,
							normalTotalRightButtomFormat));
					// sheet.addCell(new Blank(1, row,normalTotalButtomFormat));
					// sheet.addCell(new Blank(2, row,normalTotalButtomFormat));
					// sheet.addCell(new Blank(3, row,normalTotalButtomFormat));
					// sheet.addCell(new Blank(4, row,normalTotalButtomFormat));
					// sheet.addCell(new Blank(5, row,normalTotalButtomFormat));
					// sheet.addCell(new Blank(6, row,normalTotalButtomFormat));
					// sheet.addCell(new Blank(7, row,normalTotalButtomFormat));
					// sheet.addCell(new Blank(8,row,normalTotalButtomFormat));
					// sheet.addCell(new Blank(9,row,normalTotalButtomFormat));
					// sheet.addCell(new Blank(10,row,normalTotalButtomFormat));
					// sheet.addCell(new
					// Blank(11,row,normalTotalRightButtomFormat));
					row = row + 1;
				}
				// //////////////////////////////////////////////////////////////
			}
			ww.removeSheet(0);
			ww.write();
			ww.close();
			wb.close();
			in.close();
			return new ModelAndView("CTWGRP101");
		} else {
			sheet.setRowView(5, 320);
			sheet.mergeCells(0, 5, 11, 5);
			sheet.addCell(new Label(0, 5, "��辺������", normalCenterLastFormat));		
			sheet.getSettings().setPassword("028313766");
			sheet.getSettings().setProtected(true);
			ww.write();
			ww.close();
			wb.close();
			in.close();
			return new ModelAndView("CTWGRP101");
		}
	}
}
