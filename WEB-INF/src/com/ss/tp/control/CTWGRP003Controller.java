/*
 * Created on 17 ?.?. 2549
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.ss.tp.control;

import java.io.InputStream;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;

import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.CellFormat;
import jxl.write.Label;

import jxl.write.Blank;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import com.ss.tp.dto.WgPrWorkDayVO;
import com.ss.tp.service.WgPrWorkDayService;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

/**
 * @author Kiet
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class CTWGRP003Controller extends MultiActionController {

	private String ChgNullToEmpty(String str1, String str2) {
		if (str1 == null) {
			str1 = str2;
		}
		return str1;
	}

	private WgPrWorkDayService getWgPrWorkDayService() {
		return (WgPrWorkDayService) this.getApplicationContext().getBean(
				"wgPrWorkDayService");
	}

	public ModelAndView doPrintReport(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String ouCode = "";
		String userId = "";
		String year = "";
		String period = "";
		String deptDesc = "";
		String dept = "";
		String periodName = "";

		DecimalFormat df = new DecimalFormat("###,##0.00");
		DecimalFormat dfInt = new DecimalFormat("###,##0");

		if (request.getParameter("ouCode") != null) {
			ouCode = request.getParameter("ouCode");
		}

		if (request.getParameter("userId") != null) {
			userId = request.getParameter("userId");
		}

		if (request.getParameter("year") != null
				&& !"".equals(request.getParameter("year"))) {
			year = request.getParameter("year");
		}

		if (request.getParameter("period") != null
				&& !"".equals(request.getParameter("period"))) {
			period = request.getParameter("period");
		}

		if (request.getParameter("periodName") != null) {
			periodName = request.getParameter("periodName");
		}

		String orgName = this.getWgPrWorkDayService().findOrgName(ouCode);
		List wgPrWorkDayList = this.getWgPrWorkDayService()
				.findWgPrWorkDayReport(ouCode, userId, Integer.parseInt(year),
						Integer.parseInt(period));

		// ----------------- Generate Report Detail
		// -------------------------------

		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition",
				"attachment; filename=./page/report/CTWGRP003.xls");
		InputStream in = this.getServletContext().getResourceAsStream(
				"/page/report/CTWGRP003.xls");
		Workbook wb = Workbook.getWorkbook(in);
		WritableWorkbook ww = Workbook.createWorkbook(
				response.getOutputStream(), wb);
		WritableSheet sheet1 = ww.getSheet(0);

		WritableCellFormat borderEndLine = new WritableCellFormat();
		borderEndLine.setBorder(Border.TOP, BorderLineStyle.THIN);

		WritableCellFormat borderAll = new WritableCellFormat();
		borderAll.setAlignment(Alignment.CENTRE);
		borderAll.setBorder(Border.ALL, BorderLineStyle.THIN);

		WritableFont fontBold = new WritableFont(WritableFont.ARIAL);
		fontBold.setBoldStyle(WritableFont.BOLD);
		fontBold.setPointSize(9);

		WritableCellFormat borderNumber2 = new WritableCellFormat();
		borderNumber2.setBorder(Border.ALL, BorderLineStyle.THIN);
		borderNumber2.setAlignment(Alignment.RIGHT);
		borderNumber2.setFont(fontBold);

		WritableCellFormat headRight = new WritableCellFormat();
		headRight.setAlignment(Alignment.RIGHT);
		headRight.setFont(fontBold);

		GregorianCalendar gd = new GregorianCalendar();
		SimpleDateFormat sdfPrint = new SimpleDateFormat("dd/MM/yyyy HH:mm",
				new java.util.Locale("th", "TH"));

		CellFormat cellOrgName = sheet1.getWritableCell(0, 0).getCellFormat();
		CellFormat cellPeriod = sheet1.getWritableCell(0, 2).getCellFormat();
		double sumWageAmt = 0.0;
		double sumWorkDay = 0.0;
		double sumWorkAmt = 0.0;
		String flagSum = "";

		if (wgPrWorkDayList.size() > 0) {

			// ++++++++++Set Cell Format++++++++++
			CellFormat colDeptDesc = sheet1.getWritableCell(0, 4)
					.getCellFormat();
			WritableCellFormat cellDeptDesc = new WritableCellFormat(
					colDeptDesc);
			cellDeptDesc.setBorder(Border.BOTTOM, BorderLineStyle.THIN);

			CellFormat colEname = sheet1.getWritableCell(1, 4).getCellFormat();
			WritableCellFormat cellEname = new WritableCellFormat(colEname);
			cellEname.setBorder(Border.BOTTOM, BorderLineStyle.THIN);

			CellFormat colFullDay = sheet1.getWritableCell(2, 4)
					.getCellFormat();
			WritableCellFormat cellFullDay = new WritableCellFormat(colFullDay);
			cellFullDay.setBorder(Border.BOTTOM, BorderLineStyle.THIN);

			CellFormat colHalfDay = sheet1.getWritableCell(3, 4)
					.getCellFormat();
			WritableCellFormat cellHalfDay = new WritableCellFormat(colHalfDay);
			cellHalfDay.setBorder(Border.BOTTOM, BorderLineStyle.THIN);

			CellFormat colFlag = sheet1.getWritableCell(4, 4).getCellFormat();
			WritableCellFormat cellFlag = new WritableCellFormat(colFlag);
			cellFlag.setBorder(Border.BOTTOM, BorderLineStyle.THIN);

			WgPrWorkDayVO vo = null;
			int startRow = 4;
			int i = 0;
			int j = 0;
			int s = 1; // Sheet's name
			int m = 0;
			Alignment dataAlignCenter = Alignment.CENTRE;

			WritableCellFormat cellEmpNo = new WritableCellFormat(colDeptDesc);
			cellEmpNo.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
			cellEmpNo.setAlignment(dataAlignCenter);
			// cellEmpNo.setAlignment(Alignment.CENTRE);

			WritableCellFormat colEmpNo = new WritableCellFormat(colDeptDesc);
			cellEmpNo.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
			colEmpNo.setAlignment(dataAlignCenter);

			WritableCellFormat DeptDescF = new WritableCellFormat(colDeptDesc);
			WritableFont font = new WritableFont(WritableFont.ARIAL);
			font.setBoldStyle(WritableFont.BOLD);
			font.setPointSize(9);
			DeptDescF.setFont(font);

			// Cell OrgName
			sheet1.addCell(new Label(0, 0, ChgNullToEmpty(orgName, ""),
					cellOrgName));

			ww.copySheet("Template", ChgNullToEmpty(period, "") + "_"
					+ ChgNullToEmpty(year, ""), ww.getNumberOfSheets());

			i = 0;
			j++;
			sheet1.getSettings().setPassword("123");
			sheet1.getSettings().setProtected(true);
			sheet1 = ww.getSheet(j);
			sheet1.addCell(new Label(0, 2,"��ШӧǴ "+periodName+" �.�. "+year+"                          �ѹ������� : "+sdfPrint.format(gd.getTime()),headRight));	
			
			for (Iterator itt = wgPrWorkDayList.iterator(); itt.hasNext();) {
				vo = (WgPrWorkDayVO) itt.next();

				dept = ChgNullToEmpty(vo.getSecCode(),
						ChgNullToEmpty(vo.getAreaCode(), ""));
				dept = dept + " " + ChgNullToEmpty(vo.getAreaDesc(), "");
				dept = dept + " " + ChgNullToEmpty(vo.getSecDesc(), "");

				// Column DeptDesc
				if (!deptDesc.equalsIgnoreCase(ChgNullToEmpty(dept, ""))) {
					if (!flagSum.equals("")) {
						sheet1.mergeCells(0, startRow + i, 1, startRow + i);
						sheet1.addCell(new Label(0, startRow+i, "���", borderNumber2));
						sheet1.addCell(new Label(2, startRow + i,
								convertFormatInt(new Double(sumWageAmt)),
								borderNumber2));
						sheet1.addCell(new Label(3, startRow + i,
								convertFormatHour(new Double(sumWorkDay)),
								borderNumber2));
						sheet1.addCell(new Label(4, startRow + i,
								convertFormatHour(new Double(sumWorkAmt)),
								borderNumber2));
						sumWageAmt = 0.0;
						sumWorkDay = 0.0;
						sumWorkAmt = 0.0;
						flagSum = "";
						i++;
					}
					sheet1.setRowView(startRow + i, 320);
					sheet1.addCell(new Label(0, startRow + i, String.valueOf(ChgNullToEmpty(dept, "")), DeptDescF));
					deptDesc = ChgNullToEmpty(dept, "");
					sheet1.addCell(new Blank(1, startRow + i, colEname));
					sheet1.addCell(new Blank(2, startRow + i, colFullDay));
					sheet1.addCell(new Blank(3, startRow + i, colHalfDay));
					sheet1.addCell(new Blank(4, startRow + i, colFlag));
					i++;

				}

				sheet1.setRowView(startRow + i, 320);

				if (m == (wgPrWorkDayList.size() - 1)) {
					// Column EmpCode
					sheet1.addCell(new Label(0, startRow + i, vo.getEmpCode(),
							cellEmpNo));

					// Column Ename
					sheet1.addCell(new Label(1, startRow + i, vo
							.getPrefixName()
							+ " "
							+ vo.getFirstName()
							+ " "
							+ vo.getLastName(), cellEname));

					// Column Full Day
					sheet1.addCell(new Label(2, startRow + i, String
							.valueOf(dfInt.format(vo.getWageAmt())),
							cellFullDay));

					// Column Half Day
					sheet1.addCell(new Label(3, startRow + i, String.valueOf(df
							.format(vo.getWorkDay())), cellHalfDay));

					// Column FlagDesc
					sheet1.addCell(new Label(4, startRow + i, String.valueOf(df
							.format(vo.getWorkAmt())), cellFlag));
					i++;
					flagSum = "sum";
					if (vo.getWageAmt() != null) {
						sumWageAmt += vo.getWageAmt().doubleValue();
					}
					if (vo.getWorkDay() != null) {
						sumWorkDay += vo.getWorkDay().doubleValue();
					}
					if (vo.getWorkAmt() != null) {
						sumWorkAmt += vo.getWorkAmt().doubleValue();
					}
					if (!flagSum.equals("")) {
						sheet1.mergeCells(0, startRow + i, 1, startRow + i);
						sheet1.addCell(new Label(0, startRow+i, "���", borderNumber2));
						sheet1.addCell(new Label(2, startRow + i,
								convertFormatInt(new Double(sumWageAmt)),
								borderNumber2));
						sheet1.addCell(new Label(3, startRow + i,
								convertFormatHour(new Double(sumWorkDay)),
								borderNumber2));
						sheet1.addCell(new Label(4, startRow + i,
								convertFormatHour(new Double(sumWorkAmt)),
								borderNumber2));
						sumWageAmt = 0.0;
						sumWorkDay = 0.0;
						sumWorkAmt = 0.0;
						flagSum = "";
						i++;
					}
				} else {
					// Column EmpCode
					sheet1.addCell(new Label(0, startRow + i, vo.getEmpCode(),
							colEmpNo));

					// Column Ename
					sheet1.addCell(new Label(1, startRow + i, vo
							.getPrefixName()
							+ " "
							+ vo.getFirstName()
							+ " "
							+ vo.getLastName(), colEname));

					// Column Full Day
					sheet1.addCell(new Label(2, startRow + i, String
							.valueOf(dfInt.format(vo.getWageAmt())), colFullDay));

					// Column Half Day
					sheet1.addCell(new Label(3, startRow + i, String.valueOf(df
							.format(vo.getWorkDay())), colHalfDay));

					// Column FlagDesc
					sheet1.addCell(new Label(4, startRow + i, String.valueOf(df
							.format(vo.getWorkAmt())), colFlag));
					flagSum = "sum";
					if (vo.getWageAmt() != null) {
						sumWageAmt += vo.getWageAmt().doubleValue();
					}
					if (vo.getWorkDay() != null) {
						sumWorkDay += vo.getWorkDay().doubleValue();
					}
					if (vo.getWorkAmt() != null) {
						sumWorkAmt += vo.getWorkAmt().doubleValue();
					}
				}

				i++;
				m++;
			}

			sheet1.getSettings().setPassword("123");
			sheet1.getSettings().setProtected(true);

			ww.removeSheet(0);
			ww.setProtected(true);
			ww.write();
			ww.close();

			wb.close();
			in.close();

			year = "";
			period = "";
			deptDesc = "";
			dept = "";
			m = 0;

			return null;
		} else {
			ww.copySheet("Template", "����բ�����" , ww.getNumberOfSheets());	
			sheet1 = ww.getSheet(1);

			sheet1.addCell(new Label(0, 0, ChgNullToEmpty(orgName, ""),cellOrgName));
			sheet1.addCell(new Label(0, 2,"��ШӧǴ "+periodName+" �.�. "+year+"                          �ѹ������� : "+sdfPrint.format(gd.getTime()),headRight));
			
			sheet1.mergeCells(0,4,4,4);
			sheet1.addCell(new Label(0, 4, "����բ�����",borderAll));
			
			sheet1.getSettings().setPassword("123");
			sheet1.getSettings().setProtected(true);
			ww.removeSheet(0);
			ww.setProtected(true);
			ww.write();
			ww.close();
			wb.close();
			in.close();
			return null;
		}

	}

	/**
	 * method ����Ѻ�ŧ Total Format
	 * 
	 * @param hour
	 * @return
	 */
	private String convertFormatHour(Double total) {
		String rlst = "";
		// DecimalFormat dec = new DecimalFormat("###,##0");
		DecimalFormat dec = new DecimalFormat("###,##0.00");
		rlst = dec.format(total != null ? total.doubleValue() : 0.0);
		return rlst;
	}

	private String convertFormatInt(Double total) {
		String rlst = "";
		// DecimalFormat dec = new DecimalFormat("###,##0");
		DecimalFormat dec = new DecimalFormat("###,##0");
		rlst = dec.format(total != null ? total.doubleValue() : 0.0);
		return rlst;
	}
}
