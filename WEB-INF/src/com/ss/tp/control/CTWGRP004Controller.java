package com.ss.tp.control;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.CellFormat;
import jxl.format.VerticalAlignment;
import jxl.write.Blank;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.ss.tp.dto.WgPrIncDecOtherVO;
import com.ss.tp.service.SuOrganizeService;
import com.ss.tp.service.WgIncomeDeductService;

public class CTWGRP004Controller extends MultiActionController {

	public SuOrganizeService getSuOrganizeService() {
		return (SuOrganizeService) this.getApplicationContext().getBean(
				"suOrganizeService");
	}

	private WgIncomeDeductService getWgIncomeDeductService() {
		return (WgIncomeDeductService) this.getApplicationContext().getBean(
				"wgIncomeDeductService");
	}

	public ModelAndView doPrintReport(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		String ouCode = "";
		String userId = "";
		int year = 0;
		int period = 0;
		String periodName = "";
		String flag = "";
		String incDecCode = "";
		String incDecName = "";

		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition",
				"attachment; filename=./page/report/CTWGRP004.xls");
		InputStream in = this.getServletContext().getResourceAsStream(
				"/page/report/CTWGRP004.xls");
		if (request.getParameter("ouCode") != null) {
			ouCode = request.getParameter("ouCode");
		}

		if (request.getParameter("userId") != null) {
			userId = request.getParameter("userId");
		}

		if (request.getParameter("year") != null
				&& !"".equals(request.getParameter("year"))) {
			year = Integer.parseInt(request.getParameter("year"));
		}

		if (request.getParameter("period") != null) {
			period = Integer.parseInt(request.getParameter("period"));
		}
		if (request.getParameter("periodName") != null) {
			periodName = request.getParameter("periodName");
		}

		if (request.getParameter("flag") != null) {
			flag = request.getParameter("flag");
		}

		if (request.getParameter("incDecTmp") != null) {
			incDecCode = request.getParameter("incDecTmp");
			if (!incDecCode.equals("%")) {
				incDecName = this.getWgIncomeDeductService().getIncDecName(
						ouCode, 1, incDecCode);
			}
		}
		System.out.println("\n\n inDecCode after request==>>" + incDecCode);
		Workbook wb = Workbook.getWorkbook(in);
		WritableWorkbook ww = Workbook.createWorkbook(
				response.getOutputStream(), wb);
		String ouName = this.getSuOrganizeService().findOrganizeName(ouCode);

		WritableFont fontBold = new WritableFont(WritableFont.ARIAL);
		fontBold.setBoldStyle(WritableFont.BOLD);
		fontBold.setPointSize(9);

		WritableFont fontNormal = new WritableFont(WritableFont.ARIAL);
		fontNormal.setBoldStyle(WritableFont.NO_BOLD);
		fontNormal.setPointSize(9);

		DecimalFormat df = new DecimalFormat("###,##0.00");

		Alignment dataAlignLeft = Alignment.LEFT;
		Alignment dataAlignRight = Alignment.RIGHT;
		Alignment dataAlignCenter = Alignment.CENTRE;

		WritableCellFormat boldLeftFormat = new WritableCellFormat();
		boldLeftFormat.setBorder(Border.LEFT, BorderLineStyle.THIN);
		boldLeftFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		boldLeftFormat.setAlignment(dataAlignLeft);
		boldLeftFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		boldLeftFormat.setFont(fontBold);

		WritableCellFormat HeadFormat = new WritableCellFormat();
		HeadFormat.setAlignment(dataAlignCenter);
		HeadFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		HeadFormat.setFont(fontBold);

		WritableCellFormat HeadLeftFormat = new WritableCellFormat();
		HeadLeftFormat.setAlignment(dataAlignLeft);
		HeadLeftFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		HeadLeftFormat.setFont(fontBold);

		WritableCellFormat normalLeftFormat = new WritableCellFormat();
		normalLeftFormat.setBorder(Border.LEFT, BorderLineStyle.THIN);
		normalLeftFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		normalLeftFormat.setAlignment(dataAlignLeft);
		normalLeftFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalLeftFormat.setFont(fontNormal);

		WritableCellFormat normalCenterFormat = new WritableCellFormat();
		normalCenterFormat.setBorder(Border.LEFT, BorderLineStyle.THIN);
		normalCenterFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		normalCenterFormat.setAlignment(dataAlignCenter);
		normalCenterFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalCenterFormat.setFont(fontNormal);

		WritableCellFormat normalRightFormat = new WritableCellFormat();
		normalRightFormat.setBorder(Border.LEFT, BorderLineStyle.THIN);
		normalRightFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		normalRightFormat.setAlignment(dataAlignRight);
		normalRightFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalRightFormat.setFont(fontNormal);

		WritableCellFormat normalLeftLastFormat = new WritableCellFormat();
		normalLeftLastFormat.setBorder(Border.LEFT, BorderLineStyle.THIN);
		normalLeftLastFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		normalLeftLastFormat.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
		normalLeftLastFormat.setAlignment(dataAlignLeft);
		normalLeftLastFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalLeftLastFormat.setFont(fontNormal);

		WritableCellFormat normalCenterLastFormat = new WritableCellFormat();
		normalCenterLastFormat.setBorder(Border.LEFT, BorderLineStyle.THIN);
		normalCenterLastFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		normalCenterLastFormat.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
		normalCenterLastFormat.setAlignment(dataAlignCenter);
		normalCenterLastFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalCenterLastFormat.setFont(fontNormal);

		WritableCellFormat normalRightLastFormat = new WritableCellFormat();
		normalRightLastFormat.setBorder(Border.LEFT, BorderLineStyle.THIN);
		normalRightLastFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		normalRightLastFormat.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
		normalRightLastFormat.setAlignment(dataAlignRight);
		normalRightLastFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalRightLastFormat.setFont(fontNormal);

		WritableCellFormat borderEndLine = new WritableCellFormat();
		borderEndLine.setBorder(Border.TOP, BorderLineStyle.THIN);
		borderEndLine.setVerticalAlignment(VerticalAlignment.CENTRE);

		WritableCellFormat borderNumber2 = new WritableCellFormat();
		borderNumber2.setBorder(Border.ALL, BorderLineStyle.THIN);
		borderNumber2.setAlignment(Alignment.RIGHT);
		borderNumber2.setFont(fontBold);

		WritableCellFormat headRight = new WritableCellFormat();
		headRight.setAlignment(Alignment.RIGHT);
		headRight.setFont(fontBold);

		GregorianCalendar gd = new GregorianCalendar();
		SimpleDateFormat sdfPrint = new SimpleDateFormat("dd/MM/yyyy HH:mm",
				new java.util.Locale("th", "TH"));

		List empList = this.getWgIncomeDeductService().incDecOtherReport(
				userId, ouCode, new Long(year), new Long(period), incDecCode,
				flag);// hard code
		List sheetList = this.getWgIncomeDeductService()
				.incDecOtherReportCountSheet(userId, ouCode, new Long(year),
						new Long(period), incDecCode, flag);// hard code
		String ouDesc = this.getSuOrganizeService().findOrganizeName(ouCode);
		// ------------ Start Add code by bow----------------
		List sheetNameList = sheetList;
		int countSheetName = sheetNameList.size();
		int countIncDecCode = sheetNameList.size();
		String tempInDecCode = "";
		String tempInDecName = "";
		WgPrIncDecOtherVO voSheet = null;

		WritableSheet sheet[];
		String nameSheet = "";
		if (countIncDecCode == 0) {// for build Sheet
			sheet = new WritableSheet[1];
		} else {
			sheet = new WritableSheet[countIncDecCode];
		}

		if (countSheetName > 0) {// for Sheet Name
			for (int i = 0; i < countSheetName - 1; i++) {
				voSheet = (WgPrIncDecOtherVO) sheetNameList.get(i + 1);
				ww.copySheet("Sheet1", voSheet.getIncDecName(),
						ww.getNumberOfSheets());
			}
		}
		if (sheetList.size() > 0) {
			voSheet = (WgPrIncDecOtherVO) sheetList.get(0);
			tempInDecCode = voSheet.getIncDecCode();
			tempInDecName = voSheet.getIncDecName();
		}
		sheet[0] = ww.getSheet(0);
		if (countSheetName == 0) {
			sheet[0].setName("��辺������");
		} else {
			sheet[0].setName(tempInDecName);
		}
		// ------------- End Add Code by bow-----------------------
		CellFormat head = sheet[0].getWritableCell(0, 0).getCellFormat();
		CellFormat group = sheet[0].getWritableCell(2, 7).getCellFormat();
		CellFormat borderLR = sheet[0].getWritableCell(5, 7).getCellFormat();
		CellFormat textFt = sheet[0].getWritableCell(4, 7).getCellFormat();
		CellFormat number = sheet[0].getWritableCell(1, 7).getCellFormat();

		/* start title */
		sheet[0].addCell(new Label(0, 0, ouName, head));
		sheet[0].addCell(new Label(0, 1,"����� CTWGRP004                                 ��§ҹ "+(tempInDecName.equals("")?incDecName.equals("")?"�����������":incDecName:tempInDecName),HeadLeftFormat));
		sheet[0].addCell(new Label(0,2,"��ШӧǴ "+periodName+" �.�."+year+"                           �ѹ������� : "+sdfPrint.format(gd.getTime()), headRight));
		sheet[0].getSettings().setPassword("123");
		sheet[0].getSettings().setProtected(true);

		/* end title */

		WgPrIncDecOtherVO wgPrEmpVo = null;
		System.out.println(year);
		System.out.println(period);
		System.out.println(incDecCode);
		System.out.println(flag);

		int startRow = 4;
		int j = 0;
		if (empList.size() > 0) {
			String orgDesc = "";
			String flagDesc = "";
			int row = startRow;
			int count = 0;
			double sumTotalAmt = 0.0;
			String flagTotal = "";
			for (Iterator itt = empList.iterator(); itt.hasNext();) {
				WgPrIncDecOtherVO vo = (WgPrIncDecOtherVO) itt.next();
				count = count + 1;
				if (!tempInDecCode.equals(vo.getIncDecCode())) {
					if (!flagTotal.equals("")) {
						sheet[j].mergeCells(0, row, 1, row);
						sheet[j].addCell(new Label(0, row, "���", borderNumber2));
						sheet[j].addCell(new Label(2, row,
								convertFormatHour(new Double(sumTotalAmt)),
								borderNumber2));
						sheet[j].addCell(new Label(3, row, "", borderNumber2));
						sumTotalAmt = 0.0;
						flagTotal = "";
						row++;
					}
					tempInDecCode = vo.getIncDecCode();
					tempInDecName = vo.getIncDecName();
					if (j > 0) {
						for (int y = 0; y < 4; y++) {
							sheet[j].setRowView(startRow, 320);
							sheet[j].addCell(new Label(y, row, "",
									borderEndLine));
						}
					}

					j++;

					row = startRow;
					sheet[j] = ww.getSheet(j);
					sheet[j].setRowView(row, 320);
					sheet[j].addCell(new Label(0, 0, ouName, head));
					sheet[j].addCell(new Label(0, 1,"����� CTWGRP004                                 ��§ҹ"+tempInDecName,HeadLeftFormat));
					sheet[j].addCell(new Label(0,2,"��ШӧǴ "+periodName+" �.�."+year+"                           �ѹ������� : "+sdfPrint.format(gd.getTime()), headRight));
					sheet[j].getSettings().setPassword("123");
					sheet[j].getSettings().setProtected(true);
				}
				if (!orgDesc.equals(vo.getOrgDesc())
						&& !"".equals(ChgNullToEmpty(vo.getOrgDesc().trim(), ""))) {
					if (!flagTotal.equals("")) {
						sheet[j].mergeCells(0, row, 1, row);
						sheet[j].addCell(new Label(0, row, "���", borderNumber2));
						sheet[j].addCell(new Label(2, row,
								convertFormatHour(new Double(sumTotalAmt)),
								borderNumber2));
						sheet[j].addCell(new Label(3, row, "", borderNumber2));
						sumTotalAmt = 0.0;
						flagTotal = "";
						row++;
					}
					sheet[j].setRowView(row, 320);
					sheet[j].addCell(new Label(0, row, ChgNullToEmpty(
							vo.getOrgDesc(), ""), boldLeftFormat));
					sheet[j].addCell(new Blank(1, row, boldLeftFormat));
					sheet[j].addCell(new Blank(2, row, boldLeftFormat));
					sheet[j].addCell(new Blank(3, row, boldLeftFormat));
					orgDesc = ChgNullToEmpty(vo.getOrgDesc(), "");
					row++;
				}

				if (empList.size() == count) {
					sheet[j].setRowView(row, 320);
					sheet[j].addCell(new Label(0, row, vo.getEmpCode(),
							normalCenterLastFormat));
					sheet[j].addCell(new Label(1, row, vo.getEmpName(),
							normalLeftLastFormat));
					sheet[j].addCell(new Label(2, row, String.valueOf(df
							.format(vo.getTotalAmt())), normalRightLastFormat));
					flagTotal = "sum";
					if (vo.getTotalAmt() != null) {
						sumTotalAmt += vo.getTotalAmt().doubleValue();

					}
					flagDesc = "";
					if (vo.getFlagPr() != null) {
						flagDesc = "���ԡ���¡�׹";
						if (vo.getFlagPr().equals("N")){ flagDesc = "";}
						if (vo.getFlagPr().equals("A")){ flagDesc = "��Ѻ��ا";}
						if (vo.getFlagPr().equals("R")){ flagDesc = "���¡�׹";}
						if (vo.getFlagPr().equals("B")){ flagDesc = "���ԡ��Ѻ��ا";}
					}
					sheet[j].addCell(new Label(3, row, flagDesc,
							normalLeftLastFormat));
					row++;
					if (!flagTotal.equals("")) {
						sheet[j].mergeCells(0, row, 1, row);
						sheet[j].addCell(new Label(0, row, "���", borderNumber2));
						sheet[j].addCell(new Label(2, row,
								convertFormatHour(new Double(sumTotalAmt)),
								borderNumber2));
						sheet[j].addCell(new Label(3, row, "", borderNumber2));
						sumTotalAmt = 0.0;
						flagTotal = "";

					}

				} else {
					sheet[j].setRowView(row, 320);
					sheet[j].addCell(new Label(0, row, vo.getEmpCode(),
							normalCenterFormat));
					sheet[j].addCell(new Label(1, row, vo.getEmpName(),
							normalLeftFormat));
					sheet[j].addCell(new Label(2, row, String.valueOf(df
							.format(vo.getTotalAmt())), normalRightFormat));
					flagTotal = "sum";
					if (vo.getTotalAmt() != null) {
						sumTotalAmt += vo.getTotalAmt().doubleValue();

					}
					flagDesc = "";
					if (vo.getFlagPr() != null) {
						flagDesc = "���ԡ���¡�׹";
						if (vo.getFlagPr().equals("N")){ flagDesc = "";}
						if (vo.getFlagPr().equals("A")){ flagDesc = "��Ѻ��ا";}
						if (vo.getFlagPr().equals("R")){ flagDesc = "���¡�׹";}
						if (vo.getFlagPr().equals("B")){ flagDesc = "���ԡ��Ѻ��ا";}
					}
					sheet[j].addCell(new Label(3, row, flagDesc,
							normalLeftFormat));
				}
				row++;
				if (j == 0 || (j + 1) == empList.size()) {
					for (int y = 0; y < 4; y++) {
						sheet[j].setRowView(row, 320);
						sheet[j].addCell(new Label(y, row, "", borderEndLine));
					}
				}
			}
			ww.write();
			ww.close();
			wb.close();
			in.close();
			return null;
		} else {
			sheet[0].addCell(new Label(0, 0, ouDesc, HeadFormat));
			sheet[0].setRowView(4, 320);
			sheet[0].mergeCells(0, 4, 3, 4);
			sheet[0].addCell(new Label(0, 4, "��辺������",normalCenterLastFormat));	
			sheet[0].removeRow(5);
			ww.write();
			ww.close();
			wb.close();
			in.close();
			return null;
		}

	}

	private String ChgNullToEmpty(String str1, String str2) {
		if (str1 == null) {
			str1 = str2;
		}
		return str1;
	}

	/**
	 * method ����Ѻ�ŧ Total Format
	 * 
	 * @param hour
	 * @return
	 */
	private String convertFormatHour(Double total) {
		String rlst = "";
		// DecimalFormat dec = new DecimalFormat("###,##0");
		DecimalFormat dec = new DecimalFormat("###,##0.00");
		rlst = dec.format(total != null ? total.doubleValue() : 0.0);
		return rlst;
	}
}
