/*
 * Created on 17 ?.?. 2549
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.ss.tp.control;







import java.io.InputStream;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;

import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.CellFormat;
import jxl.format.VerticalAlignment;
import jxl.write.Blank;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableFont.FontName;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;


import com.ss.tp.dto.ApTransferVO;
import com.ss.tp.security.UserInfo;
import com.ss.tp.service.ApTableService;
import com.ss.tp.service.SuOrganizeService;






/**
 * @author
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class CTAPRP006Controller extends MultiActionController {

	private String ChgNullToEmpty(String str1, String str2) {
		if (str1 == null) {
			str1 = str2;
		}
		return str1;
	}
	
	private String ConvertMonth(String evaMonth) {
		int m = Integer.parseInt(evaMonth);
		String monthText="";
		    if (m==1){
		    	monthText ="���Ҥ�";
		    }else if(m==2){
		    	monthText ="����Ҿѹ��";
		    }else if(m==3){
		    	monthText ="�չҤ�";
		    }else if(m==4){
		    	monthText ="����¹";
		    }else if(m==5){
		    	monthText ="����Ҥ�";
		    }else if(m==6){
		    	monthText ="�Զع�¹";
		    }else if(m==7){
		    	monthText ="�á�Ҥ�";
		    }else if(m==8){
		    	monthText ="�ԧ�Ҥ�";
		    }else if(m==9){
		    	monthText ="�ѹ��¹";
		    }else if(m==10){
		    	monthText ="���Ҥ�";
		    }else if(m==11){
		    	monthText ="��Ȩԡ�¹";
		    }else if(m==12){
		    	monthText ="�ѹ�Ҥ�";
		    }
		    return monthText;
	}

	private ApTableService getApTableService() {
		return (ApTableService) this.getApplicationContext().getBean(
				"apTableService");
	}

	public SuOrganizeService getSuOrganizeService() {
		return (SuOrganizeService) this.getApplicationContext().getBean(
				"suOrganizeService");
	}

	

	public ModelAndView doPrintReport(HttpServletRequest request,HttpServletResponse response) throws Exception {
		DecimalFormat df = new DecimalFormat("###,##0.00");
		DecimalFormat dfInt = new DecimalFormat("###,##0");
		DecimalFormat dfYear = new DecimalFormat("###########0");
		UserInfo userInfo = (UserInfo) request.getSession().getAttribute("UserLogin");
		//String ouCode = "";
		//String userId = "";
		
		int row = 0;
		int seq = 0;

		String evaYear = "";
		String evaMonth = "";
		
		

		double sumAllDebitMoney = 0.00;
	    double sumAllCreditMoney = 0.00;
	    double sumAllRec = 0;


		if (request.getParameter("workYear") != null
				&& !"".equals(request.getParameter("workYear"))) {
			evaYear = request.getParameter("workYear");
		}
		if (request.getParameter("workMonth") != null
				&& !"".equals(request.getParameter("workMonth"))) {
			evaMonth = request.getParameter("workMonth");
		}

		
     
		
		
      
		//String ouDesc = this.getSuOrganizeService().findOrganizeName(ouCode);

		// ----------------- Generate Report Detail
		// -------------------------------

		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition",
				"attachment; filename=./page/report/CTAPRP006.xls");
		InputStream in = this.getServletContext().getResourceAsStream(
				"/page/report/CTAPRP006.xls");
		    
		String ouDesc = this.getSuOrganizeService().findOrganizeName(userInfo.getOuCode());

		
		WritableFont fontBold = new WritableFont(WritableFont.ARIAL);
		fontBold.setBoldStyle(WritableFont.BOLD);
		fontBold.setPointSize(9);

		// WritableFont fontNormal = new WritableFont(WritableFont.ARIAL);
		// fontNormal.setBoldStyle(WritableFont.NO_BOLD);
		// fontNormal.setPointSize(9);

		FontName fontName = WritableFont.createFont("ARIAL");
		WritableFont fontNormal = new WritableFont(fontName, 9);
		fontNormal.setBoldStyle(WritableFont.NO_BOLD);
		fontNormal.setPointSize(9);

		Alignment dataAlignLeft = Alignment.LEFT;
		Alignment dataAlignRight = Alignment.RIGHT;
		Alignment dataAlignCenter = Alignment.CENTRE;

		WritableCellFormat boldLeftFormat = new WritableCellFormat();
		boldLeftFormat.setBorder(Border.LEFT, BorderLineStyle.THIN);
		boldLeftFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		boldLeftFormat.setAlignment(dataAlignLeft);
		boldLeftFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		boldLeftFormat.setFont(fontBold);

		WritableCellFormat HeadFormat = new WritableCellFormat();
		HeadFormat.setAlignment(dataAlignCenter);
		HeadFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		HeadFormat.setFont(fontBold);

		WritableCellFormat headRight = new WritableCellFormat();
		headRight.setAlignment(dataAlignRight);
		headRight.setVerticalAlignment(VerticalAlignment.CENTRE);
		headRight.setFont(fontBold);

		WritableCellFormat headLeft = new WritableCellFormat();
		headLeft.setAlignment(dataAlignLeft);
		headLeft.setVerticalAlignment(VerticalAlignment.CENTRE);
		headLeft.setFont(fontBold);

		
		WritableCellFormat normalLeftFormat = new WritableCellFormat();
		normalLeftFormat.setBorder(Border.LEFT,BorderLineStyle.THIN);
		normalLeftFormat.setBorder(Border.RIGHT,BorderLineStyle.THIN);
		normalLeftFormat.setAlignment(dataAlignLeft);
		normalLeftFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalLeftFormat.setFont(fontNormal);

		WritableCellFormat normalCenterFormat = new WritableCellFormat();
		normalCenterFormat.setBorder(Border.LEFT,BorderLineStyle.THIN);
		normalCenterFormat.setBorder(Border.RIGHT,BorderLineStyle.THIN);
		normalCenterFormat.setAlignment(dataAlignCenter);
		normalCenterFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalCenterFormat.setFont(fontNormal);

		WritableCellFormat normalRightFormat = new WritableCellFormat();
		normalRightFormat.setBorder(Border.LEFT,BorderLineStyle.THIN);
		normalRightFormat.setBorder(Border.RIGHT,BorderLineStyle.THIN);
		normalRightFormat.setAlignment(dataAlignRight);
		normalRightFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalRightFormat.setFont(fontNormal);

		WritableCellFormat normalLeftLastFormat = new WritableCellFormat();
		normalLeftLastFormat.setBorder(Border.LEFT,BorderLineStyle.THIN);
		normalLeftLastFormat.setBorder(Border.RIGHT,BorderLineStyle.THIN);
		normalLeftLastFormat.setBorder(Border.BOTTOM,BorderLineStyle.THIN);
		normalLeftLastFormat.setAlignment(dataAlignLeft);
		normalLeftLastFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalLeftLastFormat.setFont(fontNormal);

		WritableCellFormat normalCenterLastFormat = new WritableCellFormat();
		normalCenterLastFormat.setBorder(Border.LEFT,BorderLineStyle.THIN);
		normalCenterLastFormat.setBorder(Border.RIGHT,BorderLineStyle.THIN);
		normalCenterLastFormat.setBorder(Border.BOTTOM,BorderLineStyle.THIN);
		normalCenterLastFormat.setAlignment(dataAlignCenter);
		normalCenterLastFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalCenterLastFormat.setFont(fontNormal);

		WritableCellFormat normalRightLastFormat = new WritableCellFormat();
		normalRightLastFormat.setBorder(Border.LEFT,BorderLineStyle.THIN);
		normalRightLastFormat.setBorder(Border.RIGHT,BorderLineStyle.THIN);
		normalRightLastFormat.setBorder(Border.BOTTOM,BorderLineStyle.THIN);
		normalRightLastFormat.setAlignment(dataAlignRight);
		normalRightLastFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalRightLastFormat.setFont(fontNormal);

		WritableCellFormat normalRightLastBoldFormat = new WritableCellFormat();
		normalRightLastBoldFormat.setBorder(Border.LEFT,BorderLineStyle.THIN);
		normalRightLastBoldFormat.setBorder(Border.RIGHT,BorderLineStyle.THIN);
		normalRightLastBoldFormat.setBorder(Border.BOTTOM,BorderLineStyle.THIN);
		normalRightLastBoldFormat.setAlignment(dataAlignRight);
		normalRightLastBoldFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalRightLastBoldFormat.setFont(fontBold);

		WritableCellFormat borderNumber2 = new WritableCellFormat();
		borderNumber2.setBorder(Border.ALL, BorderLineStyle.THIN);
		borderNumber2.setAlignment(Alignment.RIGHT);
		borderNumber2.setFont(fontBold);

		Workbook wb = Workbook.getWorkbook(in);
		WritableWorkbook ww = Workbook.createWorkbook(response.getOutputStream(), wb);
		WritableSheet sheet1 = ww.getSheet(0);
		
	

		GregorianCalendar gd = new GregorianCalendar();
		SimpleDateFormat sdfPrint = new SimpleDateFormat("dd/MM/yyyy HH:mm",new java.util.Locale("th", "TH"));
		List empList = this.getApTableService().apTransferReport( evaYear, evaMonth);
			
		sheet1.addCell(new Label(0, 0, ouDesc, HeadFormat));
		// sheet1.addCell(new Label(0,
		// 2,"��ШӧǴ "+section+" �.�. "+year+" �ѹ����~� :
		// "+sdfPrint.format(gd.getTime()),headRight));
		sheet1.addCell(new Label(0,3, "�������  "+userInfo.getUserName(), headLeft));
		
		
		sheet1.addCell(new Label(2, 3,"��Ш���͹ "+ConvertMonth(evaMonth)+" �.�. "+evaYear,HeadFormat));
		sheet1.addCell(new Label(3, 3,"�ѹ������� : "+sdfPrint.format(gd.getTime()),headRight));
		
		if (empList.size() > 0) {

			row = 6;
		

			for (Iterator itt = empList.iterator(); itt.hasNext();) {
				ApTransferVO vo = (ApTransferVO) itt.next();
				
				String k="";
			
				
				//String accName="";
				String accname1 ="����ѡ�Ҿ�Һ�Ţͧ��ѡ�ҹ";
				String accname2 ="����ѡ�Ҿ�Һ�Ţͧ��ͺ���Ǿ�ѡ�ҹ";
				String accname3 ="�ѭ�վѡ�����Թ��͹";
                 
				double c = Double.valueOf(vo.getCont().trim()).doubleValue();
				sheet1.addCell(new Label(0, row, String.valueOf(dfInt.format(c)),normalRightLastFormat));
				sheet1.addCell(new Label(1, row, vo.getAccount(),normalRightLastFormat));
				k = vo.getAccount();
				if  (k.equals("51020201")){
					//accName = accname1;
					sheet1.addCell(new Label(2,row,accname1,normalLeftLastFormat));
				}else if  (k.equals("51020204")){
					//accName = accname2;
					sheet1.addCell(new Label(2,row,accname2,normalLeftLastFormat));
				}else if  (k.equals("52020201")){
					//accName = accname1;
					sheet1.addCell(new Label(2,row,accname1,normalLeftLastFormat));
				}else if  (k.equals("52020204")){
					//accName = accname2;
					sheet1.addCell(new Label(2,row,accname2,normalLeftLastFormat));
				}else if  (k.equals("72020207")){
					//accName = accname3;
					sheet1.addCell(new Label(2,row,accname3,normalLeftLastFormat));
					sumAllRec= Double.valueOf(vo.getCont().trim()).doubleValue();
				}
				
				
			
				//sheet1.addCell(new Label(2,row,accName,normalLeftLastFormat));
				double d = Double.valueOf(vo.getDebit().trim()).doubleValue();
				double dd = Double.valueOf(vo.getCredit().trim()).doubleValue();
				
				if (d!=0.00){
				sheet1.addCell(new Label(3, row, String.valueOf(df.format(d)),normalRightLastFormat));
				}else {
					sheet1.addCell(new Label(3, row, "", borderNumber2));	
				}
				if (dd!=0.00){
				sheet1.addCell(new Label(4, row, String.valueOf(df.format(dd)),normalRightLastFormat));
				}else {
					sheet1.addCell(new Label(4, row, "", borderNumber2));	
				}
				//sheet1.addCell(new Label(4, row, String.valueOf(df.format(dd)),
				//		normalRightLastFormat));
				//sheet1.addCell(new Label(3, row, df.format(vo.getDebit()),normalCenterLastFormat));
				//sheet1.addCell(new Label(4, row, df.format(vo.getCredit()),normalLeftLastFormat));
			
				
				sumAllDebitMoney=sumAllDebitMoney+d;
				sumAllCreditMoney=sumAllCreditMoney+dd;
				
				row++;
				
				}
			    
				
				
				//sumAllMoney=sumAllEmpMoney+sumAllOthMoney;

			sheet1.addCell(new Label(0, row, "", borderNumber2));
			sheet1.addCell(new Label(1, row, "", borderNumber2));
			sheet1.addCell(new Label(2, row, "", borderNumber2));
			sheet1.addCell(new Label(3, row, "", borderNumber2));
			sheet1.addCell(new Label(4, row, "", borderNumber2));
			
				row++;
		
			
			sheet1.mergeCells(0, row, 2, row);
			sheet1.addCell(new Label(0, row , "���������   "+ String.valueOf(dfInt.format(sumAllRec))+"  ��¡��", borderNumber2));
			sheet1.addCell(new Label(3, row,String.valueOf(df.format(sumAllDebitMoney)),normalRightLastBoldFormat));
			sheet1.addCell(new Label(4, row,String.valueOf(df.format(sumAllCreditMoney)),normalRightLastBoldFormat));
			//sheet1.addCell(new Label(8, row, "", borderNumber2));
			row++;
			
			
			sheet1.getSettings().setPassword("#028313766#$");
			sheet1.getSettings().setProtected(true);
			ww.setProtected(true);

			ww.write();
			ww.close();
			wb.close();
			in.close();
			return null;
		} else {
			sheet1.addCell(new Label(0, 0, ouDesc, HeadFormat));
			sheet1.addCell(new Label(0,3, "�������  "+userInfo.getUserName(), headLeft));
	
			sheet1.addCell(new Label(4, 3,"�ѹ������� : "+sdfPrint.format(gd.getTime()),headRight));
			sheet1.setRowView(5, 320);
			sheet1.mergeCells(0,5, 10,5);
			sheet1.addCell(new Label(0, 8, "��辺������",normalCenterLastFormat));	
			sheet1.getSettings().setPassword("#028313766#$");
			sheet1.getSettings().setProtected(true);
			ww.setProtected(true);

			ww.close();
			wb.close();
			in.close();
			return null;
		}
	}



	/**
	 * method ����Ѻ�ŧ Time Format
	 * 
	 * @param hour
	 * @return
	 */
	private String convertFormatHour(Double hour) {
		String rlst = "";
		DecimalFormat dec = new DecimalFormat("###,##0");
		rlst = dec.format(hour != null ? hour.intValue() : 0);
		return rlst;
	}
}
