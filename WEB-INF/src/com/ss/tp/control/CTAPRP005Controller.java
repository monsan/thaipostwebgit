/*
 * Created on 17 ?.?. 2549
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.ss.tp.control;







import java.io.InputStream;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;

import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.CellFormat;
import jxl.format.VerticalAlignment;
import jxl.write.Blank;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableFont.FontName;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;


import com.ss.tp.dto.ApEmployeeVO;
import com.ss.tp.dto.ApSumBatchVO;
import com.ss.tp.security.UserInfo;
import com.ss.tp.service.ApTableService;
import com.ss.tp.service.SuOrganizeService;






/**
 * @author
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class CTAPRP005Controller extends MultiActionController {

	private String ChgNullToEmpty(String str1, String str2) {
		if (str1 == null) {
			str1 = str2;
		}
		return str1;
	}
	
	private String ConvertMonth(String evaMonth) {
		int m = Integer.parseInt(evaMonth);
		String monthText="";
		    if (m==1){
		    	monthText ="���Ҥ�";
		    }else if(m==2){
		    	monthText ="����Ҿѹ��";
		    }else if(m==3){
		    	monthText ="�չҤ�";
		    }else if(m==4){
		    	monthText ="����¹";
		    }else if(m==5){
		    	monthText ="����Ҥ�";
		    }else if(m==6){
		    	monthText ="�Զع�¹";
		    }else if(m==7){
		    	monthText ="�á�Ҥ�";
		    }else if(m==8){
		    	monthText ="�ԧ�Ҥ�";
		    }else if(m==9){
		    	monthText ="�ѹ��¹";
		    }else if(m==10){
		    	monthText ="���Ҥ�";
		    }else if(m==11){
		    	monthText ="��Ȩԡ�¹";
		    }else if(m==12){
		    	monthText ="�ѹ�Ҥ�";
		    }
		    return monthText;
	}

	private ApTableService getApTableService() {
		return (ApTableService) this.getApplicationContext().getBean(
				"apTableService");
	}

	public SuOrganizeService getSuOrganizeService() {
		return (SuOrganizeService) this.getApplicationContext().getBean(
				"suOrganizeService");
	}

	

	public ModelAndView doPrintReport(HttpServletRequest request,HttpServletResponse response) throws Exception {
		DecimalFormat df = new DecimalFormat("###,##0.00");
		DecimalFormat dfInt = new DecimalFormat("###,##0");
		DecimalFormat dfYear = new DecimalFormat("###########0");
		UserInfo userInfo = (UserInfo) request.getSession().getAttribute("UserLogin");
		//String ouCode = "";
		//String userId = "";
		
		int row = 0;
		int seq = 0;

		String evaYear = "";
		String evaMonth = "";
		String evaVolumeFrom = "";
		String evaVolumeTo = "";
		
		

		double sumAllEmpMoney = 0.00;
	    double sumAllOthMoney = 0.00;
	    double sumAllMoney = 0.00;


		if (request.getParameter("workYear") != null
				&& !"".equals(request.getParameter("workYear"))) {
			evaYear = request.getParameter("workYear");
		}
		if (request.getParameter("workMonth") != null
				&& !"".equals(request.getParameter("workMonth"))) {
			evaMonth = request.getParameter("workMonth");
		}

		if (request.getParameter("volumeSetFrom") != null
				&& !"".equals(request.getParameter("volumeSetFrom"))) {
			evaVolumeFrom = request.getParameter("volumeSetFrom");
		}

		if (request.getParameter("volumeSetTo") != null
				&& !"".equals(request.getParameter("volumeSetTo"))) {
			evaVolumeTo = request.getParameter("volumeSetTo");
		}
		
		
		
		
      
		//String ouDesc = this.getSuOrganizeService().findOrganizeName(ouCode);

		// ----------------- Generate Report Detail
		// -------------------------------

		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition",
				"attachment; filename=./page/report/CTAPRP005.xls");
		InputStream in = this.getServletContext().getResourceAsStream(
				"/page/report/CTAPRP005.xls");
	     
		String ouDesc = this.getSuOrganizeService().findOrganizeName(userInfo.getOuCode());

		
		WritableFont fontBold = new WritableFont(WritableFont.ARIAL);
		fontBold.setBoldStyle(WritableFont.BOLD);
		fontBold.setPointSize(9);

		// WritableFont fontNormal = new WritableFont(WritableFont.ARIAL);
		// fontNormal.setBoldStyle(WritableFont.NO_BOLD);
		// fontNormal.setPointSize(9);

		FontName fontName = WritableFont.createFont("ARIAL");
		WritableFont fontNormal = new WritableFont(fontName, 9);
		fontNormal.setBoldStyle(WritableFont.NO_BOLD);
		fontNormal.setPointSize(9);

		Alignment dataAlignLeft = Alignment.LEFT;
		Alignment dataAlignRight = Alignment.RIGHT;
		Alignment dataAlignCenter = Alignment.CENTRE;

		WritableCellFormat boldLeftFormat = new WritableCellFormat();
		boldLeftFormat.setBorder(Border.LEFT, BorderLineStyle.THIN);
		boldLeftFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN);
		boldLeftFormat.setAlignment(dataAlignLeft);
		boldLeftFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		boldLeftFormat.setFont(fontBold);

		WritableCellFormat HeadFormat = new WritableCellFormat();
		HeadFormat.setAlignment(dataAlignCenter);
		HeadFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		HeadFormat.setFont(fontBold);

		WritableCellFormat headRight = new WritableCellFormat();
		headRight.setAlignment(dataAlignRight);
		headRight.setVerticalAlignment(VerticalAlignment.CENTRE);
		headRight.setFont(fontBold);

		WritableCellFormat headLeft = new WritableCellFormat();
		headLeft.setAlignment(dataAlignLeft);
		headLeft.setVerticalAlignment(VerticalAlignment.CENTRE);
		headLeft.setFont(fontBold);

		
		WritableCellFormat normalLeftFormat = new WritableCellFormat();
		normalLeftFormat.setBorder(Border.LEFT,BorderLineStyle.THIN);
		normalLeftFormat.setBorder(Border.RIGHT,BorderLineStyle.THIN);
		normalLeftFormat.setAlignment(dataAlignLeft);
		normalLeftFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalLeftFormat.setFont(fontNormal);

		WritableCellFormat normalCenterFormat = new WritableCellFormat();
		normalCenterFormat.setBorder(Border.LEFT,BorderLineStyle.THIN);
		normalCenterFormat.setBorder(Border.RIGHT,BorderLineStyle.THIN);
		normalCenterFormat.setAlignment(dataAlignCenter);
		normalCenterFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalCenterFormat.setFont(fontNormal);

		WritableCellFormat normalRightFormat = new WritableCellFormat();
		normalRightFormat.setBorder(Border.LEFT,BorderLineStyle.THIN);
		normalRightFormat.setBorder(Border.RIGHT,BorderLineStyle.THIN);
		normalRightFormat.setAlignment(dataAlignRight);
		normalRightFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalRightFormat.setFont(fontNormal);

		WritableCellFormat normalLeftLastFormat = new WritableCellFormat();
		normalLeftLastFormat.setBorder(Border.LEFT,BorderLineStyle.THIN);
		normalLeftLastFormat.setBorder(Border.RIGHT,BorderLineStyle.THIN);
		normalLeftLastFormat.setBorder(Border.BOTTOM,BorderLineStyle.THIN);
		normalLeftLastFormat.setAlignment(dataAlignLeft);
		normalLeftLastFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalLeftLastFormat.setFont(fontNormal);

		WritableCellFormat normalCenterLastFormat = new WritableCellFormat();
		normalCenterLastFormat.setBorder(Border.LEFT,BorderLineStyle.THIN);
		normalCenterLastFormat.setBorder(Border.RIGHT,BorderLineStyle.THIN);
		normalCenterLastFormat.setBorder(Border.BOTTOM,BorderLineStyle.THIN);
		normalCenterLastFormat.setAlignment(dataAlignCenter);
		normalCenterLastFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalCenterLastFormat.setFont(fontNormal);

		WritableCellFormat normalRightLastFormat = new WritableCellFormat();
		normalRightLastFormat.setBorder(Border.LEFT,BorderLineStyle.THIN);
		normalRightLastFormat.setBorder(Border.RIGHT,BorderLineStyle.THIN);
		normalRightLastFormat.setBorder(Border.BOTTOM,BorderLineStyle.THIN);
		normalRightLastFormat.setAlignment(dataAlignRight);
		normalRightLastFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		normalRightLastFormat.setFont(fontNormal);

		WritableCellFormat borderNumber2 = new WritableCellFormat();
		borderNumber2.setBorder(Border.ALL, BorderLineStyle.THIN);
		borderNumber2.setAlignment(Alignment.RIGHT);
		borderNumber2.setFont(fontBold);

		Workbook wb = Workbook.getWorkbook(in);
		WritableWorkbook ww = Workbook.createWorkbook(response.getOutputStream(), wb);
		WritableSheet sheet1 = ww.getSheet(0);
		
	

		GregorianCalendar gd = new GregorianCalendar();
		SimpleDateFormat sdfPrint = new SimpleDateFormat("dd/MM/yyyy HH:mm",new java.util.Locale("th", "TH"));
		List empList = this.getApTableService().apSumBatchApproveReport( evaYear, evaMonth,evaVolumeFrom,evaVolumeTo);
			
		sheet1.addCell(new Label(0, 0, ouDesc, HeadFormat));
		// sheet1.addCell(new Label(0,
		// 2,"��ШӧǴ "+section+" �.�. "+year+" �ѹ����~� :
		// "+sdfPrint.format(gd.getTime()),headRight));
		//sheet1.addCell(new Label(0,3, "�������  "+userInfo.getUserName(), headLeft));
		
		
		sheet1.addCell(new Label(2, 3,"��Ш���͹ "+ConvertMonth(evaMonth)+" �.�. "+evaYear,HeadFormat));
		sheet1.addCell(new Label(4, 3,"�ѹ������� : "+sdfPrint.format(gd.getTime()),headRight));
		
		if (empList.size() > 0) {

			row = 6;
		

			for (Iterator itt = empList.iterator(); itt.hasNext();) {
				ApSumBatchVO vo = (ApSumBatchVO) itt.next();
				
			
				seq = seq + 1;

				sheet1.addCell(new Label(0, row, String.valueOf(dfInt.format(seq)),normalRightLastFormat));
				sheet1.addCell(new Label(1, row, vo.getVolume(),normalRightLastFormat));
				//sheet1.addCell(new Label(1, row, String.valueOf(dfYear.format(vo.getVolume())),normalRightLastFormat));
				
				
				double d = Double.valueOf(vo.getSelf().trim()).doubleValue();
				double dd = Double.valueOf(vo.getOth().trim()).doubleValue();
				double t = Double.valueOf(vo.getTotal().trim()).doubleValue();
				
				if (d!=0.00){
				sheet1.addCell(new Label(2, row, String.valueOf(df.format(d)),normalRightLastFormat));
				}else {
					sheet1.addCell(new Label(2, row, "", borderNumber2));	
				}
				if (dd!=0.00){
				sheet1.addCell(new Label(3, row, String.valueOf(df.format(dd)),normalRightLastFormat));
				}else {
					sheet1.addCell(new Label(3, row, "", borderNumber2));	
				}
				//sheet1.addCell(new Label(4, row, String.valueOf(df.format(dd)),
				//		normalRightLastFormat));
				//sheet1.addCell(new Label(3, row, df.format(vo.getDebit()),normalCenterLastFormat));
				//sheet1.addCell(new Label(4, row, df.format(vo.getCredit()),normalLeftLastFormat));
			
				sheet1.addCell(new Label(4, row, String.valueOf(df.format(t)),normalRightLastFormat));
				
				
				sumAllEmpMoney=sumAllEmpMoney+d;
				
				sumAllOthMoney=sumAllOthMoney+dd;
				
			
				
				
				//sheet1.addCell(new Label(2, row, String.valueOf(df.format(vo.getSelf())),normalCenterLastFormat));
				//sheet1.addCell(new Label(3, row, String.valueOf(df.format(vo.getOth())),normalLeftLastFormat));
				//sheet1.addCell(new Label(4, row, String.valueOf(df.format(vo.getTotal())),normalLeftLastFormat));
				
				
				//k = vo.getNewGworkCode();
			
				//if  (k.equals("1")){
				//	t=String.valueOf(df.format(vo.getTotAmt()));	
				//	sumAllEmpMoney=sumAllEmpMoney+vo.getTotAmt().doubleValue();
				//	sheet1.addCell(new Label(6, row,t,normalRightLastFormat));
				//	sheet1.addCell(new Label(7, row,null,normalRightLastFormat));
				//}else if(k.equals("2")) {
				//	v=String.valueOf(df.format(vo.getTotAmt()));		
				//	sumAllOthMoney=sumAllOthMoney+vo.getTotAmt().doubleValue();
				//	sheet1.addCell(new Label(6, row,null,normalRightLastFormat));
				//	sheet1.addCell(new Label(7, row,v,normalRightLastFormat));
				//}
			    
			
				
				sumAllMoney=sumAllEmpMoney+sumAllOthMoney;

				
			
				row++;
			};
			
			sheet1.mergeCells(0, row, 1, row);
			sheet1.addCell(new Label(0, row , "���������", borderNumber2));
			sheet1.addCell(new Label(2, row,String.valueOf(df.format(sumAllEmpMoney)),normalRightLastFormat));
			sheet1.addCell(new Label(3, row,String.valueOf(df.format(sumAllOthMoney)),normalRightLastFormat));
			sheet1.addCell(new Label(4, row,String.valueOf(df.format(sumAllMoney)),normalRightLastFormat));
			row++;
			/*sheet1.mergeCells(0, row, 5, row);
			sheet1.mergeCells(6, row, 8, row);
			sheet1.addCell(new Label(0, row , "���������", borderNumber2));
			sheet1.addCell(new Label(6, row,String.valueOf(df.format(sumAllMoney)),normalRightLastFormat));
			
			row=row+9;
			sheet1.mergeCells(1, row, 3, row);
			sheet1.mergeCells(4, row, 5, row);
			sheet1.mergeCells(6, row, 8, row);
			sheet1.addCell(new Label(1, row , "(................................................)",HeadFormat));
			sheet1.addCell(new Label(4, row , "(................................................)",HeadFormat));
			sheet1.addCell(new Label(6, row , "(................................................)",HeadFormat));
			row++;
			sheet1.mergeCells(1, row, 3, row);
			sheet1.mergeCells(4, row, 5, row);
			sheet1.mergeCells(6, row, 8, row);
			sheet1.addCell(new Label(1, row , "���Ѵ��",HeadFormat));
			sheet1.addCell(new Label(4, row , "�����Ǩ�ͺ",HeadFormat));
			sheet1.addCell(new Label(6, row , "����Ѻ�͡���",HeadFormat));
			
			*/
			sheet1.getSettings().setPassword("#028313766#$");
			sheet1.getSettings().setProtected(true);
			ww.setProtected(true);

			ww.write();
			ww.close();
			wb.close();
			in.close();
			return null;
		} else {
			sheet1.addCell(new Label(0, 0, ouDesc, HeadFormat));
			sheet1.addCell(new Label(0,3, "�������  "+userInfo.getUserName(), headLeft));
	
			sheet1.addCell(new Label(6, 3,"�ѹ������� : "+sdfPrint.format(gd.getTime()),headRight));
			sheet1.setRowView(5, 320);
			sheet1.mergeCells(0,5, 10,5);
			sheet1.addCell(new Label(0, 8, "��辺������",normalCenterLastFormat));	
			sheet1.getSettings().setPassword("#028313766#$");
			sheet1.getSettings().setProtected(true);
			ww.setProtected(true);

			ww.close();
			wb.close();
			in.close();
			return null;
		}
	}



	/**
	 * method ����Ѻ�ŧ Time Format
	 * 
	 * @param hour
	 * @return
	 */
	private String convertFormatHour(Double hour) {
		String rlst = "";
		DecimalFormat dec = new DecimalFormat("###,##0");
		rlst = dec.format(hour != null ? hour.intValue() : 0);
		return rlst;
	}
}
