package com.ss.tp.service.impl;


import com.ss.tp.dto.ApEmployeeVO;
import com.ss.tp.dto.DefaultYearSectionVO;
import com.ss.tp.dto.WeEmployeeVO;

import com.ss.tp.dto.ApTableVO;

import com.ss.tp.model.PrPeriodLine;
import com.ss.tp.service.ApTableService;

import java.io.Serializable;

import java.util.Date;
import java.util.List;
import com.ss.tp.dao.ApTableDAO;

//import com.ss.tp.dto.PayRollEmployeeVO;


public class ApTableServiceImpl implements ApTableService,
		Serializable {

	private ApTableDAO ApTableDAO;

	public ApTableDAO getApTableDAO() {
		return ApTableDAO;
	}

	public void setApTableDAO(ApTableDAO ApTableDAO) {
		this.ApTableDAO = ApTableDAO;
	}

	public DefaultYearSectionVO getDefaultYearAndSection(String ouCode,
			String userId) throws Exception {
		String[] val = ApTableDAO.findMaxYearPeriod(ouCode);
		
		boolean isConfirm = ApTableDAO.isConfirmFlag(ouCode, val[0],
				val[1], userId);
	

		DefaultYearSectionVO vo = new DefaultYearSectionVO();
		vo.setYear(val[0]);
		vo.setMonth(val[1]);
		vo.setConfirm(isConfirm);

	
		System.out.println("month : " + vo.getMonth());

		return vo;
	}
	
	public DefaultYearSectionVO getDefaultYearAndSectionAP001(String ouCode,
			String userId) throws Exception {
		String[] val = ApTableDAO.findMaxYearPeriodAP001(ouCode);
		
		boolean isConfirm = ApTableDAO.isConfirmFlag(ouCode, val[0],
				val[1], userId);
	

		DefaultYearSectionVO vo = new DefaultYearSectionVO();
		vo.setYear(val[0]);
		vo.setMonth(val[1]);
		vo.setConfirm(isConfirm);

	
		System.out.println("month : " + vo.getMonth());

		return vo;
	}
	
	public DefaultYearSectionVO getDefaultYearAndSectionAP002(String ouCode,
			String userId) throws Exception {
		String[] val = ApTableDAO.findMaxYearPeriodAP002(ouCode);
		
		boolean isConfirm = ApTableDAO.isConfirmFlag(ouCode, val[0],
				val[1], userId);
	

		DefaultYearSectionVO vo = new DefaultYearSectionVO();
		vo.setYear(val[0]);
		vo.setMonth(val[1]);
		vo.setConfirm(isConfirm);

	
		System.out.println("month : " + vo.getMonth());

		return vo;
	}
	
	public DefaultYearSectionVO getDefaultYearAndSectionBK(String ouCode,
			String userId) throws Exception {
		String[] val = ApTableDAO.findMaxYearPeriodBK(ouCode);
		
		boolean isConfirm = ApTableDAO.isConfirmFlag(ouCode, val[0],
				val[1], userId);
	

		DefaultYearSectionVO vo = new DefaultYearSectionVO();
		vo.setYear(val[0]);
		vo.setMonth(val[1]);
		vo.setConfirm(isConfirm);

	
		System.out.println("month : " + vo.getMonth());

		return vo;
	}
	
	public DefaultYearSectionVO getDefaultYearAndSectionTR(String ouCode,
			String userId) throws Exception {
		String[] val = ApTableDAO.findMaxYearPeriodTR(ouCode);
		
		boolean isConfirm = ApTableDAO.isConfirmFlag(ouCode, val[0],
				val[1], userId);
	

		DefaultYearSectionVO vo = new DefaultYearSectionVO();
		vo.setYear(val[0]);
		vo.setMonth(val[1]);
		vo.setConfirm(isConfirm);

	
		System.out.println("month : " + vo.getMonth());

		return vo;
	}
	
	
	public void insertApTable(ApTableVO aptablevo)
			throws Exception {
		this.ApTableDAO.insertApTable(aptablevo);
	}

	public void updateApTable(ApTableVO aptablevo)
			throws Exception {
		this.ApTableDAO.updateApTable(aptablevo);
	}

	public void updateApTableApprove(ApTableVO aptablevo)
			throws Exception {
		this.ApTableDAO.updateApTableApprove(aptablevo);
	}

	public void deleteApTable(ApTableVO aptablevo)
			throws Exception {
		this.ApTableDAO.deleteApTable(aptablevo);
	}

	public void insertApTables(List aptablevolist)
			throws Exception {
		this.ApTableDAO.insertApTables(aptablevolist);
	}

	public List findByCriteria(String userId, String evaOuCode, long evaYear,
			long evaMonth, String evaEmpCodeFrom, String evaEmpCodeTo) {
		return this.ApTableDAO.findByCriteria(userId, evaOuCode,
				new Long(evaYear), new Long(evaMonth), evaEmpCodeFrom,
				evaEmpCodeTo);
	}

	public Integer countData(String userId, String evaOuCode, long evaYear,
			long evaMonth, long evaVolume) {
		return this.ApTableDAO.countData(userId, evaOuCode, new Long(
				evaYear), new Long(evaMonth), new Long(evaVolume));
	}
	
	public Integer countDataApprove(String userId, String evaOuCode, long evaYear,
			long evaMonth, long evaVolume) {
		return this.ApTableDAO.countDataApprove(userId, evaOuCode, new Long(
				evaYear), new Long(evaMonth), new Long(evaVolume));
	}
	
	public Integer countEmpData(String userId,String empCode, String evaOuCode, long evaYear) {
		return this.ApTableDAO.countEmpData(userId,empCode, evaOuCode, new Long(
				evaYear));
	}

	public ApEmployeeVO findByEmpCodeDetail(String empCode, String ouCode) {
		return this.ApTableDAO.findByEmpCodeDetail(empCode, ouCode);
	}

	public List findByEmpCodeList(String userId, String ouCode, long yearPn,
			long monthPn, String empCode) {
		return this.ApTableDAO.findByEmpCodeList(userId, ouCode,
				new Long(yearPn), new Long(monthPn), empCode);
	}

	// public List rwPremiumReportByOrg(String userId, String evaOuCode, long
	// evaYear,long evaPeriod,String evaFlag){
	// return this.ApTableDAO.rwPremiumReportByOrg(userId, evaOuCode,new
	// Long (evaYear), new Long (evaPeriod), evaFlag);
	// }

	public void addList(ApTableVO rpVo, boolean isSave) {
		this.ApTableDAO.addList(rpVo);

		if (isSave) {
			try {
				this.ApTableDAO.insertAndUpdateApTables();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void addListApprove(ApTableVO rpVo, boolean isSave) {
		this.ApTableDAO.addList(rpVo);

		if (isSave) {
			try {
				this.ApTableDAO.insertAndUpdateApTablesApprove();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void insertAndUpdate() throws Exception {
		this.ApTableDAO.insertAndUpdateApTables();
	}
	
	


	public boolean canDelete(String ouCode, String year, String month,
			String userId) throws Exception {
		return this.ApTableDAO.canDelete(ouCode, year, month, userId);
	}

	public List findByCriteriaList(String userId, String evaOuCode,
			long evaYear, long evaMonth, long evaVolume, int count,
			int countRecord) {
		return this.ApTableDAO.findByCriteriaList(userId, evaOuCode,
				new Long(evaYear), new Long(evaMonth), new Long(evaVolume),
				count, countRecord);
	}
	
	public List findByCriteriaListApprove(String userId, String evaOuCode,
			long evaYear, long evaMonth, long evaVolume, int count,
			int countRecord) {
		return this.ApTableDAO.findByCriteriaListApprove(userId, evaOuCode,
				new Long(evaYear), new Long(evaMonth), new Long(evaVolume),
				count, countRecord);
	}
	
	public List findByEmpCriteriaList(String userId,String empCode, String evaOuCode,
			long evaYear, int count,int countRecord) {
		return this.ApTableDAO.findByEmpCriteriaList(userId,empCode, evaOuCode,
				new Long(evaYear),count, countRecord);
	}


	public List findByCriteriaReport(String userId, String evaOuCode,
			String evaYear, String evaMonth, String evaVolume) {
		return this.ApTableDAO.findByCriteriaReport(userId, evaOuCode,
				evaYear, evaMonth, evaVolume);
	}

	public List findByCriteriaPromoteLevelReport(String userId,
			String evaOuCode, long evaYear, long evaMonth, long evaVolume) {
		return this.ApTableDAO.findByCriteriaPromoteLevelReport(userId,
				evaOuCode, new Long(evaYear), new Long(evaMonth), new Long(
						evaVolume));
	}

	public List findVolumeBySecurity(String userId, String ouCode,String year,String month)
			throws Exception {
		return this.ApTableDAO.findVolumeBySecurity(userId, ouCode,year,month);
	}
	public List findVolumeBySecurityApprove(String userId, String ouCode,String evaYear,String evaMonth)
			throws Exception {
		return this.ApTableDAO.findVolumeBySecurityApprove(userId, ouCode,evaYear,evaMonth);
	}
	
	public List findVolumeBySecurityPm001(String userId, String ouCode,String evaYear,String evaMonth)
			throws Exception {
		return this.ApTableDAO.findVolumeBySecurityPm001(userId, ouCode,evaYear,evaMonth);
			}
			
	public List findVolumeBySecurityPm002(String userId, String ouCode,String evaYear,String evaMonth)
			throws Exception {
		return this.ApTableDAO.findVolumeBySecurityPm002(userId, ouCode,evaYear,evaMonth);
			}
	
	
	public List findVolumeBySecurityTransfer(String userId, String ouCode,String evaYear,String evaMonth)
			throws Exception {
		return this.ApTableDAO.findVolumeBySecurityTransfer(userId, ouCode,evaYear,evaMonth);
			}
	
	public List findVolumeBySecurityReport1(String userId, String ouCode,String year,String month)
			throws Exception {
		return this.ApTableDAO.findVolumeBySecurityReport1(userId, ouCode,year,month);
	}
	public List findVolumeBySecurityApproveReport(String userId, String ouCode,String evaYear,String evaMonth)
			throws Exception {
		return this.ApTableDAO.findVolumeBySecurityApproveReport(userId, ouCode,evaYear,evaMonth);
	}
	
	
	
	
	public boolean isConfirmFlag(String ouCode, String year, String month,
			String userId) throws Exception {
		return this.ApTableDAO.isConfirmFlag(ouCode, year, month, userId);
	}
	public String[] findMaxYearPeriod(String ouCode) {
		return this.ApTableDAO.findMaxYearPeriod(ouCode);
	}
	
	public String[] findMaxYearPeriodAP001(String ouCode) {
		return this.ApTableDAO.findMaxYearPeriodAP001(ouCode);
	}
	
	public String[] findMaxYearPeriodAP002(String ouCode) {
		return this.ApTableDAO.findMaxYearPeriodAP002(ouCode);
	}
	
	public String[] findMaxYearPeriodTR(String ouCode) {
		return this.ApTableDAO.findMaxYearPeriodTR(ouCode);
	}
    

    
	public void deleteApTableInputError(String ouCode, String yearPn,
			String monthPn,String volumeSet,String userId) throws Exception {
		this.ApTableDAO.deleteApTableInputError(ouCode, yearPn, monthPn,volumeSet,userId);
	}
	
	public Integer confirmData(String ouCode, long yearPn, long monthPn,
			String volumeSetFrom,String volumeSetTo, String userId) {
		return this.ApTableDAO.confirmData(ouCode, new Long(yearPn),
				new Long(monthPn), volumeSetFrom,volumeSetTo, userId);
	}
	
	public Double sumAllMoneyData(String ouCode, long yearPn, long monthPn,
			String volumeSetFrom,String volumeSetTo, String userId) {
		return this.ApTableDAO.sumAllMoneyData(ouCode, new Long(yearPn),
				new Long(monthPn), volumeSetFrom,volumeSetTo, userId);
	}
	
	public Double sumMoneyData(String ouCode, long yearPn, long monthPn,
			String volumeSetFrom,String volumeSetTo, String userId,String flag) {
		return this.ApTableDAO.sumMoneyData(ouCode, new Long(yearPn),
				new Long(monthPn), volumeSetFrom,volumeSetTo, userId,flag);
	}
	
	public void updateConfirmData(String ouCode, long yearPn, long monthPn,String volumeSetFrom,String volumeSetTo,
			String userId) throws Exception {
		this.ApTableDAO.updateConfirmData(ouCode, new Long(yearPn),
				new Long(monthPn),volumeSetFrom,volumeSetTo,  userId);
	}
	
	public Integer confirmDataApprove(String ouCode, long yearPn, long monthPn,
			String volumeSetFrom,String volumeSetTo, String userId) {
		return this.ApTableDAO.confirmDataApprove(ouCode, new Long(yearPn),
				new Long(monthPn), volumeSetFrom,volumeSetTo, userId);
	}
	
	public Double sumAllMoneyDataApprove(String ouCode, long yearPn, long monthPn,
			String volumeSetFrom,String volumeSetTo, String userId) {
		return this.ApTableDAO.sumAllMoneyDataApprove(ouCode, new Long(yearPn),
				new Long(monthPn), volumeSetFrom,volumeSetTo, userId);
	}
	
	public Double sumMoneyDataApprove(String ouCode, long yearPn, long monthPn,
			String volumeSetFrom,String volumeSetTo, String userId,String flag) {
		return this.ApTableDAO.sumMoneyDataApprove(ouCode, new Long(yearPn),
				new Long(monthPn), volumeSetFrom,volumeSetTo, userId,flag);
	}
	
	public void updateConfirmDataApprove(String ouCode, long yearPn, long monthPn,String volumeSetFrom,String volumeSetTo,
			String userId) throws Exception {
		this.ApTableDAO.updateConfirmDataApprove(ouCode, new Long(yearPn),
				new Long(monthPn),volumeSetFrom,volumeSetTo,  userId);
	}
	
	
	public List apEmpListReport(String userId, String evaOuCode, String evaYear,
			String evaMonth,String volume) {
		return this.ApTableDAO.apEmpListReport(userId, evaOuCode,
				evaYear, evaMonth,volume);
	}
	
	public List apEmpListApproveReport(String userId, String evaOuCode, String evaYear,
			String evaMonth,String volume,String approve) {
		return this.ApTableDAO.apEmpListApproveReport(userId, evaOuCode,
				evaYear,evaMonth,volume,approve);
	}

	public List apEmpKtbBankReport(String userId, String evaOuCode, int evaYear,
			int evaMonth,String volumeFrom,String volumeTo) {
		return this.ApTableDAO.apEmpKtbBankReport(userId, evaOuCode,
				new Integer(evaYear), new Integer(evaMonth),volumeFrom,volumeTo);
	}
	
	public List apEmpScbBankReport(String userId, String evaOuCode, int evaYear,
			int evaMonth,String volumeFrom,String volumeTo) {
		return this.ApTableDAO.apEmpScbBankReport(userId, evaOuCode,
				new Integer(evaYear), new Integer(evaMonth),volumeFrom,volumeTo);
	}
	
	public List apTransferReport(String evaYear,
			String evaMonth) {
		return this.ApTableDAO.apTransferReport(evaYear, evaMonth);
	}
	
	public List apSumBatchApproveReport(String evaYear,
			String evaMonth,String volumeFrom,String volumeTo) 
	{		return this.ApTableDAO.apSumBatchApproveReport(evaYear, evaMonth,volumeFrom,volumeTo);
	}
	
	public String apToGl(String userId,String evaOuCode, String evaYear,
			String evaMonth, String evaVolume1, String evaVolume2, String accDate) {
		return this.ApTableDAO.apToGl(userId, evaOuCode,evaYear, evaMonth,evaVolume1,evaVolume2,accDate);
	}
    
	public void updateConfirmDataBankKTB(String ouCode, long yearPn, long monthPn,String volumeSetFrom,String volumeSetTo,
			String userId) throws Exception {
		this.ApTableDAO.updateConfirmDataBankKTB(ouCode, new Long(yearPn),
				new Long(monthPn),volumeSetFrom,volumeSetTo,  userId);
	}
	public void updateConfirmDataBankSCB(String ouCode, long yearPn, long monthPn,String volumeSetFrom,String volumeSetTo,
			String userId) throws Exception {
		this.ApTableDAO.updateConfirmDataBankSCB(ouCode, new Long(yearPn),
				new Long(monthPn),volumeSetFrom,volumeSetTo,  userId);
	}
	public void updateConfirmDataTransfer(String ouCode, long yearPn, long monthPn,String volumeSetFrom,String volumeSetTo,
			String userId) throws Exception {
		this.ApTableDAO.updateConfirmDataTransfer(ouCode, new Long(yearPn),
				new Long(monthPn),volumeSetFrom,volumeSetTo,  userId);
	}
	
	

}
