package com.ss.tp.service.impl;

import java.io.Serializable;

import com.ss.tp.dao.TestEmpDAO;
import com.ss.tp.service.TestEmpService;

public class TestEmpServiceImpl implements TestEmpService, Serializable {
	TestEmpDAO testEmpDAO;

	public void loadTestEmp() {
		this.testEmpDAO.loadTestEmp();
	}

	public TestEmpDAO getTestEmpDAO() {
		return testEmpDAO;
	}

	public void setTestEmpDAO(TestEmpDAO testEmpDAO) {
		this.testEmpDAO = testEmpDAO;
	}

}
