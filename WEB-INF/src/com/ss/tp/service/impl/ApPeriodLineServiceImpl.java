package com.ss.tp.service.impl;

import com.ss.tp.dao.ApPeriodLineDAO;
import com.ss.tp.dao.ApTableDAO;
import com.ss.tp.dto.DefaultYearSectionVO;
import com.ss.tp.model.ApPeriodLine;
import com.ss.tp.service.ApPeriodLineService;



import java.io.Serializable;

import java.util.List;


public class ApPeriodLineServiceImpl implements ApPeriodLineService,
		Serializable {

	
	private ApTableDAO apTableDAO;
	private ApPeriodLineDAO apPeriodLineDAO;

	

	public ApTableDAO getApTableDAO() {
		return apTableDAO;
	}

	public void setApTableDAO(ApTableDAO apTableDAO) {
		this.apTableDAO = apTableDAO;
	}

	public ApPeriodLineDAO getApPeriodLineDAO() {
		return apPeriodLineDAO;
	}

	public void setApPeriodLineDAO(ApPeriodLineDAO apPeriodLineDAO) {
		this.apPeriodLineDAO = apPeriodLineDAO;
	}

	public DefaultYearSectionVO getDefaultYearAndSection(String ouCode,
			String userId) throws Exception {
		String[] val = apTableDAO.findMaxYearPeriod(ouCode);
		String[] valm = apTableDAO.findMaxMonthInYear(ouCode, val[0]);
		// System.out.println("1234");
		boolean isConfirm = apTableDAO.isConfirmFlag(ouCode, val[0],
				val[1], userId);
		// System.out.println("5678");
		
		DefaultYearSectionVO vo = new DefaultYearSectionVO();
		vo.setYear(val[0]);
		vo.setVolumeSet(val[1]);
		vo.setMonth(valm[0]);
		vo.setConfirm(isConfirm);
		System.out.println("period : " + vo.getPeriod());
		System.out.println("month : " + vo.getMonth());

		return vo;
	}

	public List findYearInPeriodLine(String ouCode) throws Exception {
		return this.apPeriodLineDAO.findYearInPeriodLine(ouCode);
	}

	public List findPeriodInPeriodLine(String ouCode, double year)
			throws Exception {
		return this.apPeriodLineDAO.findPeriodInPeriodLine(ouCode, new Double(
				year));
	}

	public boolean canDeleteData(String ouCode, String year, String month, String volumeSet)
			throws Exception {
		return this.apPeriodLineDAO.canDeleteData(ouCode, year, month, volumeSet);
	}

	public boolean isCloseTranClose(String ouCode, String year, String month , String volumeSet)
			throws Exception {
		return this.apPeriodLineDAO.isCloseTranClose(ouCode, year, month ,volumeSet);
	}
	
	public boolean isCloseMasterClose(String ouCode, String year, String month , String volumeSet)
			throws Exception {
		return this.apPeriodLineDAO.isCloseMasterClose(ouCode, year, month  ,volumeSet);
	}


	public ApPeriodLine findPeriodLine(String ouCode, String year, String volumeSet)
			throws Exception {
		return this.apPeriodLineDAO.findPeriodLine(ouCode, year,volumeSet);
	}

}
