package com.ss.tp.service;



import java.util.Date;
import java.util.List;

import com.ss.tp.dto.ApToGlVO;
import com.ss.tp.dto.DefaultYearSectionVO;
import com.ss.tp.dto.WeEmployeeVO;


public interface ApGenerateGlService {
	public void insertApToGl(ApToGlVO aptoglvo)
			throws Exception;


	public void deleteApToGl(ApToGlVO aptoglvo)
			throws Exception;

	public void insertApToGls(List aptoglvolist)
			throws Exception;

	public List findByCriteria(String userId, String evaOuCode, long evaYear,
			long evaMonth, String evaEmpCodeFrom, String evaEmpCodeTo);

	public Integer countData(String userId, String evaOuCode, long evaYear,
			long evaMonth, long evaVolume);
	
	

	public void addList(ApToGlVO rpVo, boolean isSave);
	
	

	public boolean canDelete(Date accountingDate) throws Exception;

	public List findByCriteriaList(String userId, String evaOuCode,
			long evaYear, long evaMonth, long evaVolume, int count,
			int countRecord);

	
	
	public void deleteApToGlInputError(String ouCode, String yearPn,
			String monthPn,String volumeSet,String userId) 
			throws Exception;
	
	public Integer confirmData(String ouCode, long yearPn, long monthPn,
			String volumeSet, String userId);
    

	public List addApToGl(String userId, String evaOuCode, String evaYear,
			String evaMonth, String volume1, String volume2,String accDate);

	public DefaultYearSectionVO getDefaultYearAndSectionGL(String ouCode,
			String userId) throws Exception;

	public List findDateBySecurity(String evaOuCode)
			throws Exception;

}