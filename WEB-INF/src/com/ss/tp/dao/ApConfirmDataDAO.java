package com.ss.tp.dao;

import com.ss.tp.dto.ApConfirmDataVO;


public interface ApConfirmDataDAO {
	public void insertApConfirmData(ApConfirmDataVO vo) throws Exception;
	
	//public void insertApConfirmDataApprove(ApConfirmDataVO vo) throws Exception;

	public boolean isConfirmInputData(String ouCode, String year,
			String month,String volumeSet, String userId) throws Exception;

	public boolean isConfirmApData(String ouCode, String year, String month,String volumeSet,
			String userId) throws Exception;
}