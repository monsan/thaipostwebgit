package com.ss.tp.dao;

import com.ss.tp.dto.ApToGlVO;
import com.ss.tp.dto.WeEmployeeVO;

import com.ss.tp.dto.ApToGlVO;


import java.util.Date;
import java.util.List;


public interface ApGenerateGlDAO {
	public void insertApToGl(ApToGlVO aptoglvo)
			throws Exception;



	public void deleteApToGl(ApToGlVO aptoglvo)
			throws Exception;

	public void insertApToGls(List aptoglvolist)
			throws Exception;

	public List findByCriteria(String userId, String evaOuCode, Long evaYear,
			Long evaMonth, String evaEmpCodeFrom, String evaEmpCodeTo);

	public Integer countData(String userId, String evaOuCode, Long evaYear,
			Long evaMonth, Long evaVolume);


    public void addList(ApToGlVO aptoglvo);
	
	
	public boolean canDelete(Date accountingDate) throws Exception;

	public List findByCriteriaList(String userId, String evaOuCode,
			Long evaYear, Long evaMonth, Long evaVolume, int count,
			int countRecord);
	
   public void deleteApToGlInputError(String ouCode, String yearPn,
			String monthPn,String volumeSet, String userId) throws Exception;
	
	public Integer confirmData(String ouCode, Long yearPn, Long monthPn,
			String volumeSet, String userId);

	public List addApToGl(String userId, String evaOuCode, String evaYear,
			String evaMonth, String volume1, String volume2,String accDate);



	public String[] findMaxDateGL(String ouCode);
    
	public List findDateBySecurity( String ouCode)
			throws Exception;

}