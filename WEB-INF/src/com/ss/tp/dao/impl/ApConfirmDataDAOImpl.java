package com.ss.tp.dao.impl;

import com.ss.tp.dao.ApConfirmDataDAO;
import com.ss.tp.dto.ApConfirmDataVO;
import com.ss.tp.model.ApConfirmData;
import com.ss.tp.model.ApConfirmDataPK;

import java.io.Serializable;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.beanutils.BeanUtils;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;


public class ApConfirmDataDAOImpl extends HibernateDaoSupport implements
              ApConfirmDataDAO, Serializable {
	public ApConfirmDataDAOImpl() {
		Locale.setDefault(Locale.US);
	}

	public void insertApConfirmData(ApConfirmDataVO vo) throws Exception {
		ApConfirmData rw = new ApConfirmData();
		ApConfirmDataPK pk = new ApConfirmDataPK();
		try {
			BeanUtils.copyProperties(pk, vo);
			rw.setPk(pk);
			BeanUtils.copyProperties(rw, vo);
			rw.setCreDate(new Date());
			this.getHibernateTemplate().save(rw);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public boolean isConfirmInputData(String ouCode, String year,
			String month,String volumeSet, String userId) throws Exception {

		StringBuffer hql = new StringBuffer();
		hql.append(" from ApConfirmData ");
		hql.append(" where pk.ouCode = '" + ouCode + "' ");
		hql.append(" and pk.yearPn = " + year);
		hql.append(" and pk.monthPn = " + month);
		hql.append(" and pk.volumeSet = " + volumeSet);
		hql.append(" and pk.userId = '" + userId + "' ");
		hql.append(" and pk.flag = '1' ");

		try {
			// Object obj =
			// this.getSession().createQuery(hql.toString()).uniqueResult();
			List ls = this.getHibernateTemplate().find(hql.toString());

			System.out.println("count confirm input : " + ls.size());

			if (ls != null && ls.size() > 0)
				return true;
			else
				return false;
		} catch (Exception e) {
			// if error default confrim
			return true;
		}
	}

	public boolean isConfirmApData(String ouCode, String year, String month,String volumeSet,
			String userId) throws Exception {

		StringBuffer hql = new StringBuffer();
		hql.append(" from ApConfirmData ");
		hql.append(" where pk.ouCode = '" + ouCode + "' ");
		hql.append(" and pk.year = " + year);
		hql.append(" and pk.monthPn = " + month);
		hql.append(" and pk.volumeSet = " + volumeSet);
		hql.append(" and pk.userId = '" + userId + "' ");
		hql.append(" and pk.flag = '2' ");

		try {
			// Object obj =
			// this.getSession().createQuery(hql.toString()).uniqueResult();
			List ls = this.getHibernateTemplate().find(hql.toString());

			System.out.println("count confirm ap approve : " + ls.size());

			if (ls != null && ls.size() > 0)
				return true;
			else
				return false;
		} catch (Exception e) {
			// if error default confrim
			return true;
		}
	}

}