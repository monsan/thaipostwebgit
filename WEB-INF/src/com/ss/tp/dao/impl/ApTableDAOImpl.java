package com.ss.tp.dao.impl;





import java.io.Serializable;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;








import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.map.ListOrderedMap;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;




import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;






import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.JdbcTemplate;




import org.springframework.dao.DataAccessException;

import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.ss.tp.dao.ApTableDAO;

import com.ss.tp.dto.ApTableVO;

import com.ss.tp.dto.ApEmpBankVO;
import com.ss.tp.dto.ApSumBatchVO;
import com.ss.tp.dto.ApTransferVO;
import com.ss.tp.dto.WeEmployeeVO;
import com.ss.tp.dto.ApEmployeeVO;
import com.ss.tp.model.ApTable;







//import com.ss.tp.dto.ApTableReportByOrgVO;

public class ApTableDAOImpl extends HibernateDaoSupport implements
		ApTableDAO, Serializable {
	List rwList = new ArrayList();

	private JdbcTemplate jdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public ApTableDAOImpl() {
		Locale.setDefault(Locale.US);
	}

	public void insertApTable(ApTableVO aptablevo)
			throws Exception {
		ApTable rw = new ApTable();
		try {
			BeanUtils.copyProperties(rw, aptablevo);
			rw.setCreDate(new Date());
			rw.setUpdDate(new Date());
			rw.setConfirmFlag("N");
			rw.setApproveClose("N");
			rw.setApproveFlag("N");
			rw.setTransferFlag("N");
			rw.setTransferClose("N");			
			rw.setBankClose("N");
			rw.setBankFlag("N");
			this.getHibernateTemplate().save(rw);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public ApTable loadApTable(ApTableVO rpVo) {
		ApTable rwp = new ApTable();
		try {
			// BeanUtils.copyProperties(rwp, rpVo);
			rwp = (ApTable) this.getHibernateTemplate().load(
					ApTable.class, rpVo.getKeySeq());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rwp;
	}

	public void updateApTable(ApTableVO aptablevo)
			throws Exception {
		ApTable rw = new ApTable();
		try {
			rw = this.loadApTable(aptablevo);
			rw.setEmpCode(aptablevo.getEmpCode());
			rw.setCodeSeq(aptablevo.getCodeSeq());
			//rw.setNewCodeSeq(aptablevo.getNewCodeSeq());
			//rw.setNewOldDuty(aptablevo.getNewOldDuty());
			//rw.setNewDuty(aptablevo.getNewDuty());
			//rw.setNewPositionCode(aptablevo.getNewPositionCode());
			//rw.setNewLevelCode(aptablevo.getNewLevelCode());
			rw.setNewGworkCode(aptablevo.getNewGworkCode());
			//rw.setApproveFlag(aptablevo.getApproveFlag());
			//rw.setNewOrgCode(aptablevo.getNewOrgCode());
			rw.setSeqData(aptablevo.getSeqData());
			rw.setTotAmt(aptablevo.getTotAmt());
			rw.setUpdBy(aptablevo.getUpdBy());
			rw.setUpdDate(new Date());
			this.getHibernateTemplate().update(rw);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	public void updateApTableApprove(ApTableVO aptablevo)
			throws Exception {
		ApTable rw = new ApTable();
		try {
			rw = this.loadApTable(aptablevo);
			rw.setApproveFlag(aptablevo.getApproveFlag());
			rw.setApproveBy(aptablevo.getApproveBy());
			rw.setApproveDate(new Date());
			//rw.setEmpCode(aptablevo.getEmpCode());
			//rw.setCodeSeq(aptablevo.getCodeSeq());
			//rw.setNewCodeSeq(aptablevo.getNewCodeSeq());
			//rw.setNewOldDuty(aptablevo.getNewOldDuty());
			//rw.setNewDuty(aptablevo.getNewDuty());
			//rw.setNewPositionCode(aptablevo.getNewPositionCode());
			//rw.setNewLevelCode(aptablevo.getNewLevelCode());
			//rw.setNewGworkCode(aptablevo.getNewGworkCode());
			//rw.setNewOrgCode(aptablevo.getNewOrgCode());
			//rw.setSeqData(aptablevo.getSeqData());
			//rw.setTotAmt(aptablevo.getTotAmt());
			//rw.setUpdBy(aptablevo.getUpdBy());
			//rw.setUpdDate(new Date());
			this.getHibernateTemplate().update(rw);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void deleteApTable(ApTableVO aptablevo)
			throws Exception {
		ApTable rw = new ApTable();
		try {
			BeanUtils.copyProperties(rw, aptablevo);
			this.getHibernateTemplate().delete(rw);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void addList(ApTableVO aptablevo) {
		this.rwList.add(aptablevo);
	}
	
	public void addListApprove(ApTableVO aptablevo) {
		this.rwList.add(aptablevo);
	}

	public void clearList() {
		this.rwList = new ArrayList();
	}

	public void insertAndUpdateApTables() throws Exception {
		try {
			for (int i = 0; i < this.rwList.size(); i++) {
				ApTableVO aptablevo = (ApTableVO) this.rwList.get(i);
				if ((aptablevo.getKeySeq() != new Long(0))
						&& aptablevo.getKeySeq() != null
						&& !aptablevo.getKeySeq().equals("")) {
					this.updateApTable(aptablevo);
				} else {
					this.insertApTable(aptablevo);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			this.clearList();
		}
	}

	
	public void insertAndUpdateApTablesApprove() throws Exception {
		try {
			for (int i = 0; i < this.rwList.size(); i++) {
				ApTableVO aptablevo = (ApTableVO) this.rwList.get(i);
				if ((aptablevo.getKeySeq() != new Long(0))
						&& aptablevo.getKeySeq() != null
						&& !aptablevo.getKeySeq().equals("")) {
					this.updateApTableApprove(aptablevo);
				} else {
					this.insertApTable(aptablevo);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			this.clearList();
		}
	}
	public void insertApTables(List aptablevolist)
			throws Exception {
		ApTable rw = new ApTable();
		try {
			for (int i = 0; i < aptablevolist.size(); i++) {
				ApTableVO aptablevo = (ApTableVO) aptablevolist.get(i);

				BeanUtils.copyProperties(rw, aptablevo);
				rw.setCreDate(new Date());
				rw.setUpdDate(new Date());
				rw.setConfirmFlag("N");
				//rw.setApproveFlag("N");
				//rw.setTransferFlag("N");
				//rw.setBankFlag("N");
				this.getHibernateTemplate().save(rw);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List findByCriteria(String userId, String evaOuCode, Long evaYear,
			Long evaMonth, String evaEmpCodeFrom, String evaEmpCodeTo) {
		StringBuffer criteria = new StringBuffer();

		if (evaYear != null && !evaYear.equals("")) {
			criteria.append(" and rwPre.yearPn = ");
			criteria.append(evaYear);
		}

		if (evaMonth != null && !evaMonth.equals("")) {
			criteria.append(" and rwPre.monthPn = ");
			criteria.append(evaMonth);
		}

		if (evaEmpCodeFrom != null && !evaEmpCodeFrom.equals("")) {
			criteria.append(" and rwPre.empCode >= '");
			criteria.append(evaEmpCodeFrom);
			criteria.append("' ");
		}

		if (evaEmpCodeTo != null && !evaEmpCodeTo.equals("")) {
			criteria.append(" and rwPre.empCode <= '");
			criteria.append(evaEmpCodeTo);
			criteria.append("' ");
		}

		StringBuffer hql = new StringBuffer();

		hql.append(" select distinct rwPre.empCode, pnEmp.refDbPreSuff.prefixName, pnEmp.firstName, pnEmp.lastName, pnPos.positionShort, rwPre.codeSeq,pnEmp.adminCode,pnEmp.levelCode ");
		hql.append(" from ApTable rwPre , PnEmployee pnEmp , PnPosition pnPos , PnOrganization pnOrg, VPnOrganizationSecurity v ");
		hql.append(" where rwPre.ouCode = '");
		hql.append(evaOuCode);
		hql.append("' ");
		hql.append(" and v.pk.userId = '" + userId + "' ");
		hql.append(" and rwPre.ouCode = v.pk.ouCode ");
		hql.append(" and rwPre.codeSeq = v.pk.codeSeq ");
		hql.append(criteria);
		hql.append(" and rwPre.ouCode   = pnEmp.pk.ouCode ");
		hql.append(" and rwPre.empCode = pnEmp.pk.empCode ");
		hql.append(" and pnEmp.pk.ouCode = pnPos.pk.ouCode ");
		hql.append(" and pnEmp.gworkCode = pnPos.pk.gworkCode ");
		hql.append(" and pnEmp.positionCode = pnPos.pk.positionCode ");
		hql.append(" and rwPre.ouCode = pnOrg.pk.ouCode ");
		hql.append(" and rwPre.codeSeq = pnOrg.pk.codeSeq ");
		hql.append(" order by rwPre.codeSeq,pnEmp.adminCode,pnEmp.levelCode desc, rwPre.empCode ");

		System.out.println("HQL findByCriteria ==> " + hql.toString());

		List empList = this.getSession().createQuery(hql.toString()).list();
		List retList = new ArrayList();

		for (Iterator it = empList.iterator(); it.hasNext();) {
			Object[] r = (Object[]) it.next();
			String empCode = (String) r[0];
			String prefixName = (String) r[1];
			String firstName = (String) r[2];
			String lastName = (String) r[3];
			// String positionShort = (String)r[4];

			WeEmployeeVO ret = new WeEmployeeVO();
			ret.setEmpCode(empCode);
			ret.setName(prefixName + " " + firstName + " " + lastName);
			// ret.setPositionShort(positionShort);

			retList.add(ret);
		}
		return retList;
	}

	public Integer countData(String userId, String evaOuCode, Long evaYear,
			Long evaMonth, Long evaVolume) {
		StringBuffer criteria = new StringBuffer();

		if (evaYear != null && !evaYear.equals("")) {
			criteria.append(" and rwPre.yearPn = ");
			criteria.append(evaYear);
		}

		if (evaMonth != null && !evaMonth.equals("")) {
			criteria.append(" and rwPre.monthPn = ");
			criteria.append(evaMonth);
		}

		if (evaVolume != null && !evaVolume.equals("")) {
			criteria.append(" and rwPre.volumeSet = ");
			criteria.append(evaVolume);
		}

		StringBuffer hql = new StringBuffer();

		hql.append(" select count(rwPre.seqData) ");
		hql.append(" from ApTable rwPre , PnEmployee pnEmp , PnPosition pnPos , PnOrganization pnOrg, VPnOrganizationSecurity v ");
		hql.append(" where rwPre.ouCode = '");
		hql.append(evaOuCode);
		hql.append("' ");
		hql.append(" and v.pk.userId = '" + userId + "' ");
		hql.append(" and rwPre.ouCode = v.pk.ouCode ");
		hql.append(" and rwPre.codeSeq = v.pk.codeSeq ");
		hql.append(criteria);
		hql.append(" and rwPre.ouCode = pnEmp.pk.ouCode ");
		hql.append(" and rwPre.empCode = pnEmp.pk.empCode ");
		hql.append(" and pnEmp.pk.ouCode = pnPos.pk.ouCode ");
		hql.append(" and pnEmp.gworkCode = pnPos.pk.gworkCode ");
		hql.append(" and pnEmp.positionCode = pnPos.pk.positionCode ");
		hql.append(" and rwPre.ouCode = pnOrg.pk.ouCode ");
		hql.append(" and rwPre.codeSeq = pnOrg.pk.codeSeq ");

		List empList = this.getSession().createQuery(hql.toString()).list();

		return (Integer) empList.get(0);
	}
	public Integer countDataApprove(String userId, String evaOuCode, Long evaYear,
			Long evaMonth, Long evaVolume) {
		StringBuffer criteria = new StringBuffer();

		if (evaYear != null && !evaYear.equals("")) {
			criteria.append(" and rwPre.yearPn = ");
			criteria.append(evaYear);
		}

		if (evaMonth != null && !evaMonth.equals("")) {
			criteria.append(" and rwPre.monthPn = ");
			criteria.append(evaMonth);
		}

		if (evaVolume != null && !evaVolume.equals("")) {
			criteria.append(" and rwPre.volumeSet = ");
			criteria.append(evaVolume);
		}

		StringBuffer hql = new StringBuffer();

		hql.append(" select count(rwPre.seqData) ");
		hql.append(" from ApTable rwPre , PnEmployee pnEmp , PnPosition pnPos , PnOrganization pnOrg, VPnOrganizationSecurity v ");
		hql.append(" where rwPre.ouCode = '");
		hql.append(evaOuCode);
		hql.append("' ");
		hql.append(" and v.pk.userId = '" + userId + "' ");
		hql.append(" and rwPre.ouCode = v.pk.ouCode ");
		hql.append(" and rwPre.codeSeq = v.pk.codeSeq ");
		hql.append(criteria);
		hql.append(" and rwPre.ouCode = pnEmp.pk.ouCode ");
		hql.append(" and rwPre.empCode = pnEmp.pk.empCode ");
		hql.append(" and rwPre.confirmFlag = 'Y' ");
		hql.append(" and rwPre.approveClose = 'N' ");
		//hql.append(" and rwPre.volumeSet  >= ( ");
		////hql.append(" and rwPre.volumeSet  in ( ");
		//hql.append("	 select min(c.pk.volumeSetFrom) from ApConfirmData c ");
		//hql.append(" where ");
		//hql.append(" c.pk.flag = '1' ");
		//hql.append(" and c.pk.yearPn = rwPre.yearPn ");
		//hql.append(" and c.pk.monthPn = rwPre.monthPn ");
		//hql.append(" ) ");
		//hql.append(" and rwPre.volumeSet  <= ( ");
		////hql.append(" and rwPre.volumeSet  in ( ");
		//hql.append("	 select max(c.pk.volumeSetFrom) from ApConfirmData c ");
		//hql.append(" where ");
		//hql.append(" c.pk.flag = '1' ");
		//hql.append(" and c.pk.yearPn = rwPre.yearPn ");
		//hql.append(" and c.pk.monthPn = rwPre.monthPn ");
		//hql.append(" ) ");
		hql.append(" and pnEmp.pk.ouCode = pnPos.pk.ouCode ");
		hql.append(" and pnEmp.gworkCode = pnPos.pk.gworkCode ");
		hql.append(" and pnEmp.positionCode = pnPos.pk.positionCode ");		
		hql.append(" and rwPre.ouCode = pnOrg.pk.ouCode ");
		hql.append(" and rwPre.codeSeq = pnOrg.pk.codeSeq ");

		List empList = this.getSession().createQuery(hql.toString()).list();

		return (Integer) empList.get(0);
	}

	public Integer countEmpData(String userId,String empCode, String evaOuCode, Long evaYear) {
		StringBuffer criteria = new StringBuffer();

		if (evaYear != null && !evaYear.equals("")) {
			criteria.append(" and rwPre.yearPn = ");
			criteria.append(evaYear);
		}

		

		StringBuffer hql = new StringBuffer();

		hql.append(" select count(rwPre.seqData) ");
		hql.append(" from ApTable rwPre , PnEmployee pnEmp , PnPosition pnPos , PnOrganization pnOrg");
		hql.append(" where rwPre.ouCode = '");
		hql.append(evaOuCode);
		hql.append("' ");
		hql.append(criteria);
		hql.append(" and rwPre.ouCode = pnEmp.pk.ouCode ");
		hql.append(" and rwPre.empCode = pnEmp.pk.empCode ");
		hql.append(" and rwPre.empCode ='" + empCode + "' ");
		hql.append(" and pnEmp.pk.ouCode = pnPos.pk.ouCode ");
		hql.append(" and pnEmp.gworkCode = pnPos.pk.gworkCode ");
		hql.append(" and pnEmp.positionCode = pnPos.pk.positionCode ");
		hql.append(" and rwPre.ouCode = pnOrg.pk.ouCode ");
		hql.append(" and rwPre.codeSeq = pnOrg.pk.codeSeq ");

		List empList = this.getSession().createQuery(hql.toString()).list();

		return (Integer) empList.get(0);
	}
	
	
	public ApEmployeeVO findByEmpCodeDetail(String empCode, String ouCode) {

		StringBuffer hql = new StringBuffer();

		hql.append(" select pnEmp.pk.empCode, ");
		hql.append(" pnEmp.refDbPreSuff.prefixName, ");
		hql.append(" pnEmp.firstName, ");
		hql.append(" pnEmp.lastName, ");
		hql.append(" pnPos.positionShort, ");
		hql.append(" pnOrg.orgCode, ");
		hql.append(" pnOrg.divShort || ' ' || pnOrg.areaDesc || ' ' || pnOrg.secDesc || ' ' || pnOrg.workDesc , ");
		hql.append(" pnOrg.pk.codeSeq ");
		hql.append(" from ApTable rwPre, ");
		hql.append(" PnEmployee pnEmp, ");
		hql.append(" PnPosition pnPos, ");
		hql.append(" PnOrganization pnOrg ");
		hql.append(" where rwPre.empCode = '");
		hql.append(empCode);
		hql.append("' ");
		hql.append(" and rwPre.ouCode = pnEmp.pk.ouCode ");
		hql.append(" and rwPre.empCode = pnEmp.pk.empCode ");
		hql.append(" and pnEmp.pk.ouCode = pnPos.pk.ouCode ");
		hql.append(" and pnEmp.gworkCode = pnPos.pk.gworkCode ");
		hql.append(" and pnEmp.positionCode = pnPos.pk.positionCode ");
		hql.append(" and rwPre.ouCode = pnOrg.pk.ouCode ");
		hql.append(" and rwPre.codeSeq = pnOrg.pk.codeSeq ");

		System.out.println("HQL findByEmpCodeDetail ==> " + hql.toString());

		List empList = this.getSession().createQuery(hql.toString()).list();
		ApEmployeeVO ret = new ApEmployeeVO();

		if (empList != null && empList.size() > 0) {
			Object[] r = (Object[]) empList.get(0);
			String empcode = (String) r[0];
			String prefixName = (String) r[1];
			String firstName = (String) r[2];
			String lastName = (String) r[3];
			String positionShort = (String) r[4];
			String orgCode = (String) r[5];
			String orgDesc = (String) r[6];
			Long codeSeq = (Long) r[7];

			ret.setEmpCode(empcode);
			ret.setName(prefixName + " " + firstName + " " + lastName);
			ret.setOldPositionShort(positionShort);
			ret.setOrgCode(orgCode);
			ret.setOrgDesc(orgDesc);
			ret.setCodeSeq(codeSeq);
			

		}

		return ret;
	}

	public List findByEmpCodeList(String userId, String ouCode, Long yearPn,
			Long monthPn, String empCode) {
		StringBuffer hql = new StringBuffer();

		hql.append(" from ApTable rw ");
		hql.append(" where rw.ouCode = '");
		hql.append(ouCode);
		hql.append("' ");
		hql.append(" and rw.yearPn = ");
		hql.append(yearPn);
		hql.append(" and rw.monthPn = ");
		hql.append(monthPn);
		hql.append(" and rw.empCode = '");
		hql.append(empCode);
		hql.append(" and rw.creBy = ");
		hql.append(userId);
		hql.append("' ");
		hql.append(" and rw.empCode in ( ");
		hql.append(" 	select pk.empCode ");
		hql.append(" 	from VPnEmployeeSecurity ");
		hql.append(" 	where pk.userId = '" + userId + "' ");
		hql.append(" 	and pk.ouCode = '" + ouCode + "' ");
		hql.append(" ) ");

		System.out.println("HQL findByEmpCodeList ==> " + hql.toString());

		List empList = this.getSession().createQuery(hql.toString()).list();
		List retList = new ArrayList();

		if (empList != null && empList.size() > 0) {
			for (int i = 0; i < empList.size(); i++) {
				ApTableVO rw = new ApTableVO();

				try {
					BeanUtils.copyProperties(rw, empList.get(i));
					retList.add(i, rw);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return retList;
	}

	public boolean canDelete(String ouCode, String year, String month,
			String userId) throws Exception {
		StringBuffer sql = new StringBuffer(0);
		sql.append(" select count(*) ");
		sql.append(" from ApTable p, VPnEmployeeSecurity e ");
		sql.append(" where p.ouCode = '" + ouCode + "' ");
		sql.append(" and p.yearPn = " + year);
		sql.append(" and p.monthPn = " + month);
		//sql.append(" and p.creBy = '" + userId + "' ");
		sql.append(" and e.pk.userId = '" + userId + "' ");
		sql.append(" and p.empCode = e.pk.empCode ");
		sql.append(" and p.ouCode = e.pk.ouCode ");

		List ls = this.getHibernateTemplate().find(sql.toString());

		if (ls != null && ls.size() > 0) {
			Integer i = (Integer) ls.get(0);

			if (i.intValue() > 0)
				return false;
			else
				return true;
		} else
			return true;
	}

	public List findByCriteriaList(String userId, String evaOuCode,
			Long evaYear, Long evaMonth, Long evaVolume, int count,
			int countRecord)

	{
		StringBuffer criteria = new StringBuffer();

		if (evaYear != null && !evaYear.equals("")) {
			criteria.append(" and rwInc.yearPn = ");
			criteria.append(evaYear);
		}

		if (evaMonth != null && !evaMonth.equals("")) {
			criteria.append(" and rwInc.monthPn = ");
			criteria.append(evaMonth);
		}

		if (evaVolume != null && !evaVolume.equals("")) {
			criteria.append(" and rwInc.volumeSet = ");
			criteria.append(evaVolume);
		}

		StringBuffer hql = new StringBuffer();

		hql.append(" select rwInc.keySeq, ");
		hql.append(" rwInc.empCode, ");
		hql.append(" pnEmp.refDbPreSuff.prefixName, ");
		hql.append(" pnEmp.firstName, ");
		hql.append(" pnEmp.lastName, ");
		//hql.append(" nvl(pa.adminDesc,' '), ");
		hql.append(" NVL(pnEmp.levelCode,0) , ");
		hql.append(" pnPos.positionShort, ");
		hql.append(" pnOrg.pk.codeSeq, ");
		hql.append(" pnOrg.orgCode, ");
		hql.append(" pnOrg.divShort || ' ' || nvl(pnOrg.secDesc,pnOrg.areaDesc) || ' ' ||pnOrg.workDesc, ");
		hql.append(" rwInc.seqData, ");
		hql.append(" rwInc.totAmt, ");
		hql.append(" rwInc.newGworkCode ");
		hql.append(" from ApTable rwInc , PnEmployee pnEmp ,PnPosition pnPos, PnOrganization pnOrg,VPnOrganizationSecurity v ");
		hql.append(" where rwInc.ouCode = '");
		hql.append(evaOuCode);
		hql.append("' ");
		hql.append(criteria);
		hql.append(" and v.pk.userId = '" + userId + "' ");
		//hql.append(" and rwInc.creBy = '" + userId + "' ");
		hql.append(" and pnEmp.pk.ouCode = v.pk.ouCode ");
		hql.append(" and pnEmp.codeSeqAct = v.pk.codeSeq ");
		//hql.append(" and rwInc.ouCode = v.pk.ouCode ");
		//hql.append(" and rwInc.codeSeq = v.pk.codeSeq ");
		hql.append(" and rwInc.ouCode = pnEmp.pk.ouCode ");
		hql.append(" and rwInc.empCode = pnEmp.pk.empCode ");
		hql.append(" and rwInc.ouCode = pnOrg.pk.ouCode ");
		hql.append(" and rwInc.codeSeq = pnOrg.pk.codeSeq ");
		hql.append(" and pnEmp.positionCode = pnPos.pk.positionCode ");
		hql.append(" and pnEmp.gworkCode = pnPos.pk.gworkCode ");
		hql.append(" and rwInc.confirmFlag = 'N' ");		
		//hql.append(" and rwInc.approveClose = 'N' ");
		//hql.append(" and rwInc.newCodeSeq = pnOrga.pk.codeSeq ");
		hql.append(" and pnOrg.inactive = 'N' ");
		//hql.append(" and pnOrga.inactive = 'N' ");
		hql.append(" order by rwInc.seqData ");
		System.out.println("HQL findByCriteriaList ==> " + hql.toString());

		Query q = this.getSession().createQuery(hql.toString());
		List empList = q.setFirstResult(countRecord * count)
				.setMaxResults(countRecord).list();
		List retList = new ArrayList();

		/*
		 * List empList = this.getSession().createQuery(hql.toString()).list();
		 * // List retList = new ArrayList();
		 */

		for (Iterator it = empList.iterator(); it.hasNext();) {
			Object[] r = (Object[]) it.next();

			Long keySeq = (Long) r[0];
			String empCode = (String) r[1];
			String prefixName = (String) r[2];
			String firstName = (String) r[3];
			String lastName = (String) r[4];
			//String oldDuty = (String) r[5];
			String oldLevel = (String) r[5];
			String oldPosition = (String) r[6];
			Long codeSeq = (Long) r[7];
			String orgCode = (String) r[8];
			String orgDesc = (String) r[9];
			Double seqData = (Double) r[10];
			Double totAmt = (Double) r[11];
            String newGworkCode = (String) r[12];
			int intLevelCode = Integer.parseInt(oldLevel.trim());

			WeEmployeeVO ret = new WeEmployeeVO();
			ret.setKeySeq(keySeq);
			ret.setEmpCode(empCode);
			ret.setName(prefixName + " " + firstName + " " + lastName);
			//ret.setOldDuty(oldDuty);
			ret.setOldPositionShort(oldPosition + " " + intLevelCode);
			ret.setCodeSeq(codeSeq);
			//ret.setOrgCode(orgCode);
			ret.setOrgDesc(orgDesc);
			//ret.setNewPositionCode(newPositionCode);
			//ret.setNewLevelCode(newLevelCode);
			//ret.setNewOldDuty(newOldDuty);
			//ret.setNewDuty(newDuty);
			//ret.setNewOrgCode(newOrgCode);
			//ret.setNewOrgDesc(newOrgDesc);
			ret.setSeqData(seqData);
			ret.setTotAmt(totAmt);
			ret.setNewGworkCode(newGworkCode);
			//ret.setNewCodeSeq(newCodeSeq);

			retList.add(ret);
		}

		return retList;
	}
	public List findByCriteriaListApprove(String userId, String evaOuCode,
			Long evaYear, Long evaMonth, Long evaVolume, int count,
			int countRecord)

	{
		
		StringBuffer criteria = new StringBuffer();

		if (evaYear != null && !evaYear.equals("")) {
			criteria.append(" and rwInc.yearPn = ");
			criteria.append(evaYear);
		}

		if (evaMonth != null && !evaMonth.equals("")) {
			criteria.append(" and rwInc.monthPn = ");
			criteria.append(evaMonth);
		}

		if (evaVolume != null && !evaVolume.equals("")) {
			criteria.append(" and rwInc.volumeSet = ");
			criteria.append(evaVolume);
		}

		StringBuffer hql = new StringBuffer();

		hql.append(" select rwInc.keySeq, ");
		hql.append(" rwInc.empCode, ");
		hql.append(" pnEmp.refDbPreSuff.prefixName, ");
		hql.append(" pnEmp.firstName, ");
		hql.append(" pnEmp.lastName, ");
		//hql.append(" nvl(pa.adminDesc,' '), ");
		hql.append(" nvl(pnEmp.levelCode,0), ");
		hql.append(" pnPos.positionShort, ");
		hql.append(" pnOrg.pk.codeSeq, ");
		hql.append(" pnOrg.orgCode, ");
		hql.append(" pnOrg.divShort || ' ' || nvl(pnOrg.secDesc,pnOrg.areaDesc) || ' ' ||pnOrg.workDesc, ");
		hql.append(" rwInc.seqData, ");
		hql.append(" rwInc.totAmt, ");
		hql.append(" rwInc.newGworkCode, ");
		hql.append(" rwInc.approveFlag ");
		hql.append(" from ApTable rwInc , PnEmployee pnEmp ,PnAdmin pa,PnPosition pnPos, PnOrganization pnOrg,VPnOrganizationSecurity v ");
		hql.append(" where rwInc.ouCode = '");
		hql.append(evaOuCode);
		hql.append("' ");
		hql.append(criteria);
		hql.append(" and v.pk.userId = '" + userId + "' ");
		//hql.append(" and rwInc.creBy = '" + userId + "' ");
		hql.append(" and pnEmp.pk.ouCode = v.pk.ouCode ");
		hql.append(" and pnEmp.codeSeqAct = v.pk.codeSeq ");
		//hql.append(" and rwInc.ouCode = v.pk.ouCode ");
		//hql.append(" and rwInc.codeSeq = v.pk.codeSeq ");
		hql.append(" and rwInc.ouCode = pnEmp.pk.ouCode ");
		hql.append(" and rwInc.empCode = pnEmp.pk.empCode ");
		hql.append(" and rwInc.ouCode = pnOrg.pk.ouCode ");
		hql.append(" and rwInc.codeSeq = pnOrg.pk.codeSeq ");
		hql.append(" and pnEmp.pk.ouCode = pa.pk.ouCode ");
		hql.append(" and NVL(pnEmp.adminCode,'N') = pa.pk.adminCode ");
		hql.append(" and pnEmp.positionCode = pnPos.pk.positionCode ");
		hql.append(" and pnEmp.gworkCode = pnPos.pk.gworkCode ");
		//hql.append(" and rwInc.volumeSet >= ( ");
		//hql.append("	 select min(c.pk.volumeSetFrom)  from ApConfirmData c ");
		//hql.append(" where ");
		//hql.append(" c.pk.flag = '1' ");
		//hql.append(" and c.pk.yearPn = rwInc.yearPn ");
		//hql.append(" and c.pk.monthPn = rwInc.monthPn ");
		//hql.append(" ) ");
		//hql.append(" and rwInc.volumeSet <=  ( ");
		//hql.append("	 select max(c.pk.volumeSetTo)  from ApConfirmData c ");
		//hql.append(" where ");
		//hql.append(" c.pk.flag = '1' ");
		//hql.append(" and c.pk.yearPn = rwInc.yearPn ");
		//hql.append(" and c.pk.monthPn = rwInc.monthPn ");
		//hql.append(" ) ");
		//hql.append(" and cast(rwInc.volumeSet as int) not between ( ");
		//hql.append("	 select cast(c.pk.volumeSetFrom as int) from ApConfirmData c ");
		//hql.append(" where ");
		//hql.append(" c.pk.flag = '2' ");
		//hql.append(" and c.pk.yearPn = rwInc.yearPn ");
		//hql.append(" and c.pk.monthPn = rwInc.monthPn ");
		//hql.append(" ) ");
		//hql.append(" and  ( ");
		//hql.append("	 select cast(c.pk.volumeSetTo as int) from ApConfirmData c ");
		//hql.append(" where ");
		//hql.append(" c.pk.flag = '2' ");
		//hql.append(" and c.pk.yearPn = rwInc.yearPn ");
		//hql.append(" and c.pk.monthPn = rwInc.monthPn ");
		//hql.append(" ) ");		
		hql.append(" and rwInc.confirmFlag = 'Y' ");
		hql.append(" and rwInc.approveClose = 'N' ");
		hql.append(" and rwInc.bankClose = 'N' ");
		hql.append(" and rwInc.transferClose = 'N' ");
		//hql.append(" and (rwInc.volumeSet >= ( ");
		//hql.append("	 select distinct(c.pk.volumeSetFrom) from ApConfirmData c ");
		//hql.append(" where ");
		//hql.append(" (c.pk.flag = '1' and c.pk.flag <> '2') ");
		//hql.append(" ) ");	
		//hql.append(" and rwInc.ouCode = pnOrga.pk.ouCode ");
		//hql.append(" and rwInc.newCodeSeq = pnOrga.pk.codeSeq ");
		hql.append(" and pnOrg.inactive = 'N' ");
		//hql.append(" and pnOrga.inactive = 'N' ");
		hql.append(" order by rwInc.seqData ");
		System.out.println("HQL findByCriteriaListApprove ==> " + hql.toString());

		Query q = this.getSession().createQuery(hql.toString());
		List empList = q.setFirstResult(countRecord * count)
				.setMaxResults(countRecord).list();
		List retList = new ArrayList();
        
		/*
		 * List empList = this.getSession().createQuery(hql.toString()).list();
		 * // List retList = new ArrayList();
		 */

		for (Iterator it = empList.iterator(); it.hasNext();) {
			Object[] r = (Object[]) it.next();

			Long keySeq = (Long) r[0];
			String empCode = (String) r[1];
			String prefixName = (String) r[2];
			String firstName = (String) r[3];
			String lastName = (String) r[4];
			//String oldDuty = (String) r[5];
			String oldLevel = (String) r[5];
			String oldPosition = (String) r[6];
			Long codeSeq = (Long) r[7];
			String orgCode = (String) r[8];
			String orgDesc = (String) r[9];
			Double seqData = (Double) r[10];
			Double totAmt = (Double) r[11];
            String newGworkCode = (String) r[12];
            String approveFlag = (String) r[13];
			int intLevelCode = Integer.parseInt(oldLevel.trim());
			
            
			 
			WeEmployeeVO ret = new WeEmployeeVO();
			ret.setKeySeq(keySeq);
			ret.setEmpCode(empCode);
			ret.setName(prefixName + " " + firstName + " " + lastName);
			//ret.setOldDuty(oldDuty);
			ret.setOldPositionShort(oldPosition + " " + intLevelCode);
			ret.setCodeSeq(codeSeq);
			//ret.setOrgCode(orgCode);
			ret.setOrgDesc(orgDesc);
			//ret.setNewPositionCode(newPositionCode);
			//ret.setNewOldDuty(newOldDuty);
			//ret.setNewDuty(newDuty);
			//ret.setNewOrgCode(newOrgCode);
			//ret.setNewOrgDesc(newOrgDesc);
			ret.setSeqData(seqData);
			ret.setTotAmt(totAmt);
			ret.setNewGworkCode(newGworkCode);
			ret.setApproveFlag(approveFlag);
			System.out.println(newGworkCode);
			
			//ret.setNewCodeSeq(newCodeSeq);

			retList.add(ret);
		}

		return retList;
	}
	
	public List findByEmpCriteriaList(String userId,String empCode, String evaOuCode,
			Long evaYear ,int count,int countRecord)

{
		
		StringBuffer criteria = new StringBuffer();

		if (evaYear != null && !evaYear.equals("")) {
			criteria.append(" and rwInc.yearPn = ");
			criteria.append(evaYear);
		}

	

		StringBuffer hql = new StringBuffer();

		hql.append(" select rwInc.keySeq, ");
		hql.append(" rwInc.yearPn, ");
		hql.append(" rwInc.monthPn, ");
		hql.append(" rwInc.volumeSet, ");
		hql.append(" rwInc.seqData, ");
		hql.append(" rwInc.totAmt, ");
		hql.append(" rwInc.newGworkCode, ");
		hql.append(" rwInc.approveFlag, ");
		hql.append(" rwInc.bankFlag, ");
		hql.append(" rwInc.bankDate ");
		hql.append(" from ApTable rwInc  ");
		hql.append(" where rwInc.ouCode = '");
		hql.append(evaOuCode);
		hql.append("' ");
		hql.append(criteria);
		hql.append(" and rwInc.empCode =  '" + empCode + "' ");
		hql.append(" order by rwInc.yearPn desc,rwInc.monthPn desc,rwInc.seqData desc ");
		System.out.println("HQL findByEmpCriteriaList ==> " + hql.toString());

		Query q = this.getSession().createQuery(hql.toString());
		List empList = q.setFirstResult(countRecord * count).setMaxResults(countRecord).list();
		List retList = new ArrayList();
        
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy",
				new java.util.Locale("th", "TH"));
		//List empList = this.getSession().createQuery(hql.toString()).list();
		//List retList = new ArrayList();
		 

		for (Iterator it = empList.iterator(); it.hasNext();) {
			Object[] r = (Object[]) it.next();

			Long keySeq = (Long) r[0];
			Double yearPn = (Double) r[1];
			Double monthPn = (Double) r[2];
			String volumeSet = (String) r[3];
			Double seqData = (Double) r[4];
			Double totAmt = (Double) r[5];
            String newGworkCode = (String) r[6];
            String approveFlag = (String) r[7];
            String bankFlag = (String) r[8];
            Date bankDate = (Date) r[9];
			//int intLevelCode = Integer.parseInt(oldLevel.trim());
			
            
			ApEmployeeVO ret = new ApEmployeeVO();
			ret.setKeySeq(keySeq);
			ret.setYearPn(yearPn);
			ret.setMonthPn(monthPn);
			ret.setVolumeSet(volumeSet);			
			ret.setSeqData(seqData);
			ret.setTotAmt(totAmt);
			ret.setNewGworkCode(newGworkCode);
			ret.setApproveFlag(approveFlag);
			ret.setBankFlag(bankFlag);
			ret.setBankDate(bankDate);
			//ret.setNewCodeSeq(newCodeSeq);

			retList.add(ret);
		}

		return retList;
	}
	
	public List findVolumeBySecurity(String userId, String ouCode,String evaYear,String evaMonth)
			throws Exception {

		StringBuffer hql = new StringBuffer(0);
		hql.append("select distinct(a.volumeSet) from ApTable a ");
		hql.append(" where ");
		hql.append(" a.ouCode = '" + ouCode + "' ");
		hql.append(" and a.confirmFlag ='N' ");
		hql.append(" and a.yearPn = '" + evaYear + "' ");
		hql.append(" and a.monthPn = '" + evaMonth + "' ");
		hql.append(" and a.empCode in  ( ");
		hql.append("	 select b.pk.empCode from VPnEmployeeSecurityWork b ");
		hql.append("	 where b.pk.userId = '" + userId + "' ");
		hql.append("	 and b.pk.ouCode = '" + ouCode + "' ");
		hql.append("     and a.codeSeq = b.pk.codeSeqAct ");
		hql.append(" ) ");
		hql.append(" order by cast(a.volumeSet as int) desc ");

		
		List ls = this.getHibernateTemplate().find(hql.toString());
	    System.out.println(ls);
		return ls;
	}
	
	public List findVolumeBySecurityReport1(String userId, String ouCode,String evaYear,String evaMonth)
			throws Exception {

		StringBuffer hql = new StringBuffer(0);
		hql.append("select distinct(a.volumeSet) from ApTable a ");
		hql.append(" where ");
		hql.append(" a.ouCode = '" + ouCode + "' ");
		hql.append(" and a.yearPn = '" + evaYear + "' ");
		hql.append(" and a.monthPn = '" + evaMonth + "' ");
		//hql.append(" and a.confirmFlag is null ");
		hql.append(" and a.empCode in  ( ");
		hql.append("	 select b.pk.empCode from VPnEmployeeSecurityWork b ");
		hql.append("	 where b.pk.userId = '" + userId + "' ");
		hql.append("	 and b.pk.ouCode = '" + ouCode + "' ");
		hql.append(" and a.codeSeq = b.pk.codeSeqAct ");
		hql.append(" ) ");
		hql.append(" order by cast(a.volumeSet as int) desc ");

		
		List ls = this.getHibernateTemplate().find(hql.toString());
	
		return ls;
	}


	
	public List findVolumeBySecurityApprove(String userId, String ouCode,String evaYear,String evaMonth)
			throws Exception {

		StringBuffer hql = new StringBuffer(0);
		hql.append("select distinct(a.volumeSet) from ApTable a ");
		hql.append(" where ");
		hql.append(" a.ouCode = '" + ouCode + "' ");
		hql.append(" and a.yearPn = '" + evaYear + "' ");
		hql.append(" and a.monthPn = '" + evaMonth + "' ");
		hql.append(" and a.confirmFlag  = 'Y' ");
		hql.append(" and a.approveClose  = 'N' ");
		//hql.append(" and a.volumeSet >=  ( ");
		//hql.append("	 select min(c.pk.volumeSetFrom) from ApConfirmData c ");
		//hql.append(" where ");
		//hql.append(" c.pk.flag = '1' ");
		//hql.append(" and c.pk.yearPn = a.yearPn ");
		//hql.append(" and c.pk.monthPn = a.monthPn ");
		//hql.append(" ) ");
		//hql.append(" and a.volumeSet <=   ( ");
		//hql.append("	 select max(c.pk.volumeSetTo) from ApConfirmData c ");
		//hql.append(" where ");
		//hql.append(" c.pk.flag = '1' ");
		//hql.append(" and c.pk.yearPn = a.yearPn ");
		//hql.append(" and c.pk.monthPn = a.monthPn ");
		//hql.append(" ) ");
		//hql.append(" and cast(a.volumeSet as int) not between ( ");
		//hql.append("	 select cast(c.pk.volumeSetFrom as int) from ApConfirmData c ");
		//hql.append(" where ");
		//hql.append(" c.pk.flag = '2' ");
		//hql.append(" and c.pk.yearPn = a.yearPn ");
		//hql.append(" and c.pk.monthPn = a.monthPn ");
		//hql.append(" ) ");
		//hql.append(" and  ( ");
		//hql.append("	 select cast(c.pk.volumeSetTo as int) from ApConfirmData c ");
		//hql.append(" where ");
		//hql.append(" c.pk.flag = '2' ");
		//hql.append(" and c.pk.yearPn = a.yearPn ");
		//hql.append(" and c.pk.monthPn = a.monthPn ");
		//hql.append(" ) ");
		hql.append(" and a.empCode in  ( ");
		hql.append("	 select b.pk.empCode from VPnEmployeeSecurityWork b ");
		hql.append("	 where b.pk.userId = '" + userId + "' ");
		hql.append("	 and b.pk.ouCode = '" + ouCode + "' ");
		//hql.append("	 and a.creBy  = '" + userId + "' ");
		hql.append(" and a.codeSeq = b.pk.codeSeqAct ");
		hql.append(" ) ");
		hql.append(" order by cast(a.volumeSet as int) desc ");

		// System.out.println("*******************");
		List ls = this.getHibernateTemplate().find(hql.toString());
		// System.out.println("*******************");
		// System.out.println("ls : " + ls.size());
		// if( ls != null && ls.size() > 0 ){
		// for (int i=0; i<ls.size(); i++) {
		// PnEmployee element = (PnEmployee) ls.get(i);
		// PnEmployeeVO vo = new PnEmployeeVO();
		// try {
		// BeanUtils.copyProperties(vo, element);
		// BeanUtils.copyProperties(vo, element.getPk());
		// ls.set(i, vo);
		// } catch (IllegalAccessException e) {
		// e.printStackTrace();
		// throw new Exception("can't copy properties : " + e.getMessage());
		// } catch (InvocationTargetException e) {
		// e.printStackTrace();
		// throw new Exception("can't copy properties : " + e.getMessage());
		// }
		// }
		// }
		System.out.println(ls);
		return ls;
	}
	
	public List findVolumeBySecurityApproveReport(String userId, String ouCode,String evaYear,String evaMonth)
			throws Exception {

		StringBuffer hql = new StringBuffer(0);
		hql.append("select distinct(a.volumeSet) from ApTable a ");
		hql.append(" where ");
		hql.append(" a.ouCode = '" + ouCode + "' ");
		hql.append(" and a.yearPn = '" + evaYear + "' ");
		hql.append(" and a.monthPn = '" + evaMonth + "' ");
		hql.append(" and a.confirmFlag  = 'Y' ");
		hql.append(" and a.approveFlag  = 'Y' ");
		hql.append(" and a.empCode in  ( ");
		hql.append("	 select b.pk.empCode from VPnEmployeeSecurityWork b ");
		hql.append("	 where b.pk.userId = '" + userId + "' ");
		hql.append("	 and b.pk.ouCode = '" + ouCode + "' ");
		//hql.append("	 and a.creBy  = '" + userId + "' ");
		hql.append(" and a.codeSeq = b.pk.codeSeqAct ");
		hql.append(" ) ");
		hql.append(" order by cast(a.volumeSet as int) desc ");

	
		List ls = this.getHibernateTemplate().find(hql.toString());
		
		return ls;
	}
	
	
	public List findVolumeBySecurityPm001(String userId, String ouCode,String evaYear,String evaMonth)
			throws Exception {

		
		StringBuffer hql = new StringBuffer(0);
		hql.append("select distinct(a.volumeSet) from ApTable a ");
		hql.append(" where ");
		hql.append(" a.ouCode = '" + ouCode + "' ");
		hql.append(" and a.yearPn = '" + evaYear + "' ");
		hql.append(" and a.monthPn = '" + evaMonth + "' ");
		hql.append(" and a.confirmFlag  = 'Y' ");
		hql.append(" and a.approveClose  = 'Y' ");
		hql.append(" and a.bankClose  = 'N' ");		
		/*hql.append(" and a.volumeSet >=  ( ");
		hql.append("	 select min(c.pk.volumeSetFrom) from ApConfirmData c ");
		hql.append(" where ");
		hql.append(" c.pk.flag = '1' ");
		hql.append(" and c.pk.yearPn = a.yearPn ");
		hql.append(" and c.pk.monthPn = a.monthPn ");
		hql.append(" ) ");
		hql.append(" and a.volumeSet <=   ( ");
		hql.append("	 select max(c.pk.volumeSetTo) from ApConfirmData c ");
		hql.append(" where ");
		hql.append(" c.pk.flag = '1' ");
		hql.append(" and c.pk.yearPn = a.yearPn ");
		hql.append(" and c.pk.monthPn = a.monthPn ");
		hql.append(" ) ");
		hql.append(" and a.volumeSet >=  ( ");
		hql.append("	 select min(c.pk.volumeSetFrom) from ApConfirmData c ");
		hql.append(" where ");
		hql.append(" c.pk.flag = '2' ");
		hql.append(" and c.pk.yearPn = a.yearPn ");
		hql.append(" and c.pk.monthPn = a.monthPn ");
		hql.append(" ) ");
		hql.append(" and a.volumeSet <=   ( ");
		hql.append("	 select max(c.pk.volumeSetTo) from ApConfirmData c ");
		hql.append(" where ");
		hql.append(" c.pk.flag = '2' ");
		hql.append(" and c.pk.yearPn = a.yearPn ");
		hql.append(" and c.pk.monthPn = a.monthPn ");
		hql.append(" ) ");*/
		//hql.append(" and cast(a.volumeSet as int) not between ( ");
		//hql.append("	 select cast(c.pk.volumeSetFrom as int) from ApConfirmData c ");
		//hql.append(" where ");
		//hql.append(" c.pk.flag = '2' ");
		//hql.append(" and c.pk.yearPn = a.yearPn ");
		//hql.append(" and c.pk.monthPn = a.monthPn ");
		//hql.append(" ) ");
		//hql.append(" and  ( ");
		//hql.append("	 select cast(c.pk.volumeSetTo as int) from ApConfirmData c ");
		//hql.append(" where ");
		//hql.append(" c.pk.flag = '2' ");
		//hql.append(" and c.pk.yearPn = a.yearPn ");
		//hql.append(" and c.pk.monthPn = a.monthPn ");
		//hql.append(" ) ");
		hql.append(" and a.empCode in  ( ");
		hql.append("	 select b.pk.empCode from VPnEmployeeSecurityWork b ");
		hql.append("	 where b.pk.userId = '" + userId + "' ");
		hql.append("	 and b.pk.ouCode = '" + ouCode + "' ");
		//hql.append("	 and a.creBy  = '" + userId + "' ");
		hql.append(" and a.codeSeq = b.pk.codeSeqAct ");
		hql.append(" ) ");
		hql.append(" order by cast(a.volumeSet as int) desc ");
		
		List ls = this.getHibernateTemplate().find(hql.toString());
		
		return ls;
	}

	public List findVolumeBySecurityPm002(String userId, String ouCode,String evaYear,String evaMonth)
			throws Exception {

		StringBuffer hql = new StringBuffer(0);
		hql.append("select distinct(a.volumeSet) from ApTable a ");
		hql.append(" where ");
		hql.append(" a.ouCode = '" + ouCode + "' ");
		hql.append(" and a.yearPn = '" + evaYear + "' ");
		hql.append(" and a.monthPn = '" + evaMonth + "' ");
		hql.append(" and a.confirmFlag  = 'Y' ");
		hql.append(" and a.approveClose  = 'Y' ");
		hql.append(" and a.bankClose  = 'N' ");		
		/*hql.append(" and a.volumeSet >=  ( ");
		hql.append("	 select min(c.pk.volumeSetFrom) from ApConfirmData c ");
		hql.append(" where ");
		hql.append(" c.pk.flag = '1' ");
		hql.append(" and c.pk.yearPn = a.yearPn ");
		hql.append(" and c.pk.monthPn = a.monthPn ");
		hql.append(" ) ");
		hql.append(" and a.volumeSet <=   ( ");
		hql.append("	 select max(c.pk.volumeSetTo) from ApConfirmData c ");
		hql.append(" where ");
		hql.append(" c.pk.flag = '1' ");
		hql.append(" and c.pk.yearPn = a.yearPn ");
		hql.append(" and c.pk.monthPn = a.monthPn ");
		hql.append(" ) ");
		hql.append(" and a.volumeSet >=  ( ");
		hql.append("	 select min(c.pk.volumeSetFrom) from ApConfirmData c ");
		hql.append(" where ");
		hql.append(" c.pk.flag = '2' ");
		hql.append(" and c.pk.yearPn = a.yearPn ");
		hql.append(" and c.pk.monthPn = a.monthPn ");
		hql.append(" ) ");
		hql.append(" and a.volumeSet <=   ( ");
		hql.append("	 select max(c.pk.volumeSetTo) from ApConfirmData c ");
		hql.append(" where ");
		hql.append(" c.pk.flag = '2' ");
		hql.append(" and c.pk.yearPn = a.yearPn ");
		hql.append(" and c.pk.monthPn = a.monthPn ");
		hql.append(" ) ");		*/	
		hql.append(" and a.empCode in  ( ");
		hql.append("	 select b.pk.empCode from VPnEmployeeSecurityWork b ");
		hql.append("	 where b.pk.userId = '" + userId + "' ");
		hql.append("	 and b.pk.ouCode = '" + ouCode + "' ");
		//hql.append("	 and a.creBy  = '" + userId + "' ");
		hql.append(" and a.codeSeq = b.pk.codeSeqAct ");
		hql.append(" ) ");
		hql.append(" order by cast(a.volumeSet as int) desc ");

		
		List ls = this.getHibernateTemplate().find(hql.toString());
	
		return ls;
	}


	public List findVolumeBySecurityTransfer(String userId, String ouCode,String evaYear,String evaMonth)
			throws Exception {

		//evaMonth = String.valueOf(Integer.parseInt(evaMonth)-1);
		
		//if (evaMonth == "0") {
		//		evaMonth = "1";
		//}
		
		StringBuffer hql = new StringBuffer(0);
		hql.append("select distinct(a.volumeSet) from ApTable a ");
		hql.append(" where ");
		hql.append(" a.ouCode = '" + ouCode + "' ");
		hql.append(" and a.yearPn = '" + evaYear + "' ");
		hql.append(" and a.monthPn = '" + evaMonth + "' ");
		hql.append(" and a.confirmFlag  = 'Y' ");
		hql.append(" and a.approveClose  = 'Y' ");
		//hql.append(" and a.bankClose  = 'Y' ");		
		hql.append(" and a.transferClose  = 'N' ");		
		//hql.append(" and a.volumeSet >=  ( ");
		//hql.append("	 select min(c.pk.volumeSetFrom) from ApConfirmData c ");
		//hql.append(" where ");
		//hql.append(" c.pk.flag = '1' ");
		//hql.append(" and c.pk.yearPn = a.yearPn ");
		//hql.append(" and c.pk.monthPn = a.monthPn ");
		//hql.append(" ) ");
		//hql.append(" and a.volumeSet <=   ( ");
		//hql.append("	 select max(c.pk.volumeSetTo) from ApConfirmData c ");
		//hql.append(" where ");
		//hql.append(" c.pk.flag = '1' ");
		//hql.append(" and c.pk.yearPn = a.yearPn ");
		//hql.append(" and c.pk.monthPn = a.monthPn ");
		//hql.append(" ) ");
	/*	hql.append(" and a.volumeSet >=  ( ");
		hql.append("	 select min(c.pk.volumeSetFrom) from ApConfirmData c ");
		hql.append(" where ");
		hql.append(" c.pk.flag = '2' ");
		hql.append(" and c.pk.yearPn = a.yearPn ");
		hql.append(" and c.pk.monthPn = a.monthPn ");
		hql.append(" ) ");
		hql.append(" and a.volumeSet <=   ( ");
		hql.append("	 select max(c.pk.volumeSetTo) from ApConfirmData c ");
		hql.append(" where ");
		hql.append(" c.pk.flag = '2' ");
		hql.append(" and c.pk.yearPn = a.yearPn ");
		hql.append(" and c.pk.monthPn = a.monthPn ");
		hql.append(" ) ");			*/
		hql.append(" and a.empCode in  ( ");
		hql.append("	 select b.pk.empCode from VPnEmployeeSecurityWork b ");
		hql.append("	 where b.pk.userId = '" + userId + "' ");
		hql.append("	 and b.pk.ouCode = '" + ouCode + "' ");
		//hql.append("	 and a.creBy  = '" + userId + "' ");
		hql.append(" and a.codeSeq = b.pk.codeSeqAct ");
		hql.append(" ) ");
		hql.append(" order by cast(a.volumeSet as int) desc ");

		
		List ls = this.getHibernateTemplate().find(hql.toString());
	
		return ls;
	}


	public List findByCriteriaReport(String userId, String evaOuCode,
			String evaYear, String evaMonth, String evaVolume) {
		List resultList = new ArrayList();
		List returnList = new ArrayList();
		int level;
		//int newLevel;
		String sql = "";
		sql = " SELECT DB.PREFIX_NAME||' '||PN.FIRST_NAME||'   '||PN.LAST_NAME ,"
				+ "	PP.POSITION_SHORT  , "
				+ "   DECODE(TO_NUMBER(PN.LEVEL_CODE),0,NULL,TO_NUMBER(PN.LEVEL_CODE)) , "
				+ "   NVL(PO.SEC_DESC,PO.AREA_DESC)  , "
				+ "   NVL(PO.WORK_DESC,NULL)  , "
				+ "   PO.DIV_SHORT ,"
				+ "   PN.EMP_CODE , "
				+ "  DECODE(WE.NEW_GWORK_CODE,'1','��ѡ�ҹ','2','��ͺ����') ,"
				+ "   WE.TOT_AMT, "
				+ "   WE.SEQ_DATA "
				+ "   FROM DB_PRE_SUFF DB, "
				+ "  PN_EMPLOYEE PN, "
				+ "  AP_TABLE WE, "
				+ "  PN_ORGANIZATION PO, "
				+ "  PN_POSITION PP "
				+ "  WHERE DB.PRE_SUFF_CODE = PN.PRE_NAME "
				+ "   AND WE.OU_CODE = '001'"
				+ "   AND WE.CRE_BY ='"
				+ userId
				+ "'"
				+ "   AND PN.OU_CODE = WE.OU_CODE "
				+ "  AND PN.OU_CODE = PO.OU_CODE "
				+ "  AND PN.OU_CODE = PP.OU_CODE "
				+ "  AND PN.EMP_CODE = WE.EMP_CODE "
				+ "  AND PN.EMP_STATUS = 'B' "
				+ "   AND PN.GWORK_CODE = PP.GWORK_CODE "
				+ "  AND PN.POSITION_CODE = PP.POSITION_CODE "
				+ "  AND PN.CODE_SEQ_ACT = PO.CODE_SEQ"
				+ "   AND PP.INACTIVE = 'N'"
				+ "   AND PO.INACTIVE = 'N' ";
		if (!evaYear.equals("")) {
			sql += "\n and  we.year_pn ='" + evaYear + "'";
		}
		if (!evaMonth.equals("")) {
			sql += "\n and  we.month_pn  = '" + evaMonth + "'";
		}
		if (!evaVolume.equals("")) {
			sql += "\n and  we.volume_set  = '" + evaVolume + "'";
		}
		sql += "\n " + "   ORDER BY WE.SEQ_DATA ";

		resultList = this.jdbcTemplate.queryForList(sql);

		for (int i = 0; i < resultList.size(); i++) {
			ListOrderedMap map = (ListOrderedMap) resultList.get(i);

			String name = (String) map.getValue(0);
			String oldPosition = (String) map.getValue(1);
			String oldLevel = (String) map.getValue(2);
			String oldSec = (String) map.getValue(3);
			String oldWork = (String) map.getValue(4);
			String oldDiv = (String) map.getValue(5);
			//String oldDuty = (String) map.getValue(6);
			String empCode = (String) map.getValue(6);
			//String account = (String) map.getValue(8);
			//String newPosition = (String) map.getValue(9);
			//String newLevelCode = (String) map.getValue(10);
			String newGworkCode = (String) map.getValue(7);
			Double totAmt = (Double) map.getValue(8);
			Double seqData = (Double) map.getValue(9);
			//String newSec = (String) map.getValue(12);
			//String newWork = (String) map.getValue(13);
			//String newDiv = (String) map.getValue(14);
			//String newDuty = (String) map.getValue(15);

			//newLevel = Integer.parseInt(newLevelCode);
			level = Integer.parseInt(oldLevel);
			ApEmployeeVO vo = new ApEmployeeVO();

			vo.setName(name);
			vo.setOldPosition(oldPosition + " " + level);
			//vo.setOldSec(oldSec);
			//vo.setOldWork(oldWork);
			//vo.setOldDiv(oldDiv);
			vo.setOrgDesc(oldDiv+" "+oldSec+" "+oldWork);
			//vo.setOldDuty(oldDuty);
			vo.setEmpCode(empCode);
			//vo.setAccount(account);
			//vo.setNewPosition(newPosition + " " + newLevel);
			vo.setNewGworkCode(newGworkCode);
			vo.setTotAmt(totAmt);
			vo.setSeqData(seqData);
			//vo.setNewSec(newSec);
			//vo.setNewWork(newWork);
			//vo.setNewDiv(newDiv);
			//vo.setNewDuty(newDuty);

			returnList.add(vo);

		}
		return returnList;
	}

	public List findByCriteriaPromoteLevelReport(String userId,
			String evaOuCode, Long evaYear, Long evaMonth, Long evaVolume) {
		List resultList = new ArrayList();
		List returnList = new ArrayList();
		String sql = "";
		sql = "  SELECT DB.PREFIX_NAME||' '||PN.FIRST_NAME||'  '||PN.LAST_NAME NAME, "
				+ "	PP.POSITION_SHORT||' '||TO_NUMBER(PN.LEVEL_CODE) , "
				+ "	PO.SEC_DESC , "
				+ "	PO.WORK_DESC , "
				+ "	PO.DIV_SHORT , "
				+ "	DECODE(WE.NEW_OLD_DUTY,NULL,PA.ADMIN_DESC,PATT.ADMIN_DESC) , "
				+ "	PN.EMP_CODE||'/'||PN.GWORK_CODE EMP_CODE, "
				+ "	PN.ACCOUNT, "
				+ "	PN_DESC.GET_POSITION_SHORT(WE.OU_CODE,NVL(WE.NEW_GWORK_CODE,PN.GWORK_CODE),NVL(WE.NEW_POSITION_CODE,PN.POSITION_CODE))||' '||TO_NUMBER(NVL(WE.NEW_LEVEL_CODE,PN.LEVEL_CODE)), "
				+ "	POR.SEC_DESC, "
				+ "	POR.WORK_DESC, "
				+ "	POR.DIV_SHORT, "
				+ "	PAT.ADMIN_DESC, "
				+ "	PN.EMP_CODE||'/'||NVL(WE.NEW_GWORK_CODE,PN.GWORK_CODE) "
				+ "	FROM DB_PRE_SUFF DB, "
				+ "	PN_EMPLOYEE PN, "
				+ "	AP_TABLE WE, "
				+ "	PN_ORGANIZATION PO, "
				+ "	PN_ORGANIZATION POR, "
				+ "	PN_POSITION PP, "
				+ "	PN_ADMIN PA, "
				+ "	PN_ADMIN_TT PAT, "
				+ "	PN_ADMIN_TT PATT "
				+ "	WHERE DB.PRE_SUFF_CODE = PN.PRE_NAME "
				+ "	AND PN.OU_CODE = WE.OU_CODE "
				+ "	AND PN.OU_CODE = PO.OU_CODE "
				+ "	AND WE.OU_CODE = POR.OU_CODE "
				+ "	AND WE.OU_CODE = PAT.OU_CODE "
				+ "	AND WE.OU_CODE = PATT.OU_CODE "
				+ "	AND PN.OU_CODE = PP.OU_CODE "
				+ "	AND PN.OU_CODE = PA.OU_CODE "
				+ "	AND PN.EMP_CODE = WE.EMP_CODE "
				+ "	AND PN.EMP_STATUS = 'B' "
				+ "	AND PN.GWORK_CODE = PP.GWORK_CODE "
				+ "	AND PN.POSITION_CODE = PP.POSITION_CODE "
				+ "	AND NVL(PN.ADMIN_CODE,'N')= PA.ADMIN_CODE "
				+ "	AND NVL(WE.NEW_DUTY,'N') = PAT.ADMIN_CODE(+) "
				+ "	AND NVL(WE.NEW_OLD_DUTY,'N') = PATT.ADMIN_CODE(+) "
				+ "	AND PN.CODE_SEQ = PO.CODE_SEQ "
				+ "	AND WE.NEW_CODE_SEQ = POR.CODE_SEQ "
				+ "	AND PP.INACTIVE = 'N' "
				+ "	AND PO.INACTIVE = 'N' "
				+ "	AND POR.INACTIVE = 'N' "
				+ "	AND PA.INACTIVE = 'N' "
				+ "	AND PAT.INACTIVE = 'N' " + "	ORDER BY WE.SEQ_DATA ";

		resultList = this.jdbcTemplate.queryForList(sql);
		for (int i = 0; i < resultList.size(); i++) {
			ListOrderedMap map = (ListOrderedMap) resultList.get(i);
			String empName = (String) map.getValue(0);
			String oldPosition = (String) map.getValue(1);
			String oldLevel = (String) map.getValue(2);
			String oldSec = (String) map.getValue(3);
			String oldWork = (String) map.getValue(4);
			String oldDiv = (String) map.getValue(5);
			String oldDuty = (String) map.getValue(6);
			String empCode = (String) map.getValue(7);
			String accout = (String) map.getValue(8);
			String newPosition = (String) map.getValue(9);
			String newLevel = (String) map.getValue(10);
			String newSec = (String) map.getValue(11);
			String newWork = (String) map.getValue(12);
			String newDiv = (String) map.getValue(13);
			String newDuty = (String) map.getValue(14);
			String newEmpCode = (String) map.getValue(15);

			WeEmployeeVO vo = new WeEmployeeVO();

			vo.setName(empName);
			vo.setOldPositionShort(oldPosition);

			System.out.println(vo.getName());

			returnList.add(vo);

		}
		return returnList;
	}
	public boolean isConfirmFlag(String ouCode, String year, String month,
			String userId) throws Exception {
		StringBuffer sql = new StringBuffer(0);
		sql.append(" select count(*) ");
		sql.append(" from ApTable p ");
		sql.append(" where p.ouCode = '" + ouCode + "' ");
		sql.append(" and p.yearPn = " + year);
		sql.append(" and p.monthPn = " + month);
		sql.append(" and p.confirmFlag = 'Y' ");

		List ls = this.getHibernateTemplate().find(sql.toString());

		if (ls != null && ls.size() > 0) {
			Integer i = (Integer) ls.get(0);

			// System.out.println("i : " + i.intValue() + " j : " + j.intValue()
			// );

			if (i.intValue() > 0)
				return true;
			else
				return false;
		} else
			return true;
	}
	public String[] findMaxYearPeriod(String ouCode) {

		String year = "";
		
		String month = "";
		StringBuffer sql = new StringBuffer(0);
		sql.append(" SELECT a.yearPn ||MAX(a.monthPn) ");
		sql.append(" FROM   ApTable a ");
		sql.append(" WHERE  a.ouCode = '" + ouCode + "' ");
		//sql.append(" and a.confirmFlag <> 'Y' ");
		sql.append(" and a.yearPn = ( select max(b.yearPn) from ApTable b ) ");
		sql.append(" group by a.yearPn ");
		List ls = this.getHibernateTemplate().find(sql.toString());
		if (ls != null && ls.size() > 0) {
			String rs = (String) ls.get(0);
			year = rs.substring(0, 4);
			month = rs.substring(4, rs.length());
		}

		String[] val = new String[2];
		val[0] = year;
		val[1] = month;
		System.out.println("VAL0" + val[0]);
		System.out.println("VAL1" + val[1]);

		return val;
	}
	
	
	public String[] findMaxYearPeriodAP001(String ouCode) {

		String year = "";
		
		String month = "";
		StringBuffer sql = new StringBuffer(0);
		sql.append(" SELECT a.yearPn ||MAX(a.monthPn) ");
		sql.append(" FROM   ApTable a ");
		sql.append(" WHERE  a.ouCode = '" + ouCode + "' ");
		sql.append(" and a.confirmFlag = 'N' ");
		sql.append(" and a.approveFlag = 'N' ");
		sql.append(" and a.approveClose = 'N' ");
		sql.append(" and a.yearPn = ( select max(b.yearPn) from ApTable b ) ");
		sql.append(" group by a.yearPn ");
		List ls = this.getHibernateTemplate().find(sql.toString());
		if (ls != null && ls.size() > 0) {
			String rs = (String) ls.get(0);
			year = rs.substring(0, 4);
			month = rs.substring(4, rs.length());
		}

		String[] val = new String[2];
		val[0] = year;
		val[1] = month;
		System.out.println("VAL0" + val[0]);
		System.out.println("VAL1" + val[1]);

		return val;
	}
	
	public String[] findMaxYearPeriodAP002(String ouCode) {

		String year = "";
		
		String month = "";
		StringBuffer sql = new StringBuffer(0);
		sql.append(" SELECT a.yearPn ||MAX(a.monthPn) ");
		sql.append(" FROM   ApTable a ");
		sql.append(" WHERE  a.ouCode = '" + ouCode + "' ");
		sql.append(" and a.confirmFlag = 'Y' ");
		//sql.append(" and a.approveFlag = 'N' ");
		sql.append(" and a.approveClose = 'N' ");
		sql.append(" and a.yearPn = ( select max(b.yearPn) from ApTable b ) ");
		sql.append(" group by a.yearPn ");
		List ls = this.getHibernateTemplate().find(sql.toString());
		if (ls != null && ls.size() > 0) {
			String rs = (String) ls.get(0);
			year = rs.substring(0, 4);
			month = rs.substring(4, rs.length());
		}

		String[] val = new String[2];
		val[0] = year;
		val[1] = month;
		System.out.println("VAL0" + val[0]);
		System.out.println("VAL1" + val[1]);

		return val;
	}
	
	public String[] findMaxYearPeriodBK(String ouCode) {

		String year = "";
		
		String month = "";
		StringBuffer sql = new StringBuffer(0);
		sql.append(" SELECT a.yearPn ||MAX(a.monthPn) ");
		sql.append(" FROM   ApTable a ");
		sql.append(" WHERE  a.ouCode = '" + ouCode + "' ");
		sql.append(" and a.confirmFlag = 'Y' ");
		sql.append(" and a.approveFlag = 'Y' ");
		sql.append(" and a.approveClose = 'Y' ");
		sql.append(" and a.bankClose = 'N' ");
		//sql.append(" and a.approveFlag = 'N' ");
		sql.append(" and a.yearPn = ( select max(b.yearPn) from ApTable b ) ");
		sql.append(" group by a.yearPn ");
		List ls = this.getHibernateTemplate().find(sql.toString());
		if (ls != null && ls.size() > 0) {
			String rs = (String) ls.get(0);
			year = rs.substring(0, 4);
			month = rs.substring(4, rs.length());
		}

		String[] val = new String[2];
		val[0] = year;
		val[1] = month;
		System.out.println("VAL0" + val[0]);
		System.out.println("VAL1" + val[1]);

		return val;
	}
	
	public String[] findMaxYearPeriodTR(String ouCode) {

		String year = "";
		
		String month = "";
		StringBuffer sql = new StringBuffer(0);
		sql.append(" SELECT a.yearPn ||MAX(a.monthPn) ");
		sql.append(" FROM   ApTable a ");
		sql.append(" WHERE  a.ouCode = '" + ouCode + "' ");
		sql.append(" and a.confirmFlag = 'Y' ");
		sql.append(" and a.approveFlag = 'Y' ");
		sql.append(" and a.approveClose = 'Y' ");
		sql.append(" and a.bankClose = 'Y' ");
		//sql.append(" and a.transferFlag = 'N' ");
		sql.append(" and a.transferClose = 'N' ");
		//sql.append(" and a.approveFlag = 'N' ");
		//sql.append(" and a.confirmFlag <> 'Y' ");
		sql.append(" and a.yearPn = ( select max(b.yearPn) from ApTable b ) ");
		sql.append(" group by a.yearPn ");
		List ls = this.getHibernateTemplate().find(sql.toString());
		if (ls != null && ls.size() > 0) {
			String rs = (String) ls.get(0);
			year = rs.substring(0, 4);
			month = rs.substring(4, rs.length());
		}

		String[] val = new String[2];
		val[0] = year;
		val[1] = month;
		System.out.println("VAL0" + val[0]);
		System.out.println("VAL1" + val[1]);

		return val;
	}
	
	
	public String[] findMaxMonthInYear(String ouCode,String year) {		
		
		String month = "";
		StringBuffer sql = new StringBuffer(0);
		sql.append(" SELECT MAX(a.monthPn) ");
		sql.append(" FROM   ApTable a ");
		sql.append(" WHERE  a.ouCode = '" + ouCode + "' ");
		sql.append(" and a.yearPn = '" + year + "' ");
		sql.append(" group by a.yearPn ");
		List ls = this.getHibernateTemplate().find(sql.toString());
		if (ls != null && ls.size() > 0) {
			String rs = (String) ls.get(0);
			month = rs.substring(0, rs.length());
		}

		String[] val = new String[1];
		val[0] = month;

		return val;
	}
	
	public String findMaxVolume(String ouCode) {

		String volume = "";
		
		
		StringBuffer sql = new StringBuffer(0);
		sql.append(" SELECT MAX(a.volumeSet) ");
		sql.append(" FROM   ApTable a ");
		sql.append(" WHERE  a.ouCode = '" + ouCode + "' ");
		sql.append(" and a.confirmFlag <> 'Y' ");
		sql.append(" group by a.volumeSet ");
		List ls = this.getHibernateTemplate().find(sql.toString());
		if (ls != null && ls.size() > 0) {
			String rs = (String) ls.get(0);
			volume = rs.substring(0, rs.length());
		}		
		return volume;
	}
	
	public String findMinVolume(String ouCode) {

		String volume = "";	
		
		StringBuffer sql = new StringBuffer(0);
		sql.append(" SELECT MIN(a.volumeSet) ");
		sql.append(" FROM   ApTable a ");
		sql.append(" WHERE  a.ouCode = '" + ouCode + "' ");
		sql.append(" and a.confirmFlag <> 'Y' ");
		sql.append(" group by a.volumeSet ");
		List ls = this.getHibernateTemplate().find(sql.toString());
		if (ls != null && ls.size() > 0) {
			String rs = (String) ls.get(0);
			volume = rs.substring(0, rs.length());
		}		
		return volume;
	}
	
	public void deleteApTableInputError(String ouCode, String year, String month,String volumeSet,
			String userId) throws Exception {

		try {
			
			StringBuffer sql2 = new StringBuffer(0);
			sql2.append(" delete ap_table rw ");
			sql2.append(" where rw.ou_code = '" + ouCode + "' ");
			sql2.append(" and rw.year_pn = " + year);
			sql2.append(" and rw.month_pn = " + month);
			sql2.append(" and rw.code_seq is null ");
			sql2.append(" and rw.emp_code in ( ");
			sql2.append("	select v.emp_code ");
			sql2.append("	from v_pn_employee_security v ");
			sql2.append("	where v.user_id = '" + userId + "' ");
			sql2.append("	and v.code_seq = rw.code_seq ");
			sql2.append("	and ou_code = rw.ou_code ");
			sql2.append(" ) ");

			System.out.println("sql2 : " + sql2.toString());

			this.jdbcTemplate.execute(sql2.toString());
			
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	public Integer confirmData(String ouCode, Long yearPn, Long monthPn,String volumeSetFrom,String volumeSetTo, 
			String userId) {
		try {
			Integer countData = new Integer(0);
            int volumeFrom = Integer.parseInt(volumeSetFrom);
            int volumeTo = Integer.parseInt(volumeSetTo);
			StringBuffer hql1 = new StringBuffer();
			hql1.append(" select count(*) ");
			hql1.append(" from ApTable pr ");
			hql1.append(" where pr.ouCode = '");
			hql1.append(ouCode);
			hql1.append("' ");
			hql1.append(" and pr.yearPn = ");
			hql1.append(yearPn );
			hql1.append(" and pr.monthPn = ");
			hql1.append(monthPn );
			hql1.append(" and pr.volumeSet >=  ");
			hql1.append(volumeFrom);
			hql1.append(" and pr.volumeSet <=  ");
			hql1.append(volumeTo);
			//hql1.append("	and pr.creBy = '" + userId + "' ");
			hql1.append(" and  pr.confirmFlag ='N'  ");
			//hql1.append(" and  pr.empCode in ( ");
			//hql1.append("	select pk.empCode ");
			//hql1.append("	from VPnEmployeeSecurityWork  ");
			//hql1.append("	where pk.ouCode = '" + ouCode + "' ");
			//hql1.append("	and pk.userId = '" + userId + "' ");
			//hql1.append("	and pk.codeSeqAct = pr.codeSeq ");
			//hql1.append(" 	) ");

			Object confirmDataObj = this.getSession().createQuery(hql1.toString()).uniqueResult();

			if (confirmDataObj != null) {
				countData = (Integer) confirmDataObj;
			}

			return countData;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public Double sumAllMoneyData(String ouCode, Long yearPn, Long monthPn,String volumeSetFrom,String volumeSetTo, 
			String userId) {
		try {
			Double allMoneyData = new Double(0.00);
			int volumeFrom = Integer.parseInt(volumeSetFrom);
			int volumeTo = Integer.parseInt(volumeSetTo);
			

			StringBuffer hql1 = new StringBuffer();
			hql1.append(" select sum(pr.totAmt) ");
			hql1.append(" from ApTable pr ");
			hql1.append(" where pr.ouCode = '");
			hql1.append(ouCode);
			hql1.append("' ");
			hql1.append(" and pr.yearPn = ");
			hql1.append(yearPn );
			hql1.append(" and pr.monthPn = ");
			hql1.append(monthPn );
			hql1.append(" and pr.volumeSet >=  ");
			hql1.append(volumeFrom);
			hql1.append(" and pr.volumeSet <=  ");
			hql1.append(volumeTo);
			//hql1.append("	and pr.creBy = '" + userId + "' ");
			hql1.append(" and ( pr.confirmFlag ='N' ) ");
			//hql1.append(" and  pr.empCode in ( ");
			//hql1.append("	select pk.empCode ");
			//hql1.append("	from VPnEmployeeSecurityWork ");
			//hql1.append("	where pk.ouCode = '" + ouCode + "' ");
			//hql1.append("	and pk.userId = '" + userId + "' ");
			//hql1.append("	and pk.codeSeqAct = pr.codeSeq ");
			//hql1.append(" 	) ");

			Object confirmDataObj = this.getSession().createQuery(hql1.toString()).uniqueResult();

			if (confirmDataObj != null) {
				allMoneyData = (Double) confirmDataObj;
			}
			System.out.println("allMoney : " + allMoneyData);
			return allMoneyData;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public Double sumMoneyData(String ouCode, Long yearPn, Long monthPn,String volumeSetFrom,String volumeSetTo, 
			String userId,String flag) {
		try {
			Double moneyData = new Double(0.00);
			int volumeFrom = Integer.parseInt(volumeSetFrom);
			int volumeTo = Integer.parseInt(volumeSetTo);
			

			StringBuffer hql1 = new StringBuffer();
			hql1.append(" select sum(pr.totAmt) ");
			hql1.append(" from ApTable pr ");
			hql1.append(" where pr.ouCode = '");
			hql1.append(ouCode);
			hql1.append("' ");
			hql1.append(" and pr.yearPn = ");
			hql1.append(yearPn );
			hql1.append(" and pr.monthPn = ");
			hql1.append(monthPn );
			hql1.append(" and pr.volumeSet >=   ");
			hql1.append(volumeFrom);
			hql1.append(" and pr.volumeSet <=   ");
			hql1.append(volumeTo);
			hql1.append(" and pr.newGworkCode =  '" + flag + "' ");
			//hql1.append("	and pr.creBy = '" + userId + "' ");
			hql1.append(" and ( pr.confirmFlag ='N' ) ");
			//hql1.append(" and  pr.empCode in ( ");
			//hql1.append("	select pk.empCode ");
			//hql1.append("	from VPnEmployeeSecurityWork ");
			//hql1.append("	where pk.ouCode = '" + ouCode + "' ");
			//hql1.append("	and pk.userId = '" + userId + "' ");
			//hql1.append("	and pk.codeSeqAct = pr.codeSeq ");
			//hql1.append(" 	) ");

			Object confirmDataObj = this.getSession().createQuery(hql1.toString()).uniqueResult();

			if (confirmDataObj != null) {
				moneyData = (Double) confirmDataObj;
			}

			return moneyData;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public void updateConfirmData(String ouCode, Long yearPn, Long monthPn,String volumeSetFrom,String volumeSetTo,
			String userId) throws Exception {
		StringBuffer hql = new StringBuffer();

		try {
			hql.append(" UPDATE AP_TABLE pr ");
			hql.append(" SET pr.confirm_Flag = 'Y', ");
			hql.append(" pr.upd_date = sysdate, ");
			hql.append(" pr.upd_by = '" + userId + "' ");
			hql.append(" where pr.ou_Code = '");
			hql.append(ouCode);
			hql.append("' ");
			hql.append(" and pr.year_pn = ");
			hql.append(yearPn);
			hql.append(" and pr.month_pn = ");
			hql.append(monthPn);
			hql.append(" and pr.volume_set >= TO_NUMBER('" + volumeSetFrom + "') ");
			hql.append(" and pr.volume_set <= TO_NUMBER('" + volumeSetTo + "') ");
			hql.append(" and  pr.confirm_flag = 'N'  ");
			//hql.append(" and pr.cre_by = '" + userId + "' ");
			//hql.append(" and pr.code_seq in ( ");
			//hql.append(" 	select code_seq_act ");
			//hql.append("	from v_pn_organization_security_work ");
			//hql.append("	where ou_code = '" + ouCode + "' ");
			//hql.append("	and user_id = '" + userId + "' ");
			//hql.append(" ) ");

			 //System.out.println(" 000000000000000 Hql updateConfirmData : "+hql.toString());

			this.jdbcTemplate.execute(hql.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    
	
	public Integer confirmDataApprove(String ouCode, Long yearPn, Long monthPn,String volumeSetFrom,String volumeSetTo, 
			String userId) {
		try {
			Integer countData = new Integer(0);
			 int volumeFrom = Integer.parseInt(volumeSetFrom);
	         int volumeTo = Integer.parseInt(volumeSetTo);

			StringBuffer hql1 = new StringBuffer();
			hql1.append(" select count(*) ");
			hql1.append(" from ApTable pr ");
			hql1.append(" where pr.ouCode = '");
			hql1.append(ouCode);
			hql1.append("' ");
			hql1.append(" and pr.yearPn = ");
			hql1.append(yearPn );
			hql1.append(" and pr.monthPn = ");
			hql1.append(monthPn );
			hql1.append(" and pr.volumeSet >=  ");
			hql1.append(volumeFrom);
			hql1.append(" and pr.volumeSet <=  ");
			hql1.append(volumeTo);
			//hql1.append("	and pr.creBy = '" + userId + "' ");
			hql1.append(" and  pr.confirmFlag = 'Y'  ");
			hql1.append(" and  pr.approveClose ='N'  ");
			//hql1.append(" and  pr.empCode in ( ");
			//hql1.append("	select pk.empCode ");
			//hql1.append("	from VPnEmployeeSecurityWork ");
			//hql1.append("	where pk.ouCode = '" + ouCode + "' ");
			//hql1.append("	and pk.userId = '" + userId + "' ");
			//hql1.append("	and pk.codeSeqAct = pr.codeSeq ");
			//hql1.append(" 	) ");

			Object confirmDataObj = this.getSession().createQuery(hql1.toString()).uniqueResult();

			if (confirmDataObj != null) {
				countData = (Integer) confirmDataObj;
			}

			return countData;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public Double sumAllMoneyDataApprove(String ouCode, Long yearPn, Long monthPn,String volumeSetFrom,String volumeSetTo, 
			String userId) {
		try {
			Double allMoneyData = new Double(0.00);
			int volumeFrom = Integer.parseInt(volumeSetFrom);
			int volumeTo = Integer.parseInt(volumeSetTo);
			

			StringBuffer hql1 = new StringBuffer();
			hql1.append(" select sum(pr.totAmt) ");
			hql1.append(" from ApTable pr ");
			hql1.append(" where pr.ouCode = '");
			hql1.append(ouCode);
			hql1.append("' ");
			hql1.append(" and pr.yearPn = ");
			hql1.append(yearPn );
			hql1.append(" and pr.monthPn = ");
			hql1.append(monthPn );
			hql1.append(" and pr.volumeSet >=   ");
			hql1.append(volumeFrom);
			hql1.append(" and pr.volumeSet <= ");
			hql1.append(volumeTo);
			//hql1.append("	and pr.creBy = '" + userId + "' ");
			hql1.append(" and  pr.confirmFlag = 'Y'  ");
			hql1.append(" and  pr.approveFlag = 'Y'  ");
			//hql1.append(" and  pr.empCode in ( ");
			//hql1.append("	select pk.empCode ");
			//hql1.append("	from VPnEmployeeSecurityWork ");
			//hql1.append("	where pk.ouCode = '" + ouCode + "' ");
			//hql1.append("	and pk.userId = '" + userId + "' ");
			//hql1.append("	and pk.codeSeqAct = pr.codeSeq ");
			//hql1.append(" 	) ");

			Object confirmDataObj = this.getSession().createQuery(hql1.toString()).uniqueResult();

			if (confirmDataObj != null) {
				allMoneyData = (Double) confirmDataObj;
			}
			System.out.println("allMoney : " + allMoneyData);
			return allMoneyData;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public Double sumMoneyDataApprove(String ouCode, Long yearPn, Long monthPn,String volumeSetFrom,String volumeSetTo, 
			String userId,String flag) {
		try {
			Double moneyData = new Double(0.00);
			int volumeFrom = Integer.parseInt(volumeSetFrom);
			int volumeTo = Integer.parseInt(volumeSetTo);
			

			StringBuffer hql1 = new StringBuffer();
			hql1.append(" select sum(pr.totAmt) ");
			hql1.append(" from ApTable pr ");
			hql1.append(" where pr.ouCode = '");
			hql1.append(ouCode);
			hql1.append("' ");
			hql1.append(" and pr.yearPn = ");
			hql1.append(yearPn );
			hql1.append(" and pr.monthPn = ");
			hql1.append(monthPn );
			hql1.append(" and pr.volumeSet >=  ");
			hql1.append(volumeFrom );
			hql1.append(" and pr.volumeSet <=  ");
			hql1.append(volumeTo);
			hql1.append(" and pr.newGworkCode =  '" + flag + "' ");
			//hql1.append("	and pr.creBy = '" + userId + "' ");
			hql1.append(" and  pr.confirmFlag = 'Y'  ");
			hql1.append(" and  pr.approveFlag = 'Y'  ");
		//	hql1.append(" and  pr.empCode in ( ");
		//	hql1.append("	select pk.empCode ");
		//	hql1.append("	from VPnEmployeeSecurityWork ");
		//	hql1.append("	where pk.ouCode = '" + ouCode + "' ");
		//	hql1.append("	and pk.userId = '" + userId + "' ");
		//	hql1.append("	and pk.codeSeqAct = pr.codeSeq ");
		//	hql1.append(" 	) ");

			Object confirmDataObj = this.getSession().createQuery(hql1.toString()).uniqueResult();

			if (confirmDataObj != null) {
				moneyData = (Double) confirmDataObj;
			}

			return moneyData;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public void updateConfirmDataApprove(String ouCode, Long yearPn, Long monthPn,String volumeSetFrom,String volumeSetTo,
			String userId) throws Exception {
		StringBuffer hql = new StringBuffer();

		try {
			hql.append(" UPDATE AP_TABLE pr ");
			hql.append(" SET pr.approve_closed = 'Y', ");
			hql.append(" pr.upd_date = sysdate, ");
			hql.append(" pr.upd_by = '" + userId + "' ");
			hql.append(" where pr.ou_Code = '");
			hql.append(ouCode);
			hql.append("' ");
			hql.append(" and pr.year_pn = ");
			hql.append(yearPn);
			hql.append(" and pr.month_pn = ");
			hql.append(monthPn);
			hql.append(" and pr.volume_set >= TO_NUMBER('" + volumeSetFrom + "') ");
			hql.append(" and pr.volume_set <= TO_NUMBER('" + volumeSetTo + "') ");
			hql.append(" and  pr.confirm_flag = 'Y'  ");
			hql.append(" and  pr.approve_closed = 'N'  ");
			//hql.append(" and pr.cre_by = '" + userId + "' ");
			//hql.append(" and ( ");
			// hql.append(" pr.emp_code in ( ");
			// hql.append(" select emp_code ");
			// hql.append(" from v_pr_employee_security ");
			// hql.append(" where ou_code = '"+ouCode+"' ");
			// hql.append(" and year = " + yearPr);
			// hql.append(" and period = " + periodPr);
			// hql.append(" and user_id = '"+userId+"' ");
			// hql.append(" ) or ");
			//hql.append(" pr.code_seq in ( ");
			//hql.append(" 	select code_seq_act ");
			//hql.append("	from v_pn_organization_security_work ");
			//hql.append("	where ou_code = '" + ouCode + "' ");
			//hql.append("	and user_id = '" + userId + "' ");
			//hql.append(" ) ");
			//hql.append(" ) ");

			 //System.out.println(" 000000000000000 Hql updateConfirmData : "+hql.toString());

			this.jdbcTemplate.execute(hql.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void updateConfirmDataBankKTB(String ouCode, Long yearPn, Long monthPn,String volumeSetFrom,String volumeSetTo,
			String userId) throws Exception {
		StringBuffer hql = new StringBuffer();
		StringBuffer hql1 = new StringBuffer();
		
	

		try {
			hql.append(" UPDATE AP_TABLE pr ");
			hql.append(" SET pr.bank_flag = 'Y', ");
			hql.append(" pr.bank_date = sysdate, ");
			hql.append(" pr.bank_by = '" + userId + "' ");
			hql.append(" where pr.ou_Code = '");
			hql.append(ouCode);
			hql.append("' ");
			hql.append(" and pr.year_pn = ");
			hql.append(yearPn);
			hql.append(" and pr.month_pn = ");
			hql.append(monthPn);
			hql.append(" AND Pr_Desc.GET_BANK_CODE_FROM_PN('001',Pr.EMP_CODE) ='001' ");
			hql.append(" and pr.volume_set >= TO_NUMBER('" + volumeSetFrom + "') ");
			hql.append(" and pr.volume_set <= TO_NUMBER('" + volumeSetTo + "') ");
			hql.append(" and pr.emp_code IN (SELECT PN.EMP_CODE FROM PN_EMPLOYEE PN WHERE PN.EMP_CODE = pr.emp_code AND PN.OU_CODE = pr.ou_code AND PN.EMP_STATUS ='B')");
			hql.append(" and pr.confirm_flag = 'Y' ");
			hql.append(" and pr.approve_flag = 'Y' ");			
			hql.append(" and pr.approve_closed = 'Y' ");
			hql.append(" and pr.bank_flag = 'N' ");
			hql.append(" and pr.bank_closed = 'N' ");			
			//hql.append(" and pr.cre_by = '" + userId + "' ");
			//hql.append(" and pr.code_seq in ( ");
			//hql.append(" 	select code_seq_act ");
			//hql.append("	from v_pn_organization_security_work ");
			//hql.append("	where ou_code = '" + ouCode + "' ");
			//hql.append("	and user_id = '" + userId + "' ) ");
		

			 //System.out.println(" 000000000000000 Hql updateConfirmData : "+hql.toString());

			this.jdbcTemplate.execute(hql.toString());
			
			hql1.append(" UPDATE AP_TABLE pr ");
			hql1.append(" SET pr.bank_closed = 'Y', ");
			hql1.append(" pr.bank_date = sysdate, ");
			hql1.append(" pr.bank_by = '" + userId + "' ");
			hql1.append(" where pr.ou_Code = '");
			hql1.append(ouCode);
			hql1.append("' ");
			hql1.append(" and pr.year_pn = ");
			hql1.append(yearPn);
			hql1.append(" and pr.month_pn = ");
			hql1.append(monthPn);
			hql1.append(" AND Pr_Desc.GET_BANK_CODE_FROM_PN('001',Pr.EMP_CODE) ='001' ");
			hql1.append(" and pr.volume_set >= TO_NUMBER('" + volumeSetFrom + "') ");
			hql1.append(" and pr.volume_set <= TO_NUMBER('" + volumeSetTo + "') ");
			hql1.append(" and pr.confirm_flag = 'Y' ");
			hql1.append(" and pr.approve_closed = 'Y' ");
			hql1.append(" and pr.bank_closed = 'N' ");			
			//hql.append(" and pr.cre_by = '" + userId + "' ");
			//hql.append(" and pr.code_seq in ( ");
			//hql.append(" 	select code_seq_act ");
			//hql.append("	from v_pn_organization_security_work ");
			//hql.append("	where ou_code = '" + ouCode + "' ");
			//hql.append("	and user_id = '" + userId + "' ) ");
		

			 //System.out.println(" 000000000000000 Hql updateConfirmData : "+hql.toString());

			this.jdbcTemplate.execute(hql1.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void updateConfirmDataBankSCB(String ouCode, Long yearPn, Long monthPn,String volumeSetFrom,String volumeSetTo,
			String userId) throws Exception {
		StringBuffer hql = new StringBuffer();
		StringBuffer hql1 = new StringBuffer();
		
		try {
			hql.append(" UPDATE AP_TABLE pr ");
			hql.append(" SET pr.bank_flag = 'Y', ");
			hql.append(" pr.bank_date = sysdate, ");
			hql.append(" pr.bank_by = '" + userId + "' ");
			hql.append(" where pr.ou_Code = '");
			hql.append(ouCode);
			hql.append("' ");
			hql.append(" and pr.year_pn = ");
			hql.append(yearPn);
			hql.append(" and pr.month_pn = ");
			hql.append(monthPn);
			hql.append(" AND Pr_Desc.GET_BANK_CODE_FROM_PN('001',Pr.EMP_CODE) ='002' ");
			hql.append(" and pr.volume_set >= TO_NUMBER('" + volumeSetFrom + "') ");
			hql.append(" and pr.volume_set <= TO_NUMBER('" + volumeSetTo + "') ");
			hql.append(" and pr.emp_code IN (SELECT PN.EMP_CODE FROM PN_EMPLOYEE PN WHERE PN.EMP_CODE = pr.emp_code AND PN.OU_CODE = pr.ou_code AND PN.EMP_STATUS ='B')");
			hql.append(" and pr.confirm_flag = 'Y' ");
			hql.append(" and pr.approve_flag = 'Y' ");		
			hql.append(" and pr.bank_flag = 'N' ");
			hql.append(" and pr.approve_closed = 'Y' ");
			hql.append(" and pr.bank_closed = 'N' ");			
			//hql.append(" and pr.cre_by = '" + userId + "' ");
			// hql.append(" pr.emp_code in ( ");
			// hql.append(" select emp_code ");
			// hql.append(" from v_pr_employee_security ");
			// hql.append(" where ou_code = '"+ouCode+"' ");
			// hql.append(" and year = " + yearPr);
			// hql.append(" and period = " + periodPr);
			// hql.append(" and user_id = '"+userId+"' ");
			// hql.append(" ) or ");
			//hql.append(" and ( ");
			//hql.append(" pr.code_seq in ( ");
			//hql.append(" 	select code_seq_act ");
			//hql.append("	from v_pn_organization_security_work ");
			//hql.append("	where ou_code = '" + ouCode + "' ");
			//hql.append("	and user_id = '" + userId + "' ");
			//hql.append(" ) ");
			//hql.append(" ) ");

			 //System.out.println(" 000000000000000 Hql updateConfirmData : "+hql.toString());

			this.jdbcTemplate.execute(hql.toString());
			
			hql1.append(" UPDATE AP_TABLE pr ");
			hql1.append(" SET pr.bank_closed = 'Y', ");
			hql1.append(" pr.bank_date = sysdate, ");
			hql1.append(" pr.bank_by = '" + userId + "' ");
			hql1.append(" where pr.ou_Code = '");
			hql1.append(ouCode);
			hql1.append("' ");
			hql1.append(" and pr.year_pn = ");
			hql1.append(yearPn);
			hql1.append(" and pr.month_pn = ");
			hql1.append(monthPn);
			hql1.append(" AND Pr_Desc.GET_BANK_CODE_FROM_PN('001',Pr.EMP_CODE) ='002' ");
			hql1.append(" and pr.volume_set >= TO_NUMBER('" + volumeSetFrom + "') ");
			hql1.append(" and pr.volume_set <= TO_NUMBER('" + volumeSetTo + "') ");
			hql1.append(" and pr.confirm_flag = 'Y' ");
			hql1.append(" and pr.approve_closed = 'Y' ");
			hql1.append(" and pr.bank_closed = 'N' ");			
			//hql.append(" and pr.cre_by = '" + userId + "' ");
			// hql.append(" pr.emp_code in ( ");
			// hql.append(" select emp_code ");
			// hql.append(" from v_pr_employee_security ");
			// hql.append(" where ou_code = '"+ouCode+"' ");
			// hql.append(" and year = " + yearPr);
			// hql.append(" and period = " + periodPr);
			// hql.append(" and user_id = '"+userId+"' ");
			// hql.append(" ) or ");
			//hql.append(" and ( ");
			//hql.append(" pr.code_seq in ( ");
			//hql.append(" 	select code_seq_act ");
			//hql.append("	from v_pn_organization_security_work ");
			//hql.append("	where ou_code = '" + ouCode + "' ");
			//hql.append("	and user_id = '" + userId + "' ");
			//hql.append(" ) ");
			//hql.append(" ) ");

			 //System.out.println(" 000000000000000 Hql updateConfirmData : "+hql.toString());

			this.jdbcTemplate.execute(hql1.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void updateConfirmDataTransfer(String ouCode, Long yearPn, Long monthPn,String volumeSetFrom,String volumeSetTo,
			String userId) throws Exception {
		StringBuffer hql = new StringBuffer();
		StringBuffer hql1 = new StringBuffer();

	

		try {hql.append(" UPDATE AP_TABLE pr ");
			hql.append(" SET pr.transfer_flag = 'Y', ");
			hql.append(" pr.transfer_date = sysdate, ");
			hql.append(" pr.transfer_by = '" + userId + "' ");
			hql.append(" where pr.ou_Code = '");
			hql.append(ouCode);
			hql.append("' ");
			hql.append(" and pr.year_pn = ");
			hql.append(yearPn);
			hql.append(" and pr.month_pn = ");
			hql.append(monthPn);
			hql.append(" and pr.volume_set >= TO_NUMBER('" + volumeSetFrom + "') ");
			hql.append(" and pr.volume_set <= TO_NUMBER('" + volumeSetTo + "') ");
			hql.append(" and  pr.confirm_Flag = 'Y'  ");
			hql.append(" and  pr.approve_flag = 'Y' ");		
			hql.append(" and  pr.transfer_Flag = 'N'  ");
			hql.append(" and  pr.approve_closed = 'Y'  ");
			hql.append(" and  pr.bank_closed = 'Y'  ");			
			hql.append(" and  pr.transfer_closed = 'N'  ");			
			

			 //System.out.println(" 000000000000000 Hql updateConfirmData : "+hql.toString());

			this.jdbcTemplate.execute(hql.toString());
			
			hql1.append(" UPDATE AP_TABLE pr ");
			hql1.append(" SET pr.transfer_closed = 'Y', ");
			hql1.append(" pr.transfer_date = sysdate, ");
			hql1.append(" pr.transfer_by = '" + userId + "' ");
			hql1.append(" where pr.ou_Code = '");
			hql1.append(ouCode);
			hql1.append("' ");
			hql1.append(" and pr.year_pn = ");
			hql1.append(yearPn);
			hql1.append(" and pr.month_pn = ");
			hql1.append(monthPn);
			hql1.append(" and pr.volume_set >= TO_NUMBER('" + volumeSetFrom + "') ");
			hql1.append(" and pr.volume_set <= TO_NUMBER('" + volumeSetTo + "') ");
			hql1.append(" and  pr.confirm_Flag = 'Y'  ");
			hql1.append(" and  pr.approve_closed = 'Y'  ");
			hql1.append(" and  pr.bank_closed = 'Y'  ");			
			hql1.append(" and  pr.transfer_closed = 'N'  ");			
			

			 //System.out.println(" 000000000000000 Hql updateConfirmData : "+hql.toString());

			this.jdbcTemplate.execute(hql1.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public List apEmpListReport(String userId, String evaOuCode,
			String evaYear, String evaMonth,String evaVolume) {
		StringBuffer criteria = new StringBuffer();

		if (evaYear != null && !evaYear.equals("")) {
			criteria.append(" and rwInc.yearPn = ");
			criteria.append(evaYear);
		}

		if (evaMonth != null && !evaMonth.equals("")) {
			criteria.append(" and rwInc.monthPn = ");
			criteria.append(evaMonth);
		}

		if (evaVolume != null && !evaVolume.equals("")) {
			criteria.append(" and rwInc.volumeSet = ");
			criteria.append(evaVolume);
		}

		StringBuffer hql = new StringBuffer();

		hql.append(" select rwInc.keySeq, ");
		hql.append(" rwInc.empCode, ");
		hql.append(" pnEmp.refDbPreSuff.prefixName, ");
		hql.append(" pnEmp.firstName, ");
		hql.append(" pnEmp.lastName, ");
		//hql.append(" nvl(pa.adminDesc,' '), ");
		hql.append(" nvl(pnEmp.levelCode,0), ");
		hql.append(" pnPos.positionShort, ");
		hql.append(" pnOrg.pk.codeSeq, ");
		hql.append(" pnOrg.orgCode, ");
		hql.append(" pnOrg.divShort || ' ' || nvl(pnOrg.secDesc,pnOrg.areaDesc) || ' ' ||pnOrg.workDesc, ");
		hql.append(" rwInc.seqData, ");
		hql.append(" rwInc.totAmt, ");
		hql.append(" rwInc.newGworkCode ");
		hql.append(" from ApTable rwInc , PnEmployee pnEmp ,PnAdmin pa,PnPosition pnPos, PnOrganization pnOrg,VPnOrganizationSecurity v ");
		hql.append(" where rwInc.ouCode = '");
		hql.append(evaOuCode);
		hql.append("' ");
		hql.append(criteria);
		hql.append(" and v.pk.userId = '" + userId + "' ");
		//hql.append(" and rwInc.creBy = '" + userId + "' ");
		hql.append(" and pnEmp.pk.ouCode = v.pk.ouCode ");
		hql.append(" and pnEmp.codeSeqAct = v.pk.codeSeq ");
		//hql.append(" and rwInc.ouCode = v.pk.ouCode ");
		//hql.append(" and rwInc.codeSeq = v.pk.codeSeq ");
		hql.append(" and rwInc.ouCode = pnEmp.pk.ouCode ");
		hql.append(" and rwInc.empCode = pnEmp.pk.empCode ");
		hql.append(" and rwInc.ouCode = pnOrg.pk.ouCode ");
		hql.append(" and rwInc.codeSeq = pnOrg.pk.codeSeq ");
		//hql.append(" and rwInc.ouCode = v.pk.ouCode ");
		//hql.append(" and rwInc.codeSeq = v.pk.codeSeq ");
		hql.append(" and pnEmp.pk.ouCode = pa.pk.ouCode ");
		hql.append(" and NVL(pnEmp.adminCode,'N') = pa.pk.adminCode ");
		hql.append(" and pnEmp.positionCode = pnPos.pk.positionCode ");
		hql.append(" and pnEmp.gworkCode = pnPos.pk.gworkCode ");		
		//hql.append(" and rwInc.newCodeSeq = pnOrga.pk.codeSeq ");
		hql.append(" and pnOrg.inactive = 'N' ");
		//hql.append(" and pnOrga.inactive = 'N' ");
		hql.append(" order by rwInc.seqData ");
		System.out.println("HQL findByCriteriaList ==> " + hql.toString());

		List empList = this.getSession().createQuery(hql.toString()).list();
		List retList = new ArrayList();
      

		/*
		 * List empList = this.getSession().createQuery(hql.toString()).list();
		 * // List retList = new ArrayList();
		 */

		for (Iterator it = empList.iterator(); it.hasNext();) {
			Object[] r = (Object[]) it.next();

			Long keySeq = (Long) r[0];
			String empCode = (String) r[1];
			String prefixName = (String) r[2];
			String firstName = (String) r[3];
			String lastName = (String) r[4];
			//String oldDuty = (String) r[5];
			String oldLevel = (String) r[5];
			String oldPosition = (String) r[6];
			Long codeSeq = (Long) r[7];
			String orgCode = (String) r[8];
			String orgDesc = (String) r[9];
			Double seqData = (Double) r[10];
			Double totAmt = (Double) r[11];
            String newGworkCode = (String) r[12];
			int intLevelCode = Integer.parseInt(oldLevel.trim());

		
		
			ApEmployeeVO ret = new ApEmployeeVO();
			ret.setKeySeq(keySeq);
			ret.setEmpCode(empCode);
			ret.setName(prefixName + " " + firstName + " " + lastName);
			//ret.setOldDuty(oldDuty);
			ret.setOldPositionShort(oldPosition + " " + intLevelCode);
			ret.setCodeSeq(codeSeq);
			//ret.setOrgCode(orgCode);
			ret.setOrgDesc(orgDesc);
			//ret.setNewPositionCode(newPositionCode);
			//ret.setNewLevelCode(newLevelCode);
			//ret.setNewOldDuty(newOldDuty);
			//ret.setNewDuty(newDuty);
			//ret.setNewOrgCode(newOrgCode);
			//ret.setNewOrgDesc(newOrgDesc);
			ret.setSeqData(seqData);
			ret.setTotAmt(totAmt);
			ret.setNewGworkCode(newGworkCode);
			//ret.setNewCodeSeq(newCodeSeq);

			retList.add(ret);
		}

		return retList;
	}
	public List apEmpListApproveReport(String userId, String evaOuCode,
			String evaYear, String evaMonth,String evaVolume,String evaApprove) {
		StringBuffer criteria = new StringBuffer();

		if (evaYear != null && !evaYear.equals("")) {
			criteria.append(" and rwInc.yearPn = ");
			criteria.append(evaYear);
		}

		if (evaMonth != null && !evaMonth.equals("")) {
			criteria.append(" and rwInc.monthPn = ");
			criteria.append(evaMonth);
		}

		if (evaVolume != null && !evaVolume.equals("")) {
			criteria.append(" and rwInc.volumeSet = ");
			criteria.append(evaVolume);
		}
		
		
		//if (evaApprove != null && !evaApprove.equals("")) {
		//	criteria.append(" and rwInc.approveFlag = ");
		//	criteria.append(evaApprove);
		//}


		StringBuffer hql = new StringBuffer();

		hql.append(" select rwInc.keySeq, ");
		hql.append(" rwInc.empCode, ");
		hql.append(" pnEmp.refDbPreSuff.prefixName, ");
		hql.append(" pnEmp.firstName, ");
		hql.append(" pnEmp.lastName, ");
		hql.append(" nvl(pnEmp.levelCode,0), ");
		hql.append(" pnPos.positionShort, ");
		hql.append(" pnOrg.pk.codeSeq, ");
		hql.append(" pnOrg.orgCode, ");
		hql.append(" pnOrg.divShort || ' ' || nvl(pnOrg.secDesc,pnOrg.areaDesc) || ' ' ||pnOrg.workDesc, ");
		hql.append(" rwInc.seqData, ");
		hql.append(" rwInc.totAmt, ");
		hql.append(" rwInc.newGworkCode, ");
		hql.append(" rwInc.approveFlag ");
		hql.append(" from ApTable rwInc , PnEmployee pnEmp ,PnAdmin pa,PnPosition pnPos, PnOrganization pnOrg,VPnOrganizationSecurity v ");
		hql.append(" where rwInc.ouCode = '");
		hql.append(evaOuCode);
		hql.append("' ");
		hql.append(criteria);
		hql.append(" and v.pk.userId = '" + userId + "' ");
		//hql.append(" and rwInc.creBy = '" + userId + "' ");
		hql.append(" and pnEmp.pk.ouCode = v.pk.ouCode ");
		hql.append(" and pnEmp.codeSeqAct = v.pk.codeSeq ");
		//hql.append(" and rwInc.ouCode = v.pk.ouCode ");
		//hql.append(" and rwInc.codeSeq = v.pk.codeSeq ");
		hql.append(" and rwInc.ouCode = pnEmp.pk.ouCode ");
		hql.append(" and rwInc.empCode = pnEmp.pk.empCode ");
		hql.append(" and rwInc.ouCode = pnOrg.pk.ouCode ");
		hql.append(" and rwInc.codeSeq = pnOrg.pk.codeSeq ");
		//hql.append(" and rwInc.ouCode = v.pk.ouCode ");
		//hql.append(" and rwInc.codeSeq = v.pk.codeSeq ");		
		//hql.append(" and rwInc.ouCode = v.pk.ouCode ");
		//hql.append(" and rwInc.codeSeq = v.pk.codeSeq ");
		//hql.append(" and rwInc.ouCode = pnEmp.pk.ouCode ");
		//hql.append(" and rwInc.empCode = pnEmp.pk.empCode ");
		//hql.append(" and pnEmp.pk.ouCode = pnOrg.pk.ouCode ");
		//hql.append(" and pnEmp.codeSeqAct = pnOrg.pk.codeSeq ");
		hql.append(" and pnEmp.pk.ouCode = pa.pk.ouCode ");
		hql.append(" and NVL(pnEmp.adminCode,'N') = pa.pk.adminCode ");
		hql.append(" and pnEmp.positionCode = pnPos.pk.positionCode ");
		hql.append(" and pnEmp.gworkCode = pnPos.pk.gworkCode ");		
		//hql.append(" and rwInc.newCodeSeq = pnOrga.pk.codeSeq ");
		hql.append(" and pnOrg.inactive = 'N' ");
		hql.append(" and rwInc.confirmFlag = 'Y' ");
		hql.append(" and rwInc.approveFlag = '" + evaApprove + "' ");
		//hql.append(" and pnOrga.inactive = 'N' ");
		hql.append(" order by rwInc.seqData ");
		System.out.println("HQL findByCriteriaList ==> " + hql.toString());

		List empList = this.getSession().createQuery(hql.toString()).list();
		List retList = new ArrayList();
      

		/*
		 * List empList = this.getSession().createQuery(hql.toString()).list();
		 * // List retList = new ArrayList();
		 */

		for (Iterator it = empList.iterator(); it.hasNext();) {
			Object[] r = (Object[]) it.next();

			Long keySeq = (Long) r[0];
			String empCode = (String) r[1];
			String prefixName = (String) r[2];
			String firstName = (String) r[3];
			String lastName = (String) r[4];
			//String oldDuty = (String) r[5];
			String oldLevel = (String) r[5];
			String oldPosition = (String) r[6];
			Long codeSeq = (Long) r[7];
			String orgCode = (String) r[8];
			String orgDesc = (String) r[9];
			Double seqData = (Double) r[10];
			Double totAmt = (Double) r[11];
            String newGworkCode = (String) r[12];
            String approveFlag = (String) r[13];
			int intLevelCode = Integer.parseInt(oldLevel.trim());

		
		
			ApEmployeeVO ret = new ApEmployeeVO();
			ret.setKeySeq(keySeq);
			ret.setEmpCode(empCode);
			ret.setName(prefixName + " " + firstName + " " + lastName);
			//ret.setOldDuty(oldDuty);
			ret.setOldPositionShort(oldPosition + " " + intLevelCode);
			ret.setCodeSeq(codeSeq);
			//ret.setOrgCode(orgCode);
			ret.setOrgDesc(orgDesc);
			//ret.setNewPositionCode(newPositionCode);
			//ret.setNewLevelCode(newLevelCode);
			//ret.setNewOldDuty(newOldDuty);
			//ret.setNewDuty(newDuty);
			//ret.setNewOrgCode(newOrgCode);
			//ret.setNewOrgDesc(newOrgDesc);
			ret.setSeqData(seqData);
			ret.setTotAmt(totAmt);
			ret.setNewGworkCode(newGworkCode);
			ret.setApproveFlag(approveFlag);
			//ret.setNewCodeSeq(newCodeSeq);

			retList.add(ret);
		}

		return retList;
	}
	public List apEmpKtbBankReport(String userId, String evaOuCode,
			Integer evaYear, Integer evaMonth,String volumeFrom,String volumeTo) {
		List resultList = new ArrayList();
		List returnList = new ArrayList();
		
	    int month=1;
		month=evaMonth.intValue()*2;
      
		String sql = "";
		sql = " SELECT  PN.EMP_CODE EMP_CODE,TRIM(PN.BANK_ID) BANK_ID ,DB.PREFIX_NAME||' '||PN.FIRST_NAME||' '||PN.LAST_NAME NAME,"
				+ " TO_CHAR( Pr_Desc.GET_NET_CODE18('001','"
				+ evaYear
				+ "','"
				+ evaMonth
				+ "','"
				+ volumeFrom
				+ "','"
				+ volumeTo
				+ "', PN.EMP_CODE,'001')) AMOUNT "
				+ "   FROM      PN_EMPLOYEE PN,DB_PRE_SUFF DB  "
				+ "    WHERE  PN.OU_CODE    ='001'"
				+ "    AND PN.EMP_STATUS ='B'   "
				+ "    AND PN.BANK_CODE ='001'   "
				+ " AND PN.EMP_CODE IN (SELECT DISTINCT(A.EMP_CODE) FROM AP_TABLE A" 
				+ "                         WHERE       A.YEAR_PN     = '"+ evaYear+ "'" 
				+ "                           AND    A.MONTH_PN     = '"+ evaMonth+ "'" 
				+ "                           AND    A.VOLUME_SET    between to_number('"+ volumeFrom+ "') and to_number('"+ volumeTo+ "') " 
				+ "                           AND A.CONFIRM_FLAG = 'Y'" 
				+ "                           AND A.APPROVE_FLAG = 'Y'" 
				+ "                           AND A.BANK_FLAG = 'Y')" 
				+ " AND PN.PRE_NAME = DB.PRE_SUFF_CODE   "
				+ " AND Pr_Desc.GET_NET_CODE18('001','"
				+ evaYear
				+ "','"
				+ evaMonth
				+ "','"
				+ volumeFrom
				+ "','"
				+ volumeTo
				+ "', PN.EMP_CODE,'001') > 0"
				+ "                              ORDER BY PN.EMP_CODE ";

		resultList = this.jdbcTemplate.queryForList(sql);
		for (int i = 0; i < resultList.size(); i++) {
			ListOrderedMap map = (ListOrderedMap) resultList.get(i);
			String empCode = (String) map.getValue(0);
			String bankId = (String) map.getValue(1);
			String empName = (String) map.getValue(2);
			String amount = (String) map.getValue(3);
			// Double amount =(Double)map.getValue(3);

			// String eduFirst = (String) map.getValue(6);
			// String manReplace = (String) map.getValue(7);
			// String moveSeq = (String) map.getValue(8);
			// String moveOrg = (String) map.getValue(9);
			// Date dateSubmit = (Date)map.getValue(10);
			// String moveReason = (String) map.getValue(11); 
			// String moveNote = (String) map.getValue(12);

			ApEmpBankVO vo = new ApEmpBankVO();

			vo.setEmpCode(empCode);
			vo.setBankId(bankId);
			vo.setEmpName(empName);
			vo.setAmount(amount);

			// vo.setEduFirst(eduFirst);
			// vo.setManReplace(manReplace);
			// vo.setMoveSeq(moveSeq);
			// vo.setMoveOrg(moveOrg);
			// vo.setDateSubmit(dateSubmit);
			// vo.setMoveReason(moveReason);
			// vo.setMoveNote(moveNote);

			returnList.add(vo);
			//System.out.println("EMP_CODE : ", empCode );

		}
		return returnList;
	}
	
	public List apEmpScbBankReport(String userId, String evaOuCode,
			Integer evaYear, Integer evaMonth,String volumeFrom,String volumeTo) {
		List resultList = new ArrayList();
		List returnList = new ArrayList();
		
	    int month=1;
		month=evaMonth.intValue()*2;
      
		String sql = "";
		sql = " SELECT  PN.EMP_CODE EMP_CODE,TRIM(PN.BANK_ID) BANK_ID,DB.PREFIX_NAME||' '||PN.FIRST_NAME||' '||PN.LAST_NAME NAME,"
				+ " TO_CHAR( Pr_Desc.GET_NET_CODE18('001','"
				+ evaYear
				+ "','"
				+ evaMonth
				+ "','"
				+ volumeFrom
				+ "','"
				+ volumeTo
				+ "', PN.EMP_CODE,'002')) AMOUNT "
				+ "   FROM      PN_EMPLOYEE PN,DB_PRE_SUFF DB  "
				+ "    WHERE  PN.OU_CODE    ='001'"
				+ "    AND PN.EMP_STATUS ='B'"
				+ "    AND PN.BANK_CODE ='002'"
				+ " AND PN.EMP_CODE IN (SELECT DISTINCT(A.EMP_CODE) FROM AP_TABLE A" 
				+ "                         WHERE       A.YEAR_PN     = '"+ evaYear+ "'" 
				+ "                           AND    A.MONTH_PN     = '"+ evaMonth+ "'" 
				+ "                           AND    A.VOLUME_SET    between to_number('"+ volumeFrom+ "') and to_number('"+ volumeTo+ "') " 
				+ "                           AND A.CONFIRM_FLAG = 'Y'" 
				+ "                           AND A.APPROVE_FLAG = 'Y'" 
				+ "                           AND A.BANK_FLAG = 'Y')" 
				+ " AND PN.PRE_NAME = DB.PRE_SUFF_CODE   "
				+ " AND Pr_Desc.GET_NET_CODE18('001','"
				+ evaYear
				+ "','"
				+ evaMonth
				+ "','"
				+ volumeFrom
				+ "','"
				+ volumeTo
				+ "', PN.EMP_CODE,'002') > 0"
				+ "                              ORDER BY PN.EMP_CODE ";
		

		resultList = this.jdbcTemplate.queryForList(sql);
		for (int i = 0; i < resultList.size(); i++) {
			ListOrderedMap map = (ListOrderedMap) resultList.get(i);
			String empCode = (String) map.getValue(0);
			String bankId = (String) map.getValue(1);
			String empName = (String) map.getValue(2);
			String amount = (String) map.getValue(3);
			// Double amount =(Double)map.getValue(3);

			// String eduFirst = (String) map.getValue(6);
			// String manReplace = (String) map.getValue(7);
			// String moveSeq = (String) map.getValue(8);
			// String moveOrg = (String) map.getValue(9);
			// Date dateSubmit = (Date)map.getValue(10);
			// String moveReason = (String) map.getValue(11);
			// String moveNote = (String) map.getValue(12);

			ApEmpBankVO vo = new ApEmpBankVO();

			vo.setEmpCode(empCode);
			vo.setBankId(bankId);
			vo.setEmpName(empName);
			vo.setAmount(amount);

			// vo.setEduFirst(eduFirst);
			// vo.setManReplace(manReplace);
			// vo.setMoveSeq(moveSeq);
			// vo.setMoveOrg(moveOrg);
			// vo.setDateSubmit(dateSubmit);
			// vo.setMoveReason(moveReason);
			// vo.setMoveNote(moveNote);

			returnList.add(vo);

		}
		return returnList;
	}
    
	
	public List apTransferReport(String evaYear, String evaMonth) {
		StringBuffer criteria = new StringBuffer();

		if (evaYear != null && !evaYear.equals("")) {
			criteria.append(" where va.pk.year = ");
			criteria.append(evaYear);
		}

		if (evaMonth != null && !evaMonth.equals("")) {
			criteria.append(" and va.pk.month = ");
			criteria.append(evaMonth);
		}

	
		

		StringBuffer hql = new StringBuffer();

		hql.append(" select va.pk.seq, ");
		hql.append(" va.pk.year, ");
		hql.append(" va.pk.month, ");
		hql.append(" va.pk.account, ");
		hql.append(" va.debit, ");
		hql.append(" va.credit, ");
		hql.append(" va.cont ");
		hql.append(" from VApIntfaceErp va ");
		hql.append(criteria);
	    hql.append(" order by va.pk.seq ");
		System.out.println("HQL ApTransferList ==> " + hql.toString());

		List empList = this.getSession().createQuery(hql.toString()).list();
		List retList = new ArrayList();
      

		/*
		 * List empList = this.getSession().createQuery(hql.toString()).list();
		 * // List retList = new ArrayList();
		 */

		for (Iterator it = empList.iterator(); it.hasNext();) {
			Object[] r = (Object[]) it.next();

			String seq = (String) r[0];
			String year = (String) r[1];
			String month = (String) r[2];
			String account = (String) r[3];
			String debit = (String) r[4];
			//String oldDuty = (String) r[5];
			String credit = (String) r[5];
			String cont = (String) r[6];
		
		
		
			ApTransferVO ret = new ApTransferVO();
	        ret.setSeq(seq);
	        ret.setYear(year);
	        ret.setMonth(month);
	        ret.setAccount(account);
	        ret.setDebit(debit);
	        ret.setCredit(credit);
	        ret.setCont(cont);

			retList.add(ret);
		}

		return retList;
	}
	
	public List apSumBatchApproveReport(String evaYear, String evaMonth,String volumeFrom,String volumeTo) {
		
		int volumeSetFrom = Integer.parseInt(volumeFrom);
        int volumeSetTo = Integer.parseInt(volumeTo);
		StringBuffer criteria = new StringBuffer();

		if (evaYear != null && !evaYear.equals("")) {
			criteria.append(" where va.pk.year = ");
			criteria.append(evaYear);
		}

		if (evaMonth != null && !evaMonth.equals("")) {
			criteria.append(" and va.pk.month = ");
			criteria.append(evaMonth);
		}
		
		

		if (volumeFrom != null && !volumeFrom.equals("")) {
			criteria.append(" and va.pk.volume >= ");
			criteria.append(volumeSetFrom);
		}
		
		if (volumeTo != null && !volumeTo.equals("")) {
			criteria.append(" and va.pk.volume <= ");
			criteria.append(volumeSetTo);
		}
		

		StringBuffer hql = new StringBuffer();

		hql.append(" select va.pk.year, ");
		hql.append(" va.pk.month, ");
		hql.append(" va.pk.volume, ");
		hql.append(" va.self, ");
		hql.append(" va.oth, ");
		hql.append(" va.total ");
		hql.append(" from VApSumBatch va ");
		hql.append(criteria);
	    hql.append(" order by cast(va.pk.volume as int)  ");
		System.out.println("HQL ApSumBatchList ==> " + hql.toString());

		List empList = this.getSession().createQuery(hql.toString()).list();
		List retList = new ArrayList();
      

		/*
		 * List empList = this.getSession().createQuery(hql.toString()).list();
		 * // List retList = new ArrayList();
		 */

		for (Iterator it = empList.iterator(); it.hasNext();) {
			Object[] r = (Object[]) it.next();

			String year = (String) r[0];
			String month = (String) r[1];
			String volume = (String) r[2];
			String self = (String) r[3];
			//String oldDuty = (String) r[5];
			String oth = (String) r[4];
			String total = (String) r[5];
		
		
		
			ApSumBatchVO ret = new ApSumBatchVO();
	       
	        ret.setYear(year);
	        ret.setMonth(month);
	        ret.setVolume(volume);
	        ret.setSelf(self);
	        ret.setOth(oth);
	        ret.setTotal(total);
	    

			retList.add(ret);
		}

		return retList;
	}
	
	public String apToGlTests( String userId, final String evaOuCode,
			final String evaYear,final String evaMonth,final String evaVolume1,final String evaVolume2,final String accDate) {
	
           
           String result =  (String) jdbcTemplate.execute (new CallableStatementCreator () { 

                   public CallableStatement createCallableStatement (Connection con) throws SQLException { 

                      String Storedproc = "{call Ap_Generate_Gl (?,?,?,?,?,?)}" ;

                      CallableStatement cs = con.prepareCall (Storedproc); 

                      cs.setString(1, evaOuCode);
                      cs.setString(2, evaMonth);
                      cs.setString(3, evaYear);
                      cs.setString(4, evaVolume1);
                      cs.setString(5, evaVolume2);
                      cs.setString(6,  accDate);
                      cs.registerOutParameter (7,java.sql.Types.VARCHAR) ;
                      return cs;} 

                     },new CallableStatementCallback() { 

                         public Object doInCallableStatement (CallableStatement cs)   throws SQLException, DataAccessException { 

                                   cs.execute (); 

                                  return cs.getString(7);}}); 
                return null;
	          
	   }
	
	public String apToGl( String userId,  String evaOuCode,
			 String evaYear, String evaMonth, String evaVolume1, String evaVolume2, String accDate) {
	
             Session session = null;
             CallableStatement cs= null;
             String resultSet = null;
             String query = "call AP_GENERATE_GL(?,?,?,?,?,?)";
             try{
            	 SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
            	 session = sessionFactory.openSession();
            	 System.out.println("generateGl");
            	 System.out.println("evaOuCode : "+evaOuCode);
            	 System.out.println("evaYear : "+evaYear);
            	 System.out.println("evaMonth : "+evaMonth);
            	 System.out.println("evaVolume1 : "+evaVolume1);
            	 System.out.println("evaVolume2 : "+evaVolume2);
            	 System.out.println("accDate : "+accDate);
            	 cs = session.connection().prepareCall(query);
            	 cs.setString(1,evaOuCode);
            	 cs.setString(2,evaYear);
            	 cs.setString(3,evaMonth);
            	 cs.setString(4,evaVolume1);
            	 cs.setString(5,evaVolume2);
            	 cs.setString(6,accDate);
            	 cs.execute();
            	// resultSet = (String) cs.getObject(6);
            	 
            	// System.out.println(resultSet);            		 
            	 
            	 System.out.println("Done");
             }catch (Exception e) {
            	 e.printStackTrace();
             }finally{
            	 session.flush();
            	 session.close();
             }
             
        	   
        	   
           return resultSet;
	}
	
	

         
}    
   
	
	
	  
	


	

