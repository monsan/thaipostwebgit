package com.ss.tp.dto;

import java.io.Serializable;
import java.util.Date;

public class PrDaily6162OutAmtReportVO implements Serializable {

	private Integer keySeq;
	private String ouCode;

	private String divDesc;
	
	private String areaDesc;
	

	private String secDesc;
    private String orgDesc;
	
	private Double yearPr;
	private Double periodPr;
	private String refNo;
	private String empCode;
	private String fullName;
	private String flagPr;
	
	private Integer codeSeq;
	private String incDecCode;
	private String incDecName;
	private String subCode;
	private Double amt61;
	private Double out61;
	private Double amt62;
	private Double out62;
	private Double netAmt61;
	private Double netAmt62;
	public Integer getKeySeq() {
		return keySeq;
	}
	public void setKeySeq(Integer keySeq) {
		this.keySeq = keySeq;
	}
	public String getOuCode() {
		return ouCode;
	}
	public void setOuCode(String ouCode) {
		this.ouCode = ouCode;
	}
	public String getDivDesc() {
		return divDesc;
	}
	public void setDivDesc(String divDesc) {
		this.divDesc = divDesc;
	}
	public String getAreaDesc() {
		return areaDesc;
	}
	public void setAreaDesc(String areaDesc) {
		this.areaDesc = areaDesc;
	}
	public String getSecDesc() {
		return secDesc;
	}
	public void setSecDesc(String secDesc) {
		this.secDesc = secDesc;
	}
	public Double getYearPr() {
		return yearPr;
	}
	public void setYearPr(Double yearPr) {
		this.yearPr = yearPr;
	}
	public Double getPeriodPr() {
		return periodPr;
	}
	public void setPeriodPr(Double periodPr) {
		this.periodPr = periodPr;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getEmpCode() {
		return empCode;
	}
	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getFlagPr() {
		return flagPr;
	}
	public void setFlagPr(String flagPr) {
		this.flagPr = flagPr;
	}
	public Integer getCodeSeq() {
		return codeSeq;
	}
	public void setCodeSeq(Integer codeSeq) {
		this.codeSeq = codeSeq;
	}
	public String getIncDecCode() {
		return incDecCode;
	}
	public void setIncDecCode(String incDecCode) {
		this.incDecCode = incDecCode;
	}
	public String getIncDecName() {
		return incDecName;
	}
	public void setIncDecName(String incDecName) {
		this.incDecName = incDecName;
	}
	public String getSubCode() {
		return subCode;
	}
	public void setSubCode(String subCode) {
		this.subCode = subCode;
	}
	public Double getNetAmt61() {
		return netAmt61;
	}
	public void setNetAmt61(Double netAmt61) {
		this.netAmt61 = netAmt61;
	}
	public Double getNetAmt62() {
		return netAmt62;
	}
	public void setNetAmt62(Double netAmt62) {
		this.netAmt62 = netAmt62;
	}
	public String getOrgDesc() {
		return orgDesc;
	}
	public void setOrgDesc(String orgDesc) {
		this.orgDesc = orgDesc;
	}
	public Double getAmt61() {
		return amt61;
	}
	public void setAmt61(Double amt61) {
		this.amt61 = amt61;
	}
	public Double getOut61() {
		return out61;
	}
	public void setOut61(Double out61) {
		this.out61 = out61;
	}
	public Double getAmt62() {
		return amt62;
	}
	public void setAmt62(Double amt62) {
		this.amt62 = amt62;
	}
	public Double getOut62() {
		return out62;
	}
	public void setOut62(Double out62) {
		this.out62 = out62;
	}
	
	
}
