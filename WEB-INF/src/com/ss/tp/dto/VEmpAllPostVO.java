package com.ss.tp.dto;

import java.io.Serializable;


public class VEmpAllPostVO implements Serializable {
	private String deptSeq;
	private String divSeq;
	private String areaSeq;
	private String secSeq;
	private String levelCode;
	private String empCode;
	private String prefixName;
	private String firstName;
	private String lastName;
	private String positionShort;
	private String divDesc;
	private String secDesc;
	public String getDeptSeq() {
		return deptSeq;
	}
	public void setDeptSeq(String deptSeq) {
		this.deptSeq = deptSeq;
	}
	public String getDivSeq() {
		return divSeq;
	}
	public void setDivSeq(String divSeq) {
		this.divSeq = divSeq;
	}
	public String getAreaSeq() {
		return areaSeq;
	}
	public void setAreaSeq(String areaSeq) {
		this.areaSeq = areaSeq;
	}
	public String getSecSeq() {
		return secSeq;
	}
	public void setSecSeq(String secSeq) {
		this.secSeq = secSeq;
	}
	public String getLevelCode() {
		return levelCode;
	}
	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	}
	public String getEmpCode() {
		return empCode;
	}
	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}
	public String getPrefixName() {
		return prefixName;
	}
	public void setPrefixName(String prefixName) {
		this.prefixName = prefixName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPositionShort() {
		return positionShort;
	}
	public void setPositionShort(String positionShort) {
		this.positionShort = positionShort;
	}
	public String getDivDesc() {
		return divDesc;
	}
	public void setDivDesc(String divDesc) {
		this.divDesc = divDesc;
	}
	public String getSecDesc() {
		return secDesc;
	}
	public void setSecDesc(String secDesc) {
		this.secDesc = secDesc;
	}
	
	


}
