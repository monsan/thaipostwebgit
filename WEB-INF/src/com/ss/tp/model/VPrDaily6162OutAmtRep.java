/*
 * Created on 13 ?.?. 2549
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.ss.tp.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author bowpoko
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class VPrDaily6162OutAmtRep implements Serializable {
	private Integer keySeq;
	private String ouCode;
	

	private String divDesc;

	private String areaDesc;
	
	
	private String secDesc;

	
	private Double yearPr;
	private Double periodPr;
	private String refNo;
	private String empCode;
	private String fullName;
	
	private Integer codeSeq;
	private String incDecCode;
	private String groupCode;
	
	private Double amt61;
	private Double out61;
	private Double netAmt61;
	private Double amt62;
	private Double out62;
	private Double netAmt62;
	public Integer getKeySeq() {
		return keySeq;
	}
	public void setKeySeq(Integer keySeq) {
		this.keySeq = keySeq;
	}
	public String getOuCode() {
		return ouCode;
	}
	public void setOuCode(String ouCode) {
		this.ouCode = ouCode;
	}
	public String getDivDesc() {
		return divDesc;
	}
	public void setDivDesc(String divDesc) {
		this.divDesc = divDesc;
	}
	public String getAreaDesc() {
		return areaDesc;
	}
	public void setAreaDesc(String areaDesc) {
		this.areaDesc = areaDesc;
	}
	public String getSecDesc() {
		return secDesc;
	}
	public void setSecDesc(String secDesc) {
		this.secDesc = secDesc;
	}
	public Double getYearPr() {
		return yearPr;
	}
	public void setYearPr(Double yearPr) {
		this.yearPr = yearPr;
	}
	public Double getPeriodPr() {
		return periodPr;
	}
	public void setPeriodPr(Double periodPr) {
		this.periodPr = periodPr;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getEmpCode() {
		return empCode;
	}
	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Integer getCodeSeq() {
		return codeSeq;
	}
	public void setCodeSeq(Integer codeSeq) {
		this.codeSeq = codeSeq;
	}
	public String getIncDecCode() {
		return incDecCode;
	}
	public void setIncDecCode(String incDecCode) {
		this.incDecCode = incDecCode;
	}
	public String getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	public Double getNetAmt61() {
		return netAmt61;
	}
	public void setNetAmt61(Double netAmt61) {
		this.netAmt61 = netAmt61;
	}
	public Double getNetAmt62() {
		return netAmt62;
	}
	public void setNetAmt62(Double netAmt62) {
		this.netAmt62 = netAmt62;
	}
	public Double getAmt61() {
		return amt61;
	}
	public void setAmt61(Double amt61) {
		this.amt61 = amt61;
	}
	public Double getOut61() {
		return out61;
	}
	public void setOut61(Double out61) {
		this.out61 = out61;
	}
	public Double getAmt62() {
		return amt62;
	}
	public void setAmt62(Double amt62) {
		this.amt62 = amt62;
	}
	public Double getOut62() {
		return out62;
	}
	public void setOut62(Double out62) {
		this.out62 = out62;
	}
	
	

	

	/**
	 * @return Returns the adjNewsal.
	 */
	

	

}
